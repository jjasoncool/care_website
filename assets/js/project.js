// 使用 bootstrap 驗證 form 若驗證通過回傳 true
"use strict";
function valid(e) {
    var chkOK = false;
    var forms = $(".needs-validation");
    // 避免只輸入空白問題
    forms.find("input:not(:file)").each(function() {
        if ($(this).attr("type") === "file") return;
        $(this).val($.trim($(this).val()));
    });
    //刪除不需要驗證的input條件
    forms.find(":input[type=hidden], :input[type=button], :input:not(:visible)").each(function() {
        $(this)
            .removeAttr("min")
            .removeAttr("max")
            .removeAttr("maxLength")
            .removeAttr("minLength")
            .removeAttr("pattern")
            .removeAttr("required");
    });
    // console.log (forms[0]);
    // function 內的 form 等同於 forms[0]
    var validation = Array.prototype.filter.call(forms, function(form) {
        // console.log (form);
        if (form.checkValidity() === false) {
            e.preventDefault();
            e.stopPropagation();
        } else {
            chkOK = true;
        }
        form.classList.add("was-validated");
    });
    //console.log沒驗證通過的input
    if (!chkOK) {
        var notValidItems = "";
        $("input.form-control:invalid").each(function() {
            notValidItems += (this.id ? this.id : this.name) + ", ";
        });
        console.log("not valid: " + notValidItems);
    }
    return chkOK;
}

function importInputsErrMsg(inputs) {
    //regular expression 可用 pattern 和 title
    //example:  <input type='text' pattern="^\d{4}-\d{2}-\d{2}\s{1}\d{2}:\d{2}$" title="YYYY-MM-DD hh:mm".....

    //for inputs which are dynamically appended
    var inputs = inputs ? inputs : $(":input:not([type=hidden]):not([type=button]):visible");
    inputs.each(function() {
        //有自訂的invalid-feedback div 就不做事, 沒有id也不做事
        if ($(this).parent().find(".invalid-feedback").length == 0) {
            var message = "格式錯誤";
            var title = $(this).attr("title");
            switch ($(this).attr("type")) {
                case "text":
                    var maxlength = $(this).attr("maxlength");
                    var minlength = $(this).attr("minlength");
                    var req = $(this).prop("required");

                    if (req && $(this).val() == "") {
                        message = "此為必填欄位";
                    }

                    if (title) {
                        message = title;
                    } else if (maxlength && minlength) {
                        message += "文字長度需介於" + minlength + "和" + maxlength;
                    } else if (maxlength) {
                        message += "文字長度需小於" + maxlength;
                    } else if (minlength) {
                        message += "文字長度需大於" + minlength;
                    }
                    break;

                case "number":
                    var max = $(this).attr("max");
                    var min = $(this).attr("min");
                    var step = $(this).attr("step");

                    if (title) {
                        message = title;
                    } else if (max && min) {
                        message += "文字長度需介於" + min + "和" + max;
                    } else if (max) {
                        message += "文字長度需小於" + max;
                    } else if (min) {
                        message += "文字長度需大於" + min;
                    }

                    if (step) {
                        message += "最小單位為" + step;
                    }
                    break;
                default:
                    if (title) {
                        message = title;
                    }
                    break;
            }
            $(this).parent().append("<div class='invalid-feedback'>" + message + "</div>");
        }
    });
}
