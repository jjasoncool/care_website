<?php
ob_start();

require_once 'vendor/autoload.php';
require 'settings/loginCheck.php';
$page = reqParam('page', 'get');
$subPage = reqParam('sub', 'get');
$action = reqParam('action', 'get');
require_once 'inc/header.php';
?>
<body>
	<div class="wrapper">
    <!--
        Tip 1: You can change the background color of the main header using: data-background-color="blue | purple | light-blue | green | orange | red"
    -->
        <?php
        require_once get_relative_path("inc/headNav.php");
        require_once get_relative_path("inc/sideBar.php");
        require_once get_relative_path("settings/router.php");
        ?>
    <!-- End Custom template -->
        <div class="custom-template">
			<div class="title">Settings</div>
			<div class="custom-content">
				<div class="switcher">
					<div class="switch-block">
						<h4>Topbar</h4>
						<div class="btnSwitch">
							<button type="button" class="changeMainHeaderColor" data-color="blue"></button>
							<button type="button" class="selected changeMainHeaderColor" data-color="purple"></button>
							<button type="button" class="changeMainHeaderColor" data-color="light-blue"></button>
							<button type="button" class="changeMainHeaderColor" data-color="green"></button>
							<button type="button" class="changeMainHeaderColor" data-color="orange"></button>
							<button type="button" class="changeMainHeaderColor" data-color="red"></button>
						</div>
					</div>
					<div class="switch-block">
						<h4>Background</h4>
						<div class="btnSwitch">
							<button type="button" class="changeBackgroundColor" data-color="bg2"></button>
							<button type="button" class="changeBackgroundColor selected" data-color="bg1"></button>
							<button type="button" class="changeBackgroundColor" data-color="bg3"></button>
						</div>
					</div>
				</div>
			</div>
			<div class="custom-toggle">
				<i class="flaticon-settings"></i>
			</div>
		</div>
	</div>
</div>
<?php
require_once 'inc/footer.php';
?>
</body>
</html>
<?php
ob_end_flush();
