<?php
session_start();
session_unset();
session_destroy();
require_once 'inc/header.php';
?>
<body class="login">
	<div class="wrapper wrapper-login">
		<div class="container container-login animated fadeIn">
			<h3 class="text-center">CARE SERVICE</h3>
            <form method="post" action="settings/loginVerify.php">
                <div class="login-form">
                    <div class="form-group form-floating-label">
                        <input id="username" name="myusername" type="text" class="form-control input-border-bottom" required>
                        <label for="username" class="placeholder">User</label>
                    </div>
                    <div class="form-group form-floating-label">
                        <input id="password" name="mypassword" type="password" class="form-control input-border-bottom" required>
                        <label for="password" class="placeholder">Password</label>
                        <div class="show-password">
                            <i class="flaticon-interface"></i>
                        </div>
                    </div>
                    <div class="row form-sub m-0">
                        <!-- <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="rememberme">
                            <label class="custom-control-label" for="rememberme">Remember Me</label>
                        </div>

                        <a href="#" id="show-signup" class="link float-right">Forget Password ?</a> -->
                    </div>
                    <div class="form-action mb-3">
                        <button type="submit" class="btn btn-primary btn-rounded btn-login">Sign In</button>
                    </div>
                </div>
            </form>
		</div>

		<!-- <div class="container container-signup animated fadeIn">
			<h3 class="text-center">Reset Password</h3>
			<div class="login-form">
				<div class="form-group form-floating-label">
					<input  id="fullname" name="fullname" type="text" class="form-control input-border-bottom" required>
					<label for="fullname" class="placeholder">Fullname</label>
				</div>
				<div class="form-group form-floating-label">
					<input  id="email" name="email" type="email" class="form-control input-border-bottom" required>
					<label for="email" class="placeholder">Email</label>
				</div>
				<div class="form-group form-floating-label">
					<input  id="passwordsignin" name="passwordsignin" type="password" class="form-control input-border-bottom" required>
					<label for="passwordsignin" class="placeholder">Password</label>
					<div class="show-password">
						<i class="flaticon-interface"></i>
					</div>
				</div>
				<div class="form-group form-floating-label">
					<input  id="confirmpassword" name="confirmpassword" type="password" class="form-control input-border-bottom" required>
					<label for="confirmpassword" class="placeholder">Confirm Password</label>
					<div class="show-password">
						<i class="flaticon-interface"></i>
					</div>
				</div>
				<div class="form-action">
					<a href="#" id="show-signin" class="btn btn-danger btn-rounded btn-login mr-3">Cancel</a>
					<a href="#" class="btn btn-primary btn-rounded btn-login">Reset</a>
				</div>
			</div>
		</div> -->
	</div>
<?php
require_once 'inc/footer.php';
?>
</body>
<script>
"use strict";
var params = new URLSearchParams(location.search);

if (params.has("thank")) {
    swal({
        title: "資料新增成功!",
        text: "感謝您撥空協助填寫基本資料，後續將會由專人維護該資料!",
        icon: "success",
        button: "我知道了!",
    });
    params.delete("thank");
}
</script>
</html>
