<?php
define('DB_HOST', '127.0.0.1');
define('DB_PORT', 3306);
define('DB_USER', '');
define('DB_PASS', '');
// 一般root，除非有rewrite url，則須設定此項目
define('ROOT_PATH', '/');
define('root_depth', '1');

require_once 'conn/PDO.class.php';
require_once 'generalFunctions.php';
require_once 'systemFunctions.php';

if (!isset($_SESSION)) {
    session_start();
}
