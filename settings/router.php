<?php
if (empty($page)) {
    require_once get_relative_path('pages/dashboard.php');
} elseif (in_array($page, ['account', 'audit', 'assessment', 'person', 'projects', 'consultation', 'volunteer', 'resources', 'specialists', 'courses', 'statistics', 'notice'], true)) {
    if (empty($action)) {
        require_once get_relative_path("pages/{$page}/index.php");
    } elseif (in_array($action, array('view', 'dataForm', 'upload', 'download'), true)) {
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            // POST 作為送出表單使用
            require_once get_relative_path("pages/{$page}/dataProcess.php");
        } else {
            // 其他GET方法為查詢或者填寫表單頁面使用
            require_once get_relative_path("pages/{$page}/{$action}.php");
        }
    } else {
        session_unset();
        session_destroy();
        header("Location: " . get_relative_path("pages/404.php"));
    }
} else {
    session_unset();
    session_destroy();
    header("Location: " . get_relative_path("pages/404.php"));
}
