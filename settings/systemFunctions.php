<?php
/**
 * 新增通知，通知該負責人
 *
 * @param integer $noticeAcc 通知帳號ID，若為0，預設通知窗口
 * @param integer $noticeType 通知類型
 * @param string $noticeLink 通知連結(選填)
 * @param string $msg 通知訊息內容
 * @return void
 */
function addNotification($noticeAcc = 0, $noticeType = 0, $noticeLink = '', $msg = '')
{
    $db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
    $dt = new dateTime();
    if (empty($noticeAcc)) {
        $dbQuery = "SELECT * FROM FCF_careservice.Accuser a WHERE a.Acc_judge=1";
        $result = $db->query($dbQuery);
        foreach ($result as $row) {
            $noticeArray[] = [
                'noticeTime' => $dt->format('Y-m-d H:i:s'),
                'noticeAcc' => $row['IDno'],
                'noticeType' => $noticeType,
                'noticeLink' => $noticeLink,
                'noticeMsg' => $msg,
            ];
        }
    } else {
        $noticeArray = [
            [
                'noticeTime' => $dt->format('Y-m-d H:i:s'),
                'noticeAcc' => $noticeAcc,
                'noticeType' => $noticeType,
                'noticeLink' => $noticeLink,
                'noticeMsg' => $msg,
            ]
        ];
    }

    foreach ($noticeArray as $insRow) {
        $insColStr = implode('`,`', array_keys($insRow));
        $insParaStr = implode(",", array_fill(0, count($insRow), "?"));
        // SQL 用基本資料產出
        $dbQuery = "INSERT INTO FCF_careservice.notification (
            `{$insColStr}`
        ) VALUES (
            {$insParaStr}
        )";
        $db->query($dbQuery, array_values($insRow));
    }
}

function showNotification() {
    $db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
    $dbQuery = "SELECT * FROM FCF_careservice.notification WHERE noticeACC=? AND checktime is NULL LIMIT 10";
    $result = $db->query($dbQuery, [$_SESSION['userid']]);
    $count = count($result);
    $content = '';
    $iconArray = [
        0 => '<div class="notif-icon notif-primary"> <i class="fas fa-comment-dots"></i> </div>',
        1 => '<div class="notif-icon notif-primary"> <i class="far fa-file"></i> </div>',
        2 => '<div class="notif-icon notif-info"> <i class="far fa-edit"></i> </div>',
        3 => '<div class="notif-icon notif-danger"> <i class="fas fa-file-excel"></i> </div>',
        4 => '<div class="notif-icon notif-success"> <i class="fas fa-check-square"></i> </div>',
    ];

    foreach ($result as $row) {
        $showMsg = nl2br($row['noticeMsg']);
        $content .= <<<EOD
        <a href="{$row['noticeLink']}" class="clickread" data-id="{$row['IDno']}">
            {$iconArray[$row['noticeType']]}
            <div class="notif-content">
                <span class="block">{$showMsg}</span>
                <span class="time">{$row['noticeTime']}</span>
            </div>
        </a>
        EOD;
    }
    return [$count, $content];
}
