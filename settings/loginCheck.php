<?php
// 判斷登入是否成功
require_once 'cfg.php';

$ip = getIP();

if (!isset($_SESSION['footprint']) || $_SESSION['footprint'] != md5($ip . $_SERVER['HTTP_USER_AGENT'])) {
    session_destroy();
    header("location: " . ROOT_PATH . "login.php");
    exit();
}

// Auto-logout
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 14400)) {
    // last request was more than 3 hours ago
    session_destroy(); // destroy session data in storage
    session_unset(); // unset $_SESSION variable for the runtime
    header("location: " . ROOT_PATH . "?msg=1"); // "Automatically logout after idle for a long time.."
    exit();
}
// update last activity time stamp
$_SESSION['LAST_ACTIVITY'] = time();

// 公用資料
$generalData['userid'] = $_SESSION['userid'];
$generalData['username'] = $_SESSION['username'];
$generalData['serviceArea'] = $_SESSION['serviceArea'];
$generalData['userEmail'] = $_SESSION['userEmail'];
$generalData['userLevel'] = $_SESSION['userLevel'];
$generalData['managerID'] = $_SESSION['managerID'];
$generalData['trackJudge'] = $_SESSION['trackJudge'];
$generalData['ProfilePic'] = $_SESSION['ProfilePic'];

// 權限控管
require 'loginAuthority.php';
