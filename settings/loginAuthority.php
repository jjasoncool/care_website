<?php
/**
 * 權限控管
 * @internal 若有特殊的權限會被寫到這邊來，覆蓋掉原本的參數
 */

if ($generalData['userLevel'] == 5 && $_SERVER["REQUEST_METHOD"] == "GET") {
    if (isset($_GET['page']) && $_GET['page'] === 'assessment') {
        // 評估量表，第一次沒填過，避免改ID問題
        if (!isset($_SESSION['assessID'])) {
            $_SESSION['assessID'] = $_GET['id'];
        } else {
            $_GET['id'] = $_SESSION['assessID'];
        }
        $_GET['action'] = 'dataForm';
        unset($_GET['sub']);
    } else {
        // 個案帳號只能新增資料，強迫轉址
        $_GET['page'] = 'person';
        $_GET['sub'] = 'personalCase';
        $_GET['action'] = 'dataForm';
    }
}