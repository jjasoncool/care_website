<?php

require_once 'cfg.php';

// 沒有POST NOT FOUND
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);

// username and password sent from form
$myusername = $db->escape(trim(reqParam('myusername', 'post')));
$mypassword = $db->escape(trim(reqParam('mypassword', 'post')));
$rememberme = $db->escape(trim(reqParam('rememberme', 'post')));

$dbQuery = "SELECT * FROM FCF_careservice.Accuser WHERE Acc_mail = ? AND Acc_status=1";
$result = $db->row($dbQuery, array($myusername));

if ($mypassword === $result['Acc_password']) {
    $ip = getIP();
    $_SESSION['loggedin'] = true;
    // security
    $_SESSION['footprint'] = md5($ip . $_SERVER['HTTP_USER_AGENT']);
    $_SESSION['LAST_ACTIVITY'] = time();

    // session 帳號基本資料
    $_SESSION['userid'] = $result['IDno'];
    $_SESSION['username'] = $result['Acc_name'];
    $_SESSION['serviceArea'] = $result['Acc_area'];
    $_SESSION['userEmail'] = $result['Acc_mail'];
    // 1:管理者 2:職員 3:志工
    $_SESSION['userLevel'] = $result['Acc_level'];
    // 此人填寫資料須確認主管
    $_SESSION['managerID'] = $result['AdmincheckID'];
    // 評估量表窗口，負責指派由誰追蹤
    $_SESSION['trackJudge'] = boolval($result['Acc_judge']);
    // 大頭貼
    $_SESSION['ProfilePic'] = empty(trim($result['acc_stmapimg'])) ? 'default.png' : $result['acc_stmapimg'];
    // 轉到登入頁面
    header("location: ../");
} else {
    $db->closeConnection();
    header("location: ../login.php?msg=2");
    die();
}
