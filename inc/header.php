<?php
$title = isset($customTitle) ? $customTitle : '個案管理系統';
// 對應 settings/cfg.php ROOT_PATH
define('serverURL', '/sites/demo02/');
?>
<!DOCTYPE html>
<html lang="zh-tw">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport'>
	<title><?=$title?></title>
	<link rel="icon" href="<?=serverURL?>uploads/images/icon.ico" type="image/x-icon">

	<!-- Fonts and icons -->
	<script src="<?=serverURL?>assets/js/plugin/webfont/webfont.min.js"></script>
	<script>
		WebFont.load({
			google: {"families":["Open+Sans:300,400,600,700"]},
			custom: {"families":["Flaticon", "Font Awesome 5 Solid", "Font Awesome 5 Regular", "Font Awesome 5 Brands"], urls: ['<?=serverURL?>assets/css/fonts.css']},
			active: function() {
				sessionStorage.fonts = true;
			}
		});
	</script>

	<!-- CSS Files -->
	<link rel="stylesheet" href="<?=serverURL?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?=serverURL?>assets/css/azzara.min.css">
	<link rel="stylesheet" href="<?=serverURL?>assets/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="<?=serverURL?>assets/css/bootstrap-combobox.css">
    <link rel="stylesheet" href="<?=serverURL?>assets/css/responsive.dataTables.min.css">
    <link rel="stylesheet" href="<?=serverURL?>assets/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="<?=serverURL?>assets/css/buttons.bootstrap4.min.css">
    <link rel="stylesheet" href="<?=serverURL?>assets/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="<?=serverURL?>assets/css/baguetteBox.min.css">
    <link rel="stylesheet" href="<?=serverURL?>assets/css/fullcalendar.min.css">
    <link rel="stylesheet" href="<?=serverURL?>assets/css/demo.css">
    <style>
        .unselectable {
            -moz-user-select:none;
            -webkit-user-select:none;
        }
    </style>
</head>
