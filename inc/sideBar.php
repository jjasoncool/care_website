<?php
switch ($generalData['userLevel']) {
    case '1':
        $adminlevel = '管理者';
        break;
    case '2':
        $adminlevel = '使用者';
        break;
    case '3':
        $adminlevel = '志工';
        break;
    case '4':
        $adminlevel = '保險顧問';
        break;
    case '5':
        $adminlevel = '個案';
        break;
}
?>
<!-- Sidebar -->
<div class="sidebar">
    <div class="sidebar-background"></div>
    <div class="sidebar-wrapper scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="uploads/profiles/<?=$_SESSION['ProfilePic']?>" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            <?=$generalData['username']?>
                            <span class="user-level"><?=$adminlevel?></span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="collapse in" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="?page=account&sub=chgProfile">
                                    <span class="link-collapse">個人設定</span>
                                </a>
                            </li>
                            <li>
                                <a href="?page=account&sub=chgPass">
                                    <span class="link-collapse">變更密碼</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <ul class="nav">
                <li class="nav-item <?=(empty($page) ? ' active' : '')?>">
                    <a href="index.php">
                        <i class="fas fa-home"></i>
                        <p>首頁</p>
                        <!-- <span class="badge badge-count">5</span> -->
                    </a>
                </li>
                <li class="nav-section">
                    <span class="sidebar-mini-icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </span>
                    <h4 class="text-section">項目</h4>
                </li>
                <?php
                $listItems = [
                    'person' => array(
                        'permissions' => 4,
                        'name' => '個案管理',
                        'image' => 'far fa-address-book',
                        'subname' => ['personalCase' => '個案', 'family' => '家屬', 'nonClient' => '一般民眾', 'closeCase' => '結案'],
                    ),
                    'assessment' => array(
                        'permissions' => 2,
                        'name' => '評估量表',
                        'image' => 'fas fa-file-medical-alt',
                    ),
                    'projects' => array(
                        'permissions' => 2,
                        'name' => '專案',
                        'image' => 'fas fa-layer-group',
                    ),
                    'courses' => array(
                        'permissions' => 2,
                        'name' => '身心靈課程',
                        'image' => 'fas fa-desktop',
                    ),
                    'consultation' => array(
                        'permissions' => 4,
                        'name' => '線上諮詢管理',
                        'image' => 'fas fa-question-circle',
                        'subname' => ['telChat' => '電話', 'netChat' => '網路/e-mail', 'appChat' => 'APP'],
                    ),
                    'volunteer' => array(
                        'permissions' => 2,
                        'name' => '志工管理',
                        'image' => 'fas fa-pen-square',
                        'subname' => ['vol_list' => '志工名單', 'vol_hour' => '志工時數', 'vol_train' => '教育訓練'],
                    ),
                    'resources' => array(
                        'permissions' => 2,
                        'name' => '資源清單',
                        'image' => 'fas fa-table',
                        'subname' => [
                            'center' => '癌症資源中心',
                            'casemanager' => '專科個管師',
                            'hospitalsocial' => '醫院社工室',
                            'northHospital' => '北區醫院',
                            'government' => '政府單位',
                            'socialGroup' => '社福團體',
                            'group' => '病友團體',
                            'cooperation' => '合作企業',
                        ],
                    ),
                    'specialists' => array(
                        'permissions' => 2,
                        'name' => '專家名單',
                        'image' => 'fas fa-user-tie',
                        'subname' => ['oncologist' => '醫療專家', 'teachers' => '各領域專家'],
                    ),
                    'statistics' => array(
                        'permissions' => 2,
                        'name' => '統計',
                        'image' => 'far fa-chart-bar',
                        'subname' => ['cases' => '案件統計', 'other' => '其他統計'],
                    ),
                    'audit' => array(
                        'permissions' => 1,
                        'name' => '審核',
                        'image' => 'fas fa-user-check',
                    ),
                    'account' => array(
                        'permissions' => 1,
                        'name' => '帳號管理',
                        'image' => 'fas fa-user-cog',
                        'subname' => ['manager' => '帳號清單', 'chgPass' => '變更密碼'],
                    ),
                ];

                // 輸出side bar，並且判斷在哪一頁
                foreach ($listItems as $main => $details) {
                    if (!isset($details['permissions']) || $details['permissions'] >= $generalData['userLevel']) {
                        if (isset($details['subname'])) {
                            echo "
                                <li class=\"nav-item" . ($page == $main ? ' active submenu' : '') . "\">
                                    <a data-toggle=\"collapse\" href=\"#{$main}\">
                                        <i class=\"{$details['image']}\"></i>
                                        <p>{$details['name']}</p>
                                        <span class=\"caret\"></span>
                                    </a>
                                    <div class=\"collapse" . ($page == $main ? ' show' : '') . "\" id=\"{$main}\">
                                        <ul class=\"nav nav-collapse\">";
                                        foreach ($details['subname'] as $sub => $name) {
                                            echo "
                                            <li" . ($subPage == $sub ? ' class="active"' : '') . ">
                                                <a href=\"?page={$main}&sub={$sub}\">
                                                    <span class=\"sub-item\">{$name}</span>
                                                </a>
                                            </li>";
                                        }
                            echo "
                                        </ul>
                                    </div>
                                </li>";
                        } else {
                            echo "
                            <li class=\"nav-item" . ($page == $main ? ' active' : '') . "\">
                                <a href=\"?page={$main}\">
                                    <i class=\"{$details['image']}\"></i>
                                    <p>{$details['name']}</p>
                                </a>
                            </li>";
                        }
                    }
                }
                // 參數用完丟掉
                unset($main, $details, $sub, $name);
                ?>
            </ul>
        </div>
    </div>
</div>
<!-- End Sidebar -->