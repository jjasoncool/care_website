<?php
/**
 * 其他項目統計
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 */

?>

<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition[$subPage]['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['subname'][$subPage]?></a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col">
                                    <h4 class="card-title">匯出資料</h4>
                                </div>
                                <div class="col">
                                    <select class="form-control" id="year">
                                        <?=$yearOption?>
                                    </select>
                                </div>
                                <div class="col">
                                    <select class="form-control" id="cancer">
                                        <option value="">-- 全癌別 --</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">項目</th>
                                        <th scope="col">說明 <span class="text-danger">(影響變數)</span></th>
                                        <th scope="col">連結</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $exportTable = [
                                        1 => ['name' => '年度訂閱會訊(紙本/電子檔)名單', 'idtext' => 'expList'],
                                        2 => ['name' => '志工/個案生日月份', 'idtext' => 'expCase'],
                                        3 => ['name' => '各人員輸入紀錄筆數 <span class="text-danger">(年份)</span>', 'idtext' => 'expRec'],
                                        4 => ['name' => '處遇紀錄總覽 <span class="text-danger">(年份)</span>', 'idtext' => 'expTime'],
                                        5 => ['name' => '志工/個案手機號碼總覽 <span class="text-danger">(癌別)</span>', 'idtext' => 'expPhone'],
                                        6 => ['name' => '個案諮詢紀錄細項統計 <span class="text-danger">(年份, 癌別)</span>', 'idtext' => 'expConsult'],
                                        7 => ['name' => '線上諮詢紀錄細項統計 <span class="text-danger">(年份)</span>', 'idtext' => 'expOnline'],
                                    ];

                                    foreach ($exportTable as $index => $row) {
                                        echo "<tr>
                                            <th scope\"row\">{$index}</th>
                                            <td>{$row['name']}</td>
                                            <td>
                                                <form class=\"ml-auto\" method=\"post\" target=\"_blank\">
                                                    <input type=\"hidden\" name=\"page\">
                                                    <input type=\"hidden\" name=\"type\" value=\"{$row['idtext']}\">
                                                    <button type=\"button\" class=\"btn btn-outline-secondary btn-round btn-sm\" id=\"{$row['idtext']}\">
                                                        <span class=\"btn-label\"><i class=\"fas fa-download\"></i>匯出資料</span>
                                                    </button>
                                                </form>
                                            </td>
                                        </tr>";
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4 class="card-title">圖表</h4>
                        </div>
                        <div class="card-body">
                            <div class="chart-container" style="height: 300px">
                                <canvas id="recordAnalysis"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
"use strict";
var params = new URLSearchParams(location.search);
var barOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {position : 'bottom'},
    title: {display: true, text: ''},
    tooltips: {mode: 'index', intersect: false},
    scales: {xAxes: [{stacked: true,}], yAxes: [{min: 0, beginAtZero: true, stacked: true}]}
};
var objRecord;
var recordAnalysis = document.getElementById('recordAnalysis').getContext('2d');

function drawData(jsonData) {
    objRecord = new Chart(recordAnalysis, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.record.datasets[0].backgroundColor = "#FF2D2D";
    jsonData.record.datasets[1].backgroundColor = "#FF8040";
    jsonData.record.datasets[2].backgroundColor = "#FFDC35";
    jsonData.record.datasets[3].backgroundColor = "#59D05D";
    jsonData.record.datasets[4].backgroundColor = "#0080FF";
    jsonData.record.datasets[5].backgroundColor = "#4A4AFF";
    jsonData.record.datasets[6].backgroundColor = "#BE77FF";
    objRecord.data = jsonData.record;
    objRecord.options = barOptions;
    objRecord.options.title.text = "紀錄筆數";
    objRecord.update();
}

function getData(year) {
    params.set("action", "view");
    let returnData = '';
    let getAction = $.ajax({
        url: "./?" + params.toString(),
        method: "POST",
        data: {year: year, page: "otherDrawData"},
        dataType: "json"
    }).done(function (data) {
        if (data) {
            returnData = data;
        }
        drawData(returnData);
    });
}


function init() {

    // 癌別
    $.each(cancer, function (i, item) {
        $("#cancer").append($('<option>', {
            value: item,
            text : item
        }));
    });

    // 搜尋項目
    $("#cancer, #year, #selCity").select2({
        theme: "bootstrap",
        width: "100%"
    });

    $("#year").on("change", function() {
        let year = $("#year").val();
        objRecord.destroy();
        getData(year);
    });

    $("#expList, #expCase, #expRec, #expTime").on("click", function () {
        let year = $("#year").val();
        params.set("year", year);
        params.set("action", "download");
        $(this).closest("form").attr("action", "./?" + params.toString());
        $(this).siblings("[name='page']").val("otherExport");
        $(this).closest("form").submit();
    });

    $("#expPhone").on("click", function () {
        let cancer = $("#cancer").val();
        params.set("cancer", cancer);
        params.set("action", "download");
        $(this).closest("form").attr("action", "./?" + params.toString());
        $(this).siblings("[name='page']").val("otherExport");
        $(this).closest("form").submit();
    });

    $("#expConsult, #expOnline").on("click", function () {
        let year = $("#year").val();
        let cancer = $("#cancer").val();
        params.set("year", year);
        params.set("cancer", cancer);
        params.set("action", "download");
        $(this).closest("form").attr("action", "./?" + params.toString());
        $(this).siblings("[name='page']").val("otherExportConsult");
        $(this).closest("form").submit();
    });

    getData($("#year").val());
}

window.onload = init;
</script>