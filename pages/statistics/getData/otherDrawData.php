<?php
/**
 * 搜尋可以列出統計的資料
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 * @param string $action 路由到這個頁面的參數
 */

$year = intval(reqParam('year', 'post'));

// 回傳資料
$response = array();
$tempData['X'] = $tempData['Y'] = [];
// 來源件數/比例分析
$dbQuery = "SELECT t.*, a.Acc_name AS Xaxis FROM (
                SELECT  YEAR(Keydate) as yearStr, '一般諮詢' AS recType, COUNT(DISTINCT IDno) AS Yaxis, AdminID FROM FCF_careservice.Rec_ask WHERE MemberID>0 GROUP BY yearStr, AdminID
                UNION ALL
                SELECT  YEAR(Keydate) as yearStr, '醫護紀錄' AS recType, COUNT(DISTINCT IDno) AS Yaxis, AdminID FROM FCF_careservice.Rec_medical WHERE MemberID>0 GROUP BY yearStr, AdminID
                UNION ALL
                SELECT  YEAR(Keydate) as yearStr, '營養紀錄' AS recType, COUNT(DISTINCT IDno) AS Yaxis, AdminID FROM FCF_careservice.Rec_nutrition WHERE MemberID>0 GROUP BY yearStr, AdminID
                UNION ALL
                SELECT  YEAR(Keydate) as yearStr, '社工紀錄' AS recType, COUNT(DISTINCT IDno) AS Yaxis, AdminID FROM FCF_careservice.Rec_social WHERE MemberID>0 GROUP BY yearStr, AdminID
                UNION ALL
                SELECT  YEAR(Keydate) as yearStr, '心理紀錄' AS recType, COUNT(DISTINCT IDno) AS Yaxis, AdminID FROM FCF_careservice.Rec_mental WHERE MemberID>0 GROUP BY yearStr, AdminID
                UNION ALL
                SELECT  YEAR(Keydate) as yearStr, '保險紀錄' AS recType, COUNT(DISTINCT IDno) AS Yaxis, AdminID FROM FCF_careservice.Rec_insurance WHERE MemberID>0 GROUP BY yearStr, AdminID
                UNION ALL
                SELECT  YEAR(Keydate) as yearStr, '志工關懷' AS recType, COUNT(DISTINCT IDno) AS Yaxis, AdminID FROM FCF_careservice.Rec_care WHERE MemberID>0 GROUP BY yearStr, AdminID
            ) t
            INNER JOIN FCF_careservice.Accuser a ON t.AdminID=a.IDno
            WHERE t.yearStr=?";
$result = $db->query($dbQuery, [$year]);

// 紀錄種類有七種
$typeArray = ['一般諮詢', '醫護紀錄', '營養紀錄', '社工紀錄', '心理紀錄', '保險紀錄', '志工關懷'];
foreach ($typeArray as $index => $recType) {
    foreach ($result as $row) {
        // 把人名塞成X軸陣列，key也要有名子是可以避免相同 key
        $tempData['X'][$row['Xaxis']] = $row['Xaxis'];
        // 如果存在相關紀錄就抓資料值
        if ($recType === $row['recType']) {
            $tempData[$row['Xaxis']][$index] = $row['Yaxis'];
        }
    }
}

// 人名當索引找出Y值
foreach ($tempData['X'] as $peopleName) {
    foreach ($typeArray as $index => $recType) {
        $response['record']['datasets'][$index]['label'] = $recType;
        // 不存在資料填0
        if (!isset($tempData[$peopleName][$index])) {
            $tempData[$peopleName][$index] = 0;
        }
        $response['record']['datasets'][$index]['data'][] = $tempData[$peopleName][$index];
    }
}

$response['record']['labels'] = array_values($tempData['X']);

//set Content-Type to JSON
header('Content-Type: application/json; charset=utf-8');
// 清除先前頁面要顯示的快取
ob_end_clean();
//echo error message as JSON
echo json_encode($response, JSON_UNESCAPED_UNICODE);
