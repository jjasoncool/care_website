<?php
/**
 * 將資料庫的 EXCEL 資料匯出
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $subPage 子類別
 * @param string $action 執行的動作
 */

ob_end_clean();

$year = intval(reqParam('year', 'get'));
$cancer = strval(reqParam('cancer', 'get'));
$type = reqParam('type', 'post');

switch ($type) {
    case 'expConsult':
        $queryParameter = [$year, $cancer];
        // 癌症別條件
        if (empty($cancer)) {
            $SQLcondition = 'AND rc.Cancer_name!=?';
        } else {
            $SQLcondition = 'AND rc.Cancer_name=?';
        }
        $anonym = 0;
        $tabTitleHead = '個案';
        break;

    case 'expOnline':
        $queryParameter = [$year];
        $SQLcondition = '';
        $anonym = 1;
        $tabTitleHead = '線上';
        break;

    default:
        break;
}

require_once get_relative_path("pages/{$page}/dataDefinition.php");

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();

// Set document properties
$spreadsheet->getProperties()->setCreator('台灣癌症基金會')
    ->setLastModifiedBy('台灣癌症基金會')
    ->setTitle('匯出資料')
    ->setSubject('年度服務時數資料')
    ->setDescription('資料屬於基金會所有，請勿用於未授權之用途')
    ->setKeywords('canceraway')
    ->setCategory("{$definition[$pageAction]['title']}({$tabTitleHead})");

$titleStyle = [
    'font' => ['bold' => true],
];

// query
$tempData = [];
$queryArray = [
    "{$tabTitleHead}醫護紀錄統計" =>
        "SELECT SUM(med.Medical) AS '醫療需求',
            SUM(med.Med_cure) AS '癌症治療相關問題',
            SUM(med.Med_check) AS '癌症檢查相關問題',
            SUM(med.Med_drug) AS '藥品健保相關問題',
            SUM(med.Med_clinical) AS '臨床試驗',
            SUM(med.Med_suggest) AS '就醫(跨科/院/國)建議',
            SUM(med.Med_assist) AS '輔助療法',
            SUM(med.Nurse) AS '護理需求',
            SUM(med.Nur_surgery) AS '術前術後衛教',
            SUM(med.Nur_chemical) AS '化學治療衛教',
            SUM(med.Nur_target) AS '標靶治療衛教',
            SUM(med.Nur_radiation) AS '放射線治療衛教',
            SUM(med.Nur_drug) AS '其他藥物衛教',
            SUM(med.Nur_home) AS '居家照護衛教',
            SUM(med.Nur_assuage) AS '緩和照護衛教',
            SUM(med.Nur_hospital) AS '住院期間其他疑問之衛教',
            SUM(med.Nur_food) AS '原則性飲食衛教',
            SUM(med.Nur_mood) AS '情緒支持',
            SUM(med.Rights) AS '權利倡議',
            SUM(med.Right_drugpay) AS '健保藥品給付',
            SUM(med.Right_newdrug) AS '新藥審查及使用條件',
            SUM(med.Right_buydrug) AS '自購藥物管道',
            SUM(med.Right_clinical) AS '臨床試驗',
            SUM(med.Right_doctor) AS '醫病關係',
            SUM(med.Right_quality) AS '醫療品質',
            SUM(med.Right_other) AS '其他權利'
            FROM FCF_careservice.Rec_medical med
            LEFT JOIN FCF_careservice.Rec_cancer rc ON med.MemberID=rc.MemberID
            WHERE med.anonym={$anonym} AND YEAR(med.Keydate)=? {$SQLcondition}",

    "{$tabTitleHead}營養紀錄統計" =>
        "SELECT SUM(nu.Aim) AS '諮詢目的',
            SUM(nu.Aim_therapeutic) AS '治療飲食',
            SUM(nu.Aim_nutrition) AS '營養品補助',
            SUM(nu.Aim_balance) AS '均衡營養',
            SUM(nu.Aim_countermeASures) AS '生理狀況之飲食對策',
            SUM(nu.Aim_other) AS '其他諮詢目的',
            SUM(nu.Provide) AS '進食方式',
            SUM(nu.Pv_self) AS '由口進食',
            SUM(nu.Pv_ng) AS '鼻胃管餵食',
            SUM(nu.Pv_gAStric) AS '胃管餵食',
            SUM(nu.Pv_intestinal) AS '腸管餵食',
            SUM(nu.Pv_ppn) AS '週邊靜脈營養',
            SUM(nu.Pv_tpn) AS '全靜脈營養',
            SUM(nu.Pv_other) AS '其他進食方式',
            SUM(nu.beforeSurgery) AS '手術前飲食衛教',
            SUM(nu.afterSurgery) AS '手術後飲食衛教',
            SUM(nu.Cure) AS '治療期間飲食衛教',
            SUM(nu.Cure_calorie) AS '熱量需求',
            SUM(nu.Cure_protein) AS '蛋白質需求與估算',
            SUM(nu.Effect) AS '生理狀況之飲食對策',
            SUM(nu.Effect_vomit) AS '噁心嘔吐 ',
            SUM(nu.Effect_oral) AS '口腔破損',
            SUM(nu.Effect_full) AS '飽脹感',
            SUM(nu.Effect_flatulence) AS '脹氣',
            SUM(nu.Effect_diarrhea) AS '腹瀉',
            SUM(nu.Effect_constipation) AS '便秘',
            SUM(nu.Effect_lowHeme) AS '血色素不足',
            SUM(nu.Effect_lowLeukocyte) AS '白血球低下',
            SUM(nu.Effect_appetiteLoss) AS '食慾不振',
            SUM(nu.Effect_weightLoss) AS '體重下降',
            SUM(nu.Effect_gainWeight) AS '體重增加',
            SUM(nu.Effect_other) AS '其他生理狀況',
            SUM(nu.Diet) AS '飲食原則',
            SUM(nu.Diet_blance) AS '均衡飲食',
            SUM(nu.Diet_diabetes) AS '糖尿病飲食',
            SUM(nu.Diet_renalFailure) AS '慢性腎衰竭飲食',
            SUM(nu.Diet_dialysis) AS '透析飲食',
            SUM(nu.Diet_liver) AS '肝病飲食',
            SUM(nu.Diet_other) AS '其他飲食原則',
            SUM(nu.Nutrition) AS '保健營養品相關資訊',
            SUM(nu.Nu_panax) AS '蔘類',
            SUM(nu.Nu_algae) AS '藻類',
            SUM(nu.Nu_vitamins) AS '維生素類',
            SUM(nu.Nu_calcium) AS '鈣片',
            SUM(nu.Nu_mushroom) AS '菇菌類保健品',
            SUM(nu.Nu_phytochemicals) AS '植化素萃取物',
            SUM(nu.Nu_other) AS '其他保健營養品',
            SUM(nu.Eat) AS '飲食製備方式',
            SUM(nu.Eat_normal) AS '普通飲食',
            SUM(nu.Eat_liquid) AS '流質飲食',
            SUM(nu.Eat_bland) AS '溫和飲食',
            SUM(nu.Eat_lowResidue) AS '低渣飲食',
            SUM(nu.Eat_highFiber) AS '高纖飲食',
            SUM(nu.Eat_tube) AS '管灌飲食',
            SUM(nu.Eat_elemental) AS '元素飲',
            SUM(nu.Eat_other) AS '其他飲食方法',
            SUM(nu.Special) AS '特殊營養品需求',
            SUM(nu.Sp_glutamine) AS '麩醯胺酸',
            SUM(nu.Sp_balanced) AS '均衡配方',
            SUM(nu.Sp_tumor) AS '腫瘤配方',
            SUM(nu.Sp_pneumonia) AS '肺病配方',
            SUM(nu.Sp_diabetes) AS '糖尿病配方',
            SUM(nu.Sp_nephropathy) AS '腎病配方',
            SUM(nu.Sp_other) AS '其他特殊營養品'
            FROM FCF_careservice.Rec_nutrition nu
            LEFT JOIN FCF_careservice.Rec_cancer rc ON nu.MemberID=rc.MemberID
            WHERE nu.anonym={$anonym} AND YEAR(nu.Keydate)=? {$SQLcondition}",

    "{$tabTitleHead}社工紀錄統計" =>
        "SELECT SUM(so.Aim_wig) AS '假髮租借',
            SUM(so.Aim_welfare) AS '福利諮詢(原就業/服務需求)',
            SUM(so.Aim_goods) AS '物資需求',
            SUM(so.Aim_volcare) AS '志工關懷需求',
            SUM(so.Aim_other) AS '其他',
            SUM(so.Grants) AS '補助評估',
            SUM(so.Grant_help) AS '急難救助',
            SUM(so.Grant_nutrition) AS '營養品補助',
            SUM(so.Grant_traffic) AS '交通補助',
            SUM(so.Grant_respitecare) AS '喘息服務'
            FROM FCF_careservice.Rec_social so
            LEFT JOIN FCF_careservice.Rec_cancer rc ON so.MemberID=rc.MemberID
            WHERE so.anonym={$anonym} AND YEAR(so.Keydate)=? {$SQLcondition}",

    "{$tabTitleHead}心理紀錄統計" =>
        "SELECT SUM(mt.Mind) AS '諮詢目的',
            SUM(mt.Mind_feeling) AS '情緒議題',
            SUM(mt.Mind_self) AS '自我概念',
            SUM(mt.Mind_adapt) AS '生活適應',
            SUM(mt.Mind_friends) AS '人際關係',
            SUM(mt.Mind_family) AS '家庭議題',
            SUM(mt.Mind_sickbed) AS '疾病末期',
            SUM(mt.Mind_suicide) AS '自殺意念',
            SUM(mt.Mind_selfcare) AS '自我照顧',
            SUM(mt.Mind_lost) AS '失落經驗',
            SUM(mt.Mind_other) AS '其他'
            FROM FCF_careservice.Rec_mental mt
            LEFT JOIN FCF_careservice.Rec_cancer rc ON mt.MemberID=rc.MemberID
            WHERE mt.anonym={$anonym} AND YEAR(mt.Keydate)=? {$SQLcondition}",

    "{$tabTitleHead}保險紀錄統計" =>
        "SELECT SUM(ins.Ins_med) AS '醫療相關',
            SUM(ins.Ins_admission) AS '住院給付(醫療)',
            SUM(ins.Ins_clinic) AS '門診給付(醫療)',
            SUM(ins.Ins_surgery) AS '手術給付(醫療)',
            SUM(ins.Ins_expenses) AS '自費醫材給付(醫療)',
            SUM(ins.Ins_targeted) AS '標靶藥物給付(醫療)',
            SUM(ins.Ins_radiology) AS '放射治療給付(醫療)',
            SUM(ins.Ins_chemotherapy) AS '化學治療給付(醫療)',
            SUM(ins.Ins_immunotherapy) AS '免疫治療給付(醫療)',
            SUM(ins.Ins_discharged) AS '出院帶藥給付(醫療)',
            SUM(ins.Ins_tcm) AS '中醫門診(醫療)',
            SUM(ins.Ins_hospice) AS '安寧病房給付(醫療)',
            SUM(ins.Ins_policy) AS '保單相關',
            SUM(ins.Ins_view) AS '保單檢視(保單)',
            SUM(ins.Ins_plan) AS '保險規劃(保單)',
            SUM(ins.Ins_law) AS '保險法律(保單)'
            FROM FCF_careservice.Rec_insurance ins
            LEFT JOIN FCF_careservice.Rec_cancer rc ON ins.MemberID=rc.MemberID
            WHERE ins.anonym={$anonym} AND YEAR(ins.Keydate)=? {$SQLcondition}",

    "{$tabTitleHead}志工紀錄統計" =>
        "SELECT SUM(cr.Act_care) AS '近況了解及關懷',
            SUM(cr.Act_expshare) AS '療程經驗分享',
            SUM(cr.Act_support) AS '情緒支持',
            SUM(cr.Act_other) AS '其他服務',
            SUM(cr.Transfer) AS '轉介專業人員',
            SUM(cr.Trans_medical) AS '護理師',
            SUM(cr.Trans_nutrition) AS '營養師',
            SUM(cr.Trans_social) AS '社工師',
            SUM(cr.Trans_mental) AS '心理師'
            FROM FCF_careservice.Rec_care cr
            LEFT JOIN FCF_careservice.Rec_cancer rc ON cr.MemberID=rc.MemberID
            WHERE cr.anonym={$anonym} AND YEAR(cr.Keydate)=? {$SQLcondition}",
];

// 線上諮詢才有一般紀錄
if ($type === 'expOnline') {
    $queryArray["線上一般紀錄統計"] = "SELECT SUM(ask.vegetarian) AS '蔬果防癌觀念',
            SUM(ask.Vege_1) AS '蔬果彩虹579',
            SUM(ask.Vege_2) AS '高纖蔬果',
            SUM(ask.Vege_3) AS '蔬果的農藥清洗',
            SUM(ask.Vege_4) AS '減脂防癌',
            SUM(ask.Vege_5) AS '食物保存',
            SUM(ask.Vege_6) AS '食品安全',
            SUM(ask.Vege_other) AS '其他蔬果觀念',
            SUM(ask.prevent) AS '癌症預防觀念',
            SUM(ask.prev_1) AS '子宮頸癌疫苗問題及衛教',
            SUM(ask.prev_2) AS '健康檢查',
            SUM(ask.prev_3) AS '生活防癌全民練5功衛教',
            SUM(ask.Knowledge) AS '疾病的認識',
            SUM(ask.Konw_1) AS '癌症的認識',
            SUM(ask.Konw_2) AS '治療的認識',
            SUM(ask.Konw_3) AS '其他疾病的認識',
            SUM(ask.Other) AS '其他癌症預防觀念',
            SUM(ask.Oth_1) AS '陳情及建議',
            SUM(ask.Oth_2) AS '感謝',
            SUM(ask.Oth_3) AS '異業結盟',
            SUM(ask.hair) AS '捐贈頭髮'
            FROM FCF_careservice.Rec_ask ask
            WHERE ask.anonym=1 AND YEAR(ask.Keydate)=?";
}


// 搜尋query陣列，存到暫存陣列
foreach ($queryArray as $name => $dbQuery) {
    $result = $db->query($dbQuery, $queryParameter);
    foreach ($result as $row) {
        $tempData[$name][] = $row;
    }
}

// 頁面陣列
$sheetNum = 0;
$pageArray = array_keys($tempData);
foreach ($pageArray as $pageName) {
    $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, 1, "{$year}年統計");
    $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(2, 1, $cancer);
    // Add some data
    $column = 1;
    foreach (array_keys($tempData[$pageName][0]) as $key => $colhead) {
        $rownum = $key + 2;
        $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $rownum, $colhead);
        $spreadsheet->getActiveSheet()->getCellByColumnAndRow($column, $rownum)->getStyle()->getFont()->setBold(true);
    }
    $column++;

    foreach ($tempData[$pageName] as $row) {
        // 塞入資料
        $rownum = 1;
        foreach ($row as $key => $value) {
            // 內容欄位從第1欄開始
            $rownum++;
            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $rownum, $value);
        }
        $column++;
    }
    // Rename worksheet
    $spreadsheet->getActiveSheet()->setTitle($pageName);
    $sheetNum++;
    // 超過項目總數就不用再加表了
    if ($sheetNum < count($pageArray)) {
        $spreadsheet->createSheet();
        // 一頁寫完換下一頁
        $spreadsheet->setActiveSheetIndex($sheetNum);
    }
}

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment;filename=\"{$tabTitleHead}{$definition[$pageAction]['title']}.xlsx\"");
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit("<script>window.close();</script>");
