<?php
/**
 * 將資料庫的 EXCEL 資料匯出
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $subPage 子類別
 * @param string $action 執行的動作
 */

// 清除先前頁面要顯示的快取
ob_end_clean();

$year = intval(reqParam('year', 'get'));
$cancer = strval(reqParam('cancer', 'get'));
$type = reqParam('type', 'post');
// 癌症別條件
if (empty($cancer)) {
    $SQLcondition = 'AND rc.Cancer_name!=?';
} else {
    $SQLcondition = 'AND rc.Cancer_name=?';
}

// 基本設定
require_once get_relative_path("pages/{$page}/dataDefinition.php");

$queryParameter = [];
switch ($type) {
    case 'expList':
        $dbQuery = "SELECT C_name,
                        CASE C_subscribe WHEN 1 THEN '紙本' WHEN 2 THEN '電子檔' END AS 'method',
                        CASE C_subscribe WHEN 1 THEN CONCAT(C_zip, C_city, C_zone, C_address) WHEN 2 THEN C_mail END AS 'contact'
                    FROM FCF_careservice.Memberdata WHERE C_status=1 AND C_subscribe!=0 ORDER BY C_subscribe";
        break;

    case 'expCase':
        $dbQuery = "SELECT CASE m.C_member WHEN 1 THEN '個案' WHEN 3 THEN '志工' END AS memberType,
                        m.C_name, m.BD_mm
                    FROM FCF_careservice.Memberdata m
                    WHERE m.C_member IN (1,3) AND m.C_status=1 AND m.BD_mm IS NOT NULL AND m.BD_mm BETWEEN 1 AND 12
                    ORDER BY m.BD_mm";
        break;

    case 'expRec':
        $queryParameter = [$year];
        $dbQuery = "SELECT t.*, a.Acc_name FROM (
                        SELECT YEAR(Keydate) as yearStr, '一般諮詢' AS recType, COUNT(DISTINCT IDno) AS recNum, AdminID FROM FCF_careservice.Rec_ask WHERE MemberID>0 GROUP BY yearStr, AdminID
                        UNION ALL
                        SELECT YEAR(Keydate) as yearStr, '醫護紀錄' AS recType, COUNT(DISTINCT IDno) AS recNum, AdminID FROM FCF_careservice.Rec_medical WHERE MemberID>0 GROUP BY yearStr, AdminID
                        UNION ALL
                        SELECT YEAR(Keydate) as yearStr, '營養紀錄' AS recType, COUNT(DISTINCT IDno) AS recNum, AdminID FROM FCF_careservice.Rec_nutrition WHERE MemberID>0 GROUP BY yearStr, AdminID
                        UNION ALL
                        SELECT YEAR(Keydate) as yearStr, '社工紀錄' AS recType, COUNT(DISTINCT IDno) AS recNum, AdminID FROM FCF_careservice.Rec_social WHERE MemberID>0 GROUP BY yearStr, AdminID
                        UNION ALL
                        SELECT YEAR(Keydate) as yearStr, '心理紀錄' AS recType, COUNT(DISTINCT IDno) AS recNum, AdminID FROM FCF_careservice.Rec_mental WHERE MemberID>0 GROUP BY yearStr, AdminID
                        UNION ALL
                        SELECT YEAR(Keydate) as yearStr, '保險紀錄' AS recType, COUNT(DISTINCT IDno) AS recNum, AdminID FROM FCF_careservice.Rec_insurance WHERE MemberID>0 GROUP BY yearStr, AdminID
                        UNION ALL
                        SELECT YEAR(Keydate) as yearStr, '志工關懷' AS recType, COUNT(DISTINCT IDno) AS recNum, AdminID FROM FCF_careservice.Rec_care WHERE MemberID>0 GROUP BY yearStr, AdminID
                    ) t
                    INNER JOIN FCF_careservice.Accuser a ON t.AdminID=a.IDno
                    WHERE t.yearStr=? ORDER BY recType";
        break;

    case 'expTime':
        $queryParameter = [$year];
        $dbQuery = "SELECT t.* FROM (
                        SELECT YEAR(Keydate) as yearStr, '一般諮詢' AS recType,
                            CASE R_way WHEN 'tel' THEN '電話' WHEN 'face' THEN '面談' WHEN 'web' THEN '網路' WHEN 'email' THEN '電子郵件' ELSE '其他' END AS recWay,
                            ROUND(SUM(R_time)/60, 2) AS recTime, COUNT(DISTINCT IDno) AS recNum
                        FROM FCF_careservice.Rec_ask WHERE MemberID>0 GROUP BY yearStr, R_way
                        UNION ALL
                        SELECT YEAR(Keydate) as yearStr, '醫護紀錄' AS recType,
                            CASE R_way WHEN 'tel' THEN '電話' WHEN 'face' THEN '面談' WHEN 'web' THEN '網路' WHEN 'email' THEN '電子郵件' ELSE '其他' END AS recWay,
                            ROUND(SUM(R_time)/60, 2) AS recTime, COUNT(DISTINCT IDno) AS recNum
                        FROM FCF_careservice.Rec_medical WHERE MemberID>0 GROUP BY yearStr, R_way
                        UNION ALL
                        SELECT YEAR(Keydate) as yearStr, '營養紀錄' AS recType,
                            CASE R_way WHEN 'tel' THEN '電話' WHEN 'face' THEN '面談' WHEN 'web' THEN '網路' WHEN 'email' THEN '電子郵件' ELSE '其他' END AS recWay,
                            ROUND(SUM(R_time)/60, 2) AS recTime, COUNT(DISTINCT IDno) AS recNum
                        FROM FCF_careservice.Rec_nutrition WHERE MemberID>0 GROUP BY yearStr, R_way
                        UNION ALL
                        SELECT YEAR(Keydate) as yearStr, '社工紀錄' AS recType,
                            CASE R_way WHEN 'tel' THEN '電話' WHEN 'face' THEN '面談' WHEN 'web' THEN '網路' WHEN 'email' THEN '電子郵件' ELSE '其他' END AS recWay,
                            ROUND(SUM(R_time)/60, 2) AS recTime, COUNT(DISTINCT IDno) AS recNum
                        FROM FCF_careservice.Rec_social WHERE MemberID>0 GROUP BY yearStr, R_way
                        UNION ALL
                        SELECT YEAR(Keydate) as yearStr, '心理紀錄' AS recType,
                            CASE R_way WHEN 'tel' THEN '電話' WHEN 'face' THEN '面談' WHEN 'web' THEN '網路' WHEN 'email' THEN '電子郵件' ELSE '其他' END AS recWay,
                            ROUND(SUM(R_time)/60, 2) AS recTime, COUNT(DISTINCT IDno) AS recNum
                        FROM FCF_careservice.Rec_mental WHERE MemberID>0 GROUP BY yearStr, R_way
                        UNION ALL
                        SELECT YEAR(Keydate) as yearStr, '保險紀錄' AS recType,
                            CASE R_way WHEN 'tel' THEN '電話' WHEN 'face' THEN '面談' WHEN 'web' THEN '網路' WHEN 'email' THEN '電子郵件' ELSE '其他' END AS recWay,
                            ROUND(SUM(R_time)/60, 2) AS recTime, COUNT(DISTINCT IDno) AS recNum
                        FROM FCF_careservice.Rec_insurance WHERE MemberID>0 GROUP BY yearStr, R_way
                        UNION ALL
                        SELECT YEAR(Keydate) as yearStr, '志工關懷' AS recType,
                            CASE R_way WHEN 'tel' THEN '電話' WHEN 'face' THEN '面談' WHEN 'web' THEN '網路' WHEN 'email' THEN '電子郵件' ELSE '其他' END AS recWay,
                            ROUND(SUM(R_time)/60, 2) AS recTime, COUNT(DISTINCT IDno) AS recNum
                        FROM FCF_careservice.Rec_care WHERE MemberID>0 GROUP BY yearStr, R_way
                    ) t
                    WHERE t.yearStr=? ORDER BY recType";
        break;

    case 'expPhone':
        $queryParameter = [$cancer];
        $dbQuery = "SELECT YEAR(m.Keydate) AS 'keyYear', CASE m.FCFvolunte WHEN 1 THEN '志工' ELSE '個案' END AS mType,
                        rc.Cancer_name, m.C_name, m.C_city, m.C_mobile1, m.C_mobile2
                    FROM FCF_careservice.Memberdata m
                    LEFT JOIN FCF_careservice.Rec_cancer rc ON rc.MemberID=m.IDno
                    WHERE m.C_status=1 AND m.C_mobile1 IS NOT NULL AND m.C_mobile1!='' {$SQLcondition}
                    ORDER BY m.C_city, rc.Cancer_name, keyYear";
        break;

    default:
        exit();
        break;
}

// query資料
$result = $db->query($dbQuery, $queryParameter);

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();

// Set document properties
$spreadsheet->getProperties()->setCreator('台灣癌症基金會')
    ->setLastModifiedBy('台灣癌症基金會')
    ->setTitle('匯出資料')
    ->setSubject('年度服務時數資料')
    ->setDescription('資料屬於基金會所有，請勿用於未授權之用途')
    ->setKeywords('canceraway')
    ->setCategory($definition[$subPage]['title']);

$titleStyle = [
    'font' => ['bold' => true],
];

// 分組陣列
$sheetNum = 0;
$pageArray = $definition[$subPage][$type]['exportPage'];
foreach ($pageArray as $category => $pageName) {
    // Add some data
    $rownum = 1;
    foreach ($definition[$subPage][$type]['colhead'] as $key => $colhead) {
        $column = $key + 1;
        $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $rownum, $colhead);
        $spreadsheet->getActiveSheet()->getCellByColumnAndRow($column, $rownum)->getStyle()->getFont()->setBold(true);
    }
    $rownum++;

    foreach ($result as $row) {
        // 符合分類條件或沒有分類
        if (!isset($definition[$subPage][$type]['filter']) || $row[$definition[$subPage][$type]['filter']] === $category) {
            foreach ($definition[$subPage][$type]['col'] as $key => $columnName) {
                // 內容欄位從第1欄開始
                $column = $key + 1;
                $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $rownum, $row[$columnName]);
            }
            $rownum++;
        }
    }
    // Rename worksheet
    $spreadsheet->getActiveSheet()->setTitle($pageName);
    $sheetNum++;
    // 超過項目總數就不用再加表了
    if ($sheetNum < count($pageArray)) {
        $spreadsheet->createSheet();
        // 一頁寫完換下一頁
        $spreadsheet->setActiveSheetIndex($sheetNum);
    }
}

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment;filename=\"{$definition[$subPage][$type]['title']}.xlsx\"");
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit("<script>window.close();</script>");
