<?php
/**
 * 搜尋可以列出統計的資料
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 * @param string $action 路由到這個頁面的參數
 */

$year = intval(reqParam('year', 'post'));
$cancer = strval(reqParam('cancer', 'post'));
// 癌症別條件
if (empty($cancer)) {
    $SQLcondition = 'AND rc.Cancer_name!=?';
} else {
    $SQLcondition = 'AND rc.Cancer_name=?';
}
// 回傳資料
$response = array();

$tempData['X'] = $tempData['Y'] = [];
// 來源件數/比例分析
$dbQuery = "SELECT COUNT(1) AS Yaxis, m.C_comefrom AS Xaxis FROM FCF_careservice.Memberdata m
            INNER JOIN FCF_careservice.Rec_cancer rc ON rc.MemberID=m.IDno
            WHERE YEAR(m.Keydate)=? AND m.C_comefrom!='' AND m.C_comefrom IS NOT NULL {$SQLcondition} GROUP BY m.C_comefrom;";
$result = $db->query($dbQuery, [$year, $cancer]);
foreach ($result as $row) {
    $tempData['X'][] = $row['Xaxis'];
    $tempData['Y'][] = $row['Yaxis'];
}
$response['basic']['labels'] = $tempData['X'];
$response['basic']['datasets'][0]['label'] = '人數';
$response['basic']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 各癌別/年齡分布
$dbQuery = "SELECT SUM(CASE WHEN YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))<=10 THEN 1 ELSE 0 END) AS '1~10',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))>10 AND YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))<=20) THEN 1 ELSE 0 END) AS '11~20',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))>20 AND YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))<=30) THEN 1 ELSE 0 END) AS '21~30',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))>30 AND YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))<=40) THEN 1 ELSE 0 END) AS '31~40',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))>40 AND YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))<=50) THEN 1 ELSE 0 END) AS '41~50',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))>50 AND YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))<=60) THEN 1 ELSE 0 END) AS '51~60',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))>60 AND YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))<=70) THEN 1 ELSE 0 END) AS '61~70',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))>70 AND YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))<=80) THEN 1 ELSE 0 END) AS '71~80',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))>80 AND YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))<=90) THEN 1 ELSE 0 END) AS '81~90',
                SUM(CASE WHEN (YEAR(NOW())-YEAR(STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e'))>90) THEN 1 ELSE 0 END) AS '91~'
            FROM FCF_careservice.Rec_cancer rc
            INNER JOIN FCF_careservice.Memberdata m ON rc.MemberID=m.IDno
            WHERE YEAR(rc.Keydate)=? {$SQLcondition}";
$result = $db->row($dbQuery, [$year, $cancer]);
foreach ($result as $title => $nums) {
    $tempData['X'][] = $title;
    $tempData['Y'][] = $nums;
}
$response['age']['labels'] = $tempData['X'];
$response['age']['datasets'][0]['label'] = '人數';
$response['age']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 性別
$dbQuery = "SELECT SUM(1) AS 'all',
                SUM(CASE m.C_sex WHEN 'male' THEN 1 ELSE 0 END) AS '男性',
                SUM(CASE m.C_sex WHEN 'female' THEN 1 ELSE 0 END) AS '女性'
            FROM FCF_careservice.Rec_cancer rc
            INNER JOIN FCF_careservice.Memberdata m ON rc.MemberID=m.IDno
            WHERE YEAR(rc.Keydate)=? {$SQLcondition}";
$result = $db->row($dbQuery, [$year, $cancer]);
foreach ($result as $title => $nums) {
    $tempData['X'][] = $title;
    $tempData['Y'][] = $nums;
}
$response['sex']['labels'] = $tempData['X'];
$response['sex']['datasets'][0]['label'] = '人數';
$response['sex']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 縣市
$dbQuery = "SELECT m.C_city AS Xaxis, COUNT(DISTINCT m.IDno) AS Yaxis
            FROM FCF_careservice.Rec_cancer rc
            INNER JOIN FCF_careservice.Memberdata m ON rc.MemberID=m.IDno
            WHERE YEAR(rc.Keydate)=? AND m.C_city!='' {$SQLcondition}
            GROUP BY m.C_city";
$result = $db->query($dbQuery, [$year, $cancer]);
$total = 0;
foreach ($result as $row) {
    $tempData['X'][] = $row['Xaxis'];
    $tempData['Y'][] = $row['Yaxis'];
    $total += $row['Yaxis'];
}
$tempData['X'][] = '總人數';
$tempData['Y'][] = $total;
$response['city']['labels'] = $tempData['X'];
$response['city']['datasets'][0]['label'] = '人數';
$response['city']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 職業
$dbQuery = "SELECT CASE m.C_job
            WHEN 1 THEN '一般職業'
            WHEN 2 THEN '農牧業'
            WHEN 3 THEN '漁業'
            WHEN 4 THEN '木材森林業'
            WHEN 5 THEN '礦業採石業'
            WHEN 6 THEN '交通運輸業'
            WHEN 7 THEN '餐旅業'
            WHEN 8 THEN '建築工程業'
            WHEN 9 THEN '製造業'
            WHEN 10 THEN '新聞廣告業'
            WHEN 11 THEN '保健業'
            WHEN 12 THEN '娛樂業'
            WHEN 13 THEN '文教機關'
            WHEN 14 THEN '宗教團體'
            WHEN 15 THEN '公共事業'
            WHEN 16 THEN '一般商業'
            WHEN 17 THEN '服務業'
            WHEN 18 THEN '家庭管理'
            WHEN 19 THEN '治安人員'
            WHEN 20 THEN '軍人'
            WHEN 21 THEN '資訊業'
            WHEN 22 THEN '職業運動人員'
            WHEN 23 THEN '公務人員'
            WHEN 24 THEN '待業中'
            WHEN 25 THEN '退休'
            END AS Xaxis,
            COUNT(DISTINCT m.IDno) AS Yaxis
            FROM FCF_careservice.Rec_cancer rc
            INNER JOIN FCF_careservice.Memberdata m ON rc.MemberID=m.IDno
            WHERE YEAR(rc.Keydate)=? AND m.C_job!=0 {$SQLcondition}
            GROUP BY m.C_job";
$result = $db->query($dbQuery, [$year, $cancer]);
foreach ($result as $row) {
    $tempData['X'][] = $row['Xaxis'];
    $tempData['Y'][] = $row['Yaxis'];
}
$response['job']['labels'] = $tempData['X'];
$response['job']['datasets'][0]['label'] = '人數';
$response['job']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 福利身分
$dbQuery = "SELECT SUM(m.id_normal) as '一般', SUM(m.id_lowincome) as '低/中低收入', SUM(m.id_weakincome) as '經濟弱勢', SUM(m.id_oldman) as '老人', SUM(m.id_handicapped) as '身心障礙', SUM(m.id_indigenous) as '原住民', SUM(m.id_foreign) as '新住民', SUM(m.id_singlemon) as '單親', SUM(m.id_specstatus) as '特殊境遇'
            FROM FCF_careservice.Rec_cancer rc
            INNER JOIN FCF_careservice.Memberdata m ON rc.MemberID=m.IDno
            WHERE YEAR(rc.Keydate)=? {$SQLcondition}";
$result = $db->row($dbQuery, [$year, $cancer]);
foreach ($result as $title => $nums) {
    $tempData['X'][] = $title;
    $tempData['Y'][] = $nums;
}
$response['welfare']['labels'] = $tempData['X'];
$response['welfare']['datasets'][0]['label'] = '人次';
$response['welfare']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 教育程度
$dbQuery = "SELECT m.C_edu AS Xaxis, COUNT(DISTINCT m.IDno) AS Yaxis
            FROM FCF_careservice.Rec_cancer rc
            INNER JOIN FCF_careservice.Memberdata m ON rc.MemberID=m.IDno
            WHERE YEAR(rc.Keydate)=? AND m.C_edu!='' AND m.C_edu!='--' {$SQLcondition}
            GROUP BY m.C_edu";
$result = $db->query($dbQuery, [$year, $cancer]);
foreach ($result as $row) {
    $tempData['X'][] = $row['Xaxis'];
    $tempData['Y'][] = $row['Yaxis'];
}
$response['edu']['labels'] = $tempData['X'];
$response['edu']['datasets'][0]['label'] = '人數';
$response['edu']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 保險狀況
$dbQuery = "SELECT SUM(m.Ins_none) AS '無', SUM(m.Ins_health) AS '健保', SUM(m.Ins_social) AS '福保', SUM(m.Ins_work) AS '勞保', SUM(m.Ins_gov) AS '公保', SUM(m.Ins_fisher) AS '漁保', SUM(m.Ins_farmer) AS '農保', SUM(m.Ins_soldier) AS '軍保', SUM(m.Ins_commerce) AS '商業保險'
            FROM FCF_careservice.Rec_cancer rc
            INNER JOIN FCF_careservice.Memberdata m ON rc.MemberID=m.IDno
            WHERE YEAR(rc.Keydate)=? {$SQLcondition}";
$result = $db->row($dbQuery, [$year, $cancer]);
foreach ($result as $title => $nums) {
    $tempData['X'][] = $title;
    $tempData['Y'][] = $nums;
}
$response['insure']['labels'] = $tempData['X'];
$response['insure']['datasets'][0]['label'] = '人次';
$response['insure']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 經濟狀況
$dbQuery = "SELECT SUM(CASE WHEN FIND_IN_SET('0', REPLACE(REPLACE(m.ecoStatus, ', ', ','), ' ,', ','))>0 THEN 1 ELSE 0 END) AS '自己有工作',
            SUM(CASE WHEN FIND_IN_SET('1', REPLACE(REPLACE(m.ecoStatus, ', ', ','), ' ,', ','))>0 THEN 1 ELSE 0 END) AS '政府補助',
            SUM(CASE WHEN FIND_IN_SET('2', REPLACE(REPLACE(m.ecoStatus, ', ', ','), ' ,', ','))>0 THEN 1 ELSE 0 END) AS '父母扶養',
            SUM(CASE WHEN FIND_IN_SET('3', REPLACE(REPLACE(m.ecoStatus, ', ', ','), ' ,', ','))>0 THEN 1 ELSE 0 END) AS '子女提供',
            SUM(CASE WHEN FIND_IN_SET('4', REPLACE(REPLACE(m.ecoStatus, ', ', ','), ' ,', ','))>0 THEN 1 ELSE 0 END) AS '親友提供',
            SUM(CASE WHEN FIND_IN_SET('5', REPLACE(REPLACE(m.ecoStatus, ', ', ','), ' ,', ','))>0 THEN 1 ELSE 0 END) AS '其他'
            FROM FCF_careservice.Rec_cancer rc
            INNER JOIN FCF_careservice.Memberdata m ON rc.MemberID=m.IDno
            WHERE YEAR(rc.Keydate)=? {$SQLcondition}";
$result = $db->row($dbQuery, [$year, $cancer]);
foreach ($result as $title => $nums) {
    $tempData['X'][] = $title;
    $tempData['Y'][] = $nums;
}
$response['eco']['labels'] = $tempData['X'];
$response['eco']['datasets'][0]['label'] = '人次';
$response['eco']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 慢性疾病史
$dbQuery = "SELECT SUM(CASE WHEN FIND_IN_SET('0', REPLACE(REPLACE(m.chronic, ', ', ','), ' ,', ','))>0 THEN 1 ELSE 0 END) AS '無',
            SUM(CASE WHEN FIND_IN_SET('1', REPLACE(REPLACE(m.chronic, ', ', ','), ' ,', ','))>0 THEN 1 ELSE 0 END) AS '糖尿病',
            SUM(CASE WHEN FIND_IN_SET('2', REPLACE(REPLACE(m.chronic, ', ', ','), ' ,', ','))>0 THEN 1 ELSE 0 END) AS '高血壓',
            SUM(CASE WHEN FIND_IN_SET('3', REPLACE(REPLACE(m.chronic, ', ', ','), ' ,', ','))>0 THEN 1 ELSE 0 END) AS '心臟病',
            SUM(CASE WHEN FIND_IN_SET('4', REPLACE(REPLACE(m.chronic, ', ', ','), ' ,', ','))>0 THEN 1 ELSE 0 END) AS '中風',
            SUM(CASE WHEN FIND_IN_SET('5', REPLACE(REPLACE(m.chronic, ', ', ','), ' ,', ','))>0 THEN 1 ELSE 0 END) AS 'COPD',
            SUM(CASE WHEN FIND_IN_SET('6', REPLACE(REPLACE(m.chronic, ', ', ','), ' ,', ','))>0 THEN 1 ELSE 0 END) AS 'CRF'
            FROM FCF_careservice.Rec_cancer rc
            INNER JOIN FCF_careservice.Memberdata m ON rc.MemberID=m.IDno
            WHERE YEAR(rc.Keydate)=? {$SQLcondition}";
$result = $db->row($dbQuery, [$year, $cancer]);
foreach ($result as $title => $nums) {
    $tempData['X'][] = $title;
    $tempData['Y'][] = $nums;
}
$response['chronic']['labels'] = $tempData['X'];
$response['chronic']['datasets'][0]['label'] = '人次';
$response['chronic']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 期別
$dbQuery = "SELECT rc.Cancer_level AS Xaxis, COUNT(1) AS Yaxis FROM FCF_careservice.Rec_cancer rc
            WHERE YEAR(rc.Keydate)=? {$SQLcondition} GROUP BY rc.Cancer_level";
$result = $db->query($dbQuery, [$year, $cancer]);
foreach ($result as $row) {
    $tempData['X'][] = $row['Xaxis'];
    $tempData['Y'][] = $row['Yaxis'];
}
$response['stage']['labels'] = $tempData['X'];
$response['stage']['datasets'][0]['label'] = '人次';
$response['stage']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 診斷時間
$dbQuery = "SELECT CONCAT(MONTH(rc.Find_date), '月') AS Xaxis, COUNT(1) AS Yaxis FROM FCF_careservice.Rec_cancer rc
            WHERE YEAR(rc.Keydate)=? {$SQLcondition} GROUP BY MONTH(rc.Find_date)";
$result = $db->query($dbQuery, [$year, $cancer]);
foreach ($result as $row) {
    $tempData['X'][] = $row['Xaxis'];
    $tempData['Y'][] = $row['Yaxis'];
}
$response['diagnosis']['labels'] = $tempData['X'];
$response['diagnosis']['datasets'][0]['label'] = '人次';
$response['diagnosis']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 基因檢測
$dbQuery = "SELECT CASE WHEN LENGTH(rc.genetic_test)>1 THEN '有做檢測' ELSE '沒做檢測' END AS Xaxis, COUNT(1) AS Yaxis
            FROM FCF_careservice.Rec_cancer rc WHERE YEAR(rc.Keydate)=? {$SQLcondition} GROUP BY LENGTH(rc.genetic_test)";
$result = $db->query($dbQuery, [$year, $cancer]);
foreach ($result as $row) {
    $tempData['X'][] = $row['Xaxis'];
    $tempData['Y'][] = $row['Yaxis'];
}
$response['gene']['labels'] = $tempData['X'];
$response['gene']['datasets'][0]['label'] = '人次';
$response['gene']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 復發時間
$dbQuery = "SELECT CONCAT(MONTH(rc.recurrence), '月') AS Xaxis, COUNT(1) AS Yaxis FROM FCF_careservice.Rec_cancer rc
            WHERE YEAR(rc.Keydate)=? {$SQLcondition} AND rc.recurrence IS NOT NULL GROUP BY MONTH(rc.recurrence)";
$result = $db->query($dbQuery, [$year, $cancer]);
foreach ($result as $row) {
    $tempData['X'][] = $row['Xaxis'];
    $tempData['Y'][] = $row['Yaxis'];
}
$response['relapse']['labels'] = $tempData['X'];
$response['relapse']['datasets'][0]['label'] = '人次';
$response['relapse']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 復發時間區間
$dbQuery = "SELECT SUM(CASE WHEN TIMESTAMPDIFF(YEAR, rc.Find_date, rc.recurrence)<=1 THEN 1 ELSE 0 END) AS '1年以內',
            SUM(CASE WHEN (TIMESTAMPDIFF(YEAR, rc.Find_date, rc.recurrence)>1 AND TIMESTAMPDIFF(YEAR, rc.Find_date, rc.recurrence)<=3) THEN 1 ELSE 0 END) AS '1~3年',
            SUM(CASE WHEN (TIMESTAMPDIFF(YEAR, rc.Find_date, rc.recurrence)>3 AND TIMESTAMPDIFF(YEAR, rc.Find_date, rc.recurrence)<=5) THEN 1 ELSE 0 END) AS '3~5年',
            SUM(CASE WHEN (TIMESTAMPDIFF(YEAR, rc.Find_date, rc.recurrence)>5 AND TIMESTAMPDIFF(YEAR, rc.Find_date, rc.recurrence)<=7) THEN 1 ELSE 0 END) AS '5~7年',
            SUM(CASE WHEN (TIMESTAMPDIFF(YEAR, rc.Find_date, rc.recurrence)>7 AND TIMESTAMPDIFF(YEAR, rc.Find_date, rc.recurrence)<=10) THEN 1 ELSE 0 END) AS '7~10年',
            SUM(CASE WHEN (TIMESTAMPDIFF(YEAR, rc.Find_date, rc.recurrence)>10 AND TIMESTAMPDIFF(YEAR, rc.Find_date, rc.recurrence)<=15) THEN 1 ELSE 0 END) AS '10~15年',
            SUM(CASE WHEN (TIMESTAMPDIFF(YEAR, rc.Find_date, rc.recurrence)>15 AND TIMESTAMPDIFF(YEAR, rc.Find_date, rc.recurrence)<=20) THEN 1 ELSE 0 END) AS '15~20年',
            SUM(CASE WHEN (TIMESTAMPDIFF(YEAR, rc.Find_date, rc.recurrence)>20) THEN 1 ELSE 0 END) AS '20年以上'
            FROM FCF_careservice.Rec_cancer rc
            WHERE YEAR(rc.Keydate)=? {$SQLcondition} AND rc.recurrence IS NOT NULL";
$result = $db->row($dbQuery, [$year, $cancer]);
foreach ($result as $title => $nums) {
    $tempData['X'][] = $title;
    $tempData['Y'][] = $nums;
}
$response['interval']['labels'] = $tempData['X'];
$response['interval']['datasets'][0]['label'] = '人次';
$response['interval']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 治療方法
$dbQuery = "SELECT SUM(rc.Chemical) AS '化學', SUM(rc.Target) AS '標靶', SUM(rc.Immune) AS '免疫', SUM(rc.Radiation) AS '放射線', SUM(rc.hormone) AS '抗賀爾蒙',
            SUM(rc.integrated) AS '中西整合', SUM(rc.tracking) AS '門診追蹤', SUM(rc.peace) AS '安寧療護', SUM(rc.other) AS '其他'
            FROM FCF_careservice.Rec_cancer rc
            WHERE YEAR(rc.Keydate)=? {$SQLcondition}";
$result = $db->row($dbQuery, [$year, $cancer]);
foreach ($result as $title => $nums) {
    $tempData['X'][] = $title;
    $tempData['Y'][] = $nums;
}
$response['treatment']['labels'] = $tempData['X'];
$response['treatment']['datasets'][0]['label'] = '人次';
$response['treatment']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 諮詢紀錄統計
$dbQuery = "SELECT COUNT(DISTINCT rm.IDno) AS '醫護紀錄', COUNT(DISTINCT rn.IDno) AS '營養紀錄', COUNT(DISTINCT rs.IDno) AS '社工紀錄', COUNT(DISTINCT rhs.IDno) AS '租借假髮', COUNT(DISTINCT rmt.IDno) AS '心理紀錄', COUNT(DISTINCT ri.IDno) AS '保險紀錄'
            FROM FCF_careservice.Rec_cancer rc
            INNER JOIN (SELECT ? AS ystr FROM DUAL) ye ON true
            INNER JOIN FCF_careservice.Memberdata m ON rc.MemberID=m.IDno
            LEFT JOIN FCF_careservice.Rec_medical rm ON m.IDno=rm.MemberID AND YEAR(rm.Keydate)=ye.ystr
            LEFT JOIN FCF_careservice.Rec_nutrition rn ON m.IDno=rn.MemberID AND YEAR(rn.Keydate)=ye.ystr
            LEFT JOIN FCF_careservice.Rec_social rs ON m.IDno=rs.MemberID AND YEAR(rs.Keydate)=ye.ystr
            LEFT JOIN FCF_careservice.Rec_social rhs ON m.IDno=rhs.MemberID AND rhs.Aim_wig=1 AND YEAR(rs.Keydate)=ye.ystr
            LEFT JOIN FCF_careservice.Rec_mental rmt ON m.IDno=rmt.MemberID AND YEAR(rmt.Keydate)=ye.ystr
            LEFT JOIN FCF_careservice.Rec_insurance ri ON m.IDno=ri.MemberID AND YEAR(ri.Keydate)=ye.ystr
            WHERE 1=1 {$SQLcondition}";
$result = $db->row($dbQuery, [$year, $cancer]);
foreach ($result as $title => $nums) {
    $tempData['X'][] = $title;
    $tempData['Y'][] = $nums;
}
$response['consult']['labels'] = $tempData['X'];
$response['consult']['datasets'][0]['label'] = '筆數';
$response['consult']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 專案統計
$dbQuery = "SELECT (COUNT(DISTINCT rp.IDno)*(CASE WHEN p.C_num>0 THEN p.C_num ELSE 1 END)) AS Yaxis,
            case p.categoryID
            when 30 then '台北營養'
            when 34 then '高雄營養'
            when 14 then '急難救助'
            when 77 then '銀髮南鄉'
            when 75 then '愛飛翔'
            when 95 then '喘息服務'
            when 82 then '胰臟癌補助'
            END AS Xaxis
            FROM FCF_careservice.Rec_projects rp
            INNER JOIN FCF_careservice.Projects p ON rp.ClassID=p.IDno AND YEAR(p.C_startday)=?
            INNER JOIN FCF_careservice.EventCategory e ON e.IDno=p.categoryID
            INNER JOIN FCF_careservice.Memberdata m ON rp.MemberID=m.IDno
            INNER JOIN FCF_careservice.Rec_cancer rc ON m.IDno=rc.MemberID {$SQLcondition}
            WHERE p.categoryID IN (30, 34, 14, 77, 75, 95, 82) GROUP BY p.categoryID";
$result = $db->query($dbQuery, [$year, $cancer]);
foreach ($result as $row) {
    $tempData['X'][] = $row['Xaxis'];
    $tempData['Y'][] = $row['Yaxis'];
}
$response['project']['labels'] = $tempData['X'];
$response['project']['datasets'][0]['label'] = '參加人次';
$response['project']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 北部身心靈課程統計
$dbQuery = "SELECT (COUNT(DISTINCT rcs.IDno)*(CASE WHEN c.C_num>0 THEN c.C_num ELSE 1 END)) AS Yaxis,
            CASE c.categoryID
            WHEN 25 then '氣功養生'
            WHEN 24 then '生活繪本'
            WHEN 23 then '癌友營養'
            WHEN 22 then '園藝治療'
            WHEN 21 then '心靈成長'
            WHEN 20 then '亮彩煥顏'
            WHEN 18 then '音樂治療'
            WHEN 16 then '按摩紓壓'
            WHEN 83 then '芳香療法'
            WHEN 68 then '病友支持團體'
            WHEN 65 then '照顧者陪伴'
            WHEN 40 then '正念減壓'
            WHEN 61 then '禪繞畫'
            WHEN 60 then '彩繪幸福'
            WHEN 63 then '戲劇治療'
            END AS Xaxis
            FROM FCF_careservice.Rec_courses rcs
            INNER JOIN FCF_careservice.MindBodyCourse c ON rcs.ClassID=c.IDno AND YEAR(c.C_startday)=?
            INNER JOIN FCF_careservice.EventCategory e ON e.IDno=c.categoryID
            INNER JOIN FCF_careservice.Memberdata m ON rcs.MemberID=m.IDno
            INNER JOIN FCF_careservice.Rec_cancer rc ON m.IDno=rc.MemberID {$SQLcondition}
            WHERE c.categoryID IN (25,24,23,22,21,20,18,16,83,68,65,40,61,60,63) GROUP BY c.categoryID";
$result = $db->query($dbQuery, [$year, $cancer]);
foreach ($result as $row) {
    $tempData['X'][] = $row['Xaxis'];
    $tempData['Y'][] = $row['Yaxis'];
}
$response['tpcourse']['labels'] = $tempData['X'];
$response['tpcourse']['datasets'][0]['label'] = '參加人次';
$response['tpcourse']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 南部身心靈課程統計
$dbQuery = "SELECT (COUNT(DISTINCT rcs.IDno)*(CASE WHEN c.C_num>0 THEN c.C_num ELSE 1 END)) AS Yaxis,
            case c.categoryID
            WHEN 91 THEN '外部課程'
            WHEN 84 THEN '體適能健身'
            WHEN 76 THEN '芳香療法'
            WHEN 64 THEN '八段錦'
            WHEN 62 THEN '正念減壓'
            WHEN 56 THEN '氣功養身'
            WHEN 52 THEN '音樂治療'
            WHEN 43 THEN '亮采煥顏'
            WHEN 42 THEN '園藝治療'
            WHEN 41 THEN '按摩紓壓'
            WHEN 35 THEN '癌友營養'
            WHEN 32 THEN '新情琉璃畫'
            WHEN 31 THEN '瑜珈健身'
            END AS Xaxis
            FROM FCF_careservice.Rec_courses rcs
            INNER JOIN FCF_careservice.MindBodyCourse c ON rcs.ClassID=c.IDno AND YEAR(c.C_startday)=?
            INNER JOIN FCF_careservice.EventCategory e ON e.IDno=c.categoryID
            INNER JOIN FCF_careservice.Memberdata m ON rcs.MemberID=m.IDno
            INNER JOIN FCF_careservice.Rec_cancer rc ON m.IDno=rc.MemberID {$SQLcondition}
            WHERE c.categoryID IN (91,84,76,64,62,56,52,43,42,41,35,32,31) GROUP BY c.categoryID";
$result = $db->query($dbQuery, [$year, $cancer]);
foreach ($result as $row) {
    $tempData['X'][] = $row['Xaxis'];
    $tempData['Y'][] = $row['Yaxis'];
}
$response['khcourse']['labels'] = $tempData['X'];
$response['khcourse']['datasets'][0]['label'] = '參加人次';
$response['khcourse']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

// 病友團體統計
$dbQuery = "SELECT (COUNT(DISTINCT rcs.IDno)*(CASE WHEN c.C_num>0 THEN c.C_num ELSE 1 END)) AS Yaxis,
            CASE c.categoryID
            WHEN 46 THEN '北部-午茶話療'
            WHEN 45 THEN '北部-笑癌逐開社'
            WHEN 47 THEN '北部-肺腑之癌'
            WHEN 49 THEN '北部-肺腑之癌暨腸相挺'
            WHEN 39 THEN '北部-腸相挺'
            WHEN 36 THEN '南部-午茶話療'
            WHEN 50 THEN '南部-愛care之友'
            END AS Xaxis
            FROM FCF_careservice.Rec_courses rcs
            INNER JOIN FCF_careservice.MindBodyCourse c ON rcs.ClassID=c.IDno AND YEAR(c.C_startday)=?
            INNER JOIN FCF_careservice.EventCategory e ON e.IDno=c.categoryID
            INNER JOIN FCF_careservice.Memberdata m ON rcs.MemberID=m.IDno
            INNER JOIN FCF_careservice.Rec_cancer rc ON m.IDno=rc.MemberID {$SQLcondition}
            WHERE c.categoryID IN (46,45,47,49,39,36,50) GROUP BY c.categoryID";
$result = $db->query($dbQuery, [$year, $cancer]);
foreach ($result as $row) {
    $tempData['X'][] = $row['Xaxis'];
    $tempData['Y'][] = $row['Yaxis'];
}
$response['group']['labels'] = $tempData['X'];
$response['group']['datasets'][0]['label'] = '參加人次';
$response['group']['datasets'][0]['data'] = $tempData['Y'];
$tempData['X'] = $tempData['Y'] = [];

//set Content-Type to JSON
header('Content-Type: application/json; charset=utf-8');
// 清除先前頁面要顯示的快取
ob_end_clean();
//echo error message as JSON
echo json_encode($response, JSON_UNESCAPED_UNICODE);
