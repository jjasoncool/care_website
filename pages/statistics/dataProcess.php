<?php
/**
 * 資料處理層，將php分配到不同頁面處理
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */
require_once 'vendor/autoload.php';
require 'settings/loginCheck.php';
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
$pageAction = reqParam('page', 'post', 'string');
require_once get_relative_path("pages/{$page}/dataDefinition.php");

switch ($pageAction) {
    case 'drawData':
        // 搜尋圖表需要的資料
        require_once get_relative_path("pages/{$page}/getData/drawData.php");
        break;

    case 'otherDrawData':
        // 搜尋圖表需要的資料
        require_once get_relative_path("pages/{$page}/getData/otherDrawData.php");
        break;

    case 'export':
        // 匯出資料表
        require_once get_relative_path("pages/{$page}/getData/export.php");
        break;

    case 'otherExport':
        // 匯出資料表
        require_once get_relative_path("pages/{$page}/getData/otherExport.php");
        break;

    case 'otherExportConsult':
        // 匯出資料表
        require_once get_relative_path("pages/{$page}/getData/otherExportConsult.php");
        break;

    default:
        break;
}

exit();