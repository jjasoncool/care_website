<?php
/**
 * 定義資源清單使用資料庫欄位以及項目
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

$definition = [
    'cases' => [
        'title' => '統計圖表',
    ],
    'other' => [
        'title' => '其他統計',
        'expList' => [
            'title' => '年度訂閱會訊(紙本/電子檔)名單',
            'exportPage' => ['年度訂閱會訊'],
            'colhead' => ['姓名', '寄送方式', '聯絡資訊'],
            'col' => ['C_name', 'method', 'contact'],
        ],
        'expCase' => [
            'title' => '志工/個案生日月份',
            'exportPage' => ['個案' => '個案生日月份', '志工' => '志工生日月份'],
            'colhead' => ['類型', '姓名', '生日月份'],
            'col' => ['memberType', 'C_name', 'BD_mm'],
            'filter' => 'memberType'
        ],
        'expRec' => [
            'title' => '各人員輸入紀錄筆數',
            'exportPage' => ['各人員輸入紀錄筆數'],
            'colhead' => ['紀錄年份', '紀錄類型', '記錄人員', '紀錄筆數'],
            'col' => ['yearStr', 'recType', 'recNum', 'Acc_name'],
        ],
        'expTime' => [
            'title' => '處遇紀錄總覽',
            'exportPage' => [
                '一般諮詢' => '一般諮詢統計',
                '醫護紀錄' => '醫護紀錄統計',
                '營養紀錄' => '營養紀錄統計',
                '社工紀錄' => '社工紀錄統計',
                '心理紀錄' => '心理紀錄統計',
                '保險紀錄' => '保險紀錄統計',
                '志工關懷' => '志工關懷統計',
            ],
            'colhead' => ['紀錄年份', '諮詢方式', '時數(小時)', '紀錄筆數'],
            'col' => ['yearStr', 'recWay', 'recTime', 'recNum'],
            'filter' => 'recType'
        ],
        'expPhone' => [
            'title' => '志工/個案手機號碼總覽',
            'exportPage' => ['個案' => '個案手機號碼', '志工' => '志工手機號碼'],
            'colhead' => ['建檔年份', '類型', '癌別', '姓名', '居住城市', '手機號碼', '手機號碼(額外)'],
            'col' => ['keyYear', 'mType', 'Cancer_name', 'C_name', 'C_city', 'C_mobile1', 'C_mobile2'],
            'filter' => 'mType'
        ],
    ],
    'otherExportConsult' => [
        'title' => '諮詢紀錄細項統計',
    ],
];