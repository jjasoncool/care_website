<?php
/**
 * 統計分為兩類，直接分家去include不同頁面
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 */
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);

// 資料定義
require_once get_relative_path("pages/{$page}/dataDefinition.php");

if (!array_key_exists($subPage, $definition)) {
    header("Location: " . get_relative_path("pages/404.php"));
    exit();
}

// 抓取可以用的年份
$dbQuery = "SELECT YEAR(Keydate) AS yearOpt FROM FCF_careservice.Rec_cancer GROUP BY yearOpt";
$yearData = $db->column($dbQuery);
$yearOption = '';
foreach ($yearData as $key => $year) {
    if ($year == date("Y")) {
        $select = ' selected';
    } else {
        $select = '';
    }
    $yearOption .= "<option value=\"{$year}\"{$select}>{$year} 年</option>";
}

include get_relative_path("pages/{$page}/{$subPage}.php");

