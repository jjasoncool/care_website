<?php
/**
 * 各項案件統計
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 */

?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition[$subPage]['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['subname'][$subPage]?></a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="form-row" id="searchForm">
                                <div class="col">
                                    <div class="select2-input">
                                        <select class="form-control" id="year">
                                            <?=$yearOption?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="select2-input">
                                        <select class="form-control" id="cancer">
                                            <option value="">-- 全癌別 --</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col">
                                    <form class="ml-auto" method="post" target="_blank">
                                        <input type="hidden" name="page">
                                        <input type="hidden" name="cancer" value="">
                                        <button type="button" class="btn btn-outline-info btn-round btn-sm" id="searchData">
                                            <span class="btn-label"><i class="fas fa-search"></i>搜尋資料</span>
                                        </button>
                                        <button type="button" class="btn btn-outline-secondary btn-round btn-sm" id="expData">
                                            <span class="btn-label"><i class="fas fa-download"></i>匯出資料</span>
                                        </button>
                                    </form>
                                </div>
                            </div>
                            <ul class="nav nav-tabs card-header-tabs" id="statisticsTabs" role="tablist" style="margin-bottom: -1rem;">
                                <li class="nav-item">
                                    <a class="nav-link" id="basic-tab" data-toggle="tab" href="#basic" role="tab" aria-controls="basic" aria-selected="true">基本資料</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="disease-tab" data-toggle="tab" href="#disease" role="tab" aria-controls="disease" aria-selected="false">疾病資料</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="service-tab" data-toggle="tab" href="#service" role="tab" aria-controls="service" aria-selected="false">專業服務項目</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content" id="tabs-content">
                            <div class="tab-pane fade" id="basic" role="tabpanel" aria-labelledby="basic-tab">
                                <!-- 圖表 -->
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">來源件數/比例分析</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="sourceAnalysis"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">年齡分布</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="ageAnalysis"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">性別分布</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="sexAnalysis"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">縣市分布</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="cityAnalysis"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">職業分布</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="jobAnalysis"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">福利身分</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="welfareAnalysis"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">教育程度</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="eduAnalysis"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">保險情況</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="insureAnalysis"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">經濟狀況</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="ecoAnalysis"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">慢性疾病史</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="chronicAnalysis"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="disease" role="tabpanel" aria-labelledby="disease-tab">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">癌症期別分析</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="cancerStage"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">診斷期間分析</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="diagnosis"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">基因檢測分析</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="geneTest"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">復發期間分析</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="diseaseRelapse"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">復發區間分析</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="relapseInterval"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-4">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">治療方法分析</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="treatmentMethod"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="service" role="tabpanel" aria-labelledby="service-tab">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">諮詢紀錄統計</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="consultationRec"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">專案統計</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="projects"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">北部身心靈課程統計</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="tpmindBodyCourse"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">南部身心靈課程統計</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="khmindBodyCourse"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="card">
                                                <div class="card-header">
                                                    <div class="card-title">病友團體統計</div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="chart-container">
                                                        <canvas id="patientGroup"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
"use strict";
var params = new URLSearchParams(location.search);
var sel_year = document.getElementById("year").value;
var sel_cancer = document.getElementById("cancer").value;
var tabIndex = location.hash;
var barOptions = {
    responsive: true,
    maintainAspectRatio: false,
    legend: {position : 'bottom'},
    title: {display: true, text: ''},
    tooltips: {mode: 'index', intersect: false},
    scales: {xAxes: [{stacked: true,}], yAxes: [{min: 0, beginAtZero: true, stacked: true}]}
};

// 不同圖表
var objSource, objAge, objSex, objCity, objJob, objWelfare, objEdu, objInsure, objEco, objChronic, objStage, objDiagnosis,
    objGene, objRelapse, objInterval, objTreatment, objConsult, objProjects, objCoursetp, objCoursekh, objPatient;
var sourceAnalysis = document.getElementById('sourceAnalysis').getContext('2d');
var ageAnalysis = document.getElementById('ageAnalysis').getContext('2d');
var sexAnalysis = document.getElementById('sexAnalysis').getContext('2d');
var cityAnalysis = document.getElementById('cityAnalysis').getContext('2d');
var jobAnalysis = document.getElementById('jobAnalysis').getContext('2d');
var welfareAnalysis = document.getElementById('welfareAnalysis').getContext('2d');
var eduAnalysis = document.getElementById('eduAnalysis').getContext('2d');
var insureAnalysis = document.getElementById('insureAnalysis').getContext('2d');
var ecoAnalysis = document.getElementById('ecoAnalysis').getContext('2d');
var chronicAnalysis = document.getElementById('chronicAnalysis').getContext('2d');
var cancerStage = document.getElementById('cancerStage').getContext('2d');
var diagnosis = document.getElementById('diagnosis').getContext('2d');
var geneTest = document.getElementById('geneTest').getContext('2d');
var diseaseRelapse = document.getElementById('diseaseRelapse').getContext('2d');
var relapseInterval = document.getElementById('relapseInterval').getContext('2d');
var treatmentMethod = document.getElementById('treatmentMethod').getContext('2d');
var consultationRec = document.getElementById('consultationRec').getContext('2d');
var projects = document.getElementById('projects').getContext('2d');
var tpmindBodyCourse = document.getElementById('tpmindBodyCourse').getContext('2d');
var khmindBodyCourse = document.getElementById('khmindBodyCourse').getContext('2d');
var patientGroup = document.getElementById('patientGroup').getContext('2d');

function drawData(jsonData) {

    objSource = new Chart(sourceAnalysis, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.basic.datasets[0].backgroundColor = "#1d7af3";
    objSource.data = jsonData.basic;
    objSource.options = barOptions;
    objSource.options.title.text = "來源分布";
    objSource.update();

    objAge = new Chart(ageAnalysis, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.age.datasets[0].backgroundColor = "#FFA229";
    objAge.data = jsonData.age;
    objAge.options = barOptions;
    objAge.options.title.text = "年齡";
    objAge.update();

    objSex = new Chart(sexAnalysis, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.sex.datasets[0].backgroundColor = "#59D05D";
    objSex.data = jsonData.sex;
    objSex.options = barOptions;
    objSex.options.title.text = "性別";
    objSex.update();

    objCity = new Chart(cityAnalysis, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.city.datasets[0].backgroundColor = "#FF2D2D";
    objCity.data = jsonData.city;
    objCity.options = barOptions;
    objCity.options.title.text = "城市";
    objCity.update();

    objJob = new Chart(jobAnalysis, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.job.datasets[0].backgroundColor = "#F75000";
    objJob.data = jsonData.job;
    objJob.options = barOptions;
    objJob.options.title.text = "職業";
    objJob.update();

    objWelfare = new Chart(welfareAnalysis, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.welfare.datasets[0].backgroundColor = "#FFDC35";
    objWelfare.data = jsonData.welfare;
    objWelfare.options = barOptions;
    objWelfare.options.title.text = "福利身分";
    objWelfare.update();

    objEdu = new Chart(eduAnalysis, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.edu.datasets[0].backgroundColor = "#00AEAE";
    objEdu.data = jsonData.edu;
    objEdu.options = barOptions;
    objEdu.options.title.text = "教育程度";
    objEdu.update();

    objInsure = new Chart(insureAnalysis, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.insure.datasets[0].backgroundColor = "#7D7DFF";
    objInsure.data = jsonData.insure;
    objInsure.options = barOptions;
    objInsure.options.title.text = "保險";
    objInsure.update();

    objEco = new Chart(ecoAnalysis, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.eco.datasets[0].backgroundColor = "#FF9797";
    objEco.data = jsonData.eco;
    objEco.options = barOptions;
    objEco.options.title.text = "經濟狀況";
    objEco.update();

    objChronic = new Chart(chronicAnalysis, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.chronic.datasets[0].backgroundColor = "#FF95CA";
    objChronic.data = jsonData.chronic;
    objChronic.options = barOptions;
    objChronic.options.title.text = "慢性疾病";
    objChronic.update();

    objStage = new Chart(cancerStage, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.stage.datasets[0].backgroundColor = "#00BB00";
    objStage.data = jsonData.stage;
    objStage.options = barOptions;
    objStage.options.title.text = "癌症期別";
    objStage.update();

    objDiagnosis = new Chart(diagnosis, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.diagnosis.datasets[0].backgroundColor = "#00E3E3";
    objDiagnosis.data = jsonData.diagnosis;
    objDiagnosis.options = barOptions;
    objDiagnosis.options.title.text = "診斷月份";
    objDiagnosis.update();

    objGene = new Chart(geneTest, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.gene.datasets[0].backgroundColor = "#66B3FF";
    objGene.data = jsonData.gene;
    objGene.options = barOptions;
    objGene.options.title.text = "診斷月份";
    objGene.update();

    objRelapse = new Chart(diseaseRelapse, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.relapse.datasets[0].backgroundColor = "#BE77FF";
    objRelapse.data = jsonData.relapse;
    objRelapse.options = barOptions;
    objRelapse.options.title.text = "復發月份";
    objRelapse.update();

    objInterval = new Chart(relapseInterval, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.interval.datasets[0].backgroundColor = "#003D79";
    objInterval.data = jsonData.interval;
    objInterval.options = barOptions;
    objInterval.options.title.text = "區間";
    objInterval.update();

    objTreatment = new Chart(treatmentMethod, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.treatment.datasets[0].backgroundColor = "#FF2D2D";
    objTreatment.data = jsonData.treatment;
    objTreatment.options = barOptions;
    objTreatment.options.title.text = "方法";
    objTreatment.update();

    objConsult = new Chart(consultationRec, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.consult.datasets[0].backgroundColor = "#BE77FF";
    objConsult.data = jsonData.consult;
    objConsult.options = barOptions;
    objConsult.options.title.text = "諮詢類別";
    objConsult.update();

    objProjects = new Chart(projects, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.project.datasets[0].backgroundColor = "#FF9797";
    objProjects.data = jsonData.project;
    objProjects.options = barOptions;
    objProjects.options.title.text = "專案";
    objProjects.update();

    objCoursetp = new Chart(tpmindBodyCourse, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.tpcourse.datasets[0].backgroundColor = "#FF0080";
    objCoursetp.data = jsonData.tpcourse;
    objCoursetp.options = barOptions;
    objCoursetp.options.title.text = "北部課程";
    objCoursetp.update();

    objCoursekh = new Chart(khmindBodyCourse, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.khcourse.datasets[0].backgroundColor = "#0072E3";
    objCoursekh.data = jsonData.khcourse;
    objCoursekh.options = barOptions;
    objCoursekh.options.title.text = "南部課程";
    objCoursekh.update();

    objPatient = new Chart(patientGroup, {
        type: 'bar',
        data: {},
        options: {}
    });
    jsonData.group.datasets[0].backgroundColor = "#FFAF60";
    objPatient.data = jsonData.group;
    objPatient.options = barOptions;
    objPatient.options.title.text = "病友團體";
    objPatient.update();
}

function getData(year, cancer) {
    params.set("action", "view");
    let returnData = '';
    let getAction = $.ajax({
        url: "./?" + params.toString(),
        method: "POST",
        data: {year: year, cancer: cancer, page: "drawData"},
        dataType: "json"
    }).done(function (data) {
        if (data) {
            returnData = data;
        }
        drawData(returnData);
    });
}

function init() {
    // 癌別
    $.each(cancer, function (i, item) {
        $("#cancer").append($('<option>', {
            value: item,
            text : item
        }));
    });

    // 搜尋項目
    $("#cancer, #year").select2({
        theme: "bootstrap",
        width: "100%"
    });

    // 搜尋
    $("#searchData").on("click", function (e) {
        sel_year = $("#year").val();
        sel_cancer = $("#cancer").val();

        // 更新一次就要銷毀原本的
        objSource.destroy();
        objAge.destroy();
        objSex.destroy();
        objJob.destroy();
        objCity.destroy();
        objWelfare.destroy();
        objEdu.destroy();
        objInsure.destroy();
        objEco.destroy();
        objChronic.destroy();
        objStage.destroy();
        objDiagnosis.destroy();
        objGene.destroy();
        objRelapse.destroy();
        objInterval.destroy();
        objTreatment.destroy();
        objConsult.destroy();
        objProjects.destroy();
        objCoursetp.destroy();
        objCoursekh.destroy();
        objPatient.destroy();
        // 更新資料
        getData(sel_year, sel_cancer);
    })

    // 頁籤
    $("#statisticsTabs a.nav-link").on("show.bs.tab", function (e) {
        location.hash = $(e.target).attr("href");
    });
    // 若有頁籤直接指定頁籤，若沒有則看第一個
    if (tabIndex !== '') {
        $("#statisticsTabs a.nav-link[href='" + tabIndex + "']").tab("show");
    } else {
        $("#statisticsTabs a.nav-link").eq(0).tab("show");
    }

    // 匯出
    $("#expData").on("click", function () {
        let year = $("#year").val()
        let cancer = $("#cancer").val()
        params.set("year", year);
        params.set("action", "download");
        $(this).closest("form").attr("action", "./?" + params.toString());
        $(this).siblings("[name='page']").val("export");
        $(this).siblings("[name='cancer']").val(cancer);
        $(this).closest("form").submit();
    });

    getData(sel_year, sel_cancer);
}

window.onload = init;
</script>