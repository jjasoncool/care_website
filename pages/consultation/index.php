<?php
/**
 * 新增/以及編輯資料使用表單
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 */
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);

require_once get_relative_path("pages/{$page}/dataDefinition.php");

if (!array_key_exists($subPage, $definition)) {
    header("Location: " . get_relative_path("pages/404.php"));
    exit();
}

?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition[$subPage]['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['subname'][$subPage]?></a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title"><?=$listItems[$page]['subname'][$subPage]?>諮詢</h4>
                                <div class="ml-auto">
                                    <button type="button" class="btn btn-outline-success btn-sm mx-2" data-toggle="modal" data-target="#editModal" data-act="add">
                                        <span class="btn-label"><i class="fas fa-plus"></i>新增</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="table-responsive">
                                        <table id="tableHourList" class="display table table-striped table-hover" >
                                            <thead>
                                                <tr>
                                                    <?php
                                                    foreach ($definition[$subPage]['colhead'] as $head) {
                                                        echo "<th>{$head}</th>";
                                                    }
                                                    echo '<th>編輯</th>';
                                                    ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $dbQuery = "SELECT * FROM FCF_careservice.Anonymous WHERE 1=1 AND C_from={$definition[$subPage]['dataType']} AND del=0";
                                            $result = $db->query($dbQuery);
                                            foreach ($result as $row) {
                                                $columnContent = '';
                                                $rowdata = json_encode($row, JSON_UNESCAPED_UNICODE);
                                                foreach ($definition[$subPage]['col'] as $colname) {
                                                    $link = '';
                                                    $linkEnd = '';
                                                    if (
                                                        isset($definition[$subPage]['colLink'][$colname])
                                                        && $definition[$subPage]['colLink'][$colname]
                                                    ) {
                                                        $link = "<a href=\"./?" . http_build_query($_GET) . "&action=view&id={$row['IDno']}\">";
                                                        $linkEnd = '</a>';
                                                    }
                                                    // 性別
                                                    if ($colname == 'C_sex') {
                                                        $row[$colname] = ($row[$colname] == 'male' ? '男' : '女');
                                                    }
                                                    $columnContent .= "<td>{$link}{$row[$colname]}{$linkEnd}</td>";
                                                }
                                                $columnContent = nl2br($columnContent);
                                                $modBtn = "<button type=\"button\" title=\"修改\" class=\"btn btn-link btn-primary py-0 px-2\"
                                                                data-toggle=\"modal\" data-target=\"#editModal\" data-act=\"mod\">
                                                                <i class=\"fa fa-edit fa-lg\"></i>
                                                            </button>";
                                                $delBtn = "<button type=\"button\" title=\"刪除\" class=\"btn btn-link btn-danger py-0 pl-2\"
                                                            data-toggle=\"modal\" data-target=\"#delModal\">
                                                                <i class=\"fa fa-times fa-lg\"></i>
                                                            </button>";
                                                // 管理者才可以刪除類別
                                                if ($generalData['userLevel'] > 1) {
                                                    $delBtn = '';
                                                }
                                                if ($generalData['userLevel'] > 2) {
                                                    $modBtn = '';
                                                }
                                                echo
                                                "<tr>
                                                    {$columnContent}
                                                    <td style=\"width: 10%\">
                                                        <div class=\"form-button-action\">
                                                            <input type=\"hidden\" class=\"rowdata\" value='{$rowdata}'>
                                                            {$modBtn}
                                                            {$delBtn}
                                                        </div>
                                                    </td>
                                                </tr>";
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">諮詢 (<?=$definition[$subPage]['title']?>)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="./?<?=http_build_query($_GET)?>&action=dataForm">
                <div class="modal-body">
                    <input type="hidden" name="dataID">
                    <input type="hidden" name="action">
                    <input type="hidden" name="page" value="anonymous">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label><b>姓名/暱稱</b></label><br>
                            <input type="text" class="form-control form-control-sm" name="C_name" field="C_name" required>
                        </div>
                        <div class="col-md-6">
                            <label><b>性別</b></label><br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="C_sex" value="male" field="C_sex" checked>
                                <label class="form-check-label">男</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="C_sex" value="female" field="C_sex">
                                <label class="form-check-label">女</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label><b>聯絡電話</b></label><br>
                            <input type="text" class="form-control form-control-sm" name="C_tele" field="C_tele" <?=($subPage == 'netChat' ? '' : 'required')?>>
                        </div>
                        <div class="col-md-6">
                            <label><b>網路資訊/email</b></label><br>
                            <input type="text" class="form-control form-control-sm" name="C_mail" field="C_mail" <?=($subPage == 'netChat' ? 'required' : '')?>>
                        </div>
                    </div>
                </div>
                <div class="modal-footer no-bd">
                    <button type="submit" class="btn btn-primary"></button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">刪除諮詢 (<?=$definition[$subPage]['title']?>)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="./?<?=http_build_query($_GET)?>&action=dataForm">
                <div class="modal-body">
                    <input type="hidden" name="dataID">
                    <input type="hidden" name="action" value="del">
                    <input type="hidden" name="page" value="anonymous">
                    <h6>請問確定要<span class="text-danger"><b>刪除</b></span>此資料嗎?</h6>
                    <b><p class="my-0"></p></b>
                </div>
                <div class="modal-footer no-bd">
                    <button type="submit" class="btn btn-danger">確認刪除</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
function init() {
    $('#tableHourList').DataTable({
        lengthChange: false,
        order: [[ 0, "desc" ]],
        columnDefs: [{
            targets: [-1],
            orderable: false,
        }],
    });

    $("#editModal").on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        // Extract info from data-* attributes
        let action = button.data('act');
        let titleText = "";
        // 這個modal
        let modal = $(this);
        if (action == 'mod') {
            titleText = "修改";
            let rowdata = JSON.parse(button.siblings(".rowdata").val());
            let fields = modal.find("[field]");
            // 填欄位
            $.each(fields, function(index, fieldNode) {
                let field = $(fieldNode).attr("field");
                let inputType = $(fieldNode).attr("type") || $(fieldNode).prop("tagName");
                if (inputType != undefined) {
                    inputType = inputType.toLowerCase();
                }
                // 依據不同的 input 類型，做不同的塞值動作(單個欄位對應單筆資料)
                if (inputType == "text" || inputType == "textarea" || inputType == "number" || inputType == "hidden") {
                    $(fieldNode).val(rowdata[field]);
                } else if (inputType == "checkbox" && Boolean(rowdata[field])) {
                    $(fieldNode).prop("checked", true);
                } else if (inputType == "radio") {
                    $.each($(fieldNode), function(i) {
                        if ($(this).val() == rowdata[field]) {
                            $(this).prop("checked", true);
                        }
                    });
                } else if (inputType == "select") {
                    $(fieldNode).val(rowdata[field]).trigger("change");
                }
            });

            modal.find("input[name='dataID']").val(rowdata["IDno"]);
        } else {
            titleText = "新增";
        }

        modal.find("[name='action']").val(action);
        modal.find(".modal-title").prepend(titleText);
        modal.find("button[type='submit']").text("確認" + titleText);
    });

    $("#editModal").on('hidden.bs.modal', function (event) {
        let modal = $(this);
        let title = modal.find(".modal-title").text().slice(2);
        modal.find(".modal-title").text(title);
    });

    $("#delModal").on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        // Extract info from data-* attributes
        let titleText = "";
        let date = "";
        // 這個modal
        let modal = $(this);
        let rowdata = JSON.parse(button.siblings(".rowdata").val());

        modal.find("input[name='dataID']").val(rowdata["IDno"]);
        modal.find(".modal-body p").text(rowdata["C_name"]);
    });

    $("#delModal").on('hidden.bs.modal', function (event) {
        let modal = $(this);
        modal.find(".modal-body p").text('');
    });
}

window.onload = init;
</script>