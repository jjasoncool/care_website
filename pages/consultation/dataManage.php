<?php
/**
 * 新增/編輯基本資料 (含疾病史)
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param array $inputData 使用者輸入的資料
 * @param array $basicData 接受的欄位資料
 * @param string $subPage 子類別
 * @param int $dataID 基本資料ID
 */

$dataID = 0;
$dataID = intval(reqParam('dataID', 'post'));
$formAction = strval(reqParam('action', 'post'));
unset($_GET['action']);

$date = new DateTime();
$inputData = [];
$basicData = [
    'C_name' => 'string',
    'C_sex' => 'string',
    'C_tele' => 'string',
    'C_mail' => 'string'
];
// 基本資料
foreach ($basicData as $inputName => $inputType) {
    $inputData[$inputName] = reqParam($inputName, 'post', $inputType);
}

$dataTable = 'Anonymous';
$recAnonym = [
    "Keydate" => $date->format('Y-m-d H:i:s'),
    'AdminID' => $generalData['userid'],
    'AdName' => $generalData['username'],
    'del' => intval(boolval($formAction === 'del')),
    'C_from' => $definition[$subPage]['dataType']
];

// 判斷資料類型
if ($formAction === 'add' && empty($dataID)) {
    $insColArray = array_merge($recAnonym, $inputData);
    $insColStr = implode('`,`', array_keys($insColArray));
    $insParaStr = implode(",", array_fill(0, count($insColArray), "?"));
    // SQL 用基本資料產出
    $dbQuery = "INSERT INTO FCF_careservice.{$dataTable} (
        `$insColStr`
    ) VALUES (
        $insParaStr
    )";
    $db->query($dbQuery, array_values($insColArray));
    $_GET['id'] = $db->lastInsertId();
    $_GET['action'] = 'view';
} elseif ($formAction === 'mod' && !empty($dataID)) {
    // 類別不可改
    unset($recAnonym['C_from']);
    $updColArray = array_merge($recAnonym, $inputData);
    $updColStr = '';
    foreach ($updColArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    $dbQuery = "UPDATE FCF_careservice.{$dataTable}
                SET {$updColStr}
                WHERE IDno=?";
    $db->query($dbQuery, array_merge(array_values($updColArray), [$dataID]));
} elseif ($formAction === 'del' && !empty($dataID)) {
    // 刪除，取消此個案與此紀錄之關聯
    $dbQuery = "UPDATE FCF_careservice.{$dataTable}
                SET del={$recAnonym['del']}
                WHERE IDno=?";
    $db->query($dbQuery, [$dataID]);
}

toViewPage();

function toViewPage()
{
    header("Location:?" . http_build_query($_GET));
    exit();
}
