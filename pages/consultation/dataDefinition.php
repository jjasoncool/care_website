<?php
/**
 * 定義資源清單使用資料庫欄位以及項目
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

$definition = [
    'telChat' => [
        'title' => '電話諮詢',
        'colhead' => ['名稱','性別','電話','email'],
        'col' => ['C_name', 'C_sex', 'C_tele', 'C_mail'],
        'colLink' => ['C_name' => true],
        'dataType' => 1
    ],
    'netChat' => [
        'title' => '網路/email諮詢',
        'colhead' => ['名稱','性別','電話','網路資訊/email'],
        'col' => ['C_name', 'C_sex', 'C_tele', 'C_mail'],
        'colLink' => ['C_name' => true],
        'dataType' => 2
    ],
    'appChat' => [
        'title' => 'APP 諮詢',
        'colhead' => ['名稱','性別','電話','email'],
        'col' => ['C_name', 'C_sex', 'C_tele', 'C_mail'],
        'colLink' => ['C_name' => true],
        'dataType' => 3
    ],
];