<?php
/**
 * 查看基本資料與諮詢紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 * @param int $id 個案編號
 */

require_once get_relative_path("pages/{$page}/dataDefinition.php");
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);

if (!array_key_exists($subPage, $definition)) {
    header("Location: " . get_relative_path("pages/404.php"));
    exit();
}
// 如果這邊有帶入ID 必須先撈資料
$id = intval(reqParam('id', 'get'));
// 撈取個案病例資料
$dbQuery = "SELECT * FROM FCF_careservice.Anonymous m
            WHERE m.IDno = ? AND m.C_from={$definition[$subPage]['dataType']}";
$row = $db->row($dbQuery, array($id));

// 家屬資料不可以直接撈個案，不可以用ID直接查詢
if ($row == false) {
    echo '<div class="main-panel">
            <div class="content">
                <div class="page-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-center">查無資料</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
    exit();
}

?>

<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition[$subPage]['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="?<?="page={$page}&sub={$subPage}"?>"><?=$listItems[$page]['subname'][$subPage]?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">詳細資料</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <form class="needs-validation" method="post">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    <b>基本資料</b>
                                </div>
                            </div>
                            <div class="card-body py-0">
                                <table class="table">
                                    <?php

                                        $sex = $row['C_sex'];
                                        // 個案基本資料
                                        $basicData = [
                                            '姓名' => $row['C_name'],
                                            '性別' => ($sex == 'male' ? '男' : '女'),
                                            '電話' => $row['C_tele'],
                                            'Email' => $row['C_mail'],
                                        ];

                                        foreach ($basicData as $key => $value) {
                                            // 沒資料就不顯示了
                                            if (!empty($value)) {
                                                echo "<tr>
                                                <th scope=\"row\">{$key}</th>
                                                <td>{$value}</td>
                                                </tr>";
                                            }
                                        }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title"><b>諮詢紀錄</b></div>
                        </div>
                        <div class="card-body">
                            <?php
                            $consultArray = [
                                'med'       => ['name' => '醫護紀錄',   'table' => 'Rec_medical',   'modify' => true],
                                'nutrition' => ['name' => '營養紀錄',   'table' => 'Rec_nutrition', 'modify' => true],
                                'social'    => ['name' => '社工紀錄',   'table' => 'Rec_social',    'modify' => true],
                                'mental'    => ['name' => '心理紀錄',   'table' => 'Rec_mental',    'modify' => true],
                                'insurance' => ['name' => '保險紀錄',   'table' => 'Rec_insurance', 'modify' => true],
                                'care'      => ['name' => '志工關懷',   'table' => 'Rec_care',      'modify' => true],
                                'general'   => ['name' => '一般諮詢',   'table' => 'Rec_ask',       'modify' => true],
                            ];

                            $showColumn = [
                                'med'       => ['colhead' => ['日期', '時間', '服務方式', '地點', '紀錄'], 'col' => ['R_date', 'R_time', 'R_way', 'R_loc', 'AdName']],
                                'nutrition' => ['colhead' => ['日期', '時間', '服務方式', '地點','身高', '體重', 'BMI', '紀錄'], 'col' => ['R_date', 'R_time', 'R_way', 'R_loc', 'R_height', 'R_weight', 'R_BMI', 'AdName']],
                                'social'    => ['colhead' => ['日期', '時間', '服務方式', '地點', '紀錄'], 'col' => ['R_date', 'R_time', 'R_way', 'R_loc', 'AdName']],
                                'mental'    => ['colhead' => ['日期', '時間', '服務方式', '地點', '紀錄'], 'col' => ['R_date', 'R_time', 'R_way', 'R_loc', 'AdName']],
                                'insurance' => ['colhead' => ['日期', '時間', '服務方式', '地點', '紀錄'], 'col' => ['R_date', 'R_time', 'R_way', 'R_loc', 'AdName']],
                                'care'      => ['colhead' => ['日期', '時間', '服務方式', '地點', '紀錄'], 'col' => ['R_date', 'R_time', 'R_way', 'R_loc', 'Contactpeop']],
                                'general'   => ['colhead' => ['日期', '時間', '服務方式', '地點', '紀錄'], 'col' => ['R_date', 'R_time', 'R_way', 'R_loc', 'AdName']],
                            ];

                            // 權限控管
                            if ($generalData['userLevel'] == 4) {
                                $consultArray = array_intersect_key($consultArray, array('insurance' => ''));
                            } elseif ($generalData['userLevel'] == 3) {
                                $consultArray = array_intersect_key($consultArray, array('care' => ''));
                            }

                            // 諮詢紀錄類型
                            $recTypeArray = array_keys($consultArray);

                            // 諮詢紀錄內容
                            $countentArray = [];
                            foreach ($consultArray as $key => $detail) {
                                $columnHead = '';
                                $rowContent = '';
                                $dbQuery = "SELECT m.C_name, d.* FROM FCF_careservice.{$detail['table']} d
                                            INNER JOIN FCF_careservice.Anonymous m ON d.MemberID=m.IDno
                                            WHERE d.MemberID=? AND d.anonym=1";
                                $result = $db->query($dbQuery, array($id));

                                // 表格標頭
                                foreach ($showColumn[$key]['colhead'] as $str) {
                                    $columnHead .= "<th>{$str}</th>";
                                }
                                // 輸出每一列
                                foreach ($result as $row) {
                                    $columnContent = '';
                                    $rowdata = json_encode($row, JSON_UNESCAPED_UNICODE);
                                    foreach ($showColumn[$key]['col'] as $colname) {
                                        if ($colname === 'R_time') {
                                            $row[$colname] = $row[$colname] . '~' . ($row[$colname]+10) . '分鐘';
                                        }
                                        $columnContent .= "<td>{$row[$colname]}</td>";
                                    }
                                    // 參加課程不可以修改課程資料，僅能刪除
                                    if ($detail['modify']) {
                                        $modBtn = "<button type=\"button\" title=\"修改\" class=\"btn btn-link btn-primary py-0 px-2\"
                                                        data-toggle=\"modal\" data-target=\"#data_{$key}\" data-act=\"mod\">
                                                        <i class=\"fa fa-edit fa-lg\"></i>
                                                    </button>";
                                    } else {
                                        $modBtn = '';
                                    }
                                    $rowContent .= "<tr>
                                                        {$columnContent}
                                                        <td style=\"width: 15%\">
                                                            <div class=\"form-button-action\">
                                                                <input type=\"hidden\" class=\"dataID\" value=\"{$row['IDno']}\">
                                                                <input type=\"hidden\" class=\"rowdata\" value='{$rowdata}'>
                                                                <div class=\"print d-none\"></div>
                                                                <button type=\"button\" title=\"列印\" class=\"btn btn-link btn-primary py-0 px-2 printBtn\" data-type=\"$key\">
                                                                    <i class=\"fas fa-print fa-lg\"></i>
                                                                </button>
                                                                {$modBtn}
                                                                <button type=\"button\" title=\"刪除\" class=\"btn btn-link btn-danger py-0 px-2\"
                                                                    data-toggle=\"modal\" data-target=\"#del_{$key}\" data-act=\"del\" data-type=\"{$detail['name']}\">
                                                                    <i class=\"fa fa-times fa-lg\"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>";
                                }


                                $countentArray[$key] = "
                                    <div class=\"table-responsive\">
                                        <table id=\"{$key}_table\" class=\"display table table-striped table-hover\">
                                            <thead>
                                                <tr>
                                                    {$columnHead}
                                                    <th style=\"padding-right:0px !important;\">
                                                        <button class=\"btn btn-primary btn-sm float-right mx-0\"
                                                            data-toggle=\"modal\" data-target=\"#data_{$key}\" data-act=\"add\">
                                                            <span class=\"btn-label\">
                                                                <i class=\"fa fa-plus\"></i>
                                                            </span>
                                                            新增紀錄
                                                        </button>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {$rowContent}
                                            </tbody>
                                        </table>
                                    </div>";
                            }

                            // 輸出諮詢紀錄
                            $navItem = '';
                            $tabContent = '';
                            $count = 0;
                            foreach ($consultArray as $key => $detail) {
                                $navItem .= "<li class=\"nav-item\">
                                                <a class=\"nav-link\" id=\"{$key}-tab\" data-toggle=\"pill\" href=\"#{$key}\" role=\"tab\" aria-controls=\"{$key}\">{$detail['name']}</a>
                                            </li>";
                                $tabContent .= "<div class=\"tab-pane fade\" id=\"{$key}\" role=\"tabpanel\" aria-labelledby=\"{$key}-tab\">
                                                    {$countentArray[$key]}
                                                </div>";
                            }
                            ?>
                            <ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
                                <?=$navItem?>
                            </ul>
                            <div class="tab-content mt-2 mb-3" id="pills-tabContent">
                                <?=$tabContent?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require 'viewModal.php'; ?>

<script>
    "use strict";
    var id = <?=$id?>;
    var params = new URLSearchParams(location.search);
    var sex = "<?=$sex?>";
    var tabIndex = location.hash;

    // 確認所有勾選欄位都有勾選
    function checkboxStatus(node) {
        let status = 0;
        let items = $(node).find("input:checkbox");
        $.each(items, function(i, v) {
            if ($(v).prop("checked")) {
                status = 1;
            }
        });
        return status;
    }

    function init() {
        $("#modData").on("click", function () {
            params.set("id", id);
            params.set("edit", true);
            params.set("action", "dataForm");
            var path = location.protocol + '//' + location.host + location.pathname + '?' + params.toString();
            location.href = path;
        });

        // 頁籤
        $("#pills-tab a.nav-link").on("show.bs.tab", function (e) {
            location.hash = $(e.target).attr("href");
        });
        // 若有頁籤直接指定頁籤，若沒有則看第一個
        if (tabIndex !== '') {
            $("#pills-tab a.nav-link[href='" + tabIndex + "']").tab("show");
        } else {
            $("#pills-tab a.nav-link").eq(0).tab("show");
        }

        $('.table-responsive > table').DataTable({
            pageLength: 10,
            lengthChange: false,
            searching: false,
            order: [[ 0, "desc" ]],
            autoWidth: false,
            columnDefs: [{
                targets: [-1],
                orderable: false,
            }]
        });

        $("[field='ClassID'], [field='refID']").select2({
            theme: "bootstrap",
            width: '100%'
        });

        if (params.get("dulplicate") == true) {
            swal ("資料重複" ,  "該身分證字號已存在，請直接編輯舊有資料", "error", {
                button: "我知道了"
            });
        }

        $(".printBtn").on("click", function() {
            let rowdata = $(this).siblings(".rowdata").val();
            let form = $(this).siblings("div .print");
            let virtualForm = $("<form target=\"_blank\" method=\"POST\"></form>");
            virtualForm.attr("action", "?" + params.toString());
            virtualForm.append("<input type=\"hidden\" name=\"page\" value=\"printPDF\">");
            virtualForm.append("<input type=\"hidden\" name=\"rowdata\">");
            virtualForm.append("<input type=\"hidden\" name=\"recType\" value=\"" + $(this).data("type") + "\">");
            virtualForm.find("[name='rowdata']").val(encodeURI(rowdata));
            virtualForm.appendTo(form).submit();
            form.html("");
        });

        // 以下為 Modal 使用之 script
        let recArray = ["<?= implode("\",\"", $recTypeArray) ?>"];
        let dateIDStr = "";
        let dataModalStr = "";
        let delModalStr = "";
        // 取得每個ID
        $.each(recArray, function (i, v) {
            dateIDStr += "#R_date_" + v + ",";
            dataModalStr += "#data_" + v + ",";
            delModalStr += "#del_" + v + ",";
        });
        dateIDStr = dateIDStr.slice(0,-1);
        dataModalStr = dataModalStr.slice(0,-1);
        delModalStr = delModalStr.slice(0,-1);

        $(dateIDStr).datetimepicker({
            format: 'YYYY-MM-DD',
            allowInputToggle: true,
            useStrict: true,
            debug: false
        });

        $(dataModalStr).on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            // Extract info from data-* attributes
            let action = button.data('act');
            let titleText = "";
            let rowdata = "";
            // 這個modal
            let modal = $(this);
            let recType = modal.attr("id").replace("data_", "");

            if (action == 'add') {
                titleText = "新增";
                $("[data-target='#R_date_" + recType + "']").val(moment().format("YYYY-MM-DD"));
            } else if (action == 'mod') {
                titleText = "修改";
                rowdata = JSON.parse(button.siblings(".rowdata").val());
                let dataID = button.siblings(".dataID").val();
                let fields = modal.find("[field]");
                // 填欄位
                $.each(fields, function(index, fieldNode) {
                    let field = $(fieldNode).attr("field");
                    let inputType = $(fieldNode).attr("type") || $(fieldNode).prop("tagName");
                    if (inputType != undefined) {
                        inputType = inputType.toLowerCase();
                    }
                    // 依據不同的 input 類型，做不同的塞值動作(單個欄位對應單筆資料)
                    if (inputType == "text" || inputType == "textarea" || inputType == "number" || inputType == "hidden") {
                        $(fieldNode).val(rowdata[field]);
                    } else if (inputType == "checkbox" && Boolean(rowdata[field])) {
                        $(fieldNode).prop("checked", true);
                    } else if (inputType == "select") {
                        $(fieldNode).val(rowdata[field]).trigger("change");
                    }
                });
                // 更新資料ID
                modal.find("[name='dataID']").val(dataID);
            }
            // 勾選子項目，應該勾選hidden母項目，取消勾選亦同
            let optionBlock = modal.find(".form-group.row");
            $.each(optionBlock, function (index, node) {
                if ($(node).find("input:checkbox").length > 0) {
                    let allcheckboxs = $(node).find("input:checkbox");
                    let flag = checkboxStatus(node);
                    $.each(allcheckboxs, function(i, item) {
                        $(item).on("change", function() {
                            $(node).find("[type='hidden']").val(checkboxStatus(node));
                        });
                    });
                    $(node).find("[type='hidden']").val(flag);
                }
            });

            // 營養紀錄
            if (recType === "nutrition") {
                let BEE = 0;
                let cal = 0;
                let protein = 0;
                let height = Number(modal.find("input[name='R_height']").val());
                let weight = Number(modal.find("input[name='R_weight']").val());
                let pressure = Number(modal.find("select[name='Cal_pressure']").val());
                let actFactor = Number(modal.find("select[name='Cal_activity']").val());
                let proteinFactor = Number(modal.find("select[name='Protein_factor']").val());
                modal.find("input[name='R_height'], input[name='R_weight']").on("keyup", function () {
                    height = Number(modal.find("input[name='R_height']").val());
                    weight = Number(modal.find("input[name='R_weight']").val());
                    if (height > 0 && weight > 0) {
                        let BMI = (weight/Math.pow(height/100, 2)).toFixed(1);
                        let analysis = '---';
                        modal.find("input[name='R_BMI']").val(BMI);
                        // BMI 分析
                        switch (true) {
                            case BMI > 35:
                                analysis = '重度肥胖';
                                break;
                            case BMI > 30 && BMI <= 35:
                                analysis = '中度肥胖';
                                break;
                            case BMI >= 27 && BMI <= 30:
                                analysis = '輕度肥胖';
                                break;
                            case BMI >= 24 && BMI < 27:
                                analysis = '過重';
                                break;
                            case BMI >= 18.5 && BMI < 24:
                                analysis = '正常';
                                break;
                            case BMI < 18.5:
                                analysis = '過輕';
                                break;
                            default:
                                break;
                        }
                        modal.find("input[name='R_analysis']").val(analysis);

                        // 基本能量消耗
                        if (age > 0) {
                            if (sex === 'male') {
                                BEE = Number((66 + 13.7 * weight + 5 * height - 6.8 * age).toFixed(1));
                            } else if (sex === 'female') {
                                BEE = Number((655 + 9.6 * weight + 1.7 * height - 4.7 * age).toFixed(1));
                            }
                        } else {
                            BEE = '個案沒有年齡參數';
                        }

                        // 蛋白質需求，只需要看體重
                        protein = Number((weight * proteinFactor).toFixed(1));
                        modal.find("input[name='Protein']").val(protein);
                    } else {
                        // 沒有身高體重，沒辦法算其他因子
                        modal.find("input[name='R_BMI']").val(0);
                        modal.find("input[name='R_analysis']").val('---');
                        BEE = '---';
                        if (!(weight > 0)) {
                            modal.find("input[name='Protein']").val('');
                        } else {
                            protein = Number((weight * proteinFactor).toFixed(1));
                            modal.find("input[name='Protein']").val(protein);
                        }
                    }

                    // 基本能量
                    modal.find("input.BEE").val(BEE);
                    // 計算熱量需求
                    if (typeof BEE !== 'string' && BEE > 0) {
                        cal = (BEE * pressure * actFactor).toFixed(1);
                        modal.find("input[name='Caloric']").val(cal);
                    } else {
                        modal.find("input[name='Caloric']").val('');
                    }
                });

                // 熱量因子改變
                modal.find("select[name='Cal_pressure'], select[name='Cal_activity']").on("change", function () {
                    if (typeof BEE !== 'string' && BEE > 0) {
                        pressure = Number(modal.find("select[name='Cal_pressure']").val());
                        actFactor = Number(modal.find("select[name='Cal_activity']").val());
                        cal = (BEE * pressure * actFactor).toFixed(1);
                        modal.find("input[name='Caloric']").val(cal);
                    } else {
                        modal.find("input[name='Caloric']").val('');
                    }
                });

                // 熱量因子改變
                modal.find("select[name='Protein_factor']").on("change", function () {
                    if (weight > 0) {
                        proteinFactor = Number(modal.find("select[name='Protein_factor']").val());
                        protein = Number((weight * proteinFactor).toFixed(1));
                        modal.find("input[name='Protein']").val(protein);
                    }
                });

                // 開啟原有資料更新數據
                modal.find("input[name='R_weight']").trigger("keyup");
            }

            modal.find("[name='recType']").val(recType);
            modal.find("[name='action']").val(action);
            modal.find(".modal-title").text(titleText + '紀錄');
            modal.find("button[type='submit']").text("確定" + titleText);
        });

        $(delModalStr).on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            // Extract info from data-* attributes
            let action = button.data('act');
            let type = button.data('type');
            let titleText = "";
            let date = "";
            let dataID = button.siblings(".dataID").val();
            // 這個modal
            let modal = $(this);
            let recType = modal.attr("id").replace("del_", "");

            if (action == "del") {
                titleText = "刪除";
                date = button.parents("td").siblings("td:first").text();
            }

            modal.find("[name='dataID']").val(dataID);
            modal.find("[name='recType']").val(recType);
            modal.find("[name='action']").val(action);
            modal.find(".modal-title").text(titleText + type);
            modal.find(".modal-body span").text('請問確定要刪除此資料嗎? (日期:' + date + ')');
            modal.find("button[type='submit']").text("確定" + titleText);
        });

        $(dataModalStr).on('hidden.bs.modal', function (event) {
            let modal = $(this);
            let dropzone = modal.find("div.dropzoneArea");
            modal.find("form").get(0).reset();
            // 如果上傳檔案區塊存在，需要刪除
            if (dropzone.length>0) {
                modal.find("div.dropzoneArea").get(0).dropzone.removeAllFiles(true);
                modal.find("div.dropzoneArea").get(0).dropzone.destroy();
            }
        });
    }

    window.onload = init;
</script>