<?php
/**
 * 列印諮詢紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

// 清除先前頁面要顯示的快取
ob_end_clean();

require_once 'printDefinition.php';

// 準備資料
$rowdata = json_decode(urldecode(reqParam('rowdata', 'post')));
$recType = reqParam('recType', 'post');

/**
 * 將勾選項目轉換成文字輸出
 *
 * @param object $rowdata
 * @param array $categoryArray 該諮詢紀錄定義的陣列
 * @param string $defType 陣列中的勾選類型
 * @return void
 */
function findCheck($rowdata, $categoryArray, $defType)
{
    $returnText = '';
    $title = $categoryArray[$defType]['categoryTitle'];
    unset($categoryArray[$defType]['categoryTitle']);
    // 找尋勾選項目
    foreach ($categoryArray[$defType] as $col => $label) {
        if ($rowdata->$col == 1) {
            $returnText .= "{$label},";
        }
    }
    if (!empty($returnText)) {
        $returnText = '<tr><td>' . $title . '</td><td colspan="3">' . substr($returnText, 0, -1) . '</td></tr>';
    }

    return $returnText;
}

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF
{

    //Page header
    public function Header()
    {
        // Logo
        $image_file = "{$_SERVER['DOCUMENT_ROOT']}uploads/images/logo.png";
        $this->Image($image_file, '', '', 50, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }

}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('skjan');
$pdf->SetTitle('諮詢紀錄列印');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once dirname(__FILE__) . '/lang/eng.php';
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('taipeisans', 'B', 16);

// add a page
$pdf->AddPage();

$pdf->Write(0, $printDef[$recType]['name'], '', 0, 'C', true, 0, false, false, 0);
$pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);

// 表格字體
$pdf->SetFont('taipeisans', '', 12);

// -----------------------------------------------------------------------------
$serviceTime = "{$rowdata->R_time}~" . ($rowdata->R_time + 10) . ' 分鐘';
switch ($recType) {
    case 'med':
        $Medical = findCheck($rowdata, $printDef[$recType], 'Medical');
        $Nurse = findCheck($rowdata, $printDef[$recType], 'Nurse');
        $Rights = findCheck($rowdata, $printDef[$recType], 'Rights');
        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>編號</td><td>{$rowdata->MemberID}</td><td>姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>紀錄人員</td><td>{$rowdata->AdName}</td><td>紀錄日期</td><td>{$rowdata->R_date}</td></tr>
            <tr><td>服務方式</td><td>{$service[$rowdata->R_way]}</td><td>地點</td><td>{$rowdata->R_loc}</td></tr>
            <tr><td>服務時間</td><td>{$serviceTime}</td><td colspan="2"></td></tr>
            <tr><td align="center" colspan="4">紀錄內容</td></tr>
            {$Medical}
            {$Nurse}
            {$Rights}
            <tr><td colspan="4">{$rowdata->R_note}</td></tr>
        </table>
        EOD;
        break;

    case 'nutrition':
        $Aim = findCheck($rowdata, $printDef[$recType], 'Aim');
        $Provide = findCheck($rowdata, $printDef[$recType], 'Provide');
        $Surgery = findCheck($rowdata, $printDef[$recType], 'Surgery');
        $Cure = findCheck($rowdata, $printDef[$recType], 'Cure');
        $Effect = findCheck($rowdata, $printDef[$recType], 'Effect');
        $Surgery = findCheck($rowdata, $printDef[$recType], 'Surgery');
        $Cure = findCheck($rowdata, $printDef[$recType], 'Cure');
        $Effect = findCheck($rowdata, $printDef[$recType], 'Effect');
        $Diet = findCheck($rowdata, $printDef[$recType], 'Diet');
        $Nutrition = findCheck($rowdata, $printDef[$recType], 'Nutrition');
        $Eat = findCheck($rowdata, $printDef[$recType], 'Eat');
        $Special = findCheck($rowdata, $printDef[$recType], 'Special');
        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>編號</td><td>{$rowdata->MemberID}</td><td>姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>紀錄人員</td><td>{$rowdata->AdName}</td><td>紀錄日期</td><td>{$rowdata->R_date}</td></tr>
            <tr><td>服務方式</td><td>{$service[$rowdata->R_way]}</td><td>地點</td><td>{$rowdata->R_loc}</td></tr>
            <tr><td>服務時間</td><td>{$serviceTime}</td><td colspan="2"></td></tr>
            <tr><td align="center" colspan="4">紀錄內容</td></tr>
            <tr><td>身高</td><td>{$rowdata->R_height}</td><td>體重</td><td>{$rowdata->R_weight}</td></tr>
            <tr><td>平常體重</td><td>{$rowdata->general_weight}</td><td>BMI</td><td>{$rowdata->R_BMI}</td></tr>
            <tr><td>體態</td><td colspan="3">{$rowdata->R_analysis}</td></tr>
            {$Aim}
            {$Provide}
            {$Surgery}
            {$Cure}
            {$Effect}
            <tr><td>熱量需求</td><td>{$rowdata->Caloric}</td><td>壓力因子</td><td>{$rowdata->Cal_pressure}</td></tr>
            <tr><td>活動因子</td><td>{$Cal_activity[floatval($rowdata->Cal_activity)]}</td><td>蛋白質需求</td><td>{$rowdata->Protein}</td></tr>
            <tr><td>需求量</td><td>{$rowdata->Protein_factor}</td><td colspan="2"></td></tr>
            {$Surgery}
            {$Cure}
            {$Effect}
            {$Diet}
            {$Nutrition}
            {$Eat}
            {$Special}
            <tr><td colspan="4">{$rowdata->R_note}</td></tr>
        </table>
        EOD;
        break;

    case 'social':
        $Aim = findCheck($rowdata, $printDef[$recType], 'Aim');
        $Grants = findCheck($rowdata, $printDef[$recType], 'Grants');
        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>編號</td><td>{$rowdata->MemberID}</td><td>姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>紀錄人員</td><td>{$rowdata->AdName}</td><td>紀錄日期</td><td>{$rowdata->R_date}</td></tr>
            <tr><td>服務方式</td><td>{$service[$rowdata->R_way]}</td><td>地點</td><td>{$rowdata->R_loc}</td></tr>
            <tr><td>服務時間</td><td>{$serviceTime}</td><td colspan="2"></td></tr>
            <tr><td align="center" colspan="4">紀錄內容</td></tr>
            {$Aim}
            {$Grants}
            <tr><td colspan="4">{$rowdata->R_note}</td></tr>
        </table>
        EOD;
        break;

    case 'mental':
        $Mind = findCheck($rowdata, $printDef[$recType], 'Mind');
        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>編號</td><td>{$rowdata->MemberID}</td><td>姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>紀錄人員</td><td>{$rowdata->AdName}</td><td>紀錄日期</td><td>{$rowdata->R_date}</td></tr>
            <tr><td>服務方式</td><td>{$service[$rowdata->R_way]}</td><td>地點</td><td>{$rowdata->R_loc}</td></tr>
            <tr><td>服務時間</td><td>{$serviceTime}</td><td colspan="2"></td></tr>
            <tr><td align="center" colspan="4">紀錄內容</td></tr>
            {$Mind}
            <tr><td colspan="4">{$rowdata->R_note}</td></tr>
        </table>
        EOD;
        break;

    case 'insurance':
        $Ins_med = findCheck($rowdata, $printDef[$recType], 'Ins_med');
        $Ins_policy = findCheck($rowdata, $printDef[$recType], 'Ins_policy');
        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>編號</td><td>{$rowdata->MemberID}</td><td>姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>紀錄人員</td><td>{$rowdata->AdName}</td><td>紀錄日期</td><td>{$rowdata->R_date}</td></tr>
            <tr><td>服務方式</td><td>{$service[$rowdata->R_way]}</td><td>地點</td><td>{$rowdata->R_loc}</td></tr>
            <tr><td>服務時間</td><td>{$serviceTime}</td><td colspan="2"></td></tr>
            <tr><td align="center" colspan="4">紀錄內容</td></tr>
            {$Ins_med}
            {$Ins_policy}
            <tr><td colspan="4">{$rowdata->R_note}</td></tr>
        </table>
        EOD;
        break;

    case 'care':
        $Act = findCheck($rowdata, $printDef[$recType], 'Act');
        $Transfer = findCheck($rowdata, $printDef[$recType], 'Transfer');
        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>編號</td><td>{$rowdata->MemberID}</td><td>姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>紀錄人員</td><td>{$rowdata->AdName}</td><td>紀錄日期</td><td>{$rowdata->R_date}</td></tr>
            <tr><td>服務方式</td><td>{$service[$rowdata->R_way]}</td><td>地點</td><td>{$rowdata->R_loc}</td></tr>
            <tr><td>服務時間</td><td>{$serviceTime}</td><td>關懷者姓名</td><td>{$rowdata->Contactpeop}</td></tr>
            <tr><td align="center" colspan="4">紀錄內容</td></tr>
            {$Act}
            {$Transfer}
            <tr><td colspan="4">{$rowdata->R_note}</td></tr>
        </table>
        EOD;
        break;

    case 'general':
        $vegetarian = findCheck($rowdata, $printDef[$recType], 'vegetarian');
        $prevent = findCheck($rowdata, $printDef[$recType], 'prevent');
        $Knowledge = findCheck($rowdata, $printDef[$recType], 'Knowledge');
        $Other = findCheck($rowdata, $printDef[$recType], 'Other');
        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>編號</td><td>{$rowdata->MemberID}</td><td>姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>紀錄人員</td><td>{$rowdata->AdName}</td><td>紀錄日期</td><td>{$rowdata->R_date}</td></tr>
            <tr><td>服務方式</td><td>{$service[$rowdata->R_way]}</td><td>地點</td><td>{$rowdata->R_loc}</td></tr>
            <tr><td>服務時間</td><td>{$serviceTime}</td><td colspan="2"></td></tr>
            <tr><td align="center" colspan="4">紀錄內容</td></tr>
            {$vegetarian}
            {$prevent}
            {$Knowledge}
            {$Other}
            <tr><td colspan="4">{$rowdata->R_note}</td></tr>
        </table>
        EOD;
        break;

    default:
        $printTable = '';
        break;
}

$pdf->writeHTML($printTable, true, false, false, false, '');

//Close and output PDF document
$pdf->Output('print.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
