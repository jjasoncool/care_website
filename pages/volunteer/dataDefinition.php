<?php
/**
 * 定義資源清單使用資料庫欄位以及項目
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

$definition = [
    'vol_list' => [
        'title' => '志工名單',
        'folder' => 'list',
        'subFolder' => 'linkMember',
        'sqlCondition' => " AND v.V_status=1",
        'closeStatus' => " AND v.V_status=0",
        'roster' => [
            'colhead' => ['姓名','身分','電話','手機'],
            'col' => ['V_name', 'identity', 'C_tele', 'C_mobile1'],
            'colLink' => ['V_name' => true],
            'dataType' => [
                'TP' => 'Taipei',
                'KH' => 'Kaohsiung',
            ],
        ],
        'teams' => [
            'Dept_active' => '活動組',
            'Dept_exec' => '行政組',
            'Dept_care' => '編織關懷組',
            'Dept_peopcare' => '電話關懷組',
            'Dept_dance' => '表演組',
        ],
    ],
    'vol_hour' => [
        'title' => '志工時數',
        'folder' => 'hours',
        'hoursList' => [
            'colhead' => ['年度', '服務時數'],
            'col' => ['yearstr', 'totalhours'],
            'colLink' => ['yearstr' => true],
        ],
        'hoursDetail' => [
            'colhead' => ['年度', '區域', '姓名', '組別', '1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月', '合計'],
            'col' => ['yearstr', 'Area', 'V_name', 'groupName', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec', 'Total'],
        ],
        'teams' => [
            'Dept_active' => '活動組',
            'Dept_exec' => '行政組',
            'Dept_care' => '編織關懷組',
            'Dept_peopcare' => '電話關懷組',
            'Dept_dance' => '表演組',
        ],
    ],
    'vol_train' => [
        'title' => '教育訓練',
        'folder' => 'train',
        'subFolder' => 'attendList',
        'category' => [
            'kind' => 'train',
            'colhead' => ['類別名稱'],
            'col' => ['C_name'],
            'colLink' => ['C_name' => true],
            'dataType' => [
                'TP' => ['unit' => '臺北總會'],
                'KH' => ['unit' => '高雄分會'],
            ],
        ],
        'activities' => [
            'tableName' => 'VolunteerTrain',
            'colhead' => ['日期', '項目', '期別', '合作單位', '負責人', '講師', '審核'],
            'col' => ['eventdate', 'C_name', 'C_No', 'C_partner', 'C_manager', 'C_teacher', 'Admincheck'],
        ],
    ],
];
