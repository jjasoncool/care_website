<?php
/**
 * 新增/編輯訓練資料 (不含上傳照片)
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param array $inputData 使用者輸入的資料
 * @param array $basicData 接受的欄位資料
 * @param string $subPage 子類別
 * @param int $cid 訓練類別編號
 * @param int $id 訓練編號
 */

$id = intval(reqParam('id', 'get'));
$cid = intval(reqParam('cid', 'get'));
$dataID = intval(reqParam('dataID', 'post'));
$formAction = reqParam('formAction', 'post');

$basicData = [
    'C_name' => 'string',
    'C_location' => 'string',
    'C_startday' => 'date',
    'C_endday' => 'date',
    'C_No' => 'int',
    'C_num' => 'int',
    'C_partner' => 'string',
    'C_manager' => 'string',
    'C_teacher' => 'string',
    'C_assistant' => 'string',
    'C_joinnum' => 'int',
    'C_servicenum' => 'int',
    'C_leavenum' => 'int',
    'C_note' => 'string',
    'C_specrec' => 'string',
    'C_comment' => 'string',
    'C_issue' => 'string',
    'attendPeop' => 'string',
];

// 基本資料
foreach ($basicData as $inputName => $inputType) {
    $inputData[$inputName] = reqParam($inputName, 'post', $inputType);
}

// 資料庫紀錄基本資料
$mainTable = $definition[$subPage]['activities']['tableName'];
$subTable = 'Rec_volunteerTrain';
$dt = new dateTime();
$recArray = [
    'Keydate' => $dt->format('Y-m-d H:i:s'),
    'AdminID' => $generalData['userid'],
    'AdName' => $generalData['username'],
    'C_kind' => 'class',
    'categoryID' => $cid,
    'C_name' => $inputData['C_name'],
    'C_location' => $inputData['C_location'],
    'C_startday' => $inputData['C_startday'],
    'C_endday' => $inputData['C_endday'],
    'C_No' => $inputData['C_No'],
    'C_num' => $inputData['C_num'],
    'C_partner' => $inputData['C_partner'],
    'C_manager' => $inputData['C_manager'],
    'C_teacher' => $inputData['C_teacher'],
    'C_assistant' => $inputData['C_assistant'],
    'C_joinnum' => $inputData['C_joinnum'],
    'C_servicenum' => $inputData['C_servicenum'],
    'C_leavenum' => $inputData['C_leavenum'],
    'C_note' => $inputData['C_note'],
    'C_specrec' => $inputData['C_specrec'],
    'C_comment' => $inputData['C_comment'],
    'C_issue' => $inputData['C_issue'],
    'status' => 1,
    'AdmincheckID' => $generalData['managerID'],
];

// 異動主資料表，判斷資料類型
if ($formAction === 'add') {
    $insColStr = implode('`,`', array_keys($recArray));
    $insParaStr = implode(",", array_fill(0, count($recArray), "?"));
    // SQL 用基本資料產出
    $dbQuery = "INSERT INTO FCF_careservice.{$mainTable} (
        `$insColStr`
    ) VALUES (
        $insParaStr
    )";
    $db->query($dbQuery, array_values($recArray));
    // 新增主表後，才可以新增參與人員
    $id = $db->lastInsertId();
    $_GET['id'] = $id;
} elseif ($formAction === 'mod' && !empty($id)) {
    $updColStr = '';
    // 更新時，不可變更訓練ID
    unset($recArray["categoryID"]);
    foreach ($recArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    $dbQuery = "UPDATE FCF_careservice.{$mainTable}
                SET {$updColStr}
                WHERE IDno=?";
    $db->query($dbQuery, array_merge(array_values($recArray), [$id]));
} elseif ($formAction === 'del' && !empty($dataID)) {
    $dbQuery = "UPDATE FCF_careservice.{$mainTable}
                SET `status`=0
                WHERE IDno=? AND `status`=1";
    $db->query($dbQuery, [$dataID]);
}

// 參加人員編輯
$addAttend = array();
$delAttend = array();
$attendPeopInput = array_map('intval', explode(',', $inputData['attendPeop']));
// 現有的參加人員
$dbQuery = "SELECT VolunteerID FROM FCF_careservice.{$subTable} WHERE ClassID=?";
$attendPeopOri = $db->column($dbQuery, [$id]);
if (!empty(array_filter($attendPeopInput))) {
    // 沒有在原有陣列內為新增
    foreach ($attendPeopInput as $key => $peopID) {
        // 不存在的才放到陣列內
        if (!in_array($peopID, $attendPeopOri)) {
            $addAttend[] = [
                'Keydate' => $dt->format('Y-m-d'),
                'VolunteerID' => $peopID,
                'AdminID' => $generalData['userid'],
                'AdName' => $generalData['username'],
                'ClassID' => $id,
            ];
        }
    }
    // 參考陣列中缺少值，為刪除的
    $delAttend = array_diff($attendPeopOri, $attendPeopInput);
}

// 異動參加人員
$insParaStr = '';
$insValueArray = array();
if (!empty($addAttend)) {
    foreach ($addAttend as $key => $addRow) {
        $insColStr = implode('`,`', array_keys($addRow));
        $insParaStr .= '(' . implode(",", array_fill(0, count($addRow), "?")) . '),';
        $insValueArray = array_merge($insValueArray, array_values($addRow));
    }
    $insParaStr = substr($insParaStr, 0, -1);
    // 新增 SQL 用基本資料產出
    $dbQuery = "INSERT INTO FCF_careservice.{$subTable} (
                    `$insColStr`
                ) VALUES
                    $insParaStr";
    $db->query($dbQuery, $insValueArray);
}

// 刪除參加人員
if (!empty($delAttend)) {
    $dbQuery = "DELETE FROM FCF_careservice.{$subTable} WHERE ClassID=? AND VolunteerID IN (?)";
    $db->query($dbQuery, [$id, implode(',', $delAttend)]);
}

toViewPage();

function toViewPage($error = '') {
    header("Location:?" . http_build_query($_GET));
    exit();
}