<?php
/**
 * 新增/編輯/刪除 諮詢紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param array $inputData 使用者輸入的資料
 * @param array $basicData 接受的欄位資料
 * @param string $action 對資料做的動作
 */

$dataID = intval(reqParam('dataID', 'post'));
$formAction = reqParam('action', 'post');

$dataTable = 'EventCategory';
$basicData = [
    'category' => 'string',
    'C_kind' => 'string',
    'Area' => 'string',
];

// 基本資料
foreach ($basicData as $inputName => $inputType) {
    $inputData[$inputName] = reqParam($inputName, 'post', $inputType);
}

// 紀錄基本資料
$dt = new dateTime();
$recArray = [
    "C_name" => $inputData['category'],
    "C_kind" => $definition[$subPage]['category']['kind'],
    "cityLocation" => $inputData['Area'],
    "Del" => 0,
];

// 判斷資料類型
if ($formAction === 'add') {
    $insColStr = implode('`,`', array_keys($recArray));
    $insParaStr = implode(",", array_fill(0, count($recArray), "?"));
    // SQL 用基本資料產出
    $dbQuery = "INSERT INTO FCF_careservice.{$dataTable} (
        `$insColStr`
    ) VALUES (
        $insParaStr
    )";
    $db->query($dbQuery, array_values($recArray));
} elseif ($formAction === 'mod' && !empty($dataID)) {
    // 更新時，不可變更個案ID了
    $updColStr = '';
    foreach ($recArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    $dbQuery = "UPDATE FCF_careservice.{$dataTable}
                SET {$updColStr}
                WHERE IDno=?";
    $db->query($dbQuery, array_merge(array_values($recArray), [$dataID]));
} elseif ($formAction === 'del' && !empty($dataID)) {
    // 刪除，取消此個案與此紀錄之關聯
    $dbQuery = "UPDATE FCF_careservice.{$dataTable}
                SET Del=1
                WHERE IDno=?";
    $db->query($dbQuery, [$dataID]);
}

unset($_GET['action']);
header("Location:?" . http_build_query($_GET));