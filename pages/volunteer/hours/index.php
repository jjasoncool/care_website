<?php
/**
 * 查看志工服務時數清單與統計
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 */

?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition[$subPage]['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['subname'][$subPage]?></a>
                    </li>
                </ul>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="d-flex align-items-center">
                        <h4 class="card-title">服務時數年度統計</h4>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="table-responsive">
                                <table id="tableHourList" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <?php
                                            foreach ($definition[$subPage]['hoursList']['colhead'] as $head) {
                                                echo "<th>{$head}</th>";
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $dbQuery = "SELECT YEAR(R_date) as yearNum,
                                                    CONCAT(YEAR(R_date), '年度社工服務時數') as yearstr,
                                                    CONCAT('年度服務時數共: ', CAST(SUM(V_time) AS CHAR), ' 小時') as totalhours
                                                FROM FCF_careservice.VolunteerHours
                                                GROUP BY YEAR(R_date) ORDER BY yearstr DESC";
                                    $result = $db->query($dbQuery);
                                    foreach ($result as $row) {
                                        $columnContent = '';
                                        $rowdata = json_encode($row, JSON_UNESCAPED_UNICODE);
                                        foreach ($definition[$subPage]['hoursList']['col'] as $colname) {
                                            $link = '';
                                            $linkEnd = '';
                                            if (
                                                isset($definition[$subPage]['hoursList']['colLink'][$colname])
                                                && $definition[$subPage]['hoursList']['colLink'][$colname]
                                            ) {
                                                $link = "<a href=\"./?" . http_build_query($_GET) . "&action=view&year={$row['yearNum']}\">";
                                                $linkEnd = '</a>';
                                            }
                                            $columnContent .= "<td>{$link}{$row[$colname]}{$linkEnd}</td>";
                                        }
                                        $columnContent = nl2br($columnContent);
                                        echo
                                        "<tr>
                                            {$columnContent}
                                        </tr>";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function init() {
    $('#tableHourList').DataTable({
        lengthChange: false,
        searching: false,
        order: [[ 0, "desc" ]],
        columnDefs: [{
            targets: [-1],
            orderable: false,
        }],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Details for '+data[2]+' '+data[3];
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table'
                } )
            }
        }
    });
}

window.onload = init;
</script>