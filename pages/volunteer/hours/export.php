<?php
/**
 * 將資料庫的 EXCEL 資料匯出
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $subPage 子類別
 * @param string $action 執行的動作
 */

// 清除先前頁面要顯示的快取
ob_end_clean();

$year = intval(reqParam('year', 'get'));

// 資料庫連線
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
require_once get_relative_path("pages/{$page}/dataDefinition.php");

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();

// Set document properties
$spreadsheet->getProperties()->setCreator('台灣癌症基金會')
    ->setLastModifiedBy('台灣癌症基金會')
    ->setTitle('匯出資料')
    ->setSubject('年度服務時數資料')
    ->setDescription('資料屬於基金會所有，請勿用於未授權之用途')
    ->setKeywords('canceraway')
    ->setCategory($definition[$subPage]['title']);


$dbQuery = "SELECT h.VolunteerID,
                CONCAT(YEAR(h.R_date), '年度') AS yearstr,
                v.V_name,
                h.V_group AS groupName,
                SUM(CASE WHEN MONTH(h.R_date)=1 then h.V_time else 0 end) AS 'Jan',
                SUM(CASE WHEN MONTH(h.R_date)=2 then h.V_time else 0 end) AS 'Feb',
                SUM(CASE WHEN MONTH(h.R_date)=3 then h.V_time else 0 end) AS 'Mar',
                SUM(CASE WHEN MONTH(h.R_date)=4 then h.V_time else 0 end) AS 'Apr',
                SUM(CASE WHEN MONTH(h.R_date)=5 then h.V_time else 0 end) AS 'May',
                SUM(CASE WHEN MONTH(h.R_date)=6 then h.V_time else 0 end) AS 'June',
                SUM(CASE WHEN MONTH(h.R_date)=7 then h.V_time else 0 end) AS 'July',
                SUM(CASE WHEN MONTH(h.R_date)=8 then h.V_time else 0 end) AS 'Aug',
                SUM(CASE WHEN MONTH(h.R_date)=9 then h.V_time else 0 end) AS 'Sept',
                SUM(CASE WHEN MONTH(h.R_date)=10 then h.V_time else 0 end) AS 'Oct',
                SUM(CASE WHEN MONTH(h.R_date)=11 then h.V_time else 0 end) AS 'Nov',
                SUM(CASE WHEN MONTH(h.R_date)=12 then h.V_time else 0 end) AS 'Dec',
                SUM(h.V_time) AS 'Total',
                CASE a.Acc_area WHEN 'Taipei' THEN '台北' WHEN 'Kaohsiung' THEN '高雄' END AS Area
            FROM FCF_careservice.VolunteerHours h
            INNER JOIN FCF_careservice.Volunteer v ON h.VolunteerID=v.IDno
            INNER JOIN FCF_careservice.Memberdata m ON v.MemberID=m.IDno
            LEFT JOIN FCF_careservice.Accuser a ON m.AdminID=a.IDno
            WHERE YEAR(h.R_date)=?
            GROUP BY h.VolunteerID
            ORDER BY Area";
$result = $db->query($dbQuery, [$year]);

$titleStyle = [
    'font' => ['bold' => true],
];

// 分組陣列
$teamArray = $definition[$subPage]['teams'];
$sheetNum = 0;

// Add some data
foreach ($teamArray as $team => $tagName) {
    $rownum = 1;
    foreach ($definition[$subPage]['hoursDetail']['colhead'] as $key => $colhead) {
        $column = $key + 1;
        $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $rownum, $colhead);
    }
    $rownum++;

    foreach ($result as $row) {
        // 有符合這次的組別
        if ($row['groupName'] === $team) {
            foreach ($definition[$subPage]['hoursDetail']['col'] as $key => $columnName) {
                // 組別從代碼換中文
                if ($columnName === 'groupName') {
                    $row[$columnName] = $teamArray[$row[$columnName]];
                }
                // 內容欄位從第1欄開始
                $column = $key + 1;
                $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $rownum, $row[$columnName]);
            }
            $rownum++;
        }
    }
    // Rename worksheet
    $spreadsheet->getActiveSheet()->setTitle($tagName);
    $sheetNum++;
    // 超過項目總數就不用再加表了
    if ($sheetNum < count($teamArray)) {
        $spreadsheet->createSheet();
        // 一頁寫完換下一頁
        $spreadsheet->setActiveSheetIndex($sheetNum);
    }
}


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment;filename=\"{$definition[$subPage]['title']}.xlsx\"");
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit("<script>window.close();</script>");