<?php
/**
 * 查看基本資料與諮詢紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 * @param int $id 個案編號
 */

// 如果這邊有帶入ID 必須先撈資料
$year = intval(reqParam('year', 'get'));

// 撈取該年度志工時數資料
$dbQuery = "SELECT h.VolunteerID,
                CONCAT(YEAR(h.R_date), '年度') AS yearstr,
                v.V_name,
                h.V_group AS groupName,
                SUM(CASE WHEN MONTH(h.R_date)=1 then h.V_time else 0 end) AS 'Jan',
                SUM(CASE WHEN MONTH(h.R_date)=2 then h.V_time else 0 end) AS 'Feb',
                SUM(CASE WHEN MONTH(h.R_date)=3 then h.V_time else 0 end) AS 'Mar',
                SUM(CASE WHEN MONTH(h.R_date)=4 then h.V_time else 0 end) AS 'Apr',
                SUM(CASE WHEN MONTH(h.R_date)=5 then h.V_time else 0 end) AS 'May',
                SUM(CASE WHEN MONTH(h.R_date)=6 then h.V_time else 0 end) AS 'June',
                SUM(CASE WHEN MONTH(h.R_date)=7 then h.V_time else 0 end) AS 'July',
                SUM(CASE WHEN MONTH(h.R_date)=8 then h.V_time else 0 end) AS 'Aug',
                SUM(CASE WHEN MONTH(h.R_date)=9 then h.V_time else 0 end) AS 'Sept',
                SUM(CASE WHEN MONTH(h.R_date)=10 then h.V_time else 0 end) AS 'Oct',
                SUM(CASE WHEN MONTH(h.R_date)=11 then h.V_time else 0 end) AS 'Nov',
                SUM(CASE WHEN MONTH(h.R_date)=12 then h.V_time else 0 end) AS 'Dec',
                SUM(h.V_time) AS 'Total',
                CASE a.Acc_area WHEN 'Taipei' THEN '台北' WHEN 'Kaohsiung' THEN '高雄' END AS Area
            FROM FCF_careservice.VolunteerHours h
            INNER JOIN FCF_careservice.Volunteer v ON h.VolunteerID=v.IDno
            INNER JOIN FCF_careservice.Memberdata m ON v.MemberID=m.IDno
            LEFT JOIN FCF_careservice.Accuser a ON m.AdminID=a.IDno
            WHERE YEAR(h.R_date)=?
            GROUP BY h.VolunteerID
            ORDER BY Area";
$result = $db->query($dbQuery, [$year]);
?>

<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition[$subPage]['title']?>詳細資料</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="?<?="page={$page}&sub={$subPage}"?>"><?=$listItems[$page]['subname'][$subPage]?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$year?>年度詳細資料</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <div class="card-title"><b>時數紀錄</b></div>
                                <form class="ml-auto" method="post">
                                    <input type="hidden" name="page" id="page">
                                    <button type="button" class="btn btn-outline-secondary btn-round btn-sm" id="expData">
                                        <span class="btn-label"><i class="fas fa-download"></i>匯出年度資料</span>
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <?php
                            // 志工分組
                            $teamArray = $definition[$subPage]['teams'];

                            // 諮詢紀錄類型
                            $recTypeArray = array_keys($teamArray);

                            // 諮詢紀錄內容
                            $countentArray = [];
                            foreach ($teamArray as $team => $tagName) {
                                $columnHead = '';
                                $rowContent = '';

                                // 表格標頭
                                foreach ($definition[$subPage]['hoursDetail']['colhead'] as $str) {
                                    $columnHead .= "<th>{$str}</th>";
                                }
                                // 輸出每一列
                                foreach ($result as $row) {
                                    $columnContent = '';
                                    // 符合組別
                                    if ($row['groupName'] === $team) {
                                        foreach ($definition[$subPage]['hoursDetail']['col'] as $colname) {
                                            if ($colname === 'groupName') {
                                                // 將組別代號換為中文字
                                                $row[$colname] = $teamArray[$row[$colname]];
                                            }
                                            $columnContent .= "<td>{$row[$colname]}</td>";
                                        }
                                        $rowContent .= "<tr>{$columnContent}</tr>";
                                    }
                                }


                                $countentArray[$team] = "
                                    <div class=\"table-responsive\">
                                        <table id=\"{$team}_table\" class=\"display table table-striped table-hover\">
                                            <thead>
                                                <tr>
                                                    {$columnHead}
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {$rowContent}
                                            </tbody>
                                        </table>
                                    </div>";
                            }

                            // 輸出諮詢紀錄
                            $navItem = '';
                            $tabContent = '';
                            $count = 0;
                            foreach ($teamArray as $key => $detail) {
                                $navItem .= "<li class=\"nav-item\">
                                                <a class=\"nav-link\" id=\"{$key}-tab\" data-toggle=\"pill\" href=\"#{$key}\" role=\"tab\" aria-controls=\"{$key}\">{$detail}</a>
                                            </li>";
                                $tabContent .= "<div class=\"tab-pane fade\" id=\"{$key}\" role=\"tabpanel\" aria-labelledby=\"{$key}-tab\">
                                                    {$countentArray[$key]}
                                                </div>";
                            }
                            ?>
                            <ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
                                <?=$navItem?>
                            </ul>
                            <div class="tab-content mt-2 mb-3" id="pills-tabContent">
                                <?=$tabContent?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    "use strict";
    var year = <?=$year?>;
    var params = new URLSearchParams(location.search);
    var tabIndex = location.hash;

    function init() {
        $("#expData").on("click", function () {
            params.set("year", year);
            params.set("action", "download");
            $(this).closest("form").attr("action", "./?" + params.toString());
            $(this).siblings("#page").val("exportFile");
            $(this).closest("form").submit();
        });

        // 頁籤
        $("#pills-tab a.nav-link").on("show.bs.tab", function (e) {
            location.hash = $(e.target).attr("href");
        });
        // 若有頁籤直接指定頁籤，若沒有則看第一個
        if (tabIndex !== '') {
            $("#pills-tab a.nav-link[href='" + tabIndex + "']").tab("show");
        } else {
            $("#pills-tab a.nav-link").eq(0).tab("show");
        }

        $('.table-responsive > table').DataTable({
            pageLength: 10,
            lengthChange: false,
            ordering: false,
        });

    }

    window.onload = init;
</script>