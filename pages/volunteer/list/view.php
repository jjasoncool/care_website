<?php
/**
 * 查看基本資料與諮詢紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 * @param int $id 個案編號
 */

// 如果這邊有帶入ID 必須先撈資料
$id = intval(reqParam('id', 'get'));
$active = boolval(reqParam('active', 'get'));

// 撈取該筆志工基本資料
$dbQuery = "SELECT
                v.*,
                m.IDno mIDno, m.C_tele, m.C_mobile1, m.C_mobile2,
                CASE WHEN m.C_status=1 THEN m.C_member ELSE 'close' END AS 'C_member',
                GROUP_CONCAT(DISTINCT rc.Cancer_name SEPARATOR ', ') AS 'Cancer_name'
            FROM FCF_careservice.Volunteer v
            INNER JOIN FCF_careservice.Memberdata m ON v.MemberID=m.IDno
            LEFT JOIN FCF_careservice.Rec_cancer rc ON m.IDno=rc.MemberID AND rc.`status`=1
            WHERE v.IDno=?
            GROUP BY v.IDno";
$row = $db->row($dbQuery, [$id]);

// 不同狀態顯示
if ($row['V_status'] == 0) {
    $statusStr = '(休息中)';
} else {
    $statusStr = '';
}

// 查無資料就沒資料
if ($row === false) {
    echo '<div class="main-panel">
            <div class="content">
                <div class="page-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title text-center">查無資料</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>';
    exit();
}
?>

<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition[$subPage]['title']?>詳細資料</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="?<?="page={$page}&sub={$subPage}"?>"><?=$listItems[$page]['subname'][$subPage]?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">詳細資料</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-lg-5">
                    <form class="needs-validation" method="post">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    <b>基本資料 <span class="text-danger"><?=$statusStr?></span></b>
                                    <button type="button" class="btn btn-outline-info btn-round btn-sm float-sm-right mx-2" id="modData">
                                        <span class="btn-label"><i class="fas fa-pen"></i>編輯資料</span>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body py-0">
                                <table class="table">
                                    <?php
                                        $sex = $row['V_sex'];
                                        $join = new DateTime($row['V_joindate']);

                                        // 參加組別
                                        $team = [];
                                        $teamArray = $definition[$subPage]['teams'];
                                        foreach ($teamArray as $key => $value) {
                                            if ($row[$key] == 1) {
                                                $team[] = $value;
                                            }
                                        }

                                        // 服務時段
                                        $time = [];
                                        $timeArray = [
                                            'Tim_Mon' => '星期一',
                                            'Tim_Tue' => '星期二',
                                            'Tim_Wed' => '星期三',
                                            'Tim_Thu' => '星期四',
                                            'Tim_Fri' => '星期五',
                                            'Tim_Sat' => '星期六',
                                            'Tim_Sun' => '星期日',
                                        ];
                                        foreach ($timeArray as $col => $showtext) {
                                            switch ($row[$col]) {
                                                case 'No':
                                                    $freeTime = '無';
                                                    break;
                                                case 'AM':
                                                    $freeTime = '上午';
                                                    $time[] = "{$showtext}: {$freeTime}";
                                                    break;
                                                case 'PM':
                                                    $freeTime = '下午';
                                                    $time[] = "{$showtext}: {$freeTime}";
                                                    break;
                                                case 'All':
                                                    $freeTime = '整日';
                                                    $time[] = "{$showtext}: {$freeTime}";
                                                    break;
                                            }
                                        }

                                        // 身分
                                        $identityArray = [
                                            '0' => '',
                                            '1' => "<a href=\"?page=person&sub=personalCase&action=view&id={$row['mIDno']}\">個案</a>",
                                            '2' => "<a href=\"?page=person&sub=family&action=view&id={$row['mIDno']}\">家屬</a>",
                                            '3' => '志工',
                                            'close' => "<a href=\"?page=person&sub=closeCase&action=view&id={$row['mIDno']}\">結案</a>"
                                        ];

                                        // 個案基本資料
                                        $basicData = [
                                            '姓名' => $row['V_name'],
                                            '身分證字號' => $row['V_idno'],
                                            '性別' => ($sex == 'male' ? '男' : '女'),
                                            '電話' => $row['C_tele'],
                                            '行動電話' => implode(', ', array_filter([$row['C_mobile1'], $row['C_mobile2']])),
                                            '加入時間' => $join->format('Y-m-d'),
                                            '可服務縣市' => $row['V_joincity'],
                                            '參加組別' => implode(', ', array_filter($team)),
                                            '服務時段' => implode('<br>', array_filter($time)),
                                            '身分' => $identityArray[$row['C_member']],
                                            '癌別' => $row['Cancer_name'],
                                        ];

                                        foreach ($basicData as $key => $value) {
                                            // 沒資料就不顯示了
                                            if (!empty($value)) {
                                                echo "<tr>
                                                <th scope=\"row\">{$key}</th>
                                                <td>{$value}</td>
                                                </tr>";
                                            }
                                        }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-7">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title"><b>志工紀錄</b></div>
                        </div>
                        <div class="card-body">
                            <?php
                            $consultArray = [
                                'hours' => ['name' => '志工服務時數',   'table' => 'VolunteerHours',        'modify' => true],
                                'train' => ['name' => '志工訓練紀錄',   'table' => 'Rec_volunteerTrain',    'modify' => false,  'joinTable' => 'VolunteerTrain'],
                            ];

                            $showColumn = [
                                'hours' => ['colhead' => ['日期', '服務時數', '組別', '人次', '紀錄'], 'col' => ['R_date', 'V_time', 'V_group', 'V_peop', 'Adname']],
                                'train' => ['colhead' => ['活動日期', '訓練課程', '期數', '地點', '講師'], 'col' => ['eventday', 'C_name', 'C_No', 'C_location', 'C_teacher']],
                            ];
                            // 諮詢紀錄類型
                            $recTypeArray = array_keys($consultArray);

                            // 諮詢紀錄內容
                            $countentArray = [];
                            foreach ($consultArray as $key => $detail) {
                                $columnHead = '';
                                $rowContent = '';
                                if (in_array($key, ['train'], true)) {
                                    // 課程另外撈資料
                                    $dbQuery = "SELECT m.*, CAST(m.Keydate AS DATE) as R_date,
                                                    d.C_name, d.categoryID, d.C_No, d.C_location, d.C_teacher,
                                                    concat(d.C_startday, '～', d.C_endday) AS eventday
                                                FROM FCF_careservice.{$detail['table']} m
                                                LEFT JOIN FCF_careservice.{$detail['joinTable']} d ON m.ClassID=d.IDno
                                                WHERE m.VolunteerID=?";
                                } else {
                                    $dbQuery = "SELECT * FROM FCF_careservice.{$detail['table']} m WHERE m.VolunteerID=?";
                                }
                                $result = $db->query($dbQuery, array($id));

                                // 表格標頭
                                foreach ($showColumn[$key]['colhead'] as $str) {
                                    $columnHead .= "<th>{$str}</th>";
                                }
                                // 輸出每一列
                                foreach ($result as $row) {
                                    $columnContent = '';
                                    $rowdata = json_encode($row, JSON_UNESCAPED_UNICODE);
                                    foreach ($showColumn[$key]['col'] as $colname) {
                                        if ($colname == 'V_group') {
                                            $row[$colname] = $teamArray[$row[$colname]];
                                        }
                                        $columnContent .= "<td>{$row[$colname]}</td>";
                                    }
                                    // 參加課程不可以修改課程資料，僅能刪除
                                    if ($detail['modify']) {
                                        $modBtn = "<button type=\"button\" title=\"修改\" class=\"btn btn-link btn-primary py-0 px-2\"
                                                        data-toggle=\"modal\" data-target=\"#data_{$key}\" data-act=\"mod\">
                                                        <i class=\"fa fa-edit fa-lg\"></i>
                                                    </button>";
                                    } else {
                                        $modBtn = '';
                                    }
                                    $rowContent .= "<tr>
                                                        {$columnContent}
                                                        <td style=\"width: 80px\">
                                                            <div class=\"form-button-action\">
                                                                <input type=\"hidden\" class=\"rowdata\" value='{$rowdata}'>
                                                                {$modBtn}
                                                                <button type=\"button\" title=\"刪除\" class=\"btn btn-link btn-danger py-0 px-2\"
                                                                    data-toggle=\"modal\" data-target=\"#del_{$key}\" data-act=\"del\" data-type=\"{$detail['name']}\">
                                                                    <i class=\"fa fa-times fa-lg\"></i>
                                                                </button>
                                                            </div>
                                                        </td>
                                                    </tr>";
                                }


                                $countentArray[$key] = "
                                    <div class=\"table-responsive\">
                                        <table id=\"{$key}_table\" class=\"display table table-striped table-hover\">
                                            <thead>
                                                <tr>
                                                    {$columnHead}
                                                    <th style=\"padding-right:0px !important;\">
                                                        <button class=\"btn btn-primary btn-sm float-right mx-0\"
                                                            data-toggle=\"modal\" data-target=\"#data_{$key}\" data-act=\"add\">
                                                            <span class=\"btn-label\">
                                                                <i class=\"fa fa-plus\"></i>
                                                            </span>
                                                            新增紀錄
                                                        </button>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {$rowContent}
                                            </tbody>
                                        </table>
                                    </div>";
                            }

                            // 輸出諮詢紀錄
                            $navItem = '';
                            $tabContent = '';
                            $count = 0;
                            foreach ($consultArray as $key => $detail) {
                                $navItem .= "<li class=\"nav-item\">
                                                <a class=\"nav-link\" id=\"{$key}-tab\" data-toggle=\"pill\" href=\"#{$key}\" role=\"tab\" aria-controls=\"{$key}\">{$detail['name']}</a>
                                            </li>";
                                $tabContent .= "<div class=\"tab-pane fade\" id=\"{$key}\" role=\"tabpanel\" aria-labelledby=\"{$key}-tab\">
                                                    {$countentArray[$key]}
                                                </div>";
                            }
                            ?>
                            <ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
                                <?=$navItem?>
                            </ul>
                            <div class="tab-content mt-2 mb-3" id="pills-tabContent">
                                <?=$tabContent?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require 'viewModal.php'; ?>

<script>
    "use strict";
    var id = <?=$id?>;
    var params = new URLSearchParams(location.search);
    var sex = "<?=$sex?>";
    var tabIndex = location.hash;

    // 確認所有勾選欄位都有勾選
    function checkboxStatus(node) {
        let status = 0;
        let items = $(node).find("input:checkbox");
        $.each(items, function(i, v) {
            if ($(v).prop("checked")) {
                status = 1;
            }
        });
        return status;
    }

    function init() {
        $("#modData").on("click", function () {
            params.set("id", id);
            params.set("edit", true);
            params.set("action", "dataForm");
            var path = location.protocol + '//' + location.host + location.pathname + '?' + params.toString();
            location.href = path;
        });

        // 頁籤
        $("#pills-tab a.nav-link").on("show.bs.tab", function (e) {
            location.hash = $(e.target).attr("href");
        });
        // 若有頁籤直接指定頁籤，若沒有則看第一個
        if (tabIndex !== '') {
            $("#pills-tab a.nav-link[href='" + tabIndex + "']").tab("show");
        } else {
            $("#pills-tab a.nav-link").eq(0).tab("show");
        }

        $('.table-responsive > table').DataTable({
            pageLength: 10,
            lengthChange: false,
            searching: false,
            order: [[ 0, "desc" ]],
            autoWidth: false,
            columnDefs: [{
                targets: [-1],
                orderable: false,
            }]
        });

        $("[field='ClassID'], [field='refID']").select2({
            theme: "bootstrap",
            width: '100%'
        });

        if (params.get("dulplicate") == true) {
            swal ("資料重複" ,  "該身分證字號已存在，請直接編輯舊有資料", "error", {
                button: "我知道了"
            });
        }

        // 以下為 Modal 使用之 script
        let recArray = ["<?=implode("\",\"", $recTypeArray)?>"];
        let dateIDStr = "";
        let dataModalStr = "";
        let delModalStr = "";
        // 取得每個ID
        $.each(recArray, function (i, v) {
            dateIDStr += "#R_date_" + v + ",";
            dataModalStr += "#data_" + v + ",";
            delModalStr += "#del_" + v + ",";
        });
        dateIDStr = dateIDStr.slice(0,-1);
        dataModalStr = dataModalStr.slice(0,-1);
        delModalStr = delModalStr.slice(0,-1);

        $(dateIDStr).datetimepicker({
            format: 'YYYY-MM-DD',
            allowInputToggle: true,
            useStrict: true,
            debug: false
        });

        $(dataModalStr).on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            // Extract info from data-* attributes
            let action = button.data('act');
            let titleText = "";
            let rowdata = "";
            // 這個modal
            let modal = $(this);
            let recType = modal.attr("id").replace("data_", "");

            if (action == 'add') {
                titleText = "新增";
                $("[data-target='#R_date_" + recType + "']").val(moment().format("YYYY-MM-DD"));
            } else if (action == 'mod') {
                titleText = "修改";
                rowdata = JSON.parse(button.siblings(".rowdata").val());
                let dataID = rowdata["IDno"];
                let fields = modal.find("[field]");
                // 填欄位
                $.each(fields, function(index, fieldNode) {
                    let field = $(fieldNode).attr("field");
                    let inputType = $(fieldNode).attr("type") || $(fieldNode).prop("tagName");
                    if (inputType != undefined) {
                        inputType = inputType.toLowerCase();
                    }
                    // 依據不同的 input 類型，做不同的塞值動作(單個欄位對應單筆資料)
                    if (inputType == "text" || inputType == "textarea" || inputType == "number" || inputType == "hidden") {
                        $(fieldNode).val(rowdata[field]);
                    } else if (inputType == "checkbox" && Boolean(rowdata[field])) {
                        $(fieldNode).prop("checked", true);
                    } else if (inputType == "select") {
                        $(fieldNode).val(rowdata[field]).trigger("change");
                    }
                });
                // 更新資料ID
                modal.find("[name='dataID']").val(dataID);
            }

            modal.find("[name='recType']").val(recType);
            modal.find("[name='action']").val(action);
            modal.find(".modal-title").text(titleText + '紀錄');
            modal.find("button[type='submit']").text("確定" + titleText);
        });

        $(delModalStr).on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            // Extract info from data-* attributes
            let action = button.data('act');
            let type = button.data('type');
            let titleText = "";
            let date = "";
            let item = "";
            // 這個modal
            let modal = $(this);
            let rowdata = JSON.parse(button.siblings(".rowdata").val());
            let dataID = rowdata["IDno"];
            let recType = modal.attr("id").replace("del_", "");

            if (action == "del") {
                titleText = "刪除";
                date = button.parents("td").siblings("td:first").text();
                if (recType == "train") {
                    item = "<br> 訓練課程: " + button.parents("td").siblings("td:eq(1)").text();
                } else {
                    item = "<br> 時數: " + button.parents("td").siblings("td:eq(1)").text();
                }
            }

            modal.find("[name='dataID']").val(dataID);
            modal.find("[name='recType']").val(recType);
            modal.find("[name='action']").val(action);
            modal.find(".modal-title").text(titleText + type);
            modal.find(".modal-body p").html("日期: " + date + item);
            modal.find("button[type='submit']").text("確定" + titleText);
        });

        $(dataModalStr).on('hidden.bs.modal', function (event) {
            let modal = $(this);
            let dropzone = modal.find("div.dropzoneArea");
            modal.find("form").get(0).reset();
            // 如果上傳檔案區塊存在，需要刪除
            if (dropzone.length>0) {
                modal.find("div.dropzoneArea").get(0).dropzone.removeAllFiles(true);
                modal.find("div.dropzoneArea").get(0).dropzone.destroy();
            }
        });
    }

    window.onload = init;
</script>