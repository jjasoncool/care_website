<?php
/**
 * 新增/編輯/刪除 諮詢紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param array $inputData 使用者輸入的資料
 * @param array $basicData 接受的欄位資料
 * @param string $action 對資料做的動作
 * @param int $id 志工編號
 */

$id = intval(reqParam('id', 'get'));
$dataID = intval(reqParam('dataID', 'post'));
$formAction = reqParam('action', 'post');
$recType = reqParam('recType', 'post');

$dataTable = '';
$inputData = [];
switch ($recType) {
    case 'hours':
        $dataTable = 'VolunteerHours';
        $basicData = [
            'R_date' => 'date',
            'V_time' => 'float',
            'V_group' => 'string',
            'V_peop' => 'int',
        ];
        break;

    case 'train':
        $dataTable = 'Rec_volunteerTrain';
        $basicData = [
            'ClassID' => 'int',
        ];
        break;
    default:
        break;
}

// 基本資料
foreach ($basicData as $inputName => $inputType) {
    $inputData[$inputName] = reqParam($inputName, 'post', $inputType);
}

// 紀錄基本資料
$dt = new dateTime();
$recPeople = [
    "Keydate" => $dt->format('Y-m-d H:i:s'),
    'VolunteerID' => $id,
    'AdminID' => $generalData['userid'],
    'AdName' => $generalData['username']
];

// 判斷資料類型
if ($formAction === 'add') {
    $insColArray = array_merge($recPeople, $inputData);
    $insColStr = implode('`,`', array_keys($insColArray));
    $insParaStr = implode(",", array_fill(0, count($insColArray), "?"));
    // SQL 用基本資料產出
    $dbQuery = "INSERT INTO FCF_careservice.{$dataTable} (
        `$insColStr`
    ) VALUES (
        $insParaStr
    )";
    $db->query($dbQuery, array_values($insColArray));
} elseif ($formAction === 'mod') {
    // 更新時，不可變更個案ID了
    unset($recPeople['VolunteerID']);
    $updColArray = array_merge($recPeople, $inputData);
    $updColStr = '';
    foreach ($updColArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    $dbQuery = "UPDATE FCF_careservice.{$dataTable}
                SET {$updColStr}
                WHERE IDno=?";
    $db->query($dbQuery, array_merge(array_values($updColArray), [$dataID]));
} elseif ($formAction === 'del') {
    // 刪除，取消此個案與此紀錄之關聯
    $dbQuery = "UPDATE FCF_careservice.{$dataTable}
                SET VolunteerID=-VolunteerID
                WHERE IDno=?";
    $db->query($dbQuery, [$dataID]);
}

header("Location:?" . http_build_query($_GET));