<?php
/**
 * 新增人員名單 typeahead
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $query 搜尋的字串
 */
$query = trim(reqParam('query', 'post'));

$dbQuery = "SELECT *, C_name as V_name, C_idno as V_idno, C_sex as V_sex
            FROM FCF_careservice.Memberdata
            WHERE C_name LIKE ? AND C_status=1
            ORDER BY IDno DESC LIMIT 10";
$result = $db->query($dbQuery, ["{$query}%"]);

$response = array();
foreach ($result as $key => $row) {
    if (empty($row["C_idno"])) {
        $ID = '';
    } else {
        $ID = "({$row["C_idno"]})";
    }
    $response[$key]['update'] = "{$row["C_name"]}";
    $response[$key]['name'] = "{$row["C_name"]}{$ID}";
    $response[$key]['data'] = json_encode($row, JSON_UNESCAPED_UNICODE);
}

//set Content-Type to JSON
header('Content-Type: application/json; charset=utf-8');
// 清除先前頁面要顯示的快取
ob_end_clean();
//echo error message as JSON
echo json_encode($response, JSON_UNESCAPED_UNICODE);