<?php
/**
 * 新增/以及編輯資料使用表單
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 * @param int $id 個案ID
 * @param boolean $edit 編輯資料
 */

// 如果這邊有帶入ID 必須先撈資料
$id = intval(reqParam('id', 'get'));
$edit = boolval(reqParam('edit', 'get'));

if ($edit && $id > 0) {
    $pageStatus = '編輯';
    // 一般個案基本資料
    $dbQuery = "SELECT *, STR_TO_DATE(CONCAT(m.BD_yy,'-',m.BD_mm,'-',m.BD_dd), '%Y-%c-%e') AS birthday
                FROM FCF_careservice.Volunteer v
                INNER JOIN FCF_careservice.Memberdata m ON v.MemberID=m.IDno
                WHERE v.IDno=?";
    $memberData = $db->query($dbQuery, [$id]);
    $memberDataJSON = json_encode($memberData, JSON_UNESCAPED_UNICODE);

} else {
    $pageStatus = '新增';
    $memberDataJSON = json_encode('');
}

?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition[$subPage]['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="?<?="page={$page}&sub={$subPage}"?>"><?=$listItems[$page]['subname'][$subPage]?></a>
                    </li>
                    <?php
                    // 有ID才會有詳細資料
                    if (!empty($id)) {
                        echo "
                        <li class=\"separator\">
                            <i class=\"flaticon-right-arrow\"></i>
                        </li>
                        <li class=\"nav-item\">
                            <a href=\"?page={$page}&sub={$subPage}&action=view&id=$id\">詳細資料</a>
                        </li>";
                    }
                    ?>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$pageStatus?>資料</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form class="needs-validation" method="post" id="inputDataForm">
                        <input type="hidden" name="page" value="datainfo">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title"><b>基本資料</b></div>
                            </div>
                            <div class="card-body">
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="caseName">姓名</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="caseName" name="caseName" field="V_name" required>
                                            <div class="input-group-append">
                                                <button class="btn btn-sm btn-outline-secondary" type="reset">清除</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="IDnumber">身分證字號</label>
                                        <input type="text" class="form-control" id="IDnumber" name="IDnumber" field="V_idno" maxlength="10" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>身分</label><br>
                                        <select class="form-control py-1" id="C_member" name="C_member" field="C_member">
                                            <?php
                                            $marryArray = [
                                                '3' => '志工',
                                                '0' => '一般民眾',
                                                '1' => '個案',
                                                '2' => '家屬',
                                            ];
                                            foreach ($marryArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>　</label><br>
                                        <?php
                                        $volArray = [
                                            'FCFvolunte' => '基金會志工',
                                        ];
                                        foreach ($volArray as $name => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"{$name}\" field=\"{$name}\" checked>
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label>性別</label><br>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="sex" value="male" field="V_sex" checked required>
                                            <label class="form-check-label">男</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="sex" value="female" field="V_sex">
                                            <label class="form-check-label">女</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="birthday">生日</label>
                                        <div class="input-group date" id="birthPick" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="birthday" data-target="#birthPick" field="birthday" required>
                                            <div class="input-group-append" data-target="#birthPick" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="openDate">建檔日期</label>
                                        <div class="input-group date" id="openDate" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="C_opendate" data-target="#openDate" field="C_opendate">
                                            <div class="input-group-append" data-target="#openDate" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="comefrom">得知本會來源</label>
                                        <select class="form-control py-1" id="comefrom" name="comefrom" field="C_comefrom">
                                            <?php
                                            $comeArray = ['自行前來', '醫院介紹', '網路', '朋友介紹', '曾參與活動', '其他'];
                                            foreach ($comeArray as $value) {
                                                echo "<option value=\"{$value}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-2">
                                        <label for="city">市</label>
                                        <div class="select2-input">
                                            <select class="form-control py-1" id="city" name="city" field="C_city"></select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="zone">區</label>
                                        <div class="select2-input">
                                            <select class="form-control py-1" id="zone" field="C_zone"></select>
                                            <input type="hidden" name="zone" id="hidZone">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="zip">郵遞區號</label>
                                        <input type="text" class="form-control" id="zip" name="zip" readonly>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="addr">地址</label>
                                        <input type="text" class="form-control" id="addr" name="addr" field="C_address">
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="tel">電話</label>
                                        <input type="text" class="form-control" id="tel" name="tel" field="C_tele" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="email">E-mail</label>
                                        <input type="text" class="form-control" id="email" name="email" field="C_mail">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="mobile1">行動電話</label>
                                        <input type="text" class="form-control" id="mobile1" name="mobile1" field="C_mobile1">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="mobile2">備用行動電話</label>
                                        <input type="text" class="form-control" id="mobile2" name="mobile2" field="C_mobile2">
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="contact">聯絡人姓名</label>
                                        <input type="text" class="form-control" id="contact" name="contact" field="C_contact">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="relation">關係</label>
                                        <input type="text" class="form-control" id="relation" name="relation" field="C_relation">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="reltele">聯絡人電話</label>
                                        <input type="text" class="form-control" id="reltele" name="reltele" field="C_reltele">
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="maritalStatus">婚姻狀況</label>
                                        <select class="form-control py-1" id="maritalStatus" name="maritalStatus" field="C_marriage">
                                            <?php
                                            $marryArray = [
                                                '' => '---',
                                                '未婚' => '未婚',
                                                '已婚' => '已婚',
                                                '同居' => '同居',
                                                '分居' => '分居',
                                                '離異' => '離異',
                                                '喪偶' => '喪偶',
                                                '一方失聯' => '一方失聯'
                                            ];
                                            foreach ($marryArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="haveChilds">子女</label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" id="haveChilds" name="haveChilds" placeholder="0" step="1" min="0" max="20" maxlength="2" field="haveChilds">
                                            <div class="input-group-append">
                                                <div class="input-group-text">名</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="edu">教育程度</label>
                                        <select class="form-control py-1" id="edu" name="edu" field="C_edu">
                                            <?php
                                            $eduArray = [
                                                '' => '---',
                                                '不識字' => '不識字',
                                                '國小' => '國小',
                                                '國中' => '國中',
                                                '高中/高職' => '高中/高職',
                                                '專科' => '專科',
                                                '大學' => '大學',
                                                '研究所(或以上)' => '研究所(或以上)'
                                            ];
                                            foreach ($eduArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group select2-input col-md-3">
                                        <label for="job">職業</label>
                                        <select class="form-control" id="job" name="job" field="C_job">
                                            <?php
                                            $jobArray = [
                                                '0' => '---',
                                                '1' => '一般職業(機關團體/公司行號)',
                                                '2' => '農牧業',
                                                '3' => '漁業',
                                                '4' => '木材森林業',
                                                '5' => '礦業採石業',
                                                '6' => '交通運輸業',
                                                '7' => '餐旅業',
                                                '8' => '建築工程業',
                                                '9' => '製造業',
                                                '10' => '新聞廣告業',
                                                '11' => '保健業(醫院人員/保健人員)',
                                                '12' => '娛樂業',
                                                '13' => '文教機關(教職員/學生)',
                                                '14' => '宗教團體',
                                                '15' => '公共事業',
                                                '16' => '一般商業',
                                                '17' => '服務業(銀行/保險/信託/租賃/自由業/殯葬業/其他)',
                                                '18' => '家庭管理(家管/佣人/保母)',
                                                '19' => '治安人員',
                                                '20' => '軍人',
                                                '21' => '資訊業',
                                                '22' => '職業運動人員',
                                                '23' => '公務人員',
                                                '24' => '待業中',
                                                '25' => '退休'
                                            ];
                                            foreach ($jobArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-sm-12">
                                        <label>福利身分</label><br>
                                        <?php
                                        $welfareArray = [
                                            'id_normal' => '一般',
                                            'id_lowincome' => '低/中低收入',
                                            'id_weakincome' => '經濟弱勢',
                                            'id_oldman' => '老人',
                                            'id_handicapped' => '身心障礙',
                                            'id_indigenous' => '原住民',
                                            'id_foreign' => '新住民',
                                            'id_singlemon' => '單親',
                                            'id_specstatus' => '特殊境遇'
                                        ];

                                        foreach ($welfareArray as $name => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"{$name}\" field=\"{$name}\">
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-sm-12">
                                        <label>保險情況</label><br>
                                        <?php
                                        $insArray = [
                                            'Ins_none' => '無',
                                            'Ins_health' => '健保',
                                            'Ins_social' => '福保',
                                            'Ins_work' => '勞保',
                                            'Ins_gov' => '公保',
                                            'Ins_fisher' => '漁保',
                                            'Ins_farmer' => '農保',
                                            'Ins_soldier' => '軍保',
                                            'Ins_commerce' => '商業保險'
                                        ];

                                        foreach ($insArray as $name => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"{$name}\" field=\"{$name}\">
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                        <div class="form-check form-check-inline mr-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="1" id="Ins_others">
                                                <span class="form-check-sign unselectable">其他</span>
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <input type="text" class="form-control form-control-sm" name="Ins_others" field="Ins_others">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="life_ability">日常生活功能</label>
                                        <select class="form-control py-1" id="life_ability" name="life_ability" field="life_ability">
                                            <?php
                                            $lifeArray = [
                                                '0' => '---',
                                                '1' => '正常',
                                                '2' => '需要他人幫忙',
                                                '3' => '需要輔助用具',
                                                '4' => '完全無法自行活動'
                                            ];
                                            foreach ($lifeArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="caregiver">主要照顧者</label>
                                        <select class="form-control py-1" id="caregiver" name="caregiver" field="caregiver">
                                            <?php
                                            $caregiverArray = [
                                                '0' => '---',
                                                '1' => '父母',
                                                '2' => '配偶',
                                                '3' => '子女',
                                                '4' => '同居人',
                                                '5' => '朋友',
                                                '6' => '其他'
                                            ];
                                            foreach ($caregiverArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="houseown">現在住所</label>
                                        <select class="form-control py-1" id="houseown" name="houseown" field="houseown">
                                            <?php
                                            $houseownArray = [
                                                '0' => '---',
                                                '1' => '自宅無貸款',
                                                '2' => '自宅有貸款',
                                                '3' => '租屋',
                                                '4' => '借住',
                                                '5' => '其他',
                                            ];
                                            foreach ($houseownArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="residence">居住狀況</label>
                                        <select class="form-control py-1" id="residence" name="residence" field="residence">
                                            <?php
                                            $residenceArray = [
                                                '0' => '---',
                                                '1' => '與家人同住',
                                                '2' => '獨居',
                                                '3' => '醫療機構',
                                                '4' => '安置機構',
                                                '5' => '無固定住所',
                                                '6' => '服刑中',
                                                '7' => '其他'
                                            ];
                                            foreach ($residenceArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cohabitation">同住家人</label>
                                        <input type="text" class="form-control" id="cohabitation" name="cohabitation" placeholder="同住家人" field="cohabitation">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="payMonth">居住支出/月</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="payMonth" name="payMonth" placeholder="居住支出/月" field="payMonth">
                                            <div class="input-group-append">
                                                <div class="input-group-text">元</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-sm-12">
                                        <label>經濟狀況</label><br>
                                        <?php
                                        $ecoArray = [
                                            '0' => '自己有工作',
                                            '1' => '政府補助',
                                            '2' => '父母扶養',
                                            '3' => '子女提供',
                                            '4' => '親友提供',
                                            '5' => '其他'
                                        ];

                                        foreach ($ecoArray as $value => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"{$value}\" name=\"ecoStatus[]\" field=\"ecoStatus{$value}\">
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                        <div class="form-check form-check-inline mr-0">
                                            <label class="form-check-label">
                                                <span class="form-check-sign unselectable">月收入</span>
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <input type="text" class="form-control form-control-sm" name="monthIncome" field="monthIncome">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-12">
                                        <label>慢性疾病史</label><br>
                                        <?php
                                        $chronicArray = [
                                            '0' => '無',
                                            '1' => '糖尿病',
                                            '2' => '高血壓',
                                            '3' => '心臟病',
                                            '4' => '中風',
                                            '5' => 'COPD',
                                            '6' => 'CRF',
                                        ];

                                        foreach ($chronicArray as $value => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"{$value}\" name=\"chronic[]\" field=\"chronic{$value}\">
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                        <div class="form-check form-check-inline mr-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="7" id="otherChronic" name="chronic[]" field="chronic7">
                                                <span class="form-check-sign unselectable">其他</span>
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <input type="text" class="form-control form-control-sm" name="otherChronic" field="otherChronic">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="C_note">備註</label>
                                    <textarea class="form-control" id="C_note" name="C_note" rows="5" field="C_note"></textarea>
                                </div>
                            </div>
                            <!-- 志工資料 -->
                            <div class="card-header">
                                <div class="card-title"><b>志工資料</b></div>
                            </div>
                            <div class="card-body" id="volunteerData">
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="V_school">學校</label>
                                        <input type="text" class="form-control" id="V_school" name="V_school" field="V_school">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="V_major">科系</label>
                                        <input type="text" class="form-control" id="V_major" name="V_major" field="V_major">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="city">可服務縣市</label>
                                        <div class="select2-input">
                                            <select class="form-control py-1" id="V_joincity" name="V_joincity" field="V_joincity"></select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label>語文能力</label><br>
                                        <input type="hidden" name="Skill_lang" field="Skill_lang">
                                        <?php
                                        $insArray = [
                                            'lang_english' => '英文',
                                            'lang_japanese' => '日文',
                                            'lang_taiwanese' => '台語',
                                            'lang_hakka' => '客語',
                                        ];

                                        foreach ($insArray as $name => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"{$name}\" field=\"{$name}\">
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                        <div class="form-check form-check-inline mr-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="1" id="lang_othertxt" name="lang_other" field="lang_other">
                                                <span class="form-check-sign unselectable">其他</span>
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <input type="text" class="form-control form-control-sm" name="lang_othertxt" field="lang_othertxt">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label>醫護背景</label><br>
                                        <input type="hidden" name="Skill_medical" field="Skill_medical">
                                        <?php
                                        $insArray = [
                                            'med_hospital' => '醫護',
                                            'med_social' => '社工',
                                            'med_mental' => '心理',
                                            'med_recover' => '復健',
                                            'med_nutrition' => '營養',
                                        ];

                                        foreach ($insArray as $name => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"{$name}\" field=\"{$name}\">
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                        <div class="form-check form-check-inline mr-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="1" id="med_othertxt" name="med_other" field="med_other">
                                                <span class="form-check-sign unselectable">其他</span>
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <input type="text" class="form-control form-control-sm" name="med_othertxt" field="med_othertxt">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label>電腦文書</label><br>
                                        <input type="hidden" name="Skill_PC" field="Skill_PC">
                                        <?php
                                        $insArray = [
                                            'PC_word' => 'word',
                                            'PC_excel' => 'excel',
                                            'PC_powerpoint' => 'power point',
                                            'PC_media' => '數位媒體',
                                        ];

                                        foreach ($insArray as $name => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"{$name}\" field=\"{$name}\">
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                        <div class="form-check form-check-inline mr-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="1" id="PC_othertxt" name="PC_other" field="PC_other">
                                                <span class="form-check-sign unselectable">其他</span>
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <input type="text" class="form-control form-control-sm" name="PC_othertxt" field="PC_othertxt">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label>其他專長</label><br>
                                        <input type="hidden" name="Skill_art" field="Skill_art">
                                        <?php
                                        $insArray = [
                                            'Skill_market' => '行銷企劃',
                                            'art_photo' => '攝影',
                                            'art_maker' => '手工藝',
                                        ];

                                        foreach ($insArray as $name => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"{$name}\" field=\"{$name}\">
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                        <div class="form-check form-check-inline mr-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="1" id="Skill_othertxt" name="Skill_other" field="Skill_other">
                                                <span class="form-check-sign unselectable">其他</span>
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <input type="text" class="form-control form-control-sm" size="60" name="Skill_othertxt" field="Skill_othertxt">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-12">
                                        <label>參加部門</label><br>
                                        <?php
                                        $teamArray = $definition[$subPage]['teams'];

                                        foreach ($teamArray as $name => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"{$name}\" field=\"{$name}\">
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-3">
                                        <label>志願服務紀錄冊</label><br>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="V_recbook" value="1" field="V_recbook">
                                            <label class="form-check-label">有</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="V_recbook" value="0" field="V_recbook" checked>
                                            <label class="form-check-label">無</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="V_No">手冊編號</label>
                                        <input type="text" class="form-control" id="V_No" name="V_No" field="V_No">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <caption>服務時段</caption>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="bg-warning" scope="col">星期一</th>
                                                    <th class="bg-warning" scope="col">星期二</th>
                                                    <th class="bg-warning" scope="col">星期三</th>
                                                    <th class="bg-warning" scope="col">星期四</th>
                                                    <th class="bg-warning" scope="col">星期五</th>
                                                    <th class="bg-success" scope="col">星期六</th>
                                                    <th class="bg-danger" scope="col">星期日</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                <?php
                                                $timeArray = ['Tim_Mon', 'Tim_Tue', 'Tim_Wed', 'Tim_Thu', 'Tim_Fri', 'Tim_Sat', 'Tim_Sun'];
                                                foreach ($timeArray as $key => $col) {
                                                    echo "
                                                    <td>
                                                        <select name=\"{$col}\" field=\"{$col}\">
                                                            <option value=\"No\">無</option>
                                                            <option value=\"AM\">上午</option>
                                                            <option value=\"PM\">下午</option>
                                                            <option value=\"All\">整日</option>
                                                        </select>
                                                    </td>";
                                                }
                                                ?>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>社團經驗</label>
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label for="V_expname1">名稱</label>
                                            <input type="text" class="form-control" id="V_expname1" name="V_expname1" field="V_expname1">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="V_expjob1">職務</label>
                                            <input type="text" class="form-control" id="V_expjob1" name="V_expjob1" field="V_expjob1">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="V_exptime1">年資</label>
                                            <input type="text" class="form-control" id="V_exptime1" name="V_exptime1" field="V_exptime1">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-row">
                                        <div class="form-group col-md-3">
                                            <label for="V_expname2">名稱</label>
                                            <input type="text" class="form-control" id="V_expname2" name="V_expname2" field="V_expname2">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="V_expjob2">職務</label>
                                            <input type="text" class="form-control" id="V_expjob2" name="V_expjob2" field="V_expjob2">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="V_exptime2">年資</label>
                                            <input type="text" class="form-control" id="V_exptime2" name="V_exptime2" field="V_exptime2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 志工資料結尾 -->
                            <div class="card-header">
                                <div class="card-title"><b>個資條款與服務</b></div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <tr class="form-row">
                                        <td class="col-md-2">
                                            <b class="text-danger">個資保密條款</b>
                                        </td>
                                        <td class="col-md-10">
                                            <div class="mb-3">
                                                <div class="form-check p-0">
                                                    <label class="form-check-label" style="white-space: normal;">
                                                        <input class="form-check-input" type="checkbox" value="1" name="privacy" field="privacy" checked required>
                                                        <span class="form-check-sign unselectable">
                                                            <b><u>我同意，將我個人資料作為基金會癌友關懷服務之聯繫、活動簡訊通知及本會相關問卷研究之用，並且我了解我的個人資料將被保密，不作其他用途。</u></b>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div>
                                                申請人簽名或蓋章：
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- <tr class="form-row">
                                        <td class="col-md-2">
                                            <b>是否需要本會最新活動簡訊</b>
                                        </td>
                                        <td class="col-md-10">
                                            <div>
                                                <div class="form-check p-0">
                                                    <label class="form-radio-label">
                                                        <input class="form-radio-input" type="radio" value="1" name="getSMS" checked field="getSMS">
                                                        <span class="form-radio-sign unselectable">
                                                            是的，我需要基金會最新活動消息
                                                        </span>
                                                    </label>
                                                </div>
                                                <div class="form-check p-0">
                                                    <label class="form-radio-label">
                                                        <input class="form-radio-input" type="radio" value="0" name="getSMS">
                                                        <span class="form-radio-sign unselectable">
                                                            暫時不用，謝謝!!
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr> -->
                                    <tr class="form-row">
                                        <td class="col-md-2">
                                            <b>會訊訂閱服務(三個月一本)</b>
                                        </td>
                                        <td class="col-md-10">
                                            <div>
                                                <div class="form-check p-0">
                                                    <label class="form-radio-label mr-0">我要訂閱
                                                        <input class="form-radio-input" type="radio" value="1" name="C_subscribe">
                                                        <span class="form-radio-sign unselectable">
                                                            紙本/
                                                        </span>
                                                    </label>
                                                    <label class="form-radio-label" style="white-space: normal;">
                                                        <input class="form-radio-input" type="radio" value="2" name="C_subscribe" checked>
                                                        <span class="form-radio-sign unselectable">
                                                            電子報，記得填寫e-mail
                                                        </span>
                                                    </label>
                                                </div>
                                                <div class="form-check p-0">
                                                    <label class="form-radio-label" style="white-space: normal;">
                                                        <input class="form-radio-input" type="radio" value="0" name="C_subscribe">
                                                        <span class="form-radio-sign unselectable">
                                                            我不用，謝謝!
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="card-action">
                                <button type="button" class="btn btn-success" id="submitForm">儲存</button>
                                <button type="button" class="btn btn-danger" id="goBack">取消</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    "use strict";
    var params = new URLSearchParams(location.search);
    var otherFields = ["Ins_others", "otherChronic", "lang_othertxt", "med_othertxt", "PC_othertxt", "Skill_othertxt"];
    var listFields = ["ecoStatus", "chronic"];
    var dataArray = <?=$memberDataJSON?>;

    function checkboxStatus(node) {
        let status = 0;
        let items = $(node).find("input:checkbox");
        $.each(items, function(i, v) {
            if ($(v).prop("checked")) {
                status = 1;
            }
        });
        return status;
    }

    function fillInputData(data) {
        // 把每個欄位都填資料
        $.each(data, function(key, obj) {
            $.each(obj, function(field, value) {
                let inputField = $("[field='" + field + "']");
                let inputType = $(inputField).attr("type") || $(inputField).prop("tagName");

                if (inputType != undefined) {
                    inputType = inputType.toLowerCase();
                }

                // 單欄位對應多筆資料
                if ($.inArray(field, listFields) != -1) {
                    let chkArray = value.split(",").map(function(item) {
                        return item.trim();
                    });
                    $.each(chkArray, function(i,v) {
                        $("[field='" + field + v + "']").prop("checked", true);
                    });
                } else {
                    // 依據不同的 input 類型，做不同的塞值動作(單個欄位對應單筆資料)
                    if (inputType == "text" || inputType == "textarea") {
                        $(inputField).val(value);
                        // 其他欄位有值需要勾選
                        if ($.inArray(field, otherFields) != -1 && value != "") {
                            $("#" + field).prop("checked", true).trigger("change");
                        }
                    } else if (inputType == "checkbox" && Boolean(value)) {
                        $(inputField).prop("checked", true);
                    } else if (inputType == "radio") {
                        $.each($(inputField), function(i) {
                            if ($(this).val() == value) {
                                $(this).prop("checked", true);
                            }
                        });
                    } else if (inputType == "select") {
                        if ($(inputField).prop("multiple")) {

                        } else {
                            // 地址區域另外處理
                            if (field == "C_zone") {
                                $(inputField).find("option:contains(" + value + ")").prop("selected", true).trigger("change");
                            } else {
                                $(inputField).val(value).trigger("change");
                            }
                        }
                    }
                }
            });
        });
    }

    function init() {

        // 輸入姓名可帶資料
        $("#caseName").typeahead({
            hint: true,
            highlight: true,
            minLength: 2,
            source: function (query, process) {
                params.set("action", "upload");
                return $.ajax({
                    url: "./?" + params.toString(),
                    method: "POST",
                    data: {query: query, page: "searchList"},
                    dataType: "json"
                }).done(function (res) {
                    // process 是回傳的json轉成陣列物件後，提供產生自動完成清單使用的函數
                    process(res);
                });
            },
            updater: function (item) {
                // 此plugin的物件需要使用this.$element才抓的到
                fillInputData([JSON.parse(item.data)]);
                return item.update;
            }
        });

        doubleSelect('V_joincity');
        doubleSelect('city', 'zone', 'zip');
        // 地址區域需要獨立撈出來
        $("#zone").on("change", function () {
           $("#hidZone").val($(this).find("option:selected").text());
        });

        fillInputData(dataArray);

        $('#city, #zone, #job, #V_joincity').select2({
            theme: "bootstrap",
            width: '100%'
        });

        $('#birthPick, #openDate').datetimepicker({
            format: 'YYYY-MM-DD',
            allowInputToggle: true,
            useStrict: true,
            debug: false
        });

        // 其他欄位有值，需勾選，無值不勾選
        $.each(otherFields, function(i, fieldID) {
            $("[field='" + fieldID + "']").on("change", function() {
                if ($(this).val() == "") {
                    $("#" + fieldID).prop("checked", false).trigger("change");
                } else {
                    $("#" + fieldID).prop("checked", true).trigger("change");
                }
            });
        });

        // 勾選子項目，應該勾選hidden母項目，取消勾選亦同
        let optionBlock = $("#volunteerData").find(".form-group.row");
        $.each(optionBlock, function (index, node) {
            if ($(node).find("input:checkbox").length > 0) {
                let allcheckboxs = $(node).find("input:checkbox");
                let flag = checkboxStatus(node);
                $.each(allcheckboxs, function(i, item) {
                    $(item).on("change", function() {
                        $(node).find("[type='hidden']").val(checkboxStatus(node));
                    });
                });
                $(node).find("[type='hidden']").val(flag);
            }
        });

        // 個資保密必須同意
        $("input[field='privacy'], input[field='FCFvolunte']").on("click", function(e) {
            e.preventDefault();
        });

        // 送出表單
        $("#submitForm").on("click", function (e) {
            if (valid(e)) {
                $(this).closest("form").submit();
            }
        });

        // 回上一頁
        $("#goBack").on("click", function () {
            history.back();
        });
    }

    window.onload = init;
</script>