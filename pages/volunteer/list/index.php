<?php
/**
 * 新增/以及編輯資料使用表單
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 */

// 志工是否休息
$active = !boolval(reqParam('active', 'get'));
?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition[$subPage]['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['subname'][$subPage]?></a>
                    </li>
                </ul>
            </div>
            <div class="card">
                <div class="card-header">
                    <div class="d-flex align-items-center">
                        <?php
                        if ($active) {
                            $tableTitle = '參與中';
                            $btnString = '切換為休息中';
                            $sqlClose = $definition[$subPage]['sqlCondition'];
                        } else {
                            $tableTitle = '休息中';
                            $btnString = '切換為參與中';
                            $sqlClose = $definition[$subPage]['closeStatus'];
                        }
                        $_GET['active'] = $active;
                        ?>
                        <h4 class="card-title"><?=$tableTitle?></h4>
                        <a class="btn btn-info btn-round btn-sm mr-auto ml-3" href="?<?=http_build_query($_GET)?>"><?=$btnString?></a>
                        <?php
                        // 避免新增連結帶到
                        unset($_GET['active']);
                        ?>
                        <a class="btn btn-success btn-round btn-sm mr-3 ml-auto" href="?<?=http_build_query($_GET)?>&action=dataForm">
                            <i class="fas fa-plus"></i> 新增
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <?php
                        foreach ($definition[$subPage]['roster']['dataType'] as $area => $Acc_area) {
                        ?>
                        <div class="col-xl-6">
                            <div class="table-responsive">
                                <table id="table<?=$area?>" class="display table table-striped table-hover" >
                                    <caption style="caption-side: top;"><h3><?=$Acc_area?></h3></caption>
                                    <thead>
                                        <tr>
                                            <?php
                                            foreach ($definition[$subPage]['roster']['colhead'] as $head) {
                                                echo "<th>{$head}</th>";
                                            }
                                            if ($generalData['userLevel'] == 1) {
                                                if ($active) {
                                                    echo '<th>休息</th>';
                                                } else {
                                                    echo '<th>參與</th>';
                                                }
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $dbQuery = "SELECT v.*,
                                                    CASE m.C_member
                                                    WHEN 1 THEN '個案'
                                                    WHEN 2 THEN '家屬'
                                                    WHEN 3 THEN '志工' END AS 'identity',
                                                    m.C_tele, m.C_mobile1, a.Acc_area
                                                FROM FCF_careservice.Volunteer v
                                                INNER JOIN FCF_careservice.Memberdata m ON v.MemberID=m.IDno
                                                INNER JOIN FCF_careservice.Accuser a ON m.AdminID=a.IDno
                                                WHERE a.Acc_area=? {$sqlClose}";
                                    $result = $db->query($dbQuery, [$Acc_area]);
                                    foreach ($result as $row) {
                                        $columnContent = '';
                                        $rowdata = json_encode($row, JSON_UNESCAPED_UNICODE);
                                        // 每個欄位依序列出
                                        foreach ($definition[$subPage]['roster']['col'] as $colname) {
                                            $link = '';
                                            $linkEnd = '';
                                            if (
                                                isset($definition[$subPage]['roster']['colLink'][$colname])
                                                && $definition[$subPage]['roster']['colLink'][$colname]
                                            ) {
                                                $link = "<a href=\"./?" . http_build_query($_GET) . "&action=view&id={$row['IDno']}\">";
                                                $linkEnd = '</a>';
                                            }
                                            $columnContent .= "<td>{$link}{$row[$colname]}{$linkEnd}</td>";
                                        }
                                        $columnContent = nl2br($columnContent);
                                        // 管理者才可以刪除
                                        $delBtn = '';
                                        if ($generalData['userLevel'] == 1) {
                                            if ($active) {
                                                $btnType = '<i class="fa-lg fas fa-umbrella-beach"></i>';
                                            } else {
                                                $btnType = '<i class="fa-lg fas fa-people-carry"></i>';
                                            }
                                            $delBtn = "
                                            <td style=\"width: 10%\">
                                                <div class=\"form-button-action\">
                                                    <input type=\"hidden\" class=\"rowdata\" value='{$rowdata}'>
                                                    <button type=\"button\" title=\"休息/參與\" class=\"btn btn-link btn-danger py-0 pl-2\"
                                                        data-toggle=\"modal\" data-target=\"#delModal\" data-area=\"{$area}\">
                                                        {$btnType}
                                                    </button>
                                                </div>
                                            </td>";
                                        }
                                        echo
                                        "<tr>
                                            {$columnContent}{$delBtn}
                                        </tr>";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">志工狀態變更</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="./?<?=http_build_query($_GET)?>&action=dataForm">
                <div class="modal-body">
                    <input type="hidden" name="dataID">
                    <input type="hidden" name="chgStatus" value="true">
                    <input type="hidden" name="theStatus">
                    <input type="hidden" name="page" value="datainfo">
                    <h6>請問確定要<b>變更</b>此資料狀態嗎?</h6>
                    <b><p class="my-0"></p></b>
                </div>
                <div class="modal-footer no-bd">
                    <button type="submit" class="btn btn-sm btn-primary">確認</button>
                    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
"use strict";
var params = new URLSearchParams(location.search);
var status = <?=intval($active)?>;
function init() {
    $('#tableTP ,#tableKH').DataTable({
        lengthChange: false,
        order: [[ 0, "desc" ]],
        columnDefs: [{
            targets: [-1],
            orderable: false,
        }],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Details for '+data[2]+' '+data[3];
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table'
                } )
            }
        }
    });

    $("#delModal").on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        // Extract info from data-* attributes
        let titleText = "";
        let date = "";
        // 這個modal
        let modal = $(this);
        let rowdata = JSON.parse(button.siblings(".rowdata").val());

        if (status == 1) {
            titleText = '<span class="text-danger">變更為休息</span>';
        } else {
            titleText = '<span class="text-danger">變更為參與</span>';
        }

        modal.find("input[name='dataID']").val(rowdata["IDno"]);
        modal.find("input[name='theStatus']").val(status);
        modal.find(".modal-body p").html(titleText);
    });

    $("#delModal").on('hidden.bs.modal', function (event) {
        let modal = $(this);
        modal.find(".modal-body p").text('');
    });
}

window.onload = init;
</script>