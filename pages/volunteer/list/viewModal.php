<?php
// 志工關懷
ob_start();
?>
<div class="form-group row">
    <div class="col-md-4">
        <label><b>組別</b></label>
        <select class="form-control form-control-sm" name="V_group" field="V_group">
            <?php
                $teamArray = $definition[$subPage]['teams'];
                foreach ($teamArray as $key => $value) {
                    echo "<option value=\"{$key}\">{$value}</option>";
                }
            ?>
        </select>
    </div>
    <div class="col-md-4">
        <label><b>時數</b></label>
        <div class="input-group input-group-sm">
            <input type="number" class="form-control" name="V_time" field="V_time" min="0.5" step="0.5" value="0.5">
            <div class="input-group-append">
                <span class="input-group-text">小時</span>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <label><b>人次</b></label><br>
        <div class="input-group input-group-sm">
        <input type="number" class="form-control" name="V_peop" field="V_peop" min="0" max="50" value="1">
                    <div class="input-group-append">
                <span class="input-group-text">人</span>
            </div>
        </div>
    </div>
</div>
<?php
$hoursContent = ob_get_contents();
ob_end_clean();

// 訓練課程
ob_start();
?>
<div class="form-group row">
    <div class="select2-input col-md-12">
        <label><b>志工訓練課程</b></label>
        <select class="form-control form-control-sm" name="ClassID" field="ClassID">
            <?php
            // 撈取課程資料
            $dbQuery = "SELECT * FROM FCF_careservice.VolunteerTrain ORDER BY categoryID DESC, Keydate DESC";
            $result = $db->query($dbQuery);
            foreach ($result as $rownum => $row) {
                if (!empty($row['C_No'])) {
                    $C_No = "第{$row['C_No']}期 ";
                } else {
                    $C_No = "";
                }
                echo "<option value=\"{$row['IDno']}\">{$C_No}{$row['C_name']} ({$row['C_startday']}～{$row['C_endday']})</option>";
            }
            ?>
        </select>
    </div>
</div>
<?php
$trainContent = ob_get_contents();
ob_end_clean();

// 新增，修改資料Modal
foreach ($recTypeArray as $recType) {
?>
    <div class="modal fade" id="data_<?=$recType?>" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title"></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post">
                    <div class="modal-body">
                        <input type="hidden" name="dataID">
                        <input type="hidden" name="recType">
                        <input type="hidden" name="action">
                        <input type="hidden" name="page" value="attendList">
                        <div class="form-group row">
                        <?php
                        if (!in_array($recType, ['train'], true)) {
                        ?>
                            <div class="col-md-4">
                                <label><b>日期</b></label>
                                <div class="input-group date" id="R_date_<?=$recType?>" data-target-input="nearest">
                                    <input type="text" class="form-control form-control-sm datetimepicker-input" maxlength="10" name="R_date" field="R_date" data-target="#R_date_<?=$recType?>" required>
                                    <div class="input-group-append" data-target="#R_date_<?=$recType?>" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                        </div>
                        <?=${"{$recType}Content"}?>
                    </div>
                    <div class="modal-footer no-bd">
                        <button type="submit" class="btn btn-primary"></button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
}

// 刪除資料Modal
foreach ($recTypeArray as $recType) {
?>
    <div class="modal fade" id="del_<?=$recType?>" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title"></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post">
                    <div class="modal-body">
                        <input type="hidden" name="dataID">
                        <input type="hidden" name="recType">
                        <input type="hidden" name="action" value="del">
                        <input type="hidden" name="page" value="attendList">
                        <h6>請問確定要<span class="text-danger"><b>刪除</b></span>此筆紀錄嗎?</h6>
                        <b><p class="my-0"></p></b>
                    </div>
                    <div class="modal-footer no-bd">
                        <button type="submit" class="btn btn-primary"></button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
}