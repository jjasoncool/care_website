<?php
/**
 * 新增/編輯基本資料 (含疾病史)
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param array $inputData 使用者輸入的資料
 * @param array $basicData 接受的欄位資料
 * @param string $subPage 子類別
 * @param boolean $edit 編輯資料
 * @param int $volID 志工資料編號
 * @param int $id 基本資料編號，一開始給0代表要新增基本資料
 */

$id = 0;
$edit = boolval(reqParam('edit', 'get'));
$volID = intval(reqParam('id', 'get'));
// 改變志工狀態，若存在值，基本上只更新status
$chgStatus = boolval(reqParam('chgStatus', 'post'));
if ($chgStatus) {
    // 傳過來的資料與要變更的值相反，為了方便更新DB，這邊先處理一下
    $volStatus = intval(!boolval(reqParam('theStatus', 'post')));
    $volID = intval(reqParam('dataID', 'post'));
}

$date = new DateTime();
$inputData = [];
$inputVolData = [];
$basicData = [
    'caseName' => 'string',
    'IDnumber' => 'string',
    'C_member' => 'int',
    'FCFvolunte' => 'boolean',
    'sex' => 'string',
    'birthday' => 'string',
    'C_opendate' => 'date',
    'comefrom' => 'string',
    'city' => 'string',
    'zone' => 'string',
    'zip' => 'int',
    'addr' => 'string',
    'tel' => 'string',
    'email' => 'string',
    'mobile1' => 'string',
    'mobile2' => 'string',
    'contact' => 'string',
    'relation' => 'string',
    'reltele' => 'string',
    'maritalStatus' => 'string',
    'haveChilds' => 'int',
    'edu' => 'string',
    'job' => 'int',
    'id_normal' => 'boolean',
    'id_lowincome' => 'boolean',
    'id_weakincome' => 'boolean',
    'id_oldman' => 'boolean',
    'id_handicapped' => 'boolean',
    'id_indigenous' => 'boolean',
    'id_foreign' => 'boolean',
    'id_singlemon' => 'boolean',
    'id_specstatus' => 'boolean',
    'Ins_none' => 'boolean',
    'Ins_health' => 'boolean',
    'Ins_social' => 'boolean',
    'Ins_work' => 'boolean',
    'Ins_gov' => 'boolean',
    'Ins_fisher' => 'boolean',
    'Ins_farmer' => 'boolean',
    'Ins_soldier' => 'boolean',
    'Ins_commerce' => 'boolean',
    'Ins_others' => 'boolean',
    'life_ability' => 'int',
    'caregiver' => 'int',
    'houseown' => 'int',
    'residence' => 'int',
    'cohabitation' => 'string',
    'payMonth' => 'int',
    'ecoStatus' => 'string',
    'monthIncome' => 'int',
    'chronic' => 'string',
    'otherChronic' => 'string',
    'C_note' => 'string',
    'privacy' => 'boolean',
    'getSMS' => 'boolean',
    'C_subscribe' => 'int',
];
// 基本資料
foreach ($basicData as $inputName => $inputType) {
    $inputData[$inputName] = reqParam($inputName, 'post', $inputType);
}
$volData = [
    'V_school' => 'string',
    'V_major' => 'string',
    'V_joincity' => 'string',
    'Skill_lang' => 'boolean',
    'lang_english' => 'boolean',
    'lang_japanese' => 'boolean',
    'lang_taiwanese' => 'boolean',
    'lang_hakka' => 'boolean',
    'lang_other' => 'boolean',
    'lang_othertxt' => 'string',
    'Skill_medical' => 'boolean',
    'med_hospital' => 'boolean',
    'med_social' => 'boolean',
    'med_mental' => 'boolean',
    'med_recover' => 'boolean',
    'med_nutrition' => 'boolean',
    'med_other' => 'boolean',
    'med_othertxt' => 'string',
    'Skill_PC' => 'boolean',
    'PC_word' => 'boolean',
    'PC_excel' => 'boolean',
    'PC_powerpoint' => 'boolean',
    'PC_media' => 'boolean',
    'PC_other' => 'boolean',
    'PC_othertxt' => 'string',
    'Skill_art' => 'boolean',
    'Skill_market' => 'boolean',
    'art_photo' => 'boolean',
    'art_maker' => 'boolean',
    'Skill_other' => 'boolean',
    'Skill_othertxt' => 'string',
    'Dept_active' => 'boolean',
    'Dept_exec' => 'boolean',
    'Dept_care' => 'boolean',
    'Dept_peopcare' => 'boolean',
    'Dept_dance' => 'boolean',
    'V_recbook' => 'boolean',
    'V_No' => 'string',
    'Tim_Mon' => 'string',
    'Tim_Tue' => 'string',
    'Tim_Wed' => 'string',
    'Tim_Thu' => 'string',
    'Tim_Fri' => 'string',
    'Tim_Sat' => 'string',
    'Tim_Sun' => 'string',
    'V_expname1' => 'string',
    'V_expjob1' => 'string',
    'V_exptime1' => 'string',
    'V_expname2' => 'string',
    'V_expjob2' => 'string',
    'V_exptime2' => 'string',
];
// 志工資料
foreach ($volData as $inputName => $inputType) {
    $inputVolData[$inputName] = reqParam($inputName, 'post', $inputType);
}

// 生日須拆資料
$BD_yy = 0;
$BD_mm = 0;
$BD_dd = 0;
if (isset($inputData['birthday']) && !$chgStatus) {
    $temp = explode('-', $inputData['birthday']);
    $BD_yy = $temp[0];
    $BD_mm = $temp[1];
    $BD_dd = $temp[2];
}

// 經濟狀況
$ecoStatusToSQL = '';
if (is_array($inputData['ecoStatus'])) {
    $ecoStatusToSQL = implode(',', $inputData['ecoStatus']);
}
// 慢性疾病史
$chronicToSQL = '';
if (is_array($inputData['chronic'])) {
    $chronicToSQL = implode(',', $inputData['chronic']);
}

// 先確認是否有重複身分證字號，有的話應連結資料
$dbQuery = "SELECT m.IDno, m.C_idno, m.C_member
            FROM FCF_careservice.Memberdata m
            LEFT JOIN FCF_careservice.Volunteer v ON v.MemberID=m.IDno
            WHERE m.C_idno=? OR v.IDno=?";
$result = $db->query($dbQuery, [$inputData['IDnumber'], $volID]);
if (count($result) > 0) {
    // 若存在，代表已有基本資料，需要更新到志工資料的 MemberID 關聯
    $id = $result[0]['IDno'];
    // 若有個案或家屬身分，必須保留原身分
    $inputData['C_member'] = $result[0]['C_member'];
}

// 看是新增資料還是修改資料
if (empty($id) && !$chgStatus) {
    // 新增個案基本資料
    $dbQuery = "INSERT INTO FCF_careservice.Memberdata (
                    `Keydate`, `AdminID`, `AdName`,
                    `C_opendate`, `C_status`, `C_comefrom`, `C_name`, `C_sex`,
                    `C_marriage`, `haveChilds`, `C_member`, `caseClosed`, `C_idno`,
                    `C_job`, `C_edu`, `BD_yy`, `BD_mm`, `BD_dd`,
                    `C_file`, `C_subscribe`, `C_tele`, `C_mail`, `C_mobile1`,
                    `C_mobile2`, `getSMS`, `C_area`, `C_zip`, `C_city`,
                    `C_zone`, `C_address`, `C_contact`, `C_relation`, `C_reltele`,
                    `FCFvolunte`, `C_note`, `id_normal`, `id_lowincome`, `id_weakincome`,
                    `id_oldman`, `id_handicapped`, `id_indigenous`, `id_foreign`, `id_singlemon`,
                    `id_specstatus`, `Ins_none`, `Ins_health`, `Ins_social`, `Ins_work`,
                    `Ins_gov`, `Ins_fisher`, `Ins_farmer`, `Ins_soldier`, `Ins_commerce`,
                    `Ins_others`, `life_ability`, `residence`, `cohabitation`, `houseown`,
                    `payMonth`, `caregiver`, `ecoStatus`, `monthIncome`, `chronic`,
                    `otherChronic`, `privacy`, `Last_update`, `Close_reason`, `Lastcare`
                ) VALUES (
                    NOW(), ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, NOW(), NULL, ''
                )";
    $SQLparam = [
        $generalData['userid'], $generalData['username'],
        $inputData['C_opendate'], 1, $inputData['comefrom'], $inputData['caseName'], $inputData['sex'],
        $inputData['maritalStatus'], $inputData['haveChilds'], $inputData['C_member'], 0, $inputData['IDnumber'],
        $inputData['job'], $inputData['edu'], $BD_yy, $BD_mm, $BD_dd,
        '', $inputData['C_subscribe'], $inputData['tel'], $inputData['email'], $inputData['mobile1'],
        $inputData['mobile2'], 1, $generalData['serviceArea'], $inputData['zip'], $inputData['city'],
        $inputData['zone'], $inputData['addr'], $inputData['contact'], $inputData['relation'], $inputData['reltele'],
        $inputData['FCFvolunte'], $inputData['C_note'], $inputData['id_normal'], $inputData['id_lowincome'], $inputData['id_weakincome'],
        $inputData['id_oldman'], $inputData['id_handicapped'], $inputData['id_indigenous'], $inputData['id_foreign'], $inputData['id_singlemon'],
        $inputData['id_specstatus'], $inputData['Ins_none'], $inputData['Ins_health'], $inputData['Ins_social'], $inputData['Ins_work'],
        $inputData['Ins_gov'], $inputData['Ins_fisher'], $inputData['Ins_farmer'], $inputData['Ins_soldier'], $inputData['Ins_commerce'],
        $inputData['Ins_others'], $inputData['life_ability'], $inputData['residence'], $inputData['cohabitation'], $inputData['houseown'],
        $inputData['payMonth'], $inputData['caregiver'], $ecoStatusToSQL, $inputData['monthIncome'], $chronicToSQL,
        $inputData['otherChronic'], $inputData['privacy'],
    ];
    $db->query($dbQuery, $SQLparam);
    $id = $db->lastInsertId();

} elseif (!empty($id) && !$chgStatus) {
    // 修改個案基本資料
    $dbQuery = "UPDATE FCF_careservice.Memberdata
                    SET `C_opendate`=?, `C_status`=?, `C_comefrom`=?, `C_name`=?,
                    `C_sex`=?, `C_marriage`=?, `haveChilds`=?, `C_member`=?, `caseClosed`=?,
                    `C_idno`=?, `C_job`=?, `C_edu`=?, `BD_yy`=?, `BD_mm`=?, `BD_dd`=?,
                    `C_file`=?, `C_subscribe`=?, `C_tele`=?, `C_mail`=?, `C_mobile1`=?,
                    `C_mobile2`=?, `getSMS`=?, `C_area`=?, `C_zip`=?, `C_city`=?,
                    `C_zone`=?, `C_address`=?, `C_contact`=?, `C_relation`=?, `C_reltele`=?,
                    `FCFvolunte`=?, `C_note`=?, `id_normal`=?, `id_lowincome`=?, `id_weakincome`=?,
                    `id_oldman`=?, `id_handicapped`=?, `id_indigenous`=?, `id_foreign`=?, `id_singlemon`=?,
                    `id_specstatus`=?, `Ins_none`=?, `Ins_health`=?, `Ins_social`=?, `Ins_work`=?,
                    `Ins_gov`=?, `Ins_fisher`=?, `Ins_farmer`=?, `Ins_soldier`=?, `Ins_commerce`=?,
                    `Ins_others`=?, `life_ability`=?, `residence`=?, `cohabitation`=?, `houseown`=?,
                    `payMonth`=?, `caregiver`=?, `ecoStatus`=?, `monthIncome`=?, `chronic`=?,
                    `otherChronic`=?, `Last_update`=NOW()
                WHERE IDno = ?";
    $SQLparam = [
        $inputData['C_opendate'], 1, $inputData['comefrom'], $inputData['caseName'],
        $inputData['sex'], $inputData['maritalStatus'], $inputData['haveChilds'], $inputData['C_member'], 0,
        $inputData['IDnumber'], $inputData['job'], $inputData['edu'], $BD_yy, $BD_mm, $BD_dd,
        '', $inputData['C_subscribe'], $inputData['tel'], $inputData['email'], $inputData['mobile1'],
        $inputData['mobile2'], 1, $generalData['serviceArea'], $inputData['zip'], $inputData['city'],
        $inputData['zone'], $inputData['addr'], $inputData['contact'], $inputData['relation'], $inputData['reltele'],
        $inputData['FCFvolunte'], $inputData['C_note'], $inputData['id_normal'], $inputData['id_lowincome'], $inputData['id_weakincome'],
        $inputData['id_oldman'], $inputData['id_handicapped'], $inputData['id_indigenous'], $inputData['id_foreign'], $inputData['id_singlemon'],
        $inputData['id_specstatus'], $inputData['Ins_none'], $inputData['Ins_health'], $inputData['Ins_social'], $inputData['Ins_work'],
        $inputData['Ins_gov'], $inputData['Ins_fisher'], $inputData['Ins_farmer'], $inputData['Ins_soldier'], $inputData['Ins_commerce'],
        $inputData['Ins_others'], $inputData['life_ability'], $inputData['residence'], $inputData['cohabitation'], $inputData['houseown'],
        $inputData['payMonth'], $inputData['caregiver'], $ecoStatusToSQL, $inputData['monthIncome'], $chronicToSQL,
        $inputData['otherChronic'], $id,
    ];
    $db->query($dbQuery, $SQLparam);
}

// 志工資料
$recArray = [
    "MemberID" => $id,
    "V_name" => $inputData['caseName'],
    "V_sex" => $inputData['sex'],
    "V_idno" => $inputData['IDnumber'],
    "V_joindate" => $date->format('Y-m-d H:i:s'),
    "V_status" => 1,
    "V_recbook" => $inputVolData['V_recbook'],
    "V_No" => $inputVolData['V_No'],
    "V_school" => $inputVolData['V_school'],
    "V_major" => $inputVolData['V_major'],
    "Skill_lang" => $inputVolData['Skill_lang'],
    "lang_english" => $inputVolData['lang_english'],
    "lang_japanese" => $inputVolData['lang_japanese'],
    "lang_taiwanese" => $inputVolData['lang_taiwanese'],
    "lang_hakka" => $inputVolData['lang_hakka'],
    "lang_other" => $inputVolData['lang_other'],
    "lang_othertxt" => $inputVolData['lang_othertxt'],
    "Skill_medical" => $inputVolData['Skill_medical'],
    "med_hospital" => $inputVolData['med_hospital'],
    "med_social" => $inputVolData['med_social'],
    "med_mental" => $inputVolData['med_mental'],
    "med_recover" => $inputVolData['med_recover'],
    "med_nutrition" => $inputVolData['med_nutrition'],
    "med_other" => $inputVolData['med_other'],
    "med_othertxt" => $inputVolData['med_othertxt'],
    "Skill_PC" => $inputVolData['Skill_PC'],
    "PC_word" => $inputVolData['PC_word'],
    "PC_excel" => $inputVolData['PC_excel'],
    "PC_powerpoint" => $inputVolData['PC_powerpoint'],
    "PC_media" => $inputVolData['PC_media'],
    "PC_other" => $inputVolData['PC_other'],
    "PC_othertxt" => $inputVolData['PC_othertxt'],
    "Skill_market" => $inputVolData['Skill_market'],
    "Skill_art" => $inputVolData['Skill_art'],
    "art_photo" => $inputVolData['art_photo'],
    "art_maker" => $inputVolData['art_maker'],
    "Skill_other" => $inputVolData['Skill_other'],
    "Skill_othertxt" => $inputVolData['Skill_othertxt'],
    "Dept_active" => $inputVolData['Dept_active'],
    "Dept_exec" => $inputVolData['Dept_exec'],
    "Dept_care" => $inputVolData['Dept_care'],
    "Dept_peopcare" => $inputVolData['Dept_peopcare'],
    "Dept_dance" => $inputVolData['Dept_dance'],
    "Tim_Mon" => $inputVolData['Tim_Mon'],
    "Tim_Tue" => $inputVolData['Tim_Tue'],
    "Tim_Wed" => $inputVolData['Tim_Wed'],
    "Tim_Thu" => $inputVolData['Tim_Thu'],
    "Tim_Fri" => $inputVolData['Tim_Fri'],
    "Tim_Sat" => $inputVolData['Tim_Sat'],
    "Tim_Sun" => $inputVolData['Tim_Sun'],
    "V_joincity" => $inputVolData['V_joincity'],
    "V_expname1" => $inputVolData['V_expname1'],
    "V_expjob1" => $inputVolData['V_expjob1'],
    "V_exptime1" => $inputVolData['V_exptime1'],
    "V_expname2" => $inputVolData['V_expname2'],
    "V_expjob2" => $inputVolData['V_expjob2'],
    "V_exptime2" => $inputVolData['V_exptime2']
];

if (empty($volID) && empty($edit) && !$chgStatus) {
    // 插入資料
    $insColStr = implode('`,`', array_keys($recArray));
    $insParaStr = implode(",", array_fill(0, count($recArray), "?"));
    // 若重複ID，則更新
    $updColStr = '';
    foreach ($recArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    // SQL 用基本資料產出
    $dbQuery = "INSERT INTO FCF_careservice.Volunteer (
                    `{$insColStr}`
                ) VALUES (
                    {$insParaStr}
                ) ON DUPLICATE KEY UPDATE
                    {$updColStr}";
    $recArray = array_merge(array_values($recArray), array_values($recArray));
    $db->query($dbQuery, array_values($recArray));
    $lastID = $db->lastInsertId();
} elseif (!empty($volID) && !empty($edit) && !$chgStatus) {
    // 更新時，不可變更個案ID了
    $updColStr = '';
    foreach ($recArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    $dbQuery = "UPDATE FCF_careservice.Volunteer
                SET {$updColStr}
                WHERE IDno=?";
    $db->query($dbQuery, array_merge(array_values($recArray), [$volID]));
}

// 僅變更志工狀態
if ($chgStatus) {
    if (!empty($id)) {
        $dbQuery = "UPDATE FCF_careservice.Memberdata
                        SET FCFvolunte=?
                    WHERE IDno=?";
        $db->query($dbQuery, [$volStatus, $id]);
    }
    if (!empty($volID)) {
        $dbQuery = "UPDATE FCF_careservice.Volunteer
                        SET V_status=?
                    WHERE IDno=?";
        $db->query($dbQuery, [$volStatus, $volID]);
    }
}

toViewPage();

function toViewPage($goID = '')
{
    global $volID, $edit, $lastID, $subPage, $chgStatus;
    // 回到上一個瀏覽頁面
    if (empty($volID) && empty($edit)) {
        $_GET['id'] = $lastID;
    } elseif (!empty($goID)) {
        $_GET['id'] = $goID;
        $_GET['dulplicate'] = true;
    }
    $_GET['action'] = 'view';
    if ($chgStatus) {
        // 變更資料狀態直接去index
        unset($_GET['action']);
    }
    unset($_GET['edit']);
    header("Location:?" . http_build_query($_GET));
    exit();
}
