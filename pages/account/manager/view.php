<?php
/**
 * 查看基本資料與諮詢紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 * @param int $id 個案編號
 */

$id = reqParam('id', 'get', 'int');

if ($id > 0) {
    $pageStatus = '編輯';
    // 一般個案基本資料
    $dbQuery = "SELECT *
                FROM FCF_careservice.Accuser a WHERE a.IDno=?";
    $accData = $db->row($dbQuery, [$id]);
    $accDataJSON = json_encode($accData, JSON_UNESCAPED_UNICODE);
} else {
    $pageStatus = '新增';
    $accDataJSON = json_encode('');
}

if ($generalData['userLevel'] > 1) {
    header("Location: " . get_relative_path("pages/404.php"));
    exit();
}

?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition[$subPage]['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="?<?="page={$page}&sub={$subPage}"?>"><?=$listItems[$page]['subname'][$subPage]?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$pageStatus?>帳號資料</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form class="needs-validation" method="post" id="inputDataForm">
                        <input type="hidden" name="page" value="modify">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title"><b>帳號資料</b></div>
                            </div>
                            <div class="card-body">
                                <div class="form-row px-2">
                                    <div class="form-group col-md-6">
                                        <label for="Acc_name">姓名</label>
                                        <input type="text" class="form-control" id="Acc_name" name="Acc_name" field="Acc_name" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="Acc_mail">email</label>
                                        <input type="email" class="form-control" id="Acc_mail" name="Acc_mail" field="Acc_mail" required>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-6">
                                        <label for="newPass">新密碼</label>
                                        <input type="password" class="form-control" id="newPass" name="newPass" minlength="6"
                                            autocomplete="new-password" <?=($id > 0 ? 'placeholder="不需要更改密碼請留空"' : 'required')?>>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="passConfirm">確認新密碼</label>
                                        <input type="password" class="form-control" id="passConfirm" name="passConfirm" minlength="6"
                                            autocomplete="new-password" <?=($id > 0 ? 'placeholder="不需要更改密碼請留空"' : 'required')?>>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="Acc_title">職稱</label>
                                        <select class="form-control py-1" id="Acc_title" name="Acc_title" field="Acc_title">
                                            <option value="">---</option>
                                            <?php
                                            $titleArray = ['副執行長', '總監', '主任', '專員', '社工師', '心理師', '營養師', '個管師', '系統開發'];
                                            foreach ($titleArray as $jobtitle) {
                                                echo "<option value=\"{$jobtitle}\">{$jobtitle}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>服務地區</label><br>
                                        <select class="form-control py-1" id="Acc_area" name="Acc_area" field="Acc_area">
                                            <?php
                                            $areaArray = [
                                                'Taipei' => '臺北總會',
                                                'Kaohsiung' => '高雄分會',
                                            ];
                                            foreach ($areaArray as $val => $label) {
                                                echo "<option value=\"{$val}\">{$label}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>權限</label><br>
                                        <select class="form-control py-1" id="Acc_level" name="Acc_level" field="Acc_level">
                                            <?php
                                            $levelArray = [
                                                '1' => '管理者',
                                                '2' => '一般使用者',
                                                '3' => '志工',
                                                '4' => '保險顧問',
                                                '5' => '個案',
                                            ];
                                            foreach ($levelArray as $val => $label) {
                                                echo "<option value=\"{$val}\">{$label}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>帳號狀態</label><br>
                                        <select class="form-control py-1" id="Acc_status" name="Acc_status" field="Acc_status">
                                            <option class="text-success" value="1">啟用</option>
                                            <option class="text-danger" value="0">停用</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label>審核主管</label><br>
                                        <select class="form-control py-1" id="AdmincheckID" name="AdmincheckID" field="AdmincheckID">
                                            <?php
                                            $dbQuery = "SELECT IDno, Acc_name FROM FCF_careservice.Accuser WHERE Acc_level=1";
                                            $accList = $db->query($dbQuery);
                                            foreach ($accList as $key => $value) {
                                                echo "<option value=\"{$value['IDno']}\">{$value['Acc_name']}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>評估量表窗口</label><br>
                                        <select class="form-control py-1" id="Acc_judge" name="Acc_judge" field="Acc_judge">
                                            <option value="0">否</option>
                                            <option value="1">是</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-action">
                                <button type="button" class="btn btn-success" id="submitForm">儲存</button>
                                <button type="button" class="btn btn-danger" id="goBack">取消</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    "use strict";
    var params = new URLSearchParams(location.search);
    var dataArray = <?=$accDataJSON?>;

    function fillInputData(data) {
        // 把每個欄位都填資料
        $.each(data, function(field, value) {
            let inputField = $("[field='" + field + "']");
            let inputType = $(inputField).attr("type") || $(inputField).prop("tagName");

            if (inputType != undefined) {
                inputType = inputType.toLowerCase();
            }

            // 依據不同的 input 類型，做不同的塞值動作(單個欄位對應單筆資料)
            if (inputType == "text" || inputType == "email" || inputType == "textarea") {
                $(inputField).val(value);
            } else if (inputType == "checkbox" && Boolean(value)) {
                $(inputField).prop("checked", true);
            } else if (inputType == "radio") {
                $.each($(inputField), function(i) {
                    if ($(this).val() == value) {
                        $(this).prop("checked", true);
                    }
                });
            } else if (inputType == "select") {
                $(inputField).val(value).trigger("change");
            }
        });
    }

    function init() {
        let result = params.get("result");
        fillInputData(dataArray);

        // 送出表單
        $("#submitForm").on("click", function (e) {
            if ($("#newPass").val() != '' && $("#newPass").val() != $("#passConfirm").val()) {
                $("#newPass").get(0).setCustomValidity('New password field does not match!');
                swal ("輸入錯誤",  "請確認新密碼兩筆欄位相符", "error", {
                    button: "我知道了"
                }).then(function() {
                    $("#newPass").get(0).setCustomValidity('');
                });
            } else if (valid(e)) {
                let form = $(this).closest("form");
                params.set("action", "dataForm");
                form.attr("action", "?" + params.toString());
                form.submit();
            }
        });

        // 回上一頁
        $("#goBack").on("click", function () {
            history.back();
        });

        if (result == "success") {
            swal ("<?=$pageStatus?>成功", "設定將於帳號下次登入後生效", "success").then((value)=>{
                params.delete("result");
                params.delete("action");
                location.search = "?" + params.toString();
            });
        } else if (result == 'error') {
            swal({
                title: "<?=$pageStatus?>失敗",
                icon: "error"
            });
        }

        $("#AdmincheckID").select2({
            theme: "bootstrap",
            width: '100%'
        });

    }

    window.onload = init;
</script>