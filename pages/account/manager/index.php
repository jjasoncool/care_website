<?php
/**
 * 帳號管理
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 */
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
$dbQuery = "SELECT a.IDno, a.Acc_name, a.Acc_mail, a.Acc_title,
            CASE a.Acc_area WHEN 'Taipei' THEN '台北' WHEN 'Kaohsiung' THEN '高雄' END AS Acc_area,
            CASE a.Acc_level WHEN 1 THEN '管理者' WHEN 2 THEN '一般使用者' WHEN 3 THEN '志工' WHEN 4 THEN '保險顧問' WHEN 5 THEN '個案' END AS Acc_level,
            CASE a.Acc_status WHEN 0 THEN '停用' WHEN 1 THEN '啟用' END AS Acc_status,
            adm.Acc_name AS admin_name
            FROM FCF_careservice.Accuser a
            LEFT JOIN FCF_careservice.Accuser adm ON a.AdmincheckID=adm.IDno
            ORDER BY a.Acc_status DESC";
$accData = $db->query($dbQuery);

if ($generalData['userLevel'] > 1) {
    header("Location: " . get_relative_path("pages/404.php"));
    exit();
}

?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">帳號管理</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['subname'][$subPage]?></a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title"><?=$listItems[$page]['subname'][$subPage]?></h4>
                                <a class="btn btn-outline-success btn-round btn-sm mr-3 ml-auto" href="?<?=http_build_query($_GET)?>&action=view">
                                    <i class="fas fa-plus"></i> 新增
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="basic-datatables" class="display table table-striped table-hover" >
                                    <thead>
                                    <?php
                                    foreach ($definition[$subPage]['colhead'] as $head) {
                                        echo "<th>{$head}</th>";
                                    }
                                    ?>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($accData as $row) {
                                        $columnContent = '';
                                        // 每個欄位依序列出
                                        foreach ($definition[$subPage]['col'] as $colname) {
                                            $link = '';
                                            $linkEnd = '';
                                            // 建立連結
                                            if (
                                                isset($definition[$subPage]['colLink'][$colname])
                                                && $definition[$subPage]['colLink'][$colname]
                                            ) {
                                                $link = "<a href=\"./?" . http_build_query($_GET) . "&action=view&id={$row['IDno']}\">";
                                                $linkEnd = '</a>';
                                            }
                                            $columnContent .= "<td>{$link}{$row[$colname]}{$linkEnd}</td>";
                                        }
                                        $columnContent = nl2br($columnContent);
                                        echo
                                        "<tr>
                                            {$columnContent}
                                        </tr>";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function init() {
    $('#basic-datatables').DataTable({
        order: [[ 5, "desc" ],[ 2, "desc" ]],
    });
}

window.onload = init;
</script>