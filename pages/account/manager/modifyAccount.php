<?php
/**
 * 修改帳號密碼
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param array $inputData 使用者輸入的資料
 * @param array $basicData 接受的欄位資料
 * @param string $action 對資料做的動作
 */

$_GET['result'] = 'error';
$accID = reqParam('id', 'get', 'int');

$inputData = [];
$basicData = [
    'Acc_name' => 'string',
    'Acc_mail' => 'string',
    'newPass' => 'string',
    'passConfirm' => 'string',
    'Acc_title' => 'string',
    'Acc_area' => 'string',
    'Acc_level' => 'int',
    'Acc_status' => 'int',
    'Acc_judge' => 'int',
    'AdmincheckID' => 'int',
];

// 基本資料
foreach ($basicData as $inputName => $inputType) {
    $inputData[$inputName] = reqParam($inputName, 'post', $inputType);
}

$recData = [
    'Acc_name' => $inputData['Acc_name'],
    'Acc_mail' => $inputData['Acc_mail'],
    'Acc_title' => $inputData['Acc_title'],
    'Acc_area' => $inputData['Acc_area'],
    'Acc_level' => $inputData['Acc_level'],
    'Acc_status' => $inputData['Acc_status'],
    'Acc_judge' => $inputData['Acc_judge'],
    'AdmincheckID' => $inputData['AdmincheckID'],
];

// 新增時不可重複帳號
$dbQuery = "SELECT Acc_mail FROM FCF_careservice.Accuser WHERE Acc_mail=?";
$result = $db->query($dbQuery, [$inputData['Acc_mail']]);
if (count($result) > 0 && empty($accID)) {
    toViewPage();
}

// 有輸入需要變更的密碼
if (!empty($inputData['newPass']) && $inputData['newPass'] === $inputData['passConfirm']) {
    $recData['Acc_password'] = $inputData['newPass'];
} elseif (empty($accID)) {
    // 新增資料密碼不得為空
    toViewPage();
}

// 判斷資料類型
if (empty($accID)) {
    $insColArray = $recData;
    $insColStr = implode('`,`', array_keys($insColArray));
    $insParaStr = implode(",", array_fill(0, count($insColArray), "?"));
    // SQL 用基本資料產出
    $dbQuery = "INSERT INTO FCF_careservice.Accuser (
        `$insColStr`
    ) VALUES (
        $insParaStr
    )";
    $db->query($dbQuery, array_values($insColArray));
} elseif ($accID > 0 && $inputData['Acc_status'] == 1) {
    $updColArray = $recData;
    $updColStr = '';
    foreach ($updColArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    $dbQuery = "UPDATE FCF_careservice.Accuser
                SET {$updColStr}
                WHERE IDno=?";
    $db->query($dbQuery, array_merge(array_values($updColArray), [$accID]));
} elseif ($accID > 0 && $inputData['Acc_status'] == 0) {
    // 關閉帳號
    $dbQuery = "UPDATE FCF_careservice.Accuser
                SET Acc_status=?
                WHERE IDno=?";
    $db->query($dbQuery, [$inputData['Acc_status'], $accID]);
}

toViewPage('success');

/**
 * 返回 submit 頁面
 *
 * @param string $msg 成功或錯誤
 * @return void
 */
function toViewPage($msg = 'error')
{
    $_GET['result'] = $msg;
    $_GET['action'] = 'view';
    unset($_GET['id']);
    header("Location:?" . http_build_query($_GET));
    exit();
}
