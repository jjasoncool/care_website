<?php
/**
 * 新增/以及編輯資料使用表單
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 * @param int $id 個案ID
 * @param boolean $edit 編輯資料
 */

// 帳號基本資料
$dbQuery = "SELECT * FROM FCF_careservice.Accuser a WHERE a.IDno=? AND a.Acc_status=1";
$accData = $db->row($dbQuery, [$generalData['userid']]);
unset($accData['Acc_password']);
$accDataJSON = json_encode($accData, JSON_UNESCAPED_UNICODE);

// 該帳號已不存在
if ($accData === false) {
    header("Location: " . get_relative_path("pages/404.php"));
    exit();
}
?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition[$subPage]['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$definition[$subPage]['title']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">變更密碼</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form class="needs-validation" method="post">
                        <input type="hidden" name="page" value="modify">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title"><b>修改密碼</b></div>
                            </div>
                            <div class="card-body">
                                <div class="form-row px-2">
                                    <div class="form-group col-md-6">
                                        <label for="Acc_name">姓名</label>
                                        <input type="text" class="form-control-plaintext" id="Acc_name" field="Acc_name" readonly>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="email">email</label>
                                        <input type="text" class="form-control-plaintext" id="email" field="Acc_mail" readonly>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-6">
                                        <label for="newPass">新密碼</label>
                                        <input type="password" class="form-control" id="newPass" name="newPass" minlength="6" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="passConfirm">確認新密碼</label>
                                        <input type="password" class="form-control" id="passConfirm" name="passConfirm" minlength="6" required>
                                    </div>
                                </div>
                            </div>
                            <div class="card-action">
                                <button type="button" class="btn btn-success" id="submitForm">儲存</button>
                                <button type="button" class="btn btn-danger" id="goBack">取消</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    "use strict";
    var params = new URLSearchParams(location.search);
    var dataArray = <?=$accDataJSON?>;

    function fillInputData(data) {
        // 把每個欄位都填資料
        $.each(data, function(field, value) {
            let inputField = $("[field='" + field + "']");
            let inputType = $(inputField).attr("type") || $(inputField).prop("tagName");

            if (inputType != undefined) {
                inputType = inputType.toLowerCase();
            }

            // 依據不同的 input 類型，做不同的塞值動作(單個欄位對應單筆資料)
            if (inputType == "text" || inputType == "textarea") {
                $(inputField).val(value);
            } else if (inputType == "checkbox" && Boolean(value)) {
                $(inputField).prop("checked", true);
            } else if (inputType == "radio") {
                $.each($(inputField), function(i) {
                    if ($(this).val() == value) {
                        $(this).prop("checked", true);
                    }
                });
            } else if (inputType == "select") {
                $(inputField).val(value).trigger("change");
            }
        });
    }

    function init() {
        let result = params.get("result");
        fillInputData(dataArray);

        // 送出表單
        $("#submitForm").on("click", function (e) {
            if ($("#newPass").val() != $("#passConfirm").val()) {
                $("#newPass").get(0).setCustomValidity('New password field does not match!');
                swal ("輸入錯誤",  "請重新確認新密碼與確認密碼欄位相符", "error", {
                    button: "我知道了"
                }).then(function() {
                    $("#newPass").get(0).setCustomValidity('');
                });
            } else if (valid(e)) {
                let form = $(this).closest("form");
                params.set("action", "dataForm");
                form.attr("action", "?" + params.toString());
                form.submit();
            }
        });

        // 回上一頁
        $("#goBack").on("click", function () {
            history.back();
        });

        if (result == "success") {
            swal ("密碼修改成功",  "登出後請使用新密碼登入", "success");
        } else if (result == 'error') {
            swal({
                title: "密碼修改失敗",
                icon: "error"
            });
        }

    }

    window.onload = init;
</script>