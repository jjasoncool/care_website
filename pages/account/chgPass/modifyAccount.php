<?php
/**
 * 修改帳號密碼
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param array $inputData 使用者輸入的資料
 * @param array $basicData 接受的欄位資料
 * @param string $action 對資料做的動作
 */

$_GET['result'] = 'error';
$inputData = [];
$basicData = [
    'newPass' => 'string',
    'passConfirm' => 'string',
];

// 基本資料
foreach ($basicData as $inputName => $inputType) {
    $inputData[$inputName] = reqParam($inputName, 'post', $inputType);
}

if (
    $inputData['newPass'] === $inputData['passConfirm']
    && !empty($inputData['newPass'])
) {
    $recData = [
        'Acc_password' => $inputData['newPass']
    ];

    // 更新時，不可變更個案ID了
    $updColArray = $recData;
    $updColStr = '';
    foreach ($updColArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    $dbQuery = "UPDATE FCF_careservice.Accuser
                SET {$updColStr}
                WHERE IDno=?";
    $db->query($dbQuery, array_merge(array_values($updColArray), [$generalData['userid']]));
    $_GET['result'] = 'success';
}

unset($_GET['action']);
header("Location:?" . http_build_query($_GET));