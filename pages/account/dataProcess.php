<?php
/**
 * 資料處理層，將php分配到不同頁面處理
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */
require_once 'vendor/autoload.php';
require 'settings/loginCheck.php';
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
$pageAction = reqParam('page', 'post', 'string');
unset($_GET["result"]);
require_once get_relative_path("pages/{$page}/dataDefinition.php");

switch ($pageAction) {
    // 以下為教育訓練傳送 action
    case 'modify':
        // 修改帳號資料
        require_once get_relative_path("pages/{$page}/{$definition[$subPage]['folder']}/modifyAccount.php");
        break;

    case 'uploadProfile':
        // 修改大頭貼
        require_once get_relative_path("pages/{$page}/{$definition[$subPage]['folder']}/modifyData.php");
        break;

    default:
        break;
}

exit();
