<?php
/**
 * 定義資源清單使用資料庫欄位以及項目
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

$definition = [
    'manager' => [
        'title' => '帳號管理',
        'folder' => 'manager',
        'colhead' => ['姓名', '帳號(email)', '職稱', '服務區域', '權限等級', '帳號狀態', '審核主管'],
        'col' => ['Acc_name', 'Acc_mail', 'Acc_title', 'Acc_area', 'Acc_level', 'Acc_status', 'admin_name'],
        'colLink' => ['Acc_name' => true],
    ],
    'chgPass' => [
        'title' => '帳號管理',
        'folder' => 'chgPass',
    ],
    'chgProfile' => [
        'title' => '帳號管理',
        'folder' => 'chgProfile',
    ],
];
