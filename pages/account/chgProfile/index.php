<?php
/**
 * 新增/以及編輯資料使用表單
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 * @param int $id 個案ID
 * @param boolean $edit 編輯資料
 */

// 帳號基本資料
$dbQuery = "SELECT * FROM FCF_careservice.Accuser a WHERE a.IDno=? AND a.Acc_status=1";
$accData = $db->row($dbQuery, [$generalData['userid']]);
unset($accData['Acc_password']);
$accDataJSON = json_encode($accData, JSON_UNESCAPED_UNICODE);

// 該帳號已不存在
if ($accData === false) {
    header("Location: " . get_relative_path("pages/404.php"));
    exit();
}
?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition[$subPage]['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$definition[$subPage]['title']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">個人檔案維護</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form class="needs-validation" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="page" value="uploadProfile">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title"><b>個人檔案維護</b></div>
                            </div>
                            <div class="card-body">
                                <div class="form-row px-2">
                                    <div class="col-md-3">
                                        <div class="input-file input-file-image">
                                            大頭貼
                                            <img class="img-upload-preview" width="150" src="/uploads/images/150x150.png" alt="preview">
                                            <input type="file" class="form-control form-control-file" id="file" name="file" accept="image/*" required="">
                                            <label for="file" class="  label-input-file btn btn-default btn-round">
                                                <span class="btn-label">
                                                    <i class="fa fa-file-image"></i>
                                                </span>
                                                Upload a Image
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-action">
                                <button type="button" class="btn btn-success" id="submitForm">儲存</button>
                                <button type="button" class="btn btn-danger" id="goBack">取消</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    "use strict";
    var params = new URLSearchParams(location.search);

    function init() {
        let result = params.get("result");
        // 送出表單
        $("#submitForm").on("click", function (e) {
            let form = $(this).closest("form");
            params.set("action", "upload");
            form.attr("action", "?" + params.toString());
            form.submit();
        });

        // 回上一頁
        $("#goBack").on("click", function () {
            history.back();
        });

        if (result == "success") {
            swal ("個人檔案更新成功",  "", "success");
        } else if (result == 'error') {
            swal({
                title: "OOPS! 修改失敗",
                icon: "error"
            });
        }
    }

    window.onload = init;
</script>