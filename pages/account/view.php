<?php
/**
 * 志工三類表單都不一樣，直接分家去include不同頁面
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 */
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);

// 資料定義
require_once get_relative_path("pages/{$page}/dataDefinition.php");

if (!array_key_exists($subPage, $definition)) {
    header("Location: " . get_relative_path("pages/404.php"));
    exit();
}

include get_relative_path("pages/{$page}/{$definition[$subPage]['folder']}/view.php");

