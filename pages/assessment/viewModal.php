<!-- Modal -->
<div class="modal fade" id="modalTrack" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="./?<?=http_build_query($_GET)?>">
                <div class="modal-body">
                    <input type="hidden" name="dataID">
                    <input type="hidden" name="action">
                    <input type="hidden" name="page" value="tracking">
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label><b>追蹤類型</b></label><br>
                            <select class="form-control form-control-sm" name="assess_type" field="assess_type" <?=($generalData['trackJudge'] ? '' : 'disabled')?>>
                                <?php
                                    $optArray = [
                                        0 => '---',
                                        1 => '護理',
                                        2 => '營養',
                                        3 => '社工',
                                        4 => '心理',
                                    ];
                                    foreach ($optArray as $key => $label) {
                                        echo "<option value=\"{$key}\">{$label}</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="select2-input col-md-4">
                            <label><b>追蹤人員</b></label>
                            <select class="form-control form-control-sm" name="AdminID" field="AdminID" <?=($generalData['trackJudge'] ? '' : 'disabled')?>>
                                <option value="0">---</option>
                                <?php
                                // 撈取課程資料
                                $dbQuery = "SELECT * FROM FCF_careservice.Accuser WHERE Acc_status=1 AND Acc_level=2";
                                $result = $db->query($dbQuery);
                                foreach ($result as $rownum => $row) {
                                    echo "<option value=\"{$row['IDno']}\">{$row['Acc_name']}</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label><b>轉介日期</b></label><br>
                            <input type="text" class="form-control-plaintext" field="jgdate" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label><b>轉介說明</b></label>
                            <textarea class="form-control" name="judgenote" field="judgenote" rows="3" <?=($generalData['trackJudge'] ? '' : 'readonly')?>></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label><b>預定追蹤日期</b></label>
                            <div class="input-group date" id="T_date" data-target-input="nearest">
                                <input type="text" class="form-control form-control-sm datetimepicker-input" maxlength="10" name="T_date" field="T_date" data-target="#T_date" <?=($generalData['trackJudge'] ? '' : 'required')?>>
                                <div class="input-group-append" data-target="#T_date" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label><b>追蹤電話</b></label><br>
                            <input type="text" class="form-control form-control-sm" name="T_phone" field="T_phone">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label><b>追蹤情況</b></label>
                            <textarea class="form-control" name="T_note" field="T_note" rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer no-bd">
                    <button type="submit" class="btn btn-primary"></button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="completeTrack" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">結束追蹤</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post">
                <div class="modal-body">
                    <input type="hidden" name="dataID">
                    <input type="hidden" name="action" value="close">
                    <input type="hidden" name="page" value="tracking">
                    <h6>請問確定要將此追蹤紀錄<span class="text-success"><b>結案</b></span>嗎?</h6>
                    <b><p class="my-0"></p></b>
                </div>
                <div class="modal-footer no-bd">
                    <button type="submit" class="btn btn-primary">確認結案</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
if ($generalData['userLevel'] == 1) {
?>
    <div class="modal fade" id="delTrack" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">刪除追蹤紀錄</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post">
                    <div class="modal-body">
                        <input type="hidden" name="dataID">
                        <input type="hidden" name="action" value="del">
                        <input type="hidden" name="page" value="tracking">
                        <h6>請問確定要<span class="text-danger"><b>刪除</b></span>此追蹤紀錄嗎?</h6>
                        <p class="text-danger"><b>(刪除後無法復原!!!)</p>
                        <b><p class="my-0"></p></b>
                    </div>
                    <div class="modal-footer no-bd">
                        <button type="submit" class="btn btn-danger">確認刪除</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">取消</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="delForm" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">刪除評估量表</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post">
                    <div class="modal-body">
                        <input type="hidden" name="dataID">
                        <input type="hidden" name="action" value="del">
                        <input type="hidden" name="page" value="writeForm">
                        <h6>請問確定要<span class="text-danger"><b>刪除</b></span>此評估量表紀錄嗎?</h6>
                        <p class="text-danger"><b>(刪除後無法復原!!!)</p>
                        <b><p class="my-0"></p></b>
                    </div>
                    <div class="modal-footer no-bd">
                        <button type="submit" class="btn btn-danger">確認刪除</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">取消</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php
}