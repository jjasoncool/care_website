<?php
/**
 * 定義資源清單使用資料庫欄位以及項目
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

$definition = [
    'title' => '評估量表',
    'colhead' => ['姓名', '身分', '性別', '生日', '年齡', '癌別'],
    'col' => ['C_name', 'memberType', 'C_sex', 'birthday', 'age', 'Cancer_name'],
    'colLink' => ['C_name' => true],
];

$trackArray = [
    'title' => '追蹤紀錄',
    'table' => 'Tracklist',
    'colhead' => ['評估量表日期','類型', '窗口', '轉介日期', '追蹤人員', '追蹤狀態'],
    'col' => ['opdate', 'trackType', 'judgeName', 'jgdate', 'ADname', 'status_text'],
];

$assessArray = [
    'nurse' => [
        'name' => '護理評估量表',
        'table' => 'assess_nursing',
        'colhead' => ['填寫日期', '填寫人', '總分', '備註'],
        'col' => ['key_date', 'AdName', 'score', 'R_note'],
        'scoreCare' => 6,
    ],
    'nutrition' => [
        'name' => '營養評估量表',
        'table' => 'assess_nutrition',
        'colhead' => ['填寫日期', '填寫人', '總分', '備註'],
        'col' => ['key_date', 'AdName', 'score', 'R_note'],
        'scoreCare' => 5,
    ],
    'social' => [
        'name' => '社工評估量表',
        'table' => 'assess_social',
        'colhead' => ['填寫日期', '填寫人', '總分', '備註'],
        'col' => ['key_date', 'AdName', 'score', 'R_note'],
        'scoreCare' => 18,
    ],
    'mental' => [
        'name' => '心理評估量表',
        'table' => 'assess_mental',
        'colhead' => ['填寫日期', '填寫人', '總分', '備註'],
        'col' => ['key_date', 'AdName', 'score', 'R_note'],
        'scoreCare' => 4,
    ],
];
