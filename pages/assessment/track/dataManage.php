<?php
/**
 * 新增/編輯 追蹤紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param array $inputData 使用者輸入的資料
 * @param array $basicData 接受的欄位資料
 * @param string $formAction submit 類型
 * @param int $dataID 紀錄編號
 * @param int $id 個案編號
 */

require_once get_relative_path("pages/{$page}/dataDefinition.php");

$id = intval(reqParam('id', 'get'));
$formAction = reqParam('action', 'post');
$dataID = intval(reqParam('dataID', 'post'));

// 個案姓名
$dbQuery = "SELECT C_name FROM FCF_careservice.Memberdata m WHERE m.IDno=?";
$row = $db->row($dbQuery, [$id]);

$inputData = [];
$basicData = [
    'assess_type' => 'int',
    'judgenote' => 'string',
    'AdminID' => 'int',
    'T_date' => 'date',
    'T_phone' => 'string',
    'T_note' => 'string',
];

// 紀錄基本資料
$dt = new dateTime();
$recTrack = [
    'MemberID' => $id,
    'Opendate' => $dt->format('Y-m-d H:i:s'),
];

// 非窗口不更新以下資料
if ($generalData['trackJudge']) {
    $recTrack['judgeID'] = $generalData['userid'];
    $recTrack['judgedate'] = $dt->format('Y-m-d H:i:s');
} else {
    unset($basicData['assess_type'], $basicData['judgenote'], $basicData['AdminID']);
}

// 基本資料
foreach ($basicData as $inputName => $inputType) {
    $inputData[$inputName] = reqParam($inputName, 'post', $inputType);
}

// 判斷資料類型
if ($formAction === 'add') {
    $insColArray = array_merge($recTrack, $inputData);
    $insColStr = implode('`,`', array_keys($insColArray));
    $insParaStr = implode(",", array_fill(0, count($insColArray), "?"));
    // SQL 用基本資料產出
    $dbQuery = "INSERT INTO FCF_careservice.{$trackArray['table']} (
        `{$insColStr}`
    ) VALUES (
        {$insParaStr}
    )";
    $db->query($dbQuery, array_values($insColArray));
    $dataID = $db->lastInsertId();
} elseif ($formAction === 'mod') {
    // 更新時，不可變更個案ID了
    unset($recTrack['MemberID'], $recTrack['Opendate']);
    $updColArray = array_merge($recTrack, $inputData);
    $updColStr = '';
    foreach ($updColArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    $dbQuery = "UPDATE FCF_careservice.{$trackArray['table']}
                SET {$updColStr}
                WHERE IDno=?";
    $db->query($dbQuery, array_merge(array_values($updColArray), [$dataID]));
} elseif ($formAction === 'del') {
    // 刪除，取消此個案與此紀錄之關聯
    $dbQuery = "UPDATE FCF_careservice.{$trackArray['table']}
                SET MemberID=-MemberID, Closedate=NOW()
                WHERE IDno=?";
    $db->query($dbQuery, [$dataID]);
} elseif ($formAction === 'close') {
    // 結案，此追蹤已結束
    $dbQuery = "UPDATE FCF_careservice.{$trackArray['table']}
                SET T_status=1, Closedate=NOW()
                WHERE IDno=?";
    $db->query($dbQuery, [$dataID]);
}

// 通知帳號
$dbQuery = "SELECT AdminID FROM FCF_careservice.{$trackArray['table']} WHERE IDno=?";
$noticeID = $db->row($dbQuery, [$dataID]);

$noticeType = [
    'add' => ['code' => 1, 'text' => '已新增'],
    'mod' => ['code' => 2, 'text' => '已修改'],
    'del' => ['code' => 3, 'text' => '已刪除'],
    'close' => ['code' => 4, 'text' => '已結束追蹤'],
];

addNotification(
    $noticeID['AdminID'],
    $noticeType[$formAction]['code'],
    "?page={$page}&action=view&id={$id}",
    "{$_SESSION['username']}\n{$noticeType[$formAction]['text']}個案{$row['C_name']}的追蹤紀錄"
);

toViewPage();

/**
 * 回到頁面
 *
 * @return void
 */
function toViewPage()
{
    // 回到頁面
    $_GET['action'] = 'view';
    header("Location:?" . http_build_query($_GET));
    exit();
}
