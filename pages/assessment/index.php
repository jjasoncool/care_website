<?php
/**
 * 進入評估量表個案
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 */

$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
require_once get_relative_path("pages/{$page}/dataDefinition.php");

if ($generalData['userLevel'] > $listItems[$page]['permissions']) {
    header("Location: " . get_relative_path("pages/404.php"));
    exit();
}

?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title"><?=$listItems[$page]['name']?></h4>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="table-responsive">
                                        <table id="memberTable" class="display table table-striped table-hover" >
                                            <thead>
                                                <tr>
                                                    <?php
                                                    foreach ($definition['colhead'] as $head) {
                                                        echo "<th>{$head}</th>";
                                                    }
                                                    ?>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $dbQuery = "SELECT m.*,
                                                        CASE m.C_member WHEN 1 THEN '個案' WHEN 2 THEN '家屬' END AS memberType,
                                                        STR_TO_DATE(CONCAT(m.BD_yy, '-', m.BD_mm, '-', m.BD_dd), '%Y-%c-%e') AS birthday,
                                                        TIMESTAMPDIFF(YEAR, STR_TO_DATE(CONCAT(m.BD_yy, '-', m.BD_mm, '-', m.BD_dd), '%Y-%c-%e'), DATE(NOW())) AS age,
                                                        GROUP_CONCAT(DISTINCT rc.Cancer_name SEPARATOR ', ') AS 'Cancer_name'
                                                        FROM FCF_careservice.Memberdata m
                                                        LEFT JOIN FCF_careservice.Rec_cancer rc ON m.IDno=rc.MemberID AND rc.`status`=1
                                                        WHERE 1=1 AND m.C_status=1 AND m.C_member IN (1,2) AND m.C_name!='' AND m.assessForm=1
                                                        GROUP BY m.IDno ORDER BY m.C_member DESC, m.IDno";
                                            $result = $db->query($dbQuery);
                                            foreach ($result as $row) {
                                                $columnContent = '';
                                                foreach ($definition['col'] as $colname) {
                                                    $link = '';
                                                    $linkEnd = '';
                                                    if (
                                                        isset($definition['colLink'][$colname])
                                                        && $definition['colLink'][$colname]
                                                    ) {
                                                        $link = "<a href=\"./?" . http_build_query($_GET) . "&action=view&id={$row['IDno']}\">";
                                                        $linkEnd = '</a>';
                                                    }
                                                    // 性別
                                                    if ($colname == 'C_sex') {
                                                        $row[$colname] = ($row[$colname] == 'male' ? '男' : '女');
                                                    }
                                                    $columnContent .= "<td>{$link}{$row[$colname]}{$linkEnd}</td>";
                                                }
                                                $columnContent = nl2br($columnContent);
                                                echo "<tr>{$columnContent}</tr>";
                                            }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function init() {
    $('#memberTable').DataTable();
}

window.onload = init;
</script>