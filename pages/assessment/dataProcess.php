<?php
/**
 * 資料處理層，將php分配到不同頁面處理
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */
require_once 'vendor/autoload.php';
require 'settings/loginCheck.php';
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
$pageAction = reqParam('page', 'post', 'string');

switch ($pageAction) {
    case 'writeForm':
        // 評估量表紀錄 新增/修改/刪除
        require_once get_relative_path("pages/{$page}/forms/dataManage.php");
        break;

    case 'tracking':
        // 追蹤紀錄 新增/修改/刪除
        require_once get_relative_path("pages/{$page}/track/dataManage.php");
        break;

    default:
        break;
}

exit();