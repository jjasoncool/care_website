<h5><b>1. 社會資源使用現況</b></h5>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>身心障礙手冊：</label><br>
        <?php
        $disability_book = [
            '0' => '無',
            '1' => '有',
            '2' => '其他',
        ];

        foreach ($disability_book as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"disability_book\" field=\"disability_book\" id=\"disability_book{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"disability_book{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control form-control-sm" name="disability_other" field="disability_other">
        </div>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>其他資源輔助：</label><br>
        <?php
        $other_source = [
            '0' => '無',
            '1' => '有',
            '2' => '其他',
        ];

        foreach ($other_source as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"other_source\" field=\"other_source\" id=\"other_source{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"other_source{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control form-control-sm" name="source_other" field="source_other">
        </div>
    </div>
</div>
<h5><b>2. 社會生活適應</b></h5>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>目前工作狀態：</label><br>
        <?php
        $work_status = [
            '0' => '無工作',
            '1' => '有工作',
        ];

        foreach ($work_status as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"work_status\" field=\"work_status\" id=\"work_status{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"work_status{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>生活適應(病前)</label>
        <p class="text-danger">1 代表非常不佳，5 為非常良好</p>
        <?php
        $life_before = [
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
        ];

        foreach ($life_before as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"life_before\" field=\"life_before\" id=\"life_before{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"life_before{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>生活適應(病後)</label>
        <p class="text-danger">1 代表非常不佳，5 為非常良好</p>
        <?php
        $life_after = [
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
        ];

        foreach ($life_after as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"life_after\" field=\"life_after\" id=\"life_after{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"life_after{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<h5><b>3. 人際適應和社交生活</b></h5>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>人際關係(病前)</label>
        <p class="text-danger">1 代表非常不佳，5 為非常良好</p>
        <?php
        $relation_before = [
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
        ];

        foreach ($relation_before as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"relation_before\" field=\"relation_before\" id=\"relation_before{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"relation_before{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>人際關係(病後)</label>
        <p class="text-danger">1 代表非常不佳，5 為非常良好</p>
        <?php
        $relation_after = [
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
        ];

        foreach ($relation_after as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"relation_after\" field=\"relation_after\" id=\"relation_after{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"relation_after{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>社交生活的影響(病前)</label>
        <p class="text-danger">1 代表非常不佳，5 為非常良好</p>
        <?php
        $social_before = [
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
        ];

        foreach ($social_before as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"social_before\" field=\"social_before\" id=\"social_before{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"social_before{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>社交生活的影響(病後)</label>
        <p class="text-danger">1 代表非常不佳，5 為非常良好</p>
        <?php
        $social_after = [
            '1' => '1',
            '2' => '2',
            '3' => '3',
            '4' => '4',
            '5' => '5',
        ];

        foreach ($social_after as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"social_after\" field=\"social_after\" id=\"social_after{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"social_after{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>過去ㄧ週內你曾與他人(親戚、朋友以及其他)電話交談？</label><br>
        <?php
        $tele_talk = [
            '0' => '從未有過',
            '1' => '一次',
            '2' => '兩次',
            '3' => '三次',
            '4' => '每天ㄧ次或更多',
        ];

        foreach ($tele_talk as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"tele_talk\" field=\"tele_talk\" id=\"tele_talk{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"tele_talk{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>過去ㄧ週中你曾與你同住的人相聚(你去拜訪或他來拜訪或ㄧ同外出)？</label><br>
        <?php
        $get_together = [
            '0' => '從未有過',
            '1' => '一次',
            '2' => '兩次',
            '3' => '三次',
            '4' => '每天ㄧ次或更多',
        ];

        foreach ($get_together as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"get_together\" field=\"get_together\" id=\"get_together{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"get_together{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>你是否有信任及可靠的人(家人、朋友)？</label><br>
        <?php
        $trust_people = [
            '0' => '無',
            '1' => '有',
        ];

        foreach ($trust_people as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"trust_people\" field=\"trust_people\" id=\"trust_people{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"trust_people{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>是否有人能在你生病時給你任何協助，如配偶、家人或朋友？</label><br>
        <?php
        $give_help = [
            '0' => '否',
            '1' => '是，',
        ];

        foreach ($give_help as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"give_help\" field=\"give_help\" id=\"give_help{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"give_help{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control form-control-sm" name="give_help_peop" field="give_help_peop" placeholder="協助人員">
        </div>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>承上題，可提供什麼協助？</label><br>
        <?php
        $give_help_type = [
            '1' => '經濟方面',
            '2' => '醫療方面',
            '3' => '照顧方面',
            '4' => '情感方面',
        ];

        foreach ($give_help_type as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label my-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"{$value}\" name=\"give_help_type[]\" field=\"give_help_type{$value}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>是否有人能照顧你，或僅是短期照顧或偶爾協助?<br>(如帶你看醫生或備餐點)</label><br>
        <?php
        $take_care = [
            '1' => '無',
            '2' => '幾乎沒有',
            '3' => '偶而',
            '4' => '短期照顧(數週至六個月)',
            '5' => '無限期協助',
        ];

        foreach ($take_care as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"take_care\" field=\"take_care\" id=\"take_care{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"take_care{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>