<?php

$scoreArray = [
    'nurse' => [
        'physical',
        'mucosal',
        'pain_level',
        'exhaust_level',
        'nutrition_tube',
        'special_tube',
        'caregiver',
    ],
    'nutrition' => [
        'weight_change',
        'eat_change',
        'eat_only',
        'symptom',
        'body_status',
    ],
    'social' => [
        'disability_book',
        'other_source',
        'work_status',
        'life_before',
        'life_after',
        'relation_before',
        'relation_after',
        'social_before',
        'social_after',
        'tele_talk',
        'get_together',
        'trust_people',
        'give_help',
        'give_help_type',
        'take_care',
    ],
    'mental' => [
        'mind_trouble',
    ],
];

$specialArray = [
    'nurse' => [
        'pain_level' => [
            '0' => 0,
            '1' => 1,
            '2' => 1,
            '3' => 1,
            '4' => 2,
            '5' => 2,
            '6' => 2,
            '7' => 3,
            '8' => 3,
            '9' => 3,
            '10' => 3,
        ],
        'exhaust_level' => [
            '0' => 0,
            '1' => 1,
            '2' => 1,
            '3' => 1,
            '4' => 2,
            '5' => 2,
            '6' => 2,
            '7' => 3,
            '8' => 3,
            '9' => 3,
            '10' => 3,
        ],
        'caregiver' => [
            '0' => 1,
            '1' => 0,
        ],
    ],
    'nutrition' => [
        'weight_change' => [
            '0' => 0,
            '1' => 1,
            '2' => 0,
        ],
        'eat_change' => [
            '0' => 0,
            '1' => 1,
            '2' => 0,
        ],
        'eat_only' => [
            '0' => 1,
            '1' => 2,
            '2' => 3,
            '3' => 3,
            '4' => 4,
            '5' => 0,
        ],
        'symptom' => [
            '0' => 0,
            '1' => 3,
            '2' => 1,
            '3' => 3,
            '4' => 1,
            '5' => 3,
            '6' => 2,
            '7' => 1,
            '8' => 3,
            '9' => 1,
            '10' => 2,
            '11' => 1,
        ],
        'body_status' => [
            '0' => 0,
            '1' => 1,
            '2' => 2,
            '3' => 3,
            '4' => 3,
        ],
    ],
    'social' => [
        'disability_book' => [
            '0' => 1,
            '1' => 0,
            '2' => 0,
        ],
        'other_source' => [
            '0' => 1,
            '1' => 0,
            '2' => 0,
        ],
        'tele_talk' => [
            '0' => 1,
            '1' => 2,
            '2' => 3,
            '3' => 4,
            '4' => 5,
        ],
        'get_together' => [
            '0' => 1,
            '1' => 2,
            '2' => 3,
            '3' => 4,
            '4' => 5,
        ],
        'give_help_type' => [
            '1' => 1,
            '2' => 1,
            '3' => 1,
            '4' => 1,
        ],
    ],
];

// 單純欄位計算
if (isset($scoreArray[$formType])) {
    foreach ($scoreArray[$formType] as $colname) {
        if (isset($specialArray[$formType][$colname])) {
            // 一個欄位有多個複選的情況
            if (is_array($inputData[$colname])) {
                foreach ($inputData[$colname] as $selectValue) {
                    $inputData['score'] += $specialArray[$formType][$colname][$selectValue];
                }
            } else {
                $inputData['score'] += $specialArray[$formType][$colname][strval(intval($inputData[$colname]))];
            }
        } else {
            $inputData['score'] += $inputData[$colname];
        }
    }
}

// 特殊計算
switch ($formType) {
    case 'nutrition':
        // 寫死，很醜，但目前沒有比較好的方式
        if (strlen($inputData['diagnosis']) > 0) {
            $inputData['score']++;
        }
        if ($inputData['age'] >= 65) {
            $inputData['score']++;
        }

        $weightScoreYear = 0;
        $halfyaerweight = ($inputData['weight_year'] - $inputData['weight']) / $inputData['weight_year'];
        if ($halfyaerweight >= 0.2) {
            $weightScoreYear = 4;
        } elseif ($halfyaerweight >= 0.1 && $halfyaerweight < 0.2) {
            $weightScoreYear = 3;
        } elseif ($halfyaerweight >= 0.06 && $halfyaerweight < 0.1) {
            $weightScoreYear = 2;
        } elseif ($halfyaerweight >= 0.02 && $halfyaerweight < 0.06) {
            $weightScoreYear = 1;
        }
        $weightScoreMonth = 0;
        $monthweight = ($inputData['weight_month'] - $inputData['weight']) / $inputData['weight_month'];
        if ($monthweight >= 0.1) {
            $weightScoreMonth = 4;
        } elseif ($monthweight >= 0.05 && $monthweight < 0.1) {
            $weightScoreMonth = 3;
        } elseif ($monthweight >= 0.03 && $monthweight < 0.05) {
            $weightScoreMonth = 2;
        } elseif ($monthweight >= 0.02 && $monthweight < 0.03) {
            $weightScoreMonth = 1;
        }

        $inputData['score'] += max($weightScoreYear, $weightScoreMonth);

        break;
}
