<div class="form-row px-2">
    <div class="form-group col-md-12">
        <h5><b>1. 個案目前體能狀態評估(ECOG)</b></h5>
        <?php
        $physicalArray = [
            '0' => '活動力與罹病前無異，不受疾病影響。',
            '1' => '無法做劇烈活動，但可走動與從事輕鬆或坐著的工作。',
            '2' => '可以走動，可以自我照顧，但無法工作，>50%以上的清醒時刻是可以下床。',
            '3' => '自我照顧能力有限，>50%以上的清醒時刻需臥床或坐輪椅。',
            '4' => '處於完全失能狀態，生活無法自理，完全臥床。',
        ];

        foreach ($physicalArray as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"physical\" field=\"physical\" id=\"physical{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"physical{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <h5><b>2. 是否因治療造成腸胃道黏膜受損情形</b></h5>
        <?php
        $mucosalArray = [
            '0' => '否',
            '1' => '是，請勾選：',
        ];

        foreach ($mucosalArray as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"mucosal\" field=\"mucosal\" id=\"mucosal{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"mucosal{$value}\">{$label}</label>
                </div>
            </div>";
        }

        $mucosalArea = [
            '1' => '腹瀉',
            '2' => '口腔、咽喉黏膜潰瘍',
        ];

        foreach ($mucosalArea as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label my-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"{$value}\" name=\"mucosal_area[]\" field=\"mucosal_area{$value}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-3">
        <label for="mucosal_grade">咽喉黏膜潰瘍等級</label>
        <select class="form-control py-1" id="mucosal_grade" name="mucosal_grade" field="mucosal_grade">
            <?php
            $mucosalGrade = [
                '0' => '---',
                '1' => '第一級',
                '2' => '第二級',
                '3' => '第三級',
                '4' => '第四級',
            ];
            foreach ($mucosalGrade as $key => $value) {
                echo "<option value=\"{$key}\">{$value}</option>";
            }
            ?>
        </select>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <table class="table table-sm table-hover">
            <thead>
                <tr>
                    <th width="60px">等 級</th>
                    <th>症 狀</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                        <td scope="row"><b>第一級</b></td>
                        <td><span style="color: #00A600">紅、腫、痛、無潰瘍</span></td>
                    </tr>
                    <tr>
                        <td scope="row"><b>第二級</b></td>
                        <td><span style="color: #FF8000">紅、腫、潰瘍(多處小潰瘍或白斑，範圍＜25%)，可吃固體食物</span></td>
                    </tr>
                    <tr>
                        <td scope="row"><b>第三級</b></td>
                        <td><span style="color: #F75000">紅、腫、潰瘍（融合性大潰瘍或白斑，範圍25~50%），只能吃流質食物</span></td>
                    </tr>
                    <tr>
                        <td scope="row"><b>第四級</b></td>
                        <td><span style="color: #AE0000">出血性潰瘍範圍＞50%，無法由口腔進食</span></td>
                    </tr>
                </tbody>
        </table>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <h5><b>3. 目前是否有疼痛問題</b></h5>
        <?php
        $painArray = [
            '0' => '否',
            '1' => '是',
        ];

        foreach ($painArray as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"pain\" field=\"pain\" id=\"pain{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"pain{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
        <div class="form-check form-check-inline mr-0">
            <label class="form-check-label">
                <span class="form-check-sign unselectable">疼痛部位</span>
            </label>
        </div>
        <div class="form-check-inline">
            <input type="text" class="form-control form-control-sm" name="pain_area" field="pain_area">
        </div>
        <div class="form-check form-check-inline mr-0">
            <label class="form-check-label">
                <span class="form-check-sign unselectable">止痛用藥</span>
            </label>
        </div>
        <div class="form-check-inline">
            <input type="text" class="form-control form-control-sm" name="pain_analgesics" field="pain_analgesics">
        </div>
    </div>
    <div class="form-group col-md-3">
        <label for="pain_level">疼痛指數</label>
        <select class="form-control py-1" id="pain_level" name="pain_level" field="pain_level">
            <?php
            $painLevel = [
                '0' => '---',
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
            ];
            foreach ($painLevel as $key => $value) {
                echo "<option value=\"{$key}\">{$value}</option>";
            }
            ?>
        </select>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <img src="/uploads/images/pain_table.png" alt="疼痛量表" style="max-width:100%; max-height:200px;">
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-3">
        <h5><b>4. 是否有癌因性疲憊的狀況</b></h5>
        <?php
        $exhaustedArray = [
            '0' => '否',
            '1' => '是',
        ];

        foreach ($exhaustedArray as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"exhausted\" field=\"exhausted\" id=\"exhausted{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"exhausted{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-3">
        <label for="exhaust_level">癌因性疲憊指數</label>
        <select class="form-control py-1" id="exhaust_level" name="exhaust_level" field="exhaust_level">
            <?php
            $exhaustLevel = [
                '0' => '---',
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
            ];
            foreach ($exhaustLevel as $key => $value) {
                echo "<option value=\"{$key}\">{$value}</option>";
            }
            ?>
        </select>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <h5><b>5. 有無供給營養補充之管路或使用特殊營養品</b></h5>
        <?php
        $nutritionTube = [
            '0' => '否',
            '1' => '是，請勾選：',
        ];

        foreach ($nutritionTube as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"nutrition_tube\" field=\"nutrition_tube\" id=\"nutrition_tube{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"nutrition_tube{$value}\">{$label}</label>
                </div>
            </div>";
        }

        $nutrition_tube_type = [
            '1' => '鼻胃管',
            '2' => '胃管灌食',
            '3' => '空腸灌食',
        ];

        foreach ($nutrition_tube_type as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label my-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"{$value}\" name=\"nutrition_tube_type[]\" field=\"nutrition_tube_type{$value}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <h5><b>6. 身上有無其他特殊管路</b></h5>
        <?php
        $specialTube = [
            '0' => '否',
            '1' => '是，請勾選：',
        ];

        foreach ($specialTube as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"special_tube\" field=\"special_tube\" id=\"special_tube{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"special_tube{$value}\">{$label}</label>
                </div>
            </div>";
        }

        $special_tube_type = [
            '1' => '氣切',
            '2' => '尿管',
            '3' => '人工肛門',
            '4' => '洗腎用動靜脈廔管',
        ];

        foreach ($special_tube_type as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label my-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"{$value}\" name=\"special_tube_type[]\" field=\"special_tube_type{$value}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <h5><b>7. 治療期間是否有照顧者協助</b></h5>
        <?php
        $caregiver = [
            '0' => '否',
            '1' => '是',
        ];

        foreach ($caregiver as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"caregiver\" field=\"caregiver\" id=\"caregiver{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"caregiver{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>