<?php
/**
 * 新增/編輯評估量表
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param array $inputData 使用者輸入的資料
 * @param array $basicData 接受的欄位資料
 * @param string $formType 送出評估量表樣式
 * @param int $dataID 紀錄編號
 * @param int $id 個案編號
 */

require_once get_relative_path("pages/{$page}/dataDefinition.php");

$id = intval(reqParam('id', 'get'));
$formType = strval(reqParam('formType', 'get'));
$formAction = reqParam('action', 'post');
$dataID = intval(reqParam('dataID', 'post'));

$inputData = ['score' => 0];
switch ($formType) {
    case 'nurse':
        $basicData = [
            'physical' => 'int',
            'mucosal' => 'boolean',
            'mucosal_area' => 'string',
            'mucosal_grade' => 'int',
            'pain' => 'boolean',
            'pain_area' => 'string',
            'pain_analgesics' => 'string',
            'pain_level' => 'int',
            'exhausted' => 'boolean',
            'exhaust_level' => 'int',
            'nutrition_tube' => 'boolean',
            'nutrition_tube_type' => 'string',
            'special_tube' => 'boolean',
            'special_tube_type' => 'string',
            'caregiver' => 'boolean',
            'R_note' => 'string',
        ];
        break;
    case 'nutrition':
        $basicData = [
            'height' => 'int',
            'weight' => 'int',
            'weight_year' => 'int',
            'weight_month' => 'int',
            'weight_change' => 'int',
            'eat_change' => 'int',
            'eat_only' => 'int',
            'other_nutrition' => 'boolean',
            'use_nutrition' => 'string',
            'symptom' => 'string',
            'symptom_pain' => 'string',
            'symptom_other' => 'string',
            'body_status' => 'int',
            'diagnosis' => 'string',
            'disease_level' => 'int',
            'age' => 'int',
            'R_note' => 'string',
        ];
        break;
    case 'social':
        $basicData = [
            'disability_book' => 'boolean',
            'other_source' => 'boolean',
            'disability_other' => 'string',
            'source_other' => 'string',
            'work_status' => 'boolean',
            'life_before' => 'int',
            'life_after' => 'int',
            'relation_before' => 'int',
            'relation_after' => 'int',
            'social_before' => 'int',
            'social_after' => 'int',
            'tele_talk' => 'int',
            'get_together' => 'int',
            'trust_people' => 'boolean',
            'give_help' => 'boolean',
            'give_help_peop' => 'string',
            'give_help_type' => 'string',
            'take_care' => 'int',
            'R_note' => 'string',
        ];
        break;
    case 'mental':
        $basicData = [
            'mind_trouble' => 'int',
            'life_child' => 'boolean',
            'life_live' => 'boolean',
            'life_eco' => 'boolean',
            'life_traffic' => 'boolean',
            'life_work' => 'boolean',
            'life_treatment' => 'boolean',
            'mood_melancholy' => 'boolean',
            'mood_afraid' => 'boolean',
            'mood_tension' => 'boolean',
            'mood_sad' => 'boolean',
            'mood_worry' => 'boolean',
            'mood_lose_interest' => 'boolean',
            'family_kid' => 'boolean',
            'family_mate' => 'boolean',
            'family_infertility' => 'boolean',
            'family_health' => 'boolean',
            'religious' => 'boolean',
            'body_exterior' => 'boolean',
            'body_bath_dress' => 'boolean',
            'body_breathe' => 'boolean',
            'body_pee' => 'boolean',
            'body_constipation' => 'boolean',
            'body_diarrhea' => 'boolean',
            'body_eat' => 'boolean',
            'body_tired' => 'boolean',
            'body_swelling' => 'boolean',
            'body_fever' => 'boolean',
            'body_action' => 'boolean',
            'body_indigestion' => 'boolean',
            'body_memory' => 'boolean',
            'body_mouth' => 'boolean',
            'body_nausea' => 'boolean',
            'body_nose' => 'boolean',
            'body_pain' => 'boolean',
            'body_sexual' => 'boolean',
            'body_skin' => 'boolean',
            'body_sleep' => 'boolean',
            'body_abuse' => 'boolean',
            'body_tingling' => 'boolean',
            'R_note' => 'string',
        ];
        break;

    default:
        $basicData = [];
        break;
}

// 基本資料
foreach ($basicData as $inputName => $inputType) {
    $inputData[$inputName] = reqParam($inputName, 'post', $inputType);
}

// 計算分數
require_once 'scoreCalculate.php';

// 單一欄位儲存多選選項
$multiCheck = ['symptom', 'give_help_type', 'mucosal_area', 'nutrition_tube_type', 'special_tube_type'];
foreach ($multiCheck as $key => $colname) {
    if (isset($inputData[$colname]) && is_array($inputData[$colname])) {
        $inputData[$colname] = implode(',', $inputData[$colname]);
    }
}

// 紀錄基本資料
$dt = new dateTime();
$recPeople = [
    "Keydate" => $dt->format('Y-m-d H:i:s'),
    'MemberID' => $id,
    'AdminID' => $generalData['userid'],
    'AdName' => $generalData['username'],
];

// 判斷資料類型
if ($formAction === 'add') {
    $insColArray = array_merge($recPeople, $inputData);
    $insColStr = implode('`,`', array_keys($insColArray));
    $insParaStr = implode(",", array_fill(0, count($insColArray), "?"));
    // SQL 用基本資料產出
    $dbQuery = "INSERT INTO FCF_careservice.{$assessArray[$formType]['table']} (
        `{$insColStr}`
    ) VALUES (
        {$insParaStr}
    )";
    $db->query($dbQuery, array_values($insColArray));
} elseif ($formAction === 'mod') {
    // 更新時，不可變更個案ID了
    unset($recPeople['MemberID']);
    $updColArray = array_merge($recPeople, $inputData);
    $updColStr = '';
    foreach ($updColArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    $dbQuery = "UPDATE FCF_careservice.{$assessArray[$formType]['table']}
                SET {$updColStr}
                WHERE IDno=?";
    $db->query($dbQuery, array_merge(array_values($updColArray), [$dataID]));
} elseif ($formAction === 'del') {
    // 刪除，取消此個案與此紀錄之關聯
    $dbQuery = "UPDATE FCF_careservice.{$assessArray[$formType]['table']}
                SET MemberID=-MemberID, AdmincheckID=?, Admincheck=1, checktime=NOW()
                WHERE IDno=?";
    $db->query($dbQuery, [$generalData['userid'], $dataID]);
}

// 若第一次寫評估量表會把四份跑完
toAssessPage();

// 撈出填寫資訊
$dbQuery = "SELECT m.C_name, a.Acc_area
FROM FCF_careservice.Memberdata m
LEFT JOIN FCF_careservice.Accuser a ON m.AdminID=a.IDno
WHERE m.IDno=?";
$result = $db->row($dbQuery, [$id]);
$trackData = [
    "Opendate" => $dt->format('Y-m-d H:i:s'),
    'MemberID' => $id,
    'T_name' => $result['C_name'],
];

toViewPage($trackData);

/**
 * 評估量表填寫完成，也代表需要新增一筆追蹤
 *
 * @param array $trackData
 * @return void
 */
function toViewPage($trackData)
{
    global $db, $generalData, $page, $trackArray, $formAction;

    if ($formAction === 'add') {
        // 追蹤紀錄
        $insColStr = implode('`,`', array_keys($trackData));
        $insParaStr = implode(",", array_fill(0, count($trackData), "?"));
        $dbQuery = "INSERT INTO FCF_careservice.{$trackArray['table']} (
            `{$insColStr}`
        ) VALUES (
            {$insParaStr}
        )";
        $db->query($dbQuery, array_values($trackData));
        // 新增追蹤時通知窗口
        addNotification(0, 1, "?page={$page}&action=view&id={$trackData['MemberID']}", "{$_SESSION['username']}\n已新增個案{$trackData['T_name']}的追蹤紀錄");
    }

    if ($generalData['userLevel'] > 2) {
        // 一般個案填寫完成，回登入頁
        header("Location:/login.php?thank=1");
        exit();
    } else {
        // 回到頁面
        $_GET['action'] = 'view';
        unset($_GET['dataID']);
        header("Location:?" . http_build_query($_GET) . '#' . $_GET['formType']);
        exit();
    }
}

/**
 * 轉至評估量表，若都有填寫過一次則不會再轉
 *
 * @return void
 */
function toAssessPage()
{
    global $id, $db;

    $dbQuery = "SELECT COUNT(DISTINCT ns.IDno) AS nurse, COUNT(DISTINCT nt.IDno) AS nutrition, COUNT(DISTINCT s.IDno) AS social, COUNT(DISTINCT mt.IDno) AS mental
                FROM FCF_careservice.Memberdata m
                LEFT JOIN FCF_careservice.assess_nursing ns ON m.IDno=ns.MemberID
                LEFT JOIN FCF_careservice.assess_nutrition nt ON m.IDno=nt.MemberID
                LEFT JOIN FCF_careservice.assess_social s ON m.IDno=s.MemberID
                LEFT JOIN FCF_careservice.assess_mental mt ON m.IDno=mt.MemberID
                WHERE m.IDno=?";
    $result = $db->row($dbQuery, [$id]);
    $assessType = array_search(0, $result);

    if ($assessType != false) {
        $_GET['formType'] = $assessType;
        header("Location:?" . http_build_query($_GET));
        exit();
    }
}
