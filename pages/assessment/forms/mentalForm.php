<h5><b>1. 在最近一星期，您的心理困擾程度有多大，請以數字表示。</b></h5>
<div class="form-row px-2">
    <div class="form-group col-md-3">
        <label for="mind_trouble">(0為無困擾，10為極度困擾)</label>
        <select class="form-control py-1" id="mind_trouble" name="mind_trouble" field="mind_trouble">
            <?php
            $mindTrouble = [
                '0' => '0',
                '1' => '1',
                '2' => '2',
                '3' => '3',
                '4' => '4',
                '5' => '5',
                '6' => '6',
                '7' => '7',
                '8' => '8',
                '9' => '9',
                '10' => '10',
            ];
            foreach ($mindTrouble as $key => $value) {
                echo "<option value=\"{$key}\">{$value}</option>";
            }
            ?>
        </select>
    </div>
</div>
<h5><b>2. 在最近一星期，您是否有下列的問題，請勾選符合項目。</b></h5>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>實際的問題</label><br>
        <?php
        $optionArray = [
            'life_child' => '照護小孩方面',
            'life_live' => '居住方面',
            'life_eco' => '保險/經濟方面',
            'life_traffic' => '交通方面',
            'life_work' => '工作或就學方面',
            'life_treatment' => '治療方式的決定',
        ];

        foreach ($optionArray as $colname => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label my-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"$colname\" field=\"$colname\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>情緒的問題</label><br>
        <?php
        $optionArray = [
            'mood_melancholy' => '憂鬱',
            'mood_afraid' => '害怕',
            'mood_tension' => '緊張',
            'mood_sad' => '難過、傷心',
            'mood_worry' => '擔心',
            'mood_lose_interest' => '對日常活動失去興趣',
        ];

        foreach ($optionArray as $colname => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label my-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"$colname\" field=\"$colname\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>家庭的問題</label><br>
        <?php
        $optionArray = [
            'family_kid' => '和小孩相處方面',
            'family_mate' => '和伴侶相處方面',
            'family_infertility' => '擔心不孕',
            'family_health' => '家人健康的議題',
        ];

        foreach ($optionArray as $colname => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label my-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"$colname\" field=\"$colname\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>靈性或宗教的問題</label><br>
        <?php
        $optionArray = [
            'religious' => '靈性或宗教的問題',
        ];

        foreach ($optionArray as $colname => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label my-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"$colname\" field=\"$colname\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>身體的問題</label><br>
        <?php
        $optionArray = [
            'body_exterior' => '外觀方面',
            'body_bath_dress' => '沐浴或穿衣方面',
            'body_breathe' => '呼吸方面',
            'body_pee' => '小便方面',
            'body_constipation' => '便祕',
            'body_diarrhea' => '腹瀉',
            'body_eat' => '進食方面',
            'body_tired' => '疲倦',
            'body_swelling' => '腫脹感',
            'body_fever' => '發燒',
            'body_action' => '行動方面',
            'body_indigestion' => '消化不良',
            'body_memory' => '記憶力或專注力方面',
            'body_mouth' => '嘴巴破皮',
            'body_nausea' => '噁心',
            'body_nose' => '鼻子乾或鼻塞',
            'body_pain' => '疼痛',
            'body_sexual' => '性生活方面',
            'body_skin' => '皮膚乾或癢',
            'body_sleep' => '睡眠方面',
            'body_abuse' => '物質濫用',
            'body_tingling' => '手或腳有刺痛感',
        ];

        foreach ($optionArray as $colname => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label my-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"$colname\" field=\"$colname\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>