<h5><b>1. 體重變化</b></h5>
<div class="form-row px-2">
    <div class="form-group col-md-3">
        <label for="height">我的身高約</label>
        <div class="input-group">
            <input type="text" class="form-control" id="height" name="height" field="height" required>
            <div class="input-group-append">
                <span class="input-group-text">公分</span>
            </div>
        </div>
    </div>
    <div class="form-group col-md-3">
        <label for="weight">我目前體重約</label>
        <div class="input-group">
            <input type="text" class="form-control" id="weight" name="weight" field="weight" required>
            <div class="input-group-append">
                <span class="input-group-text">公斤</span>
            </div>
        </div>
    </div>
    <div class="form-group col-md-3">
        <label for="weight_year">半年前我的體重大約</label>
        <div class="input-group">
            <input type="text" class="form-control" id="weight_year" name="weight_year" field="weight_year" required>
            <div class="input-group-append">
                <span class="input-group-text">公斤</span>
            </div>
        </div>
    </div>
    <div class="form-group col-md-3">
        <label for="weight_month">一個月前我的體重大約</label>
        <div class="input-group">
            <input type="text" class="form-control" id="weight_month" name="weight_month" field="weight_month" required>
            <div class="input-group-append">
                <span class="input-group-text">公斤</span>
            </div>
        </div>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>過去二星期，我的體重是呈現：</label><br>
        <?php
        $weightChange = [
            '0' => '沒有改變',
            '1' => '減少',
            '2' => '增加',
        ];

        foreach ($weightChange as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"weight_change\" field=\"weight_change\" id=\"weight_change{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"weight_change{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<h5><b>2. 飲食情況</b></h5>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>在過去幾個月來我吃食物的量與以往相比：</label><br>
        <?php
        $eatChange = [
            '0' => '沒有改變',
            '1' => '比以前少',
            '2' => '比以前多',
        ];

        foreach ($eatChange as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"eat_change\" field=\"eat_change\" id=\"eat_change{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"eat_change{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>我現在只吃：</label><br>
        <?php
        $eatOnly = [
            '0' => '比正常量少的一般食物',
            '1' => '只吃一點固體食物',
            '2' => '大部份都吃液體食物',
            '3' => '大部份都吃營養補充品',
            '4' => '非常少的任何食物',
            '5' => '管灌餵食或由靜脈注射營養',
        ];

        foreach ($eatOnly as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"eat_only\" field=\"eat_only\" id=\"eat_only{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"eat_only{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>* 除了正餐外，是否有補充其他營養品？</label><br>
        <?php
        $eatOnly = [
            '0' => '否',
            '1' => '是',
        ];

        foreach ($eatOnly as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"other_nutrition\" field=\"other_nutrition\" id=\"other_nutrition{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"other_nutrition{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
        <div class="form-check form-check-inline mr-0">
            <label class="form-check-label">
                <span class="form-check-sign unselectable">請敘述營養補充品項及使用頻率：</span>
            </label>
        </div>
        <div class="form-check-inline">
            <input type="text" class="form-control form-control-sm" name="use_nutrition" field="use_nutrition">
        </div>
    </div>
</div>
<h5><b>3. 症狀</b></h5>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>過去二個星期，我有下列的問題困擾，使我無法吃的足夠：<br>（請詳細檢察下列所有項目）</label>
        <div class="card border-warning mb-3" style="width: 18rem; border: solid 1px;">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">＊腹瀉：一天超過3次以上、不成型軟便</li>
                <li class="list-group-item">＊便祕：每週解便次數少於三次或排便用力且排出乾硬的糞便</li>
            </ul>
        </div>
        <?php
        $symptom = [
            '0' => '沒有飲食方面的問題',
            '1' => '沒有食慾，就是不想吃',
            '2' => '噁心',
            '3' => '嘔吐',
            '4' => '便祕',
            '5' => '腹瀉',
            '6' => '口痛',
            '7' => '口乾',
            '8' => '疼痛',
            '9' => '吃起來感覺沒有味道，或味道變得奇怪',
            '10' => '有怪味困擾著我',
            '11' => '其他',
        ];

        foreach ($symptom as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label my-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"{$value}\" name=\"symptom[]\" field=\"symptom{$value}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
        <div class="form-check form-check-inline mr-0">
            <label class="form-check-label">
                <span class="form-check-sign unselectable">若有疼痛，何處?：</span>
            </label>
        </div>
        <div class="form-check-inline">
            <input type="text" class="form-control form-control-sm" name="symptom_pain" field="symptom_pain">
        </div>
        <div class="form-check form-check-inline mr-0">
            <label class="form-check-label">
                <span class="form-check-sign unselectable">其他症狀</span>
            </label>
        </div>
        <div class="form-check-inline">
            <input type="text" class="form-control form-control-sm" name="symptom_other" field="symptom_other">
        </div>
    </div>
</div>
<h5><b>4. 身體狀況</b></h5>
<div class="form-row px-2">
    <div class="form-group col-md-12">
        <label>自我評估過去幾個月來，身體狀況處於：</label><br>
        <?php
        $bodyStatus = [
            '0' => '正常沒有任何限制',
            '1' => '與平常的我不同，但日常生活起居還能自我料理',
            '2' => '感覺不舒服，但躺在床上的時間不會長於半天',
            '3' => '只能做少數活動，大多數時間躺在床上或坐在椅子',
            '4' => '絕大多數的時間躺在床上',
        ];

        foreach ($bodyStatus as $value => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <div class=\"custom-control custom-radio\">
                    <input class=\"custom-control-input\" type=\"radio\" value=\"{$value}\" name=\"body_status\" field=\"body_status\" id=\"body_status{$value}\" required>
                    <label class=\"custom-control-label my-0 unselectable\" for=\"body_status{$value}\">{$label}</label>
                </div>
            </div>";
        }
        ?>
    </div>
</div>
<h5><b>5. 疾病及與相關營養需求</b></h5>
<div class="form-row px-2">
    <div class="form-group col-md-3">
        <label for="diagnosis">疾病診斷</label>
        <select class="form-control" id="diagnosis" name="diagnosis" field="diagnosis">
            <option value="">---</option>
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="disease_level">源發疾病分期</label>
        <select class="form-control py-1" id="disease_level" name="disease_level" field="disease_level">
            <?php
            $diseaseLevel = [
                '0' => '---',
                '1' => 'I',
                '2' => 'II',
                '3' => 'III',
                '4' => 'IV',
                '5' => '其他',
            ];
            foreach ($diseaseLevel as $key => $value) {
                echo "<option value=\"{$key}\">{$value}</option>";
            }
            ?>
        </select>
    </div>
    <div class="form-group col-md-3">
        <label for="age">年齡</label>
        <div class="input-group">
            <input type="number" class="form-control" id="age" name="age" field="age" min="0" required>
            <div class="input-group-append">
                <span class="input-group-text">歲</span>
            </div>
        </div>
    </div>
</div>

