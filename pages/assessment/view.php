<?php
/**
 * 查看基本資料與評估量表
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 * @param int $id 個案編號
 */
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
require_once get_relative_path("pages/{$page}/dataDefinition.php");
// 如果這邊有帶入ID 必須先撈資料
$id = intval(reqParam('id', 'get'));

if ($generalData['userLevel'] > $listItems[$page]['permissions']) {
    header("Location: " . get_relative_path("pages/404.php"));
    exit();
}

// 個案連結
$dbQuery = "SELECT * FROM FCF_careservice.Memberdata WHERE IDno=?";
$row = $db->row($dbQuery, [$id]);
if ($row['C_member'] == 1) {
    $caseSubPage = 'personalCase';
} elseif ($row['C_member'] == 2) {
    $caseSubPage = 'family';
}
$caseLink ="<a class=\"text-primary\" href=\"./?page=person&sub={$caseSubPage}&action=view&id={$row['IDno']}\">{$row['C_name']}</a>";

// 追蹤表格標頭
$trackcolumnHead = '';
$trackrowContent = '';
foreach ($trackArray['colhead'] as $str) {
    $trackcolumnHead .= "<th>{$str}</th>";
}
$dbQuery = "SELECT t.*, DATE(t.Opendate) AS opdate, DATE(t.judgedate) AS jgdate,
            CASE t.T_status WHEN 1 THEN '已追蹤完成' WHEN 0 THEN '待追蹤' END AS status_text,
            CASE t.assess_type WHEN 1 THEN '護理' WHEN 2 THEN '營養' WHEN 3 THEN '社工' WHEN 4 THEN '心理' ELSE '' END AS trackType,
            j.Acc_name AS judgeName, a.Acc_name AS ADname
            FROM FCF_careservice.Tracklist t
            LEFT JOIN FCF_careservice.Accuser j ON t.judgeID=j.IDno
            LEFT JOIN FCF_careservice.Accuser a ON t.AdminID=a.IDno
            WHERE t.MemberID=?
            ORDER BY t.T_status, t.Opendate DESC";
$result = $db->query($dbQuery, [$id]);
// 管理者才可以刪除追蹤紀錄
$delBtn = '';
if ($generalData['userLevel'] == 1) {
    $delBtn = "<button type=\"button\" title=\"刪除\" class=\"btn btn-link btn-danger py-0 pl-2\"
                data-toggle=\"modal\" data-target=\"#delTrack\">
                    <i class=\"fa fa-times fa-lg\"></i>
                </button>";
}
foreach ($result as $row) {
    $columnContent = '';
    $rowdata = htmlspecialchars(json_encode($row, JSON_UNESCAPED_UNICODE), ENT_QUOTES);
    foreach ($trackArray['col'] as $colname) {
        $columnContent .= "<td>{$row[$colname]}</td>";
    }
    $trackBtn = '';
    if ($row['T_status'] == 0) {
        $trackBtn = "<button type=\"button\" title=\"結束追蹤\" class=\"btn btn-link btn-success py-0 px-2\"
                        data-act=\"close\" data-toggle=\"modal\" data-target=\"#completeTrack\">
                        <i class=\"fas fa-check fa-lg\"></i>
                    </button>";
    }
    $trackrowContent .= "<tr>
                        {$columnContent}
                        <td style=\"width: 15%\">
                            <div class=\"form-button-action\">
                                <input type=\"hidden\" class=\"rowdata\" value=\"{$rowdata}\">
                                <button type=\"button\" title=\"修改\" class=\"btn btn-link btn-primary py-0 px-2\"
                                    data-act=\"mod\" data-toggle=\"modal\" data-target=\"#modalTrack\">
                                    <i class=\"fa fa-edit fa-lg\"></i>
                                </button>
                                {$trackBtn}
                                {$delBtn}
                            </div>
                        </td>
                    </tr>";
}

// 評估量表類型
$recTypeArray = array_keys($assessArray);

// 評估量表內容
$countentArray = [];
foreach ($assessArray as $key => $detail) {
    $columnHead = '';
    $rowContent = '';
    $dbQuery = "SELECT m.C_name, d.*, DATE(d.Keydate) AS key_date
                FROM FCF_careservice.{$detail['table']} d
                INNER JOIN FCF_careservice.Memberdata m ON d.MemberID=m.IDno
                WHERE d.MemberID=?";
    $result = $db->query($dbQuery, array($id));

    // 表格標頭
    foreach ($detail['colhead'] as $str) {
        $columnHead .= "<th>{$str}</th>";
    }
    // 管理者才可以刪除評估量表
    $delBtn = '';
    if ($generalData['userLevel'] == 1) {
        $delBtn = "<button type=\"button\" title=\"刪除\" class=\"btn btn-link btn-danger py-0 pl-2\"
                    data-toggle=\"modal\" data-target=\"#delForm\" data-type=\"$key\">
                        <i class=\"fa fa-times fa-lg\"></i>
                    </button>";
    }
    // 輸出每一列
    foreach ($result as $row) {
        $columnContent = '';
        foreach ($detail['col'] as $colname) {
            $columnContent .= "<td>{$row[$colname]}</td>";
        }

        $rowContent .= "<tr>
                            {$columnContent}
                            <td style=\"width: 15%\">
                                <div class=\"form-button-action\">
                                    <input type=\"hidden\" class=\"dataID\" value=\"{$row['IDno']}\">
                                    <button type=\"button\" title=\"修改\" class=\"btn btn-link btn-primary py-0 px-2 modData\"
                                        data-act=\"mod\" data-type=\"$key\">
                                        <i class=\"fa fa-edit fa-lg\"></i>
                                    </button>
                                    {$delBtn}
                                </div>
                            </td>
                        </tr>";
    }

    // 新增評估量表連結
    $_GET['action'] = 'dataForm';
    $formLink = http_build_query($_GET);
    $countentArray[$key] = "
        <div class=\"table-responsive\">
            <table id=\"{$key}_table\" class=\"display table table-striped table-hover\">
                <thead>
                    <tr>
                        {$columnHead}
                        <th style=\"padding-right:0px !important;\">
                            <a href=\"?{$formLink}&formType={$key}\" class=\"btn btn-primary btn-sm float-right mx-0\"
                                role=\"button\" aria-pressed=\"true\">
                                <span class=\"btn-label\">
                                    <i class=\"fa fa-plus\"></i>
                                </span>
                                新增紀錄
                            </a>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {$rowContent}
                </tbody>
            </table>
        </div>";
}

// 輸出評估量表
$navItem = '';
$tabContent = '';
foreach ($assessArray as $key => $detail) {
    $navItem .= "<li class=\"nav-item\">
                    <a class=\"nav-link\" id=\"{$key}-tab\" data-toggle=\"pill\" href=\"#{$key}\" role=\"tab\" aria-controls=\"{$key}\">{$detail['name']}</a>
                </li>";
    $tabContent .= "<div class=\"tab-pane fade\" id=\"{$key}\" role=\"tabpanel\" aria-labelledby=\"{$key}-tab\">
                        {$countentArray[$key]}
                    </div>";
}
?>

<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">評估量表總覽</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="?page=<?=$page?>"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">評估量表總覽</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-xl-6">
                    <form class="needs-validation" method="post">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    <b><?=$caseLink?>的<?=$trackArray['title']?></b>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="display table table-striped table-hover" id="trackTable">
                                        <thead>
                                            <tr>
                                                <?=$trackcolumnHead?>
                                                <th style="padding-right:0px !important;">
                                                    <button type="button" class="btn btn-primary btn-sm float-right mx-0"
                                                        data-toggle="modal" data-target="#modalTrack" data-act="add">
                                                        <span class="btn-label">
                                                            <i class="fa fa-plus"></i>
                                                        </span>
                                                        新增追蹤
                                                    </button>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <?=$trackrowContent?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-xl-6">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title"><b>評估量表</b></div>
                        </div>
                        <div class="card-body">
                            <ul class="nav nav-pills nav-secondary" id="pills-tab" role="tablist">
                                <?=$navItem?>
                            </ul>
                            <div class="tab-content mt-2 mb-3" id="pills-tabContent">
                                <?=$tabContent?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require 'viewModal.php'; ?>

<script>
    "use strict";
    var id = <?=$id?>;
    var params = new URLSearchParams(location.search);
    var tabIndex = location.hash;

    function init() {
        // 追蹤紀錄
        $("#trackTable").DataTable({
            pageLength: 10,
            lengthChange: false,
            order: [[ 5, "desc" ]],
            columnDefs: [{
                targets: [-1],
                orderable: false,
            }]
        });

        $(":input[readonly]").addClass("form-control-plaintext").removeClass("form-control");

        // 修改追蹤紀錄
        $("#modalTrack").on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            let action = button.data('act');
            let titleText = "";
            let rowdata = "";
            // 這個modal
            let modal = $(this);

            if (action == 'add') {
                titleText = "新增";
                // $("[data-target='#R_date_" + recType + "']").val(moment().format("YYYY-MM-DD"));
            } else if (action == 'mod') {
                titleText = "修改";
                rowdata = JSON.parse(decodeEntities(button.siblings(".rowdata").val()));
                let dataID = rowdata.IDno;
                let fields = modal.find("[field]");
                // 填欄位
                $.each(fields, function(index, fieldNode) {
                    let field = $(fieldNode).attr("field");
                    let inputType = $(fieldNode).attr("type") || $(fieldNode).prop("tagName");
                    if (inputType != undefined) {
                        inputType = inputType.toLowerCase();
                    }
                    // 依據不同的 input 類型，做不同的塞值動作(單個欄位對應單筆資料)
                    if (inputType == "text" || inputType == "textarea" || inputType == "number" || inputType == "hidden") {
                        $(fieldNode).val(rowdata[field]);
                    } else if (inputType == "checkbox" && Boolean(rowdata[field])) {
                        $(fieldNode).prop("checked", true);
                    } else if (inputType == "select") {
                        $(fieldNode).val(rowdata[field]).trigger("change");
                    }
                });
                // 更新資料ID
                modal.find("[name='dataID']").val(dataID);
            }
            modal.find("[name='action']").val(action);
            modal.find(".modal-title").text(titleText + '追蹤紀錄');
            modal.find("button[type='submit']").text("確定" + titleText);
        });

        $("#modalTrack").on('hidden.bs.modal', function (event) {
            let modal = $(this);
            modal.find("form").get(0).reset();
        });

        // 刪除、結束追蹤紀錄
        $("#delTrack, #completeTrack").on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            // 這個modal
            let modal = $(this);
            let rowdata = JSON.parse(decodeEntities(button.siblings(".rowdata").val()));

            modal.find("form").attr("action", '?' + params.toString());
            modal.find("input[name='dataID']").val(rowdata.IDno);
        });

        $("[name='AdminID']").select2({
            theme: "bootstrap",
            width: '100%'
        });

        $("#T_date").datetimepicker({
            format: 'YYYY-MM-DD',
            allowInputToggle: true,
            useStrict: true,
            debug: false
        });

        // 評估量表
        $('.table-responsive > table').DataTable({
            pageLength: 10,
            lengthChange: false,
            searching: false,
            order: [[ 0, "desc" ]],
            columnDefs: [{
                targets: [-1],
                orderable: false,
            }]
        });

        // 修改評估量表
        $(".modData").on("click", function () {
            params.set("id", id);
            params.set("dataID", $(this).siblings(".dataID").val());
            params.set("action", "dataForm");
            params.set("formType", $(this).data("type"));
            var path = location.protocol + '//' + location.host + location.pathname + '?' + params.toString();
            location.href = path;
        });

        // 刪除評估量表
        $("#delForm").on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            // 這個modal
            let modal = $(this);
            let dataID = button.siblings(".dataID").val();
            let formType = button.data("type");

            modal.find("form").attr("action", '?' + params.toString() + "&formType=" + formType);
            modal.find("input[name='dataID']").val(dataID);
        });

        // 頁籤
        $("#pills-tab a.nav-link").on("show.bs.tab", function (e) {
            location.hash = $(e.target).attr("href");
        });
        // 若有頁籤直接指定頁籤，若沒有則看第一個
        if (tabIndex !== '') {
            $("#pills-tab a.nav-link[href='" + tabIndex + "']").tab("show");
        } else {
            $("#pills-tab a.nav-link").eq(0).tab("show");
        }
    }

    window.onload = init;
</script>