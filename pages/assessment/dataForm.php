<?php
/**
 * 新增/以及編輯資料使用表單
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 * @param int $id 個案ID
 * @param boolean $edit 編輯資料
 */

$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
require_once get_relative_path("pages/{$page}/dataDefinition.php");

$id = intval(reqParam('id', 'get'));
$dataID = intval(reqParam('dataID', 'get'));
$formType = strval(reqParam('formType', 'get'));

if (empty($dataID)) {
    $pageStatus = '新增';
    $formAction = 'add';
    $formDataJSON = json_encode('');
} else {
    $pageStatus = '編輯';
    $formAction = 'mod';
    $dbQuery = "SELECT * FROM FCF_careservice.{$assessArray[$formType]['table']} WHERE IDno=?";
    $result = $db->row($dbQuery, [$dataID]);
    $formDataJSON = json_encode($result);
    // 分數在某些值之後，必須標註
    $showScoreColor = '';
    if (
        $result['score'] >= $assessArray[$formType]['scoreCare'] && $formType != 'social'
        || $result['score'] <= $assessArray[$formType]['scoreCare'] && $formType == 'social'
    ) {
        $showScoreColor = 'text-danger';
    }
}

if (!array_key_exists($formType, $assessArray)) {
    header("Location: " . get_relative_path("pages/404.php"));
    exit();
}

?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?="{$pageStatus}{$assessArray[$formType]['name']}"?></h4>
                <ul class="breadcrumbs">
                <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="?page=<?=$page?>"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="?<?="page={$page}&action=view&id={$id}"?>">評估量表細項</a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$pageStatus?><?=$assessArray[$formType]['name']?></a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form class="needs-validation" method="post" id="inputDataForm">
                        <input type="hidden" name="page" value="writeForm">
                        <input type="hidden" name="action" value="<?=$formAction?>">
                        <input type="hidden" name="dataID" value="<?=$dataID?>">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    <b><?=$assessArray[$formType]['name']?></b>
                                    <?php
                                    if ($generalData['userLevel'] < 3 && $formAction === 'mod') {
                                    ?>
                                    <b class="pl-3">總分:</b>
                                    <input type="text" class="form-control-plaintext d-inline font-weight-bold <?=$showScoreColor?>" field="score" style="width: 20px;" readonly>
                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <div class="card-body">
                                <?php
                                include realpath(dirname(__FILE__) . "/forms/{$formType}Form.php");
                                if ($generalData['userLevel'] < 3) {
                                ?>
                                <div class="form-group">
                                    <label for="R_note">備註</label>
                                    <textarea class="form-control" id="R_note" name="R_note" rows="5" field="R_note"></textarea>
                                </div>
                                <?php
                                }
                                ?>
                            </div>

                            <div class="card-action">
                                <button type="button" class="btn btn-success" id="submitForm">儲存</button>
                                <button type="button" class="btn btn-danger" id="goBack">取消</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    "use strict";
    var params = new URLSearchParams(location.search);
    var listFields = ['symptom', 'give_help_type', 'mucosal_area', 'nutrition_tube_type', 'special_tube_type'];
    var dataArray = <?=$formDataJSON?>;

    function init() {
        // 營養表單疾病診斷
        $.each(cancer, function (i, item) {
            $('#diagnosis').append($('<option>', {
                value: item,
                text : item
            }));
        });

        $("#diagnosis").select2({
            theme: "bootstrap",
            width: '100%'
        });

        // 把每個欄位都填資料
        $.each(dataArray, function(field, value) {
            let inputField = $("[field='" + field + "']");
            let inputType = $(inputField).attr("type") || $(inputField).prop("tagName");

            if (inputType != undefined) {
                inputType = inputType.toLowerCase();
            }

            // 單欄位對應多筆資料
            if ($.inArray(field, listFields) != -1) {
                let chkArray = value.split(",").map(function(item) {
                    return item.trim();
                });
                $.each(chkArray, function(i,v) {
                    $("[field='" + field + v + "']").prop("checked", true);
                });
            } else {
                // 依據不同的 input 類型，做不同的塞值動作(單個欄位對應單筆資料)
                if (inputType == "text" || inputType == "textarea" || inputType == "number") {
                    $(inputField).val(value);
                } else if (inputType == "checkbox" && Boolean(value)) {
                    $(inputField).prop("checked", true);
                } else if (inputType == "radio") {
                    $.each($(inputField), function(i) {
                        if ($(this).val() == value) {
                            $(this).prop("checked", true);
                        }
                    });
                } else if (inputType == "select") {
                    $(inputField).val(value).trigger("change");
                }
            }
        });

        // 送出表單
        $("#submitForm").on("click", function (e) {
            if (valid(e)) {
                $(this).closest("form").submit();
            }
        });

        // 回上一頁
        $("#goBack").on("click", function () {
            history.back();
        });
    }

    window.onload = init;
</script>