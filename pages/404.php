<?php
// 回傳404頁面
http_response_code(404);
// 取消登入
session_start();
session_unset();
session_destroy();

$customTitle = '404';
require_once $_SERVER['DOCUMENT_ROOT'] . '/inc/header.php';
?>
<body class="page-not-found">
	<div class="wrapper not-found">
		<h1 class="animated fadeIn">404</h1>
		<div class="desc animated fadeIn"><span>OOPS!</span><br/>Looks like you get lost</div>
		<a href="<?=serverURL?>index.php" class="btn btn-primary btn-back-home mt-4 animated fadeInUp">
			<span class="btn-label mr-2">
				<i class="flaticon-home"></i>
			</span>
			Back To Home
		</a>
	</div>
	<script src="<?=serverURL?>assets/js/core/jquery-3.5.1.min.js"></script>
	<script src="<?=serverURL?>assets/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
	<script src="<?=serverURL?>assets/js/core/popper.min.js"></script>
	<script src="<?=serverURL?>assets/js/core/bootstrap.min.js"></script>
</body>
</html>
