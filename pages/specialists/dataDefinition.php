<?php
/**
 * 定義資源清單使用資料庫欄位以及項目
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

$definition = [
    'oncologist' => [
        'title' => '醫療專家',
        'colhead' => ['區域', '類別', '單位', '姓名', '職稱', '聯絡人', '電話', '手機', '傳真', 'email', '地址', '備註'],
        'col' => ['region', 'category', 'unit', 'expertName', 'jobTitle', 'contact', 'tel', 'mobile', 'fax', 'email', 'address', 'memo'],
        'dataType'=> 1
    ],
    'teachers' => [
        'title' => '各領域專家',
        'colhead' => ['區域', '類別', '單位', '姓名', '職稱', '聯絡人', '電話', '手機', '傳真', 'email', '地址', '經歷', '備註'],
        'col' => ['region', 'category', 'unit', 'expertName', 'jobTitle', 'contact', 'tel', 'mobile', 'fax', 'email', 'address', 'experience', 'memo'],
        'dataType'=> 2
    ],
];