<?php
/**
 * 匯入 EXCEL 資料塞入資料庫
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $subPage 子類別
 */

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;

require_once get_relative_path("pages/{$page}/dataDefinition.php");

// 錯誤或成功訊息
$msg = [];
// 拖拉檔案上傳處
if (empty($_FILES)) {
    exit();
} else {
    // 額外資料
    $dataType = $_POST['dataType'];

    // 檔案處理
    $maxMB = filter_var(ini_get('upload_max_filesize'), FILTER_SANITIZE_NUMBER_INT);
    $acceptFormat = [
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/vnd.ms-excel',
        'application/vnd.oasis.opendocument.spreadsheet',
    ];


    $f_temp = $_FILES['file']['tmp_name'];
    $f_name = $_FILES['file']['name'];
    $f_type = $_FILES['file']['type'];
    $f_size = round(($_FILES['file']['size'] / 1048576), 3); //MB
    $f_error = intval($_FILES['file']['error']);

    if ($f_error != 0) {
        $msg['error'] = "上傳發生錯誤";
    }

    // 檔案大小限制
    if ($f_size > $maxMB) {
        $msg['error'] = "超過容許大小";
    }

    if (!in_array($f_type, $acceptFormat, true)) {
        $msg['error'] = "檔案格式錯誤";
    }

    $ext = ucfirst(strtolower(pathinfo($f_name)['extension']));
}


//return right HTTP code
if (!empty($msg['error'])) {
    header('http/1.1 500 internal server error');
} else {
    // 依據檔案不同匯入資料庫
    $reader = IOFactory::createReader($ext);
    // $reader->setReadDataOnly(true);
    $spreadsheet = $reader->load($f_temp)->getActiveSheet();
    $sheetData = $spreadsheet->toArray(null, true, true, true);

    $delData = "DELETE FROM FCF_careservice.Specialist WHERE dataType={$definition[$subPage]['dataType']}";
    $insertCol = implode(', ', $definition[$subPage]['col']);
    $dbQuery = "INSERT INTO FCF_careservice.Specialist
                    (Inputdate, AdminID ,dataType, {$insertCol})
                VALUES ";

    foreach ($sheetData as $key => $dataRow) {
        if ($key == 1) {
            // 確認欄位數量是否相符，不相符視為不同表單
            if (count(array_filter($dataRow)) !== count($definition[$subPage]['col'])+1) {
                header('http/1.1 500 internal server error');
                $msg['error'] = '此清單並非對應此頁面';
                break;
            }
            continue;
        }

        if (empty(array_filter($dataRow))) {
            continue;
        }

        $colArray = [];
        for ($i=2; $i <= count($definition[$subPage]['col'])+1 ; $i++) {
            $colLetter = Coordinate::stringFromColumnIndex($i);
            $colArray[] = $dataRow[$colLetter];
        }
        $colValueStr = implode("','", $colArray);
        $dbQuery .= "(NOW(), {$generalData['userid']}, {$definition[$subPage]['dataType']},'{$colValueStr}'),";
    }

    // 有錯誤即退出
    if (empty($msg['error'])) {
        $dbQuery = substr($dbQuery, 0, -1);
        $db->query($delData);
        $db->query($dbQuery);

        // 最後的成功訊息
        header("HTTP/1.1 200 OK");
        $msg['success'] = '匯入資料成功!';
    }
}

//set Content-Type to JSON
header('Content-Type: application/json; charset=utf-8');
// 清除先前頁面要顯示的快取
ob_end_clean();
//echo error message as JSON
echo json_encode($msg, JSON_UNESCAPED_UNICODE);
