<?php
/**
 * 新增/以及編輯資料使用表單
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 */
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
$close = boolval(reqParam('close', 'get'));
// 資料分類
$subPageMap = array(
    'personalCase' => [
        'title' => '基本資料清單',
        'sqlCondition' => 'AND m.C_member=1 AND m.caseClosed=0 AND m.C_status=1',
        'closeStatus' => 'AND m.C_member=1 AND m.caseClosed=1 AND m.C_status=1'
    ],
    'family' => [
        'title' => '家屬資料清單',
        'sqlCondition' => 'AND m.C_member=2 AND m.caseClosed=0 AND m.C_status=1',
        'closeStatus' => 'AND m.C_member=2 AND m.caseClosed=1 AND m.C_status=1'
    ],
    'nonClient' => [
        'title' => '一般民眾清單',
        'sqlCondition' => 'AND m.C_member=0 AND m.C_status=1 AND m.FCFvolunte=0'
    ],
    'closeCase' => [
        'title' => '結案資料清單',
        'sqlCondition' => 'AND m.C_status=-1',
        'closeStatus' => 'AND m.C_status=-1'
    ]
);

if (!array_key_exists($subPage, $subPageMap)) {
    header("Location: " . get_relative_path("pages/404.php"));
    exit();
}

?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$subPageMap[$subPage]['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['subname'][$subPage]?></a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <?php
                                $addBtn = '';
                                if ($close) {
                                    $tableTitle = '已關檔';
                                    $btnString = '切換為未關檔';
                                    $sqlClose = $subPageMap[$subPage]['closeStatus'];
                                } else {
                                    $tableTitle = '未關檔';
                                    $btnString = '切換為已關檔';
                                    $sqlClose = $subPageMap[$subPage]['sqlCondition'];
                                }
                                // 結案不顯示切換按鈕
                                if ($subPage != 'closeCase' && $subPage != 'nonClient') {
                                    $_GET['close'] = !$close;
                                    ?>
                                    <h4 class="card-title"><?=$tableTitle?></h4>
                                    <a class="btn btn-info btn-round btn-sm mr-auto ml-3" href="?<?=http_build_query($_GET)?>"><?=$btnString?></a>
                                    <?php
                                    // 一般民眾不給新增，只能查詢
                                    if ($subPage != 'nonClient' && $generalData['userLevel'] <= 2) {
                                        unset($_GET['close']);
                                        $addBtn = '<a class="btn btn-outline-success btn-round btn-sm ml-3" href="?' . http_build_query($_GET) . '&action=dataForm">
                                                <i class="fas fa-plus"></i> 新增
                                            </a>';
                                    }
                                } elseif ($subPage === 'nonClient') {
                                    echo '<h4 class="card-title">一般民眾</h4>';
                                } else {
                                    echo '<h4 class="card-title">結案</h4>';
                                }
                                ?>
                                <?=$addBtn?>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="basic-datatables" class="display table table-striped table-hover" >
                                    <thead>
                                        <tr>
                                            <th>姓名</th>
                                            <th>性別</th>
                                            <th>生日</th>
                                            <th>年齡</th>
                                            <th>縣市</th>
                                            <th>癌別</th>
                                            <th>電話</th>
                                            <th>手機</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    // 姓名、性別、生日(年齡)、癌別、電話
                                    $dbQuery = "SELECT m.*,
                                                GROUP_CONCAT(DISTINCT rc.Cancer_name SEPARATOR ', ') AS 'Cancer_name'
                                                FROM FCF_careservice.Memberdata m
                                                LEFT JOIN FCF_careservice.Rec_cancer rc ON m.IDno=rc.MemberID AND rc.`status`=1
                                                WHERE 1=1 {$sqlClose}
                                                GROUP BY m.IDno ORDER BY m.C_member DESC, m.IDno";
                                    $result = $db->query($dbQuery);
                                    foreach ($result as $key => $row) {
                                        $birthDate = DateTime::createFromFormat('Y-m-d', "{$row['BD_yy']}-{$row['BD_mm']}-{$row['BD_dd']}");
                                        // 年紀與生日，若值為空則不顯示
                                        if ($birthDate instanceof DateTime) {
                                            $showBirth = $birthDate->format('Y-m-d');
                                            $now = new DateTime();
                                            $old = $birthDate->diff($now)->format('%Y');
                                        } else {
                                            $showBirth = '';
                                            $old = '';
                                        }

                                        if (empty($row['C_name'])) {
                                            $row['C_name'] = '無名氏';
                                        }
                                        echo "
                                        <tr>
                                            <td><a href=\"?page={$page}&sub={$subPage}&action=view&id={$row['IDno']}\">{$row['C_name']}</a></td>
                                            <td>" . ($row['C_sex'] == 'male' ? '男' : '女') . "</td>
                                            <td>{$showBirth}</td>
                                            <td>{$old}</td>
                                            <td>{$row['C_city']}</td>
                                            <td>{$row['Cancer_name']}</td>
                                            <td>{$row['C_tele']}</td>
                                            <td>{$row['C_mobile1']}</td>
                                        </tr>";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function init() {
    $('#basic-datatables').DataTable({
        stateSave: true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'print',
                text: '<i class="fas fa-print fa-lg"></i> 列印',
                className: 'btn-sm',
                autoPrint: false,
            },
        ],
    });
}

window.onload = init;
</script>