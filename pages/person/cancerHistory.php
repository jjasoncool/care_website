<!-- 樣板 -->
<div id="itemTemplate" style="display: none;">
    <div class="card-header" id="headID">
        <h5 class="mb-0">
            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#contentID" aria-expanded="false" aria-controls="contentID"
                style="font-size:0.9rem; font-weight:bold;">
                第<span></span>筆疾病史
            </button>
        </h5>
    </div>
    <div id="contentID" class="collapse" aria-labelledby="headID" data-parent="#accordionCancer">
        <div class="card-body">
            <div class="form-row px-2">
                <input type="hidden" name="cancerID[]" field="cancerID">
                <div class="form-group select2-input col-md-4">
                    <label for="cancerName">癌別</label>
                    <select class="form-control py-1" data="cancerName" name="cancerName[]" field="Cancer_name" required>
                        <option value="">---</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="cancerStage">期別</label>
                    <select class="form-control py-1" data="cancerStage" name="cancerStage[]" field="Cancer_level" required>
                        <option value="0">原位癌</option>
                        <option value="1">第一期</option>
                        <option value="2">第二期</option>
                        <option value="3">第三期</option>
                        <option value="4">第四期</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="cancerTrans">轉移部位</label>
                    <input type="text" class="form-control" data="cancerTrans" name="cancerTrans[]" field="Cancer_trans">
                </div>
            </div>
            <div class="form-row px-2">
                <div class="form-group col-md-4">
                    <label for="consult">診斷時間</label>
                    <div class="input-group date" data="consult" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" maxlength="10" name="Find_date[]" data-target="#consult" field="findDate" required="required">
                        <div class="input-group-append" data-target="#consult" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <label for="hospital">就診醫院</label>
                    <input type="text" class="form-control" data="hospital" name="hospital[]" field="hospital">
                </div>
                <div class="form-group col-md-4">
                    <label for="gene">基因檢測</label>
                    <input type="text" class="form-control" data="gene" name="gene[]" field="genetic_test">
                </div>
            </div>
            <div class="form-row px-2">
                <div class="form-group col-md-4">
                    <label for="Surgery">手術切除</label>
                    <select class="form-control py-1" data="Surgery" name="Surgery[]" field="Surgery" required>
                        <option value="0">無</option>
                        <option value="1">有</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="Surgery_date">手術時間</label>
                    <div class="input-group date" data="Surgery_date" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" maxlength="10" name="Surgery_date[]" field="surgeryDate" data-target="#Surgery_date">
                        <div class="input-group-append" data-target="#Surgery_date" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-row px-2">
                <div class="form-group col-md-4">
                    <label for="cancer_type">癌症復發</label>
                    <select class="form-control py-1" data="cancer_type" name="cancer_type[]" field="Cancer_type">
                        <option value="New">無</option>
                        <option value="Again">有</option>
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label for="recurrence">復發時間</label>
                    <div class="input-group date" data="recurrence" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" maxlength="10" name="recurrence[]" field="recurrence_month" data-target="#recurrence">
                        <div class="input-group-append" data-target="#recurrence" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-header">
                <div class="card-title" style="font-size:0.9rem;"><b>現在治療方式</b></div>
            </div>
            <div class="card-body py-0">
                <!-- 化療 -->
                <div class="form-row px-2">
                    <div class="form-group col-md-2">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" value="1" name="Chemical[]" field="Chemical">
                                <span class="form-check-sign unselectable">化學治療</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="input-group date" data="Chemical_date_S" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="Chemical_date_S[]" data-target="#Chemical_date_S" field="Chemical_date_S" placeholder="化療期間 (開始)">
                            <div class="input-group-append" data-target="#Chemical_date_S" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="input-group date" data="Chemical_date_E" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="Chemical_date_E[]" data-target="#Chemical_date_E" field="Chemical_date_E" placeholder="化療期間 (結束)">
                            <div class="input-group-append" data-target="#Chemical_date_E" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <input type="text" class="form-control" data="Chemical_name" name="Chemical_name[]" field="Chemical_name" placeholder="化療使用藥名">
                    </div>
                </div>
                <!-- 標靶 -->
                <div class="form-row px-2">
                    <div class="form-group col-md-2">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" value="1" name="Target[]" field="Target">
                                <span class="form-check-sign unselectable">標靶治療</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="input-group date" data="Target_date_S" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="Target_date_S[]" data-target="#Target_date_S" field="Target_date_S" placeholder="標靶治療期間 (開始)">
                            <div class="input-group-append" data-target="#Target_date_S" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="input-group date" data="Target_date_E" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="Target_date_E[]" data-target="#Target_date_E" field="Target_date_E" placeholder="標靶治療期間 (結束)">
                            <div class="input-group-append" data-target="#Target_date_E" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <input type="text" class="form-control" data="Target_name" name="Target_name[]" field="Target_name" placeholder="標靶治療使用藥名">
                    </div>
                </div>
                <!-- 免疫治療 -->
                <div class="form-row px-2">
                    <div class="form-group col-md-2">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" value="1" name="Immune[]" field="Immune">
                                <span class="form-check-sign unselectable">免疫治療</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="input-group date" data="Immune_date_S" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="Immune_date_S[]" data-target="#Immune_date_S" field="Immune_date_S" placeholder="免疫治療期間 (開始)">
                            <div class="input-group-append" data-target="#Immune_date_S" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="input-group date" data="Immune_date_E" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="Immune_date_E[]" data-target="#Immune_date_E" field="Immune_date_E" placeholder="免疫治療期間 (結束)">
                            <div class="input-group-append" data-target="#Immune_date_E" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <input type="text" class="form-control" data="Immune_name" name="Immune_name[]" field="Immune_medicine" placeholder="免疫治療使用藥名">
                    </div>
                </div>
                <!-- 放射線治療 -->
                <div class="form-row px-2">
                    <div class="form-group col-md-2">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" value="1" name="Radiation[]" field="Radiation">
                                <span class="form-check-sign unselectable">放射線治療</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="input-group date" data="Radiat_date_S" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="Radiat_date_S[]" data-target="#Radiat_date_S" field="Radiat_date_S" placeholder="放射線治療期間 (開始)">
                            <div class="input-group-append" data-target="#Radiat_date_S" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="input-group date" data="Radiat_date_E" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="Radiat_date_E[]" data-target="#Radiat_date_E" field="Radiat_date_E" placeholder="放射線治療期間 (結束)">
                            <div class="input-group-append" data-target="#Radiat_date_E" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="input-group">
                            <input type="number" class="form-control" data="Rad_time" name="Rad_time[]" placeholder="放射線治療次數" step="1" min="0" max="40" maxlength="2" field="Rad_time">
                            <div class="input-group-append">
                                <div class="input-group-text">次</div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <input type="text" class="form-control" data="Rad_part" name="Rad_part[]" field="Rad_part" placeholder="放射線治療部位">
                    </div>
                </div>
                <!-- 抗賀爾蒙治療 -->
                <div class="form-row px-2">
                    <div class="form-group col-md-2">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" value="1" name="hormone[]" field="hormone">
                                <span class="form-check-sign unselectable">抗賀爾蒙治療</span>
                            </label>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="input-group date" data="hormone_date_S" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="hormone_date_S[]" data-target="#hormone_date_S" field="hormone_date_S" placeholder="抗賀爾蒙治療期間 (開始)">
                            <div class="input-group-append" data-target="#hormone_date_S" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="input-group date" data="hormone_date_E" data-target-input="nearest">
                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="hormone_date_E[]" data-target="#hormone_date_E" field="hormone_date_E" placeholder="抗賀爾蒙治療期間 (結束)">
                            <div class="input-group-append" data-target="#hormone_date_E" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <input type="text" class="form-control" data="hormone_name" name="hormone_name[]" field="hormone_medicine" placeholder="抗賀爾蒙治療使用藥名">
                    </div>
                </div>
                <div class="form-row px-2">
                    <div class="col-auto">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" value="1" name="integrated[]" field="integrated">
                                <span class="form-check-sign unselectable">中西醫整合療法</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" value="1" name="tracking[]" field="tracking">
                                <span class="form-check-sign unselectable">定期門診追蹤</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" value="1" name="peace[]" field="peace">
                                <span class="form-check-sign unselectable">安寧療護</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-auto">
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" value="1" name="other[]" field="other">
                                <span class="form-check-sign unselectable">其他治療</span>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="R_note">備註</label>
                            <textarea class="form-control" id="R_note" name="R_note[]" rows="3" field="R_note"></textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>