<?php
/**
 * 新增/編輯/刪除 諮詢紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param array $inputData 使用者輸入的資料
 * @param array $basicData 接受的欄位資料
 * @param string $action 對資料做的動作
 * @param int $id 個案編號
 */

$id = intval(reqParam('id', 'get'));
$dataID = intval(reqParam('dataID', 'post'));
$formAction = reqParam('action', 'post');
$recType = reqParam('recType', 'post');

$dataTable = '';
$inputData = [];
switch ($recType) {
    case 'med':
        $dataTable = 'Rec_medical';
        $basicData = [
            'R_date' => 'date',
            'R_time' => 'int',
            'R_loc' => 'string',
            'R_way' => 'string',
            'Medical' => 'boolean',
            'Med_cure' => 'boolean',
            'Med_check' => 'boolean',
            'Med_drug' => 'boolean',
            'Med_clinical' => 'boolean',
            'Med_suggest' => 'boolean',
            'Med_assist' => 'boolean',
            'Nurse' => 'boolean',
            'Nur_surgery' => 'boolean',
            'Nur_chemical' => 'boolean',
            'Nur_target' => 'boolean',
            'Nur_radiation' => 'boolean',
            'Nur_drug' => 'boolean',
            'Nur_home' => 'boolean',
            'Nur_assuage' => 'boolean',
            'Nur_hospital' => 'boolean',
            'Nur_food' => 'boolean',
            'Nur_mood' => 'boolean',
            'Rights' => 'boolean',
            'Right_drugpay' => 'boolean',
            'Right_newdrug' => 'boolean',
            'Right_buydrug' => 'boolean',
            'Right_clinical' => 'boolean',
            'Right_doctor' => 'boolean',
            'Right_quality' => 'boolean',
            'Right_other' => 'boolean',
            'Right_othertxt' => 'string',
            'R_note' => 'string',
            'filePath' => 'string',
        ];
        break;
    case 'nutrition':
        $dataTable = 'Rec_nutrition';
        $basicData = [
            'R_date' => 'date',
            'R_time' => 'int',
            'R_loc' => 'string',
            'R_way' => 'string',
            'R_height' => 'int',
            'R_weight' => 'int',
            'general_weight' => 'int',
            'R_BMI' => 'float',
            'R_analysis' => 'string',
            'Aim' => 'boolean',
            'Aim_therapeutic' => 'boolean',
            'Aim_nutrition' => 'boolean',
            'Aim_balance' => 'boolean',
            'Aim_countermeasures' => 'boolean',
            'Aim_other' => 'boolean',
            'Aim_othertxt' => 'string',
            'Provide' => 'boolean',
            'Pv_self' => 'boolean',
            'Pv_ng' => 'boolean',
            'Pv_gastric' => 'boolean',
            'Pv_intestinal' => 'boolean',
            'Pv_ppn' => 'boolean',
            'Pv_tpn' => 'boolean',
            'Pv_other' => 'boolean',
            'Pv_othertxt' => 'string',
            'Caloric' => 'float',
            'Cal_pressure' => 'float',
            'Cal_activity' => 'float',
            'Protein' => 'float',
            'Protein_factor' => 'float',
            'beforeSurgery' => 'boolean',
            'afterSurgery' => 'boolean',
            'Cure' => 'boolean',
            'Cure_calorie' => 'boolean',
            'Cure_protein' => 'boolean',
            'Effect' => 'boolean',
            'Effect_vomit' => 'boolean',
            'Effect_oral' => 'boolean',
            'Effect_full' => 'boolean',
            'Effect_flatulence' => 'boolean',
            'Effect_diarrhea' => 'boolean',
            'Effect_constipation' => 'boolean',
            'Effect_lowHeme' => 'boolean',
            'Effect_lowLeukocyte' => 'boolean',
            'Effect_appetiteLoss' => 'boolean',
            'Effect_weightLoss' => 'boolean',
            'Effect_gainWeight' => 'boolean',
            'Effect_other' => 'boolean',
            'Effect_othertxt' => 'string',
            'Diet' => 'boolean',
            'Diet_blance' => 'boolean',
            'Diet_diabetes' => 'boolean',
            'Diet_renalFailure' => 'boolean',
            'Diet_dialysis' => 'boolean',
            'Diet_liver' => 'boolean',
            'Diet_other' => 'boolean',
            'Diet_othertxt' => 'string',
            'Nutrition' => 'boolean',
            'Nu_panax' => 'boolean',
            'Nu_algae' => 'boolean',
            'Nu_vitamins' => 'boolean',
            'Nu_calcium' => 'boolean',
            'Nu_mushroom' => 'boolean',
            'Nu_phytochemicals' => 'boolean',
            'Nu_other' => 'boolean',
            'Nu_othertxt' => 'string',
            'Eat' => 'boolean',
            'Eat_normal' => 'boolean',
            'Eat_liquid' => 'boolean',
            'Eat_bland' => 'boolean',
            'Eat_lowResidue' => 'boolean',
            'Eat_highFiber' => 'boolean',
            'Eat_tube' => 'boolean',
            'Eat_elemental' => 'boolean',
            'Eat_other' => 'boolean',
            'Eat_othertxt' => 'string',
            'Special' => 'boolean',
            'Sp_glutamine' => 'boolean',
            'Sp_balanced' => 'boolean',
            'Sp_tumor' => 'boolean',
            'Sp_pneumonia' => 'boolean',
            'Sp_diabetes' => 'boolean',
            'Sp_nephropathy' => 'boolean',
            'Sp_other' => 'boolean',
            'Sp_othertxt' => 'string',
            'R_note' => 'string',
            'filePath' => 'string',
        ];
        break;
    case 'social':
        $dataTable = 'Rec_social';
        $basicData = [
            'R_date' => 'date',
            'R_time' => 'int',
            'R_loc' => 'string',
            'R_way' => 'string',
            'Aim_wig' => 'boolean',
            'Aim_welfare' => 'boolean',
            'Aim_goods' => 'boolean',
            'Aim_volcare' => 'boolean',
            'Aim_other' => 'boolean',
            'Aim_othertxt' => 'string',
            'Grants' => 'boolean',
            'Grant_help' => 'boolean',
            'Grant_nutrition' => 'boolean',
            'Grant_traffic' => 'boolean',
            'Grant_respitecare' => 'boolean',
            'R_note' => 'string',
            'filePath' => 'string',
        ];
        break;
    case 'mental':
        $dataTable = 'Rec_mental';
        $basicData = [
            'R_date' => 'date',
            'R_time' => 'int',
            'R_loc' => 'string',
            'R_way' => 'string',
            'Mind' => 'boolean',
            'Mind_feeling' => 'boolean',
            'Mind_self' => 'boolean',
            'Mind_adapt' => 'boolean',
            'Mind_friends' => 'boolean',
            'Mind_family' => 'boolean',
            'Mind_sickbed' => 'boolean',
            'Mind_suicide' => 'boolean',
            'Mind_selfcare' => 'boolean',
            'Mind_lost' => 'boolean',
            'Mind_other' => 'boolean',
            'Mind_othertxt' => 'string',
            'R_note' => 'string',
            'filePath' => 'string',
        ];
        break;
    case 'insurance':
        $dataTable = 'Rec_insurance';
        $basicData = [
            'R_date' => 'date',
            'R_time' => 'int',
            'R_loc' => 'string',
            'R_way' => 'string',
            'Ins_med' => 'boolean',
            'Ins_admission' => 'boolean',
            'Ins_clinic' => 'boolean',
            'Ins_surgery' => 'boolean',
            'Ins_expenses' => 'boolean',
            'Ins_targeted' => 'boolean',
            'Ins_radiology' => 'boolean',
            'Ins_chemotherapy' => 'boolean',
            'Ins_immunotherapy' => 'boolean',
            'Ins_discharged' => 'boolean',
            'Ins_tcm' => 'boolean',
            'Ins_hospice' => 'boolean',
            'Ins_policy' => 'boolean',
            'Ins_view' => 'boolean',
            'Ins_plan' => 'boolean',
            'Ins_law' => 'boolean',
            'R_note' => 'string',
            'filePath' => 'string',
        ];
        break;
    case 'care':
        $dataTable = 'Rec_care';
        $basicData = [
            'R_date' => 'date',
            'R_time' => 'int',
            'R_loc' => 'string',
            'R_way' => 'string',
            'Contactpeop' => 'string',
            'Act_care' => 'boolean',
            'Act_expshare' => 'boolean',
            'Act_support' => 'boolean',
            'Act_other' => 'boolean',
            'Act_othertxt' => 'string',
            'Transfer' => 'boolean',
            'Trans_medical' => 'boolean',
            'Trans_nutrition' => 'boolean',
            'Trans_social' => 'boolean',
            'Trans_mental' => 'boolean',
            'R_note' => 'string',
        ];
        break;
    case 'courses':
        $dataTable = 'Rec_courses';
        $basicData = [
            'ClassID' => 'int',
        ];
        break;
    case 'projects':
        $dataTable = 'Rec_projects';
        $basicData = [
            'ClassID' => 'int',
        ];
        break;
    case 'transfer':
        $dataTable = 'Rec_referrals';
        $basicData = [
            'AdminID' => 'int',
            'AdName' => 'string',
            'R_date' => 'date',
            'refComment' => 'string',
            'refID' => 'int',
            'refReply' => 'string',
        ];
        break;
    default:
        break;
}

// 基本資料
foreach ($basicData as $inputName => $inputType) {
    $inputData[$inputName] = reqParam($inputName, 'post', $inputType);
}

// 紀錄基本資料
$dt = new dateTime();
$recPeople = [
    "Keydate" => $dt->format('Y-m-d H:i:s'),
    'MemberID' => $id,
    'AdminID' => $generalData['userid'],
    'AdName' => $generalData['username'],
];

if ($recType === 'transfer') {
    unset($recPeople['AdminID'], $recPeople['AdName']);
}

$AdditionalCond = '';
if (in_array($recType, ['med', 'nutrition', 'social', 'mental', 'insurance', 'care'])) {
    $AdditionalCond = 'AND anonym=0';
    $recPeople['AdmincheckID'] = $generalData['managerID'];
}

// 判斷資料類型
if ($formAction === 'add') {
    $insColArray = array_merge($recPeople, $inputData);
    $insColStr = implode('`,`', array_keys($insColArray));
    $insParaStr = implode(",", array_fill(0, count($insColArray), "?"));
    // SQL 用基本資料產出
    $dbQuery = "INSERT INTO FCF_careservice.{$dataTable} (
        `$insColStr`
    ) VALUES (
        $insParaStr
    )";
    $db->query($dbQuery, array_values($insColArray));
} elseif ($formAction === 'mod') {
    // 更新時，不可變更個案ID了
    unset($recPeople['MemberID']);
    $updColArray = array_merge($recPeople, $inputData);
    $updColStr = '';
    foreach ($updColArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    $dbQuery = "UPDATE FCF_careservice.{$dataTable}
                SET {$updColStr}
                WHERE IDno=? {$AdditionalCond}";
    $db->query($dbQuery, array_merge(array_values($updColArray), [$dataID]));
} elseif ($formAction === 'del') {
    // 刪除，取消此個案與此紀錄之關聯
    $dbQuery = "UPDATE FCF_careservice.{$dataTable}
                SET MemberID=-MemberID
                WHERE IDno=? {$AdditionalCond}";
    $db->query($dbQuery, [$dataID]);
}

// 如果確認檔案為上傳檔案，移動至上傳區
if (isset($inputData['filePath']) && !empty($inputData['filePath'])) {
    $tempFilePath = "{$_SERVER['DOCUMENT_ROOT']}/uploads/temp/{$inputData['filePath']}";
    $checkedFilePath = "{$_SERVER['DOCUMENT_ROOT']}/uploads/files/{$inputData['filePath']}";
    if (is_file($tempFilePath) && file_exists($tempFilePath)) {
        rename ($tempFilePath, $checkedFilePath);
    }

    // 刪除暫存區檔案
    $tempfiles = glob("{$_SERVER['DOCUMENT_ROOT']}/uploads/temp/*"); // get all file names
    foreach ($tempfiles as $file) { // iterate files
        if (is_file($file)) {
            // delete file
            unlink($file);
        }
    }
}

header("Location:?" . http_build_query($_GET));