<?php
/**
 * 上傳檔案，依據諮詢紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

// 錯誤或成功訊息
$msg = [];
// 拖拉檔案上傳處
if (empty($_FILES)) {
    exit();
} else {
    // 額外資料
    $recType = reqParam('recType', 'post');

    // 檔案處理
    $maxMB = filter_var(ini_get('upload_max_filesize'), FILTER_SANITIZE_NUMBER_INT);
    $acceptFormat = ['application/pdf'];

    $f_temp = $_FILES['file']['tmp_name'];
    $f_name = $_FILES['file']['name'];
    $f_type = $_FILES['file']['type'];
    $f_size = round(($_FILES['file']['size'] / 1048576), 3); //MB
    $f_error = intval($_FILES['file']['error']);

    if ($f_error != 0) {
        $msg['error'] = "上傳發生錯誤";
    }

    // 檔案大小限制
    if ($f_size > $maxMB) {
        $msg['error'] = "超過容許大小";
    }

    if (!in_array($f_type, $acceptFormat, true)) {
        $msg['error'] = "檔案格式錯誤";
    }

    $ext = strtolower(pathinfo($f_name)['extension']);
}


//return right HTTP code
if (!empty($msg['error'])) {
    header('http/1.1 500 internal server error');
} else {
    // 檔案先放入暫存區
    $date = new DateTime();
    $dateString = $date->format('YmdHis');

    // 檔名
    $NewName = "File_{$dateString}";
    $target_path = "{$_SERVER['DOCUMENT_ROOT']}/uploads/temp/{$NewName}.{$ext}";

    if (!file_exists(dirname($target_path))) {
        mkdir(dirname($target_path), '0777', true);
    }

    if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
    } else {
        $msg['error'] = "There was an error uploading the file" . basename($_FILES['file']['name']) . ", please try again!";
    }

    // 有錯誤即退出
    if (empty($msg['error'])) {
        // 最後的成功訊息
        header("HTTP/1.1 200 OK");
        $msg['filename'] = "{$NewName}.{$ext}";
        $msg['success'] = '上傳檔案成功!';
    }
}

//set Content-Type to JSON
header('Content-Type: application/json; charset=utf-8');
// 清除先前頁面要顯示的快取(一開始走router前面的頁面等等)
ob_end_clean();
//echo error message as JSON
echo json_encode($msg, JSON_UNESCAPED_UNICODE);
