<?php
/**
 * 新增/以及編輯資料使用表單
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 * @param int $id 個案ID
 * @param boolean $edit 編輯資料
 */

$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
// 如果這邊有帶入ID 必須先撈資料
$id = intval(reqParam('id', 'get'));
$edit = boolval(reqParam('edit', 'get'));
if ($edit && $id > 0) {
    $pageStatus = '編輯';
    // 一般個案基本資料
    $dbQuery = "SELECT *, STR_TO_DATE(CONCAT(BD_yy,'-',BD_mm,'-',BD_dd), '%Y-%c-%e') AS birthday
                FROM FCF_careservice.Memberdata m WHERE m.IDno=?";
    $memberData = $db->row($dbQuery, [$id]);
    $handSignature = $memberData['handSignature'];
    unset($memberData['handSignature']);
    $memberDataJSON = json_encode($memberData, JSON_UNESCAPED_UNICODE);
    // 一般個案疾病史
    $cancerTime = ["Chemical_date", "Target_date", "Immune_date", "Radiat_date", "hormone_date"];
    $dbQuery = "SELECT *, IDno AS cancerID,
                    DATE_FORMAT(Find_date, '%Y-%m') AS findDate,
                    DATE_FORMAT(recurrence, '%Y-%m') AS recurrence_month,
                    DATE_FORMAT(STR_TO_DATE(concat(Surgery_date,'-01'), '%Y-%m-%d'), '%Y-%m') AS surgeryDate
                FROM FCF_careservice.Rec_cancer WHERE MemberID=? AND `status`=1";
    $cancerHistoryData = $db->query($dbQuery, [$id]);
    foreach ($cancerHistoryData as $rows => $dataRow) {
        foreach ($cancerTime as $colName) {
            if (!empty($cancerHistoryData[$rows][$colName])) {
                $tempArray = explode('~', $cancerHistoryData[$rows][$colName]);
                if (isset($tempArray[0])) {
                    $cancerHistoryData[$rows][$colName . '_S'] = $tempArray[0];
                }
                if (isset($tempArray[1])) {
                    $cancerHistoryData[$rows][$colName . '_E'] = $tempArray[1];
                }
            }
        }
    }
    $cancerHistoryJSON = json_encode($cancerHistoryData, JSON_UNESCAPED_UNICODE);
    // 家屬的話只要撈癌症類別轉成陣列即可

    // 家屬資料不可以直接撈個案，不可以用ID直接查詢
    if ($memberData['C_member'] != 2 && $subPage == 'family' || $memberData['C_member'] == 2 && $subPage != 'family') {
        exit();
    }
} else {
    $pageStatus = '新增';
    $handSignature = '';
    $memberDataJSON = json_encode('');
    $cancerHistoryJSON = json_encode(['']);
}

// 資料類型
$subPageMap = array(
    'personalCase' => ['title' => "{$pageStatus}個案"],
    'nonClient' => ['title' => "{$pageStatus}一般民眾"],
    'family' => ['title' => "{$pageStatus}家屬資料"],
    'closeCase' => ['title' => "{$pageStatus}結案資料"],
);

if (!array_key_exists($subPage, $subPageMap)) {
    header("Location: " . get_relative_path("pages/404.php"));
    exit();
}

?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$subPageMap[$subPage]['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="?<?="page={$page}&sub={$subPage}"?>"><?=$listItems[$page]['subname'][$subPage]?></a>
                    </li>
                    <?php
                    // 有ID才會有詳細資料
                    if (!empty($id)) {
                        echo "
                        <li class=\"separator\">
                            <i class=\"flaticon-right-arrow\"></i>
                        </li>
                        <li class=\"nav-item\">
                            <a href=\"?page={$page}&sub={$subPage}&action=view&id=$id\">詳細資料</a>
                        </li>";
                    }
                    ?>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$pageStatus?>資料</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form class="needs-validation" method="post" id="inputDataForm" <?=($generalData['userLevel'] == 5) ? 'action="?' . http_build_query($_GET) . '"' : ''?>>
                        <input type="hidden" name="page" value="caseinfo">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title"><b>基本資料</b></div>
                            </div>
                            <div class="card-body">
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="caseName">姓名</label>
                                        <input type="text" class="form-control" id="caseName" name="caseName" field="C_name" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="IDnumber">身分證字號</label>
                                        <input type="text" class="form-control" id="IDnumber" name="IDnumber" field="C_idno" maxlength="10" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>身分</label><br>
                                        <select class="form-control py-1" id="C_member" name="C_member" field="C_member">
                                            <?php
                                            $memberArray = [
                                                '1' => '個案',
                                                '2' => '家屬',
                                                '0' => '一般民眾'
                                            ];
                                            foreach ($memberArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>　</label><br>
                                        <?php
                                        $volArray = [
                                            'FCFvolunte' => '基金會志工',
                                        ];
                                        foreach ($volArray as $name => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"{$name}\" field=\"{$name}\">
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label>性別</label><br>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="sex" value="male" field="C_sex" checked required>
                                            <label class="form-check-label">男</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="sex" value="female" field="C_sex">
                                            <label class="form-check-label">女</label>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="birthday">生日</label>
                                        <div class="input-group date" id="birthPick" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="birthday" data-target="#birthPick" field="birthday"
                                                pattern="(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))" required>
                                            <div class="input-group-append" data-target="#birthPick" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="openDate">建檔日期</label>
                                        <div class="input-group date" id="openDate" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="C_opendate" data-target="#openDate" field="C_opendate">
                                            <div class="input-group-append" data-target="#openDate" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="comefrom">得知本會來源</label>
                                        <select class="form-control py-1" id="comefrom" name="comefrom" field="C_comefrom">
                                            <?php
                                            $comeArray = ['自行前來', '醫院介紹', '網路', '朋友介紹', '曾參與活動', '其他'];
                                            foreach ($comeArray as $value) {
                                                echo "<option value=\"{$value}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-2">
                                        <label for="city">市</label>
                                        <div class="select2-input">
                                            <select class="form-control py-1" id="city" name="city" field="C_city"></select>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="zone">區</label>
                                        <div class="select2-input">
                                            <select class="form-control py-1" id="zone" field="C_zone"></select>
                                            <input type="hidden" name="zone" id="hidZone">
                                        </div>
                                    </div>
                                    <div class="form-group col-md-2">
                                        <label for="zip">郵遞區號</label>
                                        <input type="text" class="form-control" id="zip" name="zip" readonly>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="addr">地址</label>
                                        <input type="text" class="form-control" id="addr" name="addr" field="C_address">
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="tel">電話</label>
                                        <input type="text" class="form-control" id="tel" name="tel" field="C_tele" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="email">E-mail</label>
                                        <input type="text" class="form-control" id="email" name="email" field="C_mail">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="mobile1">行動電話</label>
                                        <input type="text" class="form-control" id="mobile1" name="mobile1" field="C_mobile1">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="mobile2">備用行動電話</label>
                                        <input type="text" class="form-control" id="mobile2" name="mobile2" field="C_mobile2">
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="contact">聯絡人姓名</label>
                                        <input type="text" class="form-control" id="contact" name="contact" field="C_contact">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="relation">關係</label>
                                        <input type="text" class="form-control" id="relation" name="relation" field="C_relation">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="reltele">聯絡人電話</label>
                                        <input type="text" class="form-control" id="reltele" name="reltele" field="C_reltele">
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="maritalStatus">婚姻狀況</label>
                                        <select class="form-control py-1" id="maritalStatus" name="maritalStatus" field="C_marriage">
                                            <?php
                                            $marryArray = [
                                                '' => '---',
                                                '未婚' => '未婚',
                                                '已婚' => '已婚',
                                                '同居' => '同居',
                                                '分居' => '分居',
                                                '離異' => '離異',
                                                '喪偶' => '喪偶',
                                                '一方失聯' => '一方失聯'
                                            ];
                                            foreach ($marryArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="haveChilds">子女</label>
                                        <div class="input-group">
                                            <input type="number" class="form-control" id="haveChilds" name="haveChilds" placeholder="0" step="1" min="0" max="20" maxlength="2" field="haveChilds">
                                            <div class="input-group-append">
                                                <div class="input-group-text">名</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="edu">教育程度</label>
                                        <select class="form-control py-1" id="edu" name="edu" field="C_edu">
                                            <?php
                                            $eduArray = [
                                                '' => '---',
                                                '不識字' => '不識字',
                                                '國小' => '國小',
                                                '國中' => '國中',
                                                '高中/高職' => '高中/高職',
                                                '專科' => '專科',
                                                '大學' => '大學',
                                                '研究所(或以上)' => '研究所(或以上)'
                                            ];
                                            foreach ($eduArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group select2-input col-md-3">
                                        <label for="job">職業</label>
                                        <select class="form-control" id="job" name="job" field="C_job">
                                            <?php
                                            $jobArray = [
                                                '0' => '---',
                                                '1' => '一般職業(機關團體/公司行號)',
                                                '2' => '農牧業',
                                                '3' => '漁業',
                                                '4' => '木材森林業',
                                                '5' => '礦業採石業',
                                                '6' => '交通運輸業',
                                                '7' => '餐旅業',
                                                '8' => '建築工程業',
                                                '9' => '製造業',
                                                '10' => '新聞廣告業',
                                                '11' => '保健業(醫院人員/保健人員)',
                                                '12' => '娛樂業',
                                                '13' => '文教機關(教職員/學生)',
                                                '14' => '宗教團體',
                                                '15' => '公共事業',
                                                '16' => '一般商業',
                                                '17' => '服務業(銀行/保險/信託/租賃/自由業/殯葬業/其他)',
                                                '18' => '家庭管理(家管/佣人/保母)',
                                                '19' => '治安人員',
                                                '20' => '軍人',
                                                '21' => '資訊業',
                                                '22' => '職業運動人員',
                                                '23' => '公務人員',
                                                '24' => '待業中',
                                                '25' => '退休'
                                            ];
                                            foreach ($jobArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-sm-12">
                                        <label>福利身分</label><br>
                                        <?php
                                        $welfareArray = [
                                            'id_normal' => '一般',
                                            'id_lowincome' => '低/中低收入',
                                            'id_weakincome' => '經濟弱勢',
                                            'id_oldman' => '老人',
                                            'id_handicapped' => '身心障礙',
                                            'id_indigenous' => '原住民',
                                            'id_foreign' => '新住民',
                                            'id_singlemon' => '單親',
                                            'id_specstatus' => '特殊境遇'
                                        ];

                                        foreach ($welfareArray as $name => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"{$name}\" field=\"{$name}\">
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-sm-12">
                                        <label>保險情況</label><br>
                                        <?php
                                        $insArray = [
                                            'Ins_none' => '無',
                                            'Ins_health' => '健保',
                                            'Ins_social' => '福保',
                                            'Ins_work' => '勞保',
                                            'Ins_gov' => '公保',
                                            'Ins_fisher' => '漁保',
                                            'Ins_farmer' => '農保',
                                            'Ins_soldier' => '軍保',
                                            'Ins_commerce' => '商業保險'
                                        ];

                                        foreach ($insArray as $name => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" name=\"{$name}\" field=\"{$name}\">
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                        <div class="form-check form-check-inline mr-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="1" id="Ins_others">
                                                <span class="form-check-sign unselectable">其他</span>
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <input type="text" class="form-control form-control-sm" name="Ins_others" field="Ins_others">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="life_ability">日常生活功能</label>
                                        <select class="form-control py-1" id="life_ability" name="life_ability" field="life_ability">
                                            <?php
                                            $lifeArray = [
                                                '0' => '---',
                                                '1' => '正常',
                                                '2' => '需要他人幫忙',
                                                '3' => '需要輔助用具',
                                                '4' => '完全無法自行活動'
                                            ];
                                            foreach ($lifeArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="caregiver">主要照顧者</label>
                                        <select class="form-control py-1" id="caregiver" name="caregiver" field="caregiver">
                                            <?php
                                            $caregiverArray = [
                                                '0' => '---',
                                                '1' => '父母',
                                                '2' => '配偶',
                                                '3' => '子女',
                                                '4' => '同居人',
                                                '5' => '朋友',
                                                '6' => '其他'
                                            ];
                                            foreach ($caregiverArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="houseown">現在住所</label>
                                        <select class="form-control py-1" id="houseown" name="houseown" field="houseown">
                                            <?php
                                            $houseownArray = [
                                                '0' => '---',
                                                '1' => '自宅無貸款',
                                                '2' => '自宅有貸款',
                                                '3' => '租屋',
                                                '4' => '借住',
                                                '5' => '其他',
                                            ];
                                            foreach ($houseownArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="residence">居住狀況</label>
                                        <select class="form-control py-1" id="residence" name="residence" field="residence">
                                            <?php
                                            $residenceArray = [
                                                '0' => '---',
                                                '1' => '與家人同住',
                                                '2' => '獨居',
                                                '3' => '醫療機構',
                                                '4' => '安置機構',
                                                '5' => '無固定住所',
                                                '6' => '服刑中',
                                                '7' => '其他'
                                            ];
                                            foreach ($residenceArray as $key => $value) {
                                                echo "<option value=\"{$key}\">{$value}</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="cohabitation">同住家人</label>
                                        <input type="text" class="form-control" id="cohabitation" name="cohabitation" placeholder="同住家人" field="cohabitation">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="payMonth">居住支出/月</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="payMonth" name="payMonth" placeholder="居住支出/月" field="payMonth">
                                            <div class="input-group-append">
                                                <div class="input-group-text">元</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-sm-12">
                                        <label>經濟狀況</label><br>
                                        <?php
                                        $ecoArray = [
                                            '0' => '自己有工作',
                                            '1' => '政府補助',
                                            '2' => '父母扶養',
                                            '3' => '子女提供',
                                            '4' => '親友提供',
                                            '5' => '其他'
                                        ];

                                        foreach ($ecoArray as $value => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"{$value}\" name=\"ecoStatus[]\" field=\"ecoStatus{$value}\">
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                        <div class="form-check form-check-inline mr-0">
                                            <label class="form-check-label">
                                                <span class="form-check-sign unselectable">月收入</span>
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <input type="text" class="form-control form-control-sm" name="monthIncome" field="monthIncome">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-12">
                                        <label>慢性疾病史</label><br>
                                        <?php
                                        $chronicArray = [
                                            '0' => '無',
                                            '1' => '糖尿病',
                                            '2' => '高血壓',
                                            '3' => '心臟病',
                                            '4' => '中風',
                                            '5' => 'COPD',
                                            '6' => 'CRF',
                                        ];

                                        foreach ($chronicArray as $value => $label) {
                                            echo "
                                            <div class=\"form-check form-check-inline\">
                                                <label class=\"form-check-label\">
                                                    <input class=\"form-check-input\" type=\"checkbox\" value=\"{$value}\" name=\"chronic[]\" field=\"chronic{$value}\">
                                                    <span class=\"form-check-sign unselectable\">{$label}</span>
                                                </label>
                                            </div>";
                                        }
                                        ?>
                                        <div class="form-check form-check-inline mr-0">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" value="7" id="otherChronic" name="chronic[]" field="chronic7">
                                                <span class="form-check-sign unselectable">其他</span>
                                            </label>
                                        </div>
                                        <div class="form-check-inline">
                                            <input type="text" class="form-control form-control-sm" name="otherChronic" field="otherChronic">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="C_note">備註</label>
                                    <textarea class="form-control" id="C_note" name="C_note" rows="5" field="C_note"></textarea>
                                </div>
                            </div>
                            <?php
                            // 家屬不需要疾病史
                            if ($subPage == 'personalCase') {
                            ?>
                            <div class="card-header">
                                <div class="card-title">
                                    <b>疾病史</b>
                                    <button type="button" class="btn btn-outline-danger btn-round btn-sm float-sm-right mx-2" id="delCancer">
                                        <span class="btn-label"><i class="fas fa-minus"></i> 刪除</span>
                                    </button>
                                    <button type="button" class="btn btn-outline-success btn-round btn-sm float-sm-right mx-2" id="addCancer">
                                        <span class="btn-label"><i class="fas fa-plus"></i> 新增</span>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="accordion" id="accordionCancer">
                                    <!-- 動態產生 -->
                                    <div id="dynamaticArea" style="display:none;"></div>
                                    <!-- 動態產生 -->
                                </div>
                            </div>
                            <!-- 疾病史結尾 -->
                            <?php
                            } elseif ($subPage == 'family') {
                            ?>
                            <div class="card-header">
                                <div class="card-title"><b>家屬疾病史</b></div>
                            </div>
                            <div class="card-body">
                                <div class="form-row px-2">
                                    <div class="form-group col-md-4">
                                        <label for="familyCancer">家屬癌別</label>
                                        <div class="select2-input select2-info">
                                            <select class="form-control py-1" id="familyCancer" name="familyCancer[]" multiple></select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 家屬疾病史結尾 -->
                            <?php
                            }
                            ?>
                            <input type="hidden" id="count" name="count" value="">
                            <div class="card-header">
                                <div class="card-title"><b>個資條款與服務</b></div>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <tr class="form-row">
                                        <td class="col-md-2">
                                            <b class="text-danger">個資保密條款</b>
                                        </td>
                                        <td class="col-md-10">
                                            <div class="mb-3">
                                                <div class="form-check p-0">
                                                    <label class="form-check-label" style="white-space: normal;">
                                                        <input class="form-check-input" type="checkbox" value="1" name="privacy" field="privacy" required>
                                                        <span class="form-check-sign unselectable">
                                                            <b><u>我同意，將我個人資料作為基金會癌友關懷服務之聯繫、活動簡訊通知及本會相關問卷研究之用，並且我了解我的個人資料將被保密，不作其他用途。</u></b>
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="card border-info mb-3" style="width: 442px; border: solid 1px;">
                                                <div class="card-header">
                                                    <span class="align-middle">申請人簽名或蓋章：</span>
                                                    <button type="button" class="btn btn-sm btn-outline-info float-sm-right" id="reSign">重新簽名</button>
                                                </div>
                                                <div class="card-body">
                                                    <input type="hidden" name="handSignature" id="handSignature">
                                                    <canvas id="signature-pad" width="400" height="200"></canvas>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <!-- <tr class="form-row">
                                        <td class="col-md-2">
                                            <b>是否需要本會最新活動簡訊</b>
                                        </td>
                                        <td class="col-md-10">
                                            <div>
                                                <div class="form-check p-0">
                                                    <label class="form-radio-label">
                                                        <input class="form-radio-input" type="radio" value="1" name="getSMS" checked field="getSMS">
                                                        <span class="form-radio-sign unselectable">
                                                            是的，我需要基金會最新活動消息
                                                        </span>
                                                    </label>
                                                </div>
                                                <div class="form-check p-0">
                                                    <label class="form-radio-label">
                                                        <input class="form-radio-input" type="radio" value="0" name="getSMS">
                                                        <span class="form-radio-sign unselectable">
                                                            暫時不用，謝謝!!
                                                        </span>
                                                    </label>
                                                </div>
                                            </div>
                                        </td>
                                    </tr> -->
                                    <tr class="form-row">
                                        <td class="col-md-2">
                                            <b>會訊訂閱服務(三個月一本)</b>
                                        </td>
                                        <td class="col-md-10">
                                            <div>
                                                <div class="form-check form-check-inline p-0">
                                                    我要訂閱
                                                </div>
                                                <div class="form-check form-check-inline p-0">
                                                    <div class="custom-control custom-radio mr-0">
                                                        <input class="custom-control-input" type="radio" value="1" name="C_subscribe" field="C_subscribe" id="C_subscribe1" required>
                                                        <label class="custom-control-label my-0 unselectable" for="C_subscribe1">紙本</label>
                                                    </div>
                                                </div>
                                                <div class="form-check form-check-inline p-0">
                                                    <div class="custom-control custom-radio">
                                                        <input class="custom-control-input" type="radio" value="2" name="C_subscribe" field="C_subscribe" id="C_subscribe2" required>
                                                        <label class="custom-control-label my-0 unselectable" for="C_subscribe2">電子報，記得填寫e-mail</label>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="form-check form-check-inline p-0">
                                                    <div class="custom-control custom-radio">
                                                        <input class="custom-control-input" type="radio" value="0" name="C_subscribe" field="C_subscribe" id="C_subscribe0" required>
                                                        <label class="custom-control-label my-0 unselectable" for="C_subscribe0">我不用，謝謝!</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="form-row">
                                        <td class="col-md-2">
                                            <b>評估量表</b>
                                        </td>
                                        <td class="col-md-10">
                                            <div>
                                                <div class="form-check form-check-inline p-0">
                                                    我
                                                </div>
                                                <div class="form-check form-check-inline p-0">
                                                    <div class="custom-control custom-radio mr-0">
                                                        <input class="custom-control-input" type="radio" value="1" name="assessForm" field="assessForm" id="assessForm1" required>
                                                        <label class="custom-control-label my-0 unselectable" for="assessForm1">需要</label>
                                                    </div>
                                                </div>
                                                <div class="form-check form-check-inline p-0">
                                                    <div class="custom-control custom-radio mr-0">
                                                        <input class="custom-control-input" type="radio" value="0" name="assessForm" field="assessForm" id="assessForm0" required>
                                                        <label class="custom-control-label my-0 unselectable" for="assessForm0">不需要</label>
                                                    </div>
                                                </div>
                                                <div class="form-check form-check-inline p-0">
                                                    填寫評估量表
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="card-action">
                                <button type="button" class="btn btn-success" id="submitForm">儲存</button>
                                <button type="button" class="btn btn-danger" id="goBack">取消</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include realpath(dirname(__FILE__) . '/cancerHistory.php');
?>

<script>
    "use strict";
    var count = 0;
    var className = "disease";
    var monthDateTime = ["consult", "Surgery_date" ,"recurrence"];
    var dayDateTime = ["Chemical_date_S", "Chemical_date_E", "Target_date_S", "Target_date_E", "Immune_date_S", "Immune_date_E", "Radiat_date_S", "Radiat_date_E", "hormone_date_S", "hormone_date_E"];
    var otherFields = ["Ins_others", "otherChronic"];
    var listFields = ["ecoStatus", "chronic"];
    var dataArray = <?=$memberDataJSON?>;
    var cancerDataArray = <?=$cancerHistoryJSON?>;
    var dbSignature = "<?=$handSignature?>";
    // 只有個案新增資料會用到
    var dulplicate = Number("<?=(isset($_GET['dulplicate']) && $generalData['userLevel'] == 5) ? 1 : 0?>");

    function additems(data) {
        if ($("#dynamaticArea").length != 0) {
            data = data || "";
            let classString = className + count;
            let addForm = $("#itemTemplate").clone();
            if (count == 0) {
                $(addForm).find(".delDisease").remove();
            }
            // 動態改變選單的ID
            $(addForm).find("#headID button:first-child > span").text(count + 1);
            $(addForm).find("#headID button:first-child").attr({
                "data-target": "#content_" + classString,
                "aria-controls": "content_" + classString
            });
            // 原本的headID 會被改掉
            $(addForm).find("#headID").attr("id", "header_" + classString);
            $(addForm).find(".collapse").attr({
                "id": "content_" + classString,
                "aria-labelledby": "header_" + classString
            });

            // checkbox 需要陣列參數
            $(addForm).find("[type='checkbox']").attr("name", function () {
                return $(this).attr("field") + "[" + count + "]";
            });

            // datetimePicker需要修改獨立ID
            $(addForm).find(".date").attr("id", function(){
                let id = $(this).attr("data") + count;
                $(this).find(".datetimepicker-input, .input-group-append").attr("data-target", "#" + id);
                return id
            });

            $("#dynamaticArea").before($(addForm).html());
            // 有資料
            if (data != '') {
                let items = $("[id=content_" + className + count + "]");
                $.each(data, function(key, value) {
                    let inputField = $(items).find("[field='" + key + "']");
                    let inputType = $(inputField).attr("type") || $(inputField).prop("tagName");
                    if (inputType != undefined) {
                        inputType = inputType.toLowerCase();
                    }
                    // 依據不同的 input 類型，做不同的塞值動作(單個欄位對應單筆資料)
                    if (inputType == "text" || inputType == "textarea" || inputType == "number" || inputType == "hidden") {
                        $(inputField).val(value);
                    } else if (inputType == "checkbox" && Boolean(value)) {
                        $(inputField).prop("checked", true);
                    } else if (inputType == "select") {
                        $(inputField).val(value).trigger("change");
                    }
                });
            }
            // 疾病史內的欄位要加上套件效果
            $("#content_" + classString + " [data='cancerName']").select2({
                theme: "bootstrap",
                width: '100%'
            });

            $.each(monthDateTime, function (i, v) {
                $("#content_" + classString + " #" + v + count).datetimepicker({
                    format: 'YYYY-MM',
                    allowInputToggle: true,
                    useStrict: true,
                    debug: false
                });
            })

            $.each(dayDateTime, function (i, v) {
                $("#content_" + classString + " #" + v + count).datetimepicker({
                    format: 'YYYY-MM-DD',
                    allowInputToggle: true,
                    useStrict: true,
                    debug: false
                });
            })

            count++;
            $("#count").val(count);
            $("#content_" + classString).collapse('show');
        } else {
            // 家屬
            $.each(data, function(key, value) {
                if (key == 'Cancer_name') {
                    $("#familyCancer").find("option:contains(" + value + ")").prop("selected", true).trigger("change");
                }
            });
        }
    }

    function delitems(e) {
        if (count > 1) {
            count--;
            let classString = className + count;
            $("#header_" + classString).remove();
            $("#content_" + classString).remove();
            $("#count").val(count);
            $("#content_" + className + (count-1)).collapse('show');
        } else {
            e.preventDefault();
        }
    }

    function init() {
        // 簽名板
        var wrapper = document.getElementById("signature-pad");
        var canvas = wrapper.querySelector("canvas");
        var signaturePad = new SignaturePad(wrapper, {
            backgroundColor: 'rgb(255, 255, 255)'
        });
        $("#reSign").on("click", function () {
            signaturePad.clear();
        })
        if (dbSignature != '') {
            signaturePad.fromDataURL(dbSignature);
        }

        doubleSelect('city', 'zone', 'zip');
        // 地址區域需要獨立撈出來
        $("#zone").on("change", function () {
           $("#hidZone").val($(this).find("option:selected").text());
        });

        // 把每個欄位都填資料
        $.each(dataArray, function(field, value) {
            let inputField = $("[field='" + field + "']");
            let inputType = $(inputField).attr("type") || $(inputField).prop("tagName");

            if (inputType != undefined) {
                inputType = inputType.toLowerCase();
            }

            // 單欄位對應多筆資料
            if ($.inArray(field, listFields) != -1) {
                let chkArray = value.split(",").map(function(item) {
                    return item.trim();
                });
                $.each(chkArray, function(i,v) {
                    $("[field='" + field + v + "']").prop("checked", true);
                });
            } else {
                // 依據不同的 input 類型，做不同的塞值動作(單個欄位對應單筆資料)
                if (inputType == "text" || inputType == "textarea") {
                    $(inputField).val(value);
                    // 其他欄位有值需要勾選
                    if ($.inArray(field, otherFields) != -1 && value != "") {
                        $("#" + field).prop("checked", true);
                    }
                } else if (inputType == "checkbox" && Boolean(value)) {
                    $(inputField).prop("checked", true);
                } else if (inputType == "radio") {
                    $.each($(inputField), function(i) {
                        if ($(this).val() == value) {
                            $(this).prop("checked", true);
                        }
                    });
                } else if (inputType == "select") {
                    if ($(inputField).prop("multiple")) {

                    } else {
                        // 地址區域另外處理
                        if (field == "C_zone") {
                            $(inputField).find("option:contains(" + value + ")").prop("selected", true).trigger("change");
                        } else {
                            $(inputField).val(value).trigger("change");
                        }
                    }
                }
            }
        });

        $.each(cancer, function (i, item) {
            $('[data=cancerName], #familyCancer').append($('<option>', {
                value: item,
                text : item
            }));
        });

        $('#city, #zone, #job, #familyCancer').select2({
            theme: "bootstrap",
            width: '100%'
        });

        $('#birthPick, #openDate').datetimepicker({
            format: 'YYYY-MM-DD',
            allowInputToggle: true,
            useStrict: true,
            debug: false

        });

        // 其他欄位有值，需勾選，無值不勾選
        $.each(otherFields, function(i, fieldID) {
            $("[field='" + fieldID + "']").on("change", function() {
                if ($(this).val() == "") {
                    $("#" + fieldID).prop("checked", false);
                } else {
                    $("#" + fieldID).prop("checked", true);
                }
            });
        });

        // 定義動態疾病史
        $("#addCancer").on("click", function () {
            additems();
        });
        $("#delCancer").on("click", delitems);

        // 若是新增的話這邊只會跑一個，空陣列資料
        $.each(cancerDataArray, function(key, obj) {
            additems(obj);
        });

        // 個資保密必須同意
        $("input[field='privacy']").prop("checked",true);
        $("input[field='privacy']").on("click", function(e) {
            e.preventDefault();
        });

        // 送出表單
        $("#submitForm").on("click", function (e) {
            if (valid(e)) {
                $("#handSignature").val(signaturePad.toDataURL());
                $(this).closest("form").submit();
            }
        });

        // 回上一頁
        $("#goBack").on("click", function () {
            history.back();
        });

        // 個案權限
        if (dulplicate == 1) {
            swal ("資料重複", "該身分證字號已存在，請基金會人員協助", "error", {
                button: "我知道了"
            });
        }
    }

    window.onload = init;
</script>