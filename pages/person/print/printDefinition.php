<?php
/**
 * 列印欄位對應定義
 * **資料庫欄位相關**
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

$printDef = [
    'basicData' => [
        'name' => '個案基本資料',
        'welfare' => [
            'categoryTitle' => '福利身分',
            'id_normal' => '一般',
            'id_lowincome' => '低/中低收入',
            'id_weakincome' => '經濟弱勢',
            'id_oldman' => '老人',
            'id_handicapped' => '身心障礙',
            'id_indigenous' => '原住民',
            'id_foreign' => '新住民',
            'id_singlemon' => '單親',
            'id_specstatus' => '特殊境遇',
        ],
        'insurance' => [
            'categoryTitle' => '保險情況',
            'Ins_none' => '無',
            'Ins_health' => '健保',
            'Ins_social' => '福保',
            'Ins_work' => '勞保',
            'Ins_gov' => '公保',
            'Ins_fisher' => '漁保',
            'Ins_farmer' => '農保',
            'Ins_soldier' => '軍保',
            'Ins_commerce' => '商業保險',
        ],
        'treatment' => [
            'categoryTitle' => '現在治療方式',
            'Chemical' => '化學治療',
            'Target' => '標靶治療',
            'Immune' => '免疫治療',
            'Radiation' => '放射線治療',
            'hormone' => '抗賀爾蒙治療',
            'integrated' => '中西醫整合療法',
            'tracking' => '定期門診追蹤',
            'peace' => '安寧療護',
            'other' => '其他治療',
        ],
    ],

    'med' => [
        'name' => '醫護紀錄',
        'Medical' => [
            'categoryTitle' => '醫療需求',
            'Med_cure' => '癌症治療相關問題',
            'Med_check' => '癌症檢查相關問題',
            'Med_drug' => '藥品健保相關問題',
            'Med_clinical' => '臨床試驗',
            'Med_suggest' => '就醫(跨科/院/國)建議',
            'Med_assist' => '輔助療法',
        ],
        'Nurse' => [
            'categoryTitle' => '護理需求',
            'Nur_surgery' => '術前術後衛教',
            'Nur_chemical' => '化學治療衛教',
            'Nur_target' => '標靶治療衛教',
            'Nur_radiation' => '放射線治療衛教',
            'Nur_drug' => '其他藥物衛教',
            'Nur_home' => '居家照護衛教',
            'Nur_assuage' => '緩和照護衛教',
            'Nur_hospital' => '住院期間其他疑問之衛教',
            'Nur_food' => '原則性飲食衛教',
            'Nur_mood' => '情緒支持',
        ],
        'Rights' => [
            'categoryTitle' => '權利倡議',
            'Right_drugpay' => '健保藥品給付',
            'Right_newdrug' => '新藥審查及使用條件',
            'Right_buydrug' => '自購藥物管道',
            'Right_clinical' => '臨床試驗',
            'Right_doctor' => '醫病關係',
            'Right_quality' => '醫療品質',
            'Right_other' => '其他',
        ],
    ],
    'nutrition' => [
        'name' => '營養紀錄',
        'Aim' => [
            'categoryTitle' => '諮詢目的',
            'Aim_therapeutic' => '治療飲食',
            'Aim_nutrition' => '營養品補助',
            'Aim_balance' => '均衡營養',
            'Aim_countermeasures' => '生理狀況之飲食對策',
            'Aim_other' => '其他',
        ],
        'Provide' => [
            'categoryTitle' => '進食方式',
            'Pv_self' => '由口進食',
            'Pv_ng' => '鼻胃管餵食',
            'Pv_gastric' => '胃管餵食',
            'Pv_intestinal' => '腸管餵食',
            'Pv_ppn' => '週邊靜脈營養',
            'Pv_tpn' => '全靜脈營養',
            'Pv_other' => '其他',
        ],
        'Surgery' => [
            'categoryTitle' => '飲食衛教',
            'beforeSurgery' => '手術前飲食衛教',
            'afterSurgery' => '手術後飲食衛教',
        ],
        'Cure' => [
            'categoryTitle' => '治療期間飲食衛教',
            'Cure_calorie' => '熱量需求',
            'Cure_protein' => '蛋白質需求與估算',
        ],
        'Effect' => [
            'categoryTitle' => '生理狀況之飲食對策',
            'Effect_vomit' => '噁心嘔吐 ',
            'Effect_oral' => '口腔破損',
            'Effect_full' => '飽脹感',
            'Effect_flatulence' => '脹氣',
            'Effect_diarrhea' => '腹瀉',
            'Effect_constipation' => '便秘',
            'Effect_lowHeme' => '血色素不足',
            'Effect_lowLeukocyte' => '白血球低下',
            'Effect_appetiteLoss' => '食慾不振',
            'Effect_weightLoss' => '體重下降',
            'Effect_gainWeight' => '體重增加',
            'Effect_other' => '其他',
        ],
        'Diet' => [
            'categoryTitle' => '飲食原則',
            'Diet_blance' => '均衡飲食',
            'Diet_diabetes' => '糖尿病飲食',
            'Diet_renalFailure' => '慢性腎衰竭飲食',
            'Diet_dialysis' => '透析飲食',
            'Diet_liver' => '肝病飲食',
            'Diet_other' => '其他',
        ],
        'Nutrition' => [
            'categoryTitle' => '保健營養品相關資訊',
            'Nu_panax' => '蔘類',
            'Nu_algae' => '藻類',
            'Nu_vitamins' => '維生素類',
            'Nu_calcium' => '鈣片',
            'Nu_mushroom' => '菇菌類保健品',
            'Nu_phytochemicals' => '植化素萃取物',
            'Nu_other' => '其他',
        ],
        'Eat' => [
            'categoryTitle' => '飲食製備與禁忌資訊',
            'Eat_normal' => '普通飲食',
            'Eat_liquid' => '流質飲食',
            'Eat_bland' => '溫和飲食',
            'Eat_lowResidue' => '低渣飲食',
            'Eat_highFiber' => '高纖飲食',
            'Eat_tube' => '管灌飲食',
            'Eat_elemental' => '元素飲',
            'Eat_other' => '其他',
        ],
        'Special' => [
            'categoryTitle' => '特殊營養品需求',
            'Sp_glutamine' => '麩醯胺酸',
            'Sp_balanced' => '均衡配方',
            'Sp_tumor' => '腫瘤配方',
            'Sp_pneumonia' => '肺病配方',
            'Sp_diabetes' => '糖尿病配方',
            'Sp_nephropathy' => '腎病配方',
            'Sp_other' => '其他',
        ],
    ],
    'social' => [
        'name' => '社工紀錄',
        'Aim' => [
            'categoryTitle' => '諮詢目的',
            'Aim_wig' => '假髮租借',
            'Aim_welfare' => '福利諮詢',
            'Aim_goods' => '物資需求',
            'Aim_volcare' => '志工關懷需求',
            'Aim_other' => '其他',
        ],
        'Grants' => [
            'categoryTitle' => '補助評估',
            'Grant_help' => '急難救助',
            'Grant_nutrition' => '營養品補助',
            'Grant_traffic' => '交通補助',
            'Grant_respitecare' => '喘息服務',
        ],
    ],
    'mental' => [
        'name' => '心理紀錄',
        'Mind' => [
            'categoryTitle' => '諮詢目的',
            'Mind_feeling' => '情緒議題',
            'Mind_self' => '自我概念',
            'Mind_adapt' => '生活適應',
            'Mind_friends' => '人際關係',
            'Mind_family' => '家庭議題',
            'Mind_sickbed' => '疾病末期',
            'Mind_suicide' => '自殺意念',
            'Mind_selfcare' => '自我照顧',
            'Mind_lost' => '失落經驗',
            'Mind_other' => '其他',
        ],
    ],
    'insurance' => [
        'name' => '保險紀錄',
        'Ins_med' => [
            'categoryTitle' => '醫療相關',
            'Ins_admission' => '住院給付',
            'Ins_clinic' => '門診給付',
            'Ins_surgery' => '手術給付',
            'Ins_expenses' => '自費醫材給付',
            'Ins_targeted' => '標靶藥物給付',
            'Ins_radiology' => '放射治療給付',
            'Ins_chemotherapy' => '化學治療給付',
            'Ins_immunotherapy' => '免疫治療給付',
            'Ins_discharged' => '出院帶藥給付',
            'Ins_tcm' => '中醫門診',
            'Ins_hospice' => '安寧病房給付',
        ],
        'Ins_policy' => [
            'categoryTitle' => '保單相關',
            'Ins_view' => '保單檢視',
            'Ins_plan' => '保險規劃',
            'Ins_law' => '保險法律',
        ],
    ],
    'care' => [
        'name' => '志工關懷',
        'Act' => [
            'categoryTitle' => '提供服務',
            'Act_care' => '近況瞭解及關懷',
            'Act_expshare' => '療程經驗分享',
            'Act_support' => '情緒支持',
            'Act_other' => '其他',
        ],
        'Transfer' => [
            'categoryTitle' => '轉介專業人員',
            'Trans_medical' => '護理師',
            'Trans_nutrition' => '營養師',
            'Trans_social' => '社工師',
            'Trans_mental' => '心理師',
        ],
    ],

    'transfer' => [
        'name' => '轉介單',
    ],
];

// 其他用到的參數
$service = [
    'tel' => '電話',
    'face' => '面談',
    'web' => '網路',
    'email' => '電子郵件',
];

$Cal_activity = [0 => '', 1.2 => '臥床', 1.3 => '輕度活動', 1.4 => '中度活動'];

// 個案資料庫轉文字
$memberArray = [
    '1' => '個案',
    '2' => '家屬',
    '0' => '一般民眾',
];

$sexArray = [
    'male' => '男',
    'female' => '女',
];

$jobArray = [
    '0' => '---',
    '1' => '一般職業(機關團體/公司行號)',
    '2' => '農牧業',
    '3' => '漁業',
    '4' => '木材森林業',
    '5' => '礦業採石業',
    '6' => '交通運輸業',
    '7' => '餐旅業',
    '8' => '建築工程業',
    '9' => '製造業',
    '10' => '新聞廣告業',
    '11' => '保健業(醫院人員/保健人員)',
    '12' => '娛樂業',
    '13' => '文教機關(教職員/學生)',
    '14' => '宗教團體',
    '15' => '公共事業',
    '16' => '一般商業',
    '17' => '服務業(銀行/保險/信託/租賃/自由業/殯葬業/其他)',
    '18' => '家庭管理(家管/佣人/保母)',
    '19' => '治安人員',
    '20' => '軍人',
    '21' => '資訊業',
    '22' => '職業運動人員',
    '23' => '公務人員',
    '24' => '待業中',
    '25' => '退休',
];

$lifeArray = [
    '0' => '---',
    '1' => '正常',
    '2' => '需要他人幫忙',
    '3' => '需要輔助用具',
    '4' => '完全無法自行活動',
];

$caregiverArray = [
    '0' => '---',
    '1' => '父母',
    '2' => '配偶',
    '3' => '子女',
    '4' => '同居人',
    '5' => '朋友',
    '6' => '其他',
];

$houseownArray = [
    '0' => '---',
    '1' => '自宅無貸款',
    '2' => '自宅有貸款',
    '3' => '租屋',
    '4' => '借住',
    '5' => '其他',
];
$residenceArray = [
    '0' => '---',
    '1' => '與家人同住',
    '2' => '獨居',
    '3' => '醫療機構',
    '4' => '安置機構',
    '5' => '無固定住所',
    '6' => '服刑中',
    '7' => '其他',
];
$ecoArray = [
    '0' => '自己有工作',
    '1' => '政府補助',
    '2' => '父母扶養',
    '3' => '子女提供',
    '4' => '親友提供',
    '5' => '其他',
];

$chronicArray = [
    '0' => '無',
    '1' => '糖尿病',
    '2' => '高血壓',
    '3' => '心臟病',
    '4' => '中風',
    '5' => 'COPD',
    '6' => 'CRF',
    '7' => '其他',
];

$cancerLevel = [
    '0' => '原位癌',
    '1' => '第一期',
    '2' => '第二期',
    '3' => '第三期',
    '4' => '第四期',
];
