<?php
/**
 * 列印諮詢紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

// 清除先前頁面要顯示的快取
ob_end_clean();

require_once 'printDefinition.php';

// 準備資料
$rowdata = json_decode(urldecode(reqParam('rowdata', 'post')));
$recType = reqParam('recType', 'post');

/**
 * 將勾選項目轉換成文字輸出
 *
 * @param object $rowdata
 * @param array $categoryArray 該諮詢紀錄定義的陣列
 * @param string $defType 陣列中的勾選類型
 * @return void
 */
function findCheck($rowdata, $categoryArray, $defType)
{
    $returnText = '';
    $title = $categoryArray[$defType]['categoryTitle'];
    unset($categoryArray[$defType]['categoryTitle']);
    // 找尋勾選項目
    foreach ($categoryArray[$defType] as $col => $label) {
        if ($rowdata->$col == 1) {
            $returnText .= "{$label},";
        }
    }
    if (!empty($returnText)) {
        $returnText = '<tr><td>' . $title . '</td><td colspan="3">' . substr($returnText, 0, -1) . '</td></tr>';
    }

    return $returnText;
}

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF
{

    //Page header
    public function Header()
    {
        // Logo
        $image_file = "{$_SERVER['DOCUMENT_ROOT']}uploads/images/logo.png";
        $this->Image($image_file, '', '', 50, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }

}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('skjan');
$pdf->SetTitle('諮詢紀錄列印');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once dirname(__FILE__) . '/lang/eng.php';
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('taipeisans', 'B', 16);

// add a page
$pdf->AddPage();

$pdf->Write(0, $printDef[$recType]['name'], '', 0, 'C', true, 0, false, false, 0);
$pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);

// 表格字體
$pdf->SetFont('taipeisans', '', 12);

// -----------------------------------------------------------------------------
if (!in_array($recType, ['transfer', 'basicData'])) {
    $serviceTime = "{$rowdata->R_time}~" . ($rowdata->R_time + 10) . ' 分鐘';
}
switch ($recType) {
    case 'basicData':
        // 個案ID
        $id = intval(reqParam('id', 'get'));
        // 個案資料
        $dbQuery = "SELECT *, STR_TO_DATE(CONCAT(BD_yy,'-',BD_mm,'-',BD_dd), '%Y-%c-%e') AS birthday
                    FROM FCF_careservice.Memberdata m WHERE m.IDno=?";
        $rowdata = json_decode(json_encode($db->row($dbQuery, [$id])));
        // 輸出參數
        $volunteer = ($rowdata->FCFvolunte === 1) ? '是' : '否';
        // 簽名
        $handSignInTable = '';
        if (!empty($rowdata->handSignature)) {
            $handSign = explode(',', $rowdata->handSignature)[1];
            $handSignInTable = <<<EOD
            <tr><td>簽名</td><td colspan="3" style="text-align:center;"><img src="@{$handSign}" alt="signature" width="300"></td></tr>
            EOD;
        }
        $welfare = findCheck($rowdata, $printDef[$recType], 'welfare');
        $insurance = findCheck($rowdata, $printDef[$recType], 'insurance');
        $ecoStatus = strtr($rowdata->ecoStatus, $ecoArray);
        $chronic = strtr($rowdata->chronic, $chronicArray);
        // 疾病史
        $dbQuery = "SELECT *, IDno AS cancerID,
                        DATE_FORMAT(Find_date, '%Y-%m') AS findDate,
                        DATE_FORMAT(recurrence, '%Y-%m') AS recurrence_month,
                        DATE_FORMAT(STR_TO_DATE(concat(Surgery_date,'-01'), '%Y-%m-%d'), '%Y-%m') AS surgeryDate
                    FROM FCF_careservice.Rec_cancer WHERE MemberID=? AND `status`=1";
        $cancerHistoryData = json_decode(json_encode($db->query($dbQuery, [$id])));
        $cancerHistoryStr = '';
        foreach ($cancerHistoryData as $num => $cancerData) {
            $count = $num + 1;
            $surgery = ($cancerData->Surgery == 1) ? '是' : '否';
            $recurrence = ($cancerData->Cancer_type == 'New') ? '無' : '有';
            $treatment = findCheck($cancerData, $printDef[$recType], 'treatment');
            $cancerData->R_note = nl2br($cancerData->R_note);
            $rowdata->C_note = nl2br($rowdata->C_note);
            $cancerHistoryStr .= <<<EOD
            <h3>第{$count}筆疾病史</h3>
            <table cellspacing="0" cellpadding="4" border="1">
                <tr><td>癌別</td><td>{$cancerData->Cancer_name}</td><td>期別</td><td>{$cancerLevel[$cancerData->Cancer_level]}</td></tr>
                <tr><td>轉移部位</td><td>{$cancerData->Cancer_trans}</td><td>診斷時間</td><td>{$cancerData->Find_date}</td></tr>
                <tr><td>就診醫院</td><td>{$cancerData->hospital}</td><td>基因檢測</td><td>{$cancerData->genetic_test}</td></tr>
                <tr><td>手術切除</td><td>{$surgery}</td><td>手術時間</td><td>{$cancerData->Surgery_date}</td></tr>
                <tr><td>癌症復發</td><td>{$recurrence}</td><td>復發時間</td><td>{$cancerData->recurrence}</td></tr>
                <tr><td align="center" colspan="4">現在治療方式</td></tr>
                <tr><td>化學治療期間</td><td>{$cancerData->Chemical_date}</td><td>化學治療使用藥名</td><td>{$cancerData->Chemical_name}</td></tr>
                <tr><td>標靶治療期間</td><td>{$cancerData->Target_date}</td><td>標靶治療使用藥名</td><td>{$cancerData->Target_name}</td></tr>
                <tr><td>免疫治療期間</td><td>{$cancerData->Immune_date}</td><td>免疫治療使用藥名</td><td>{$cancerData->Immune_medicine}</td></tr>
                <tr><td>放射線治療期間</td><td>{$cancerData->Radiat_date}</td><td>放射線治療部位/次數</td><td>{$cancerData->Rad_part} {$cancerData->Rad_time}次</td></tr>
                <tr><td>抗賀爾蒙治療期間</td><td>{$cancerData->hormone_date}</td><td>抗賀爾蒙治療使用藥名</td><td>{$cancerData->hormone_medicine}</td></tr>
                {$treatment}
                <tr><td>備註</td><td colspan="3">{$cancerData->R_note}</td></tr>
            </table>
            EOD;
        }

        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>個案編號</td><td>{$rowdata->IDno}</td><td>個案姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>身分證字號</td><td>{$rowdata->C_idno}</td><td>身分</td><td>{$memberArray[$rowdata->C_member]}</td></tr>
            <tr><td>基金會志工</td><td>{$volunteer}</td><td>性別</td><td>{$sexArray[$rowdata->C_sex]}</td></tr>
            <tr><td>生日</td><td>{$rowdata->birthday}</td><td>建檔日期</td><td>{$rowdata->C_opendate}</td></tr>
            <tr><td>得知本會來源</td><td>{$rowdata->C_comefrom}</td><td>電話</td><td>{$rowdata->C_tele}</td></tr>
            <tr><td>地址</td><td colspan="3">{$rowdata->C_zip} {$rowdata->C_city}{$rowdata->C_zone}{$rowdata->C_address}</td></tr>
            <tr><td>行動電話</td><td>{$rowdata->C_mobile1}</td><td>備用行動電話</td><td>{$rowdata->C_mobile2}</td></tr>
            <tr><td>Email</td><td>{$rowdata->C_mail}</td><td>聯絡人姓名</td><td>{$rowdata->C_contact}</td></tr>
            <tr><td>關係</td><td>{$rowdata->C_relation}</td><td>聯絡人電話</td><td>{$rowdata->C_reltele}</td></tr>
            <tr><td>婚姻狀況</td><td>{$rowdata->C_marriage}</td><td>子女</td><td>{$rowdata->haveChilds}名</td></tr>
            <tr><td>教育程度</td><td>{$rowdata->C_edu}</td><td>職業</td><td>{$jobArray[$rowdata->C_job]}</td></tr>
            {$welfare}
            {$insurance}
            <tr><td>日常生活功能</td><td>{$lifeArray[$rowdata->life_ability]}</td><td>主要照顧者</td><td>{$caregiverArray[$rowdata->caregiver]}</td></tr>
            <tr><td>現在住所</td><td>{$houseownArray[$rowdata->houseown]}</td><td>居住狀況</td><td>{$residenceArray[$rowdata->residence]}</td></tr>
            <tr><td>同住家人</td><td>{$rowdata->cohabitation}</td><td>居住支出/月</td><td>{$rowdata->payMonth}</td></tr>
            <tr><td>月收入</td><td>{$rowdata->monthIncome}</td><td colspan="2"></td></tr>
            <tr><td>經濟狀況</td><td colspan="3">{$ecoStatus}</td></tr>
            <tr><td>慢性疾病史</td><td colspan="3">{$chronic}</td></tr>
            <tr><td>備註</td><td colspan="3">{$rowdata->C_note}</td></tr>
            {$handSignInTable}
        </table>
        EOD;
        break;

    case 'med':
        $Medical = findCheck($rowdata, $printDef[$recType], 'Medical');
        $Nurse = findCheck($rowdata, $printDef[$recType], 'Nurse');
        $Rights = findCheck($rowdata, $printDef[$recType], 'Rights');
        $rowdata->R_note = nl2br($rowdata->R_note);
        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>個案編號</td><td>{$rowdata->MemberID}</td><td>個案姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>紀錄人員</td><td>{$rowdata->AdName}</td><td>紀錄日期</td><td>{$rowdata->R_date}</td></tr>
            <tr><td>服務方式</td><td>{$service[$rowdata->R_way]}</td><td>地點</td><td>{$rowdata->R_loc}</td></tr>
            <tr><td>服務時間</td><td>{$serviceTime}</td><td colspan="2"></td></tr>
            <tr><td align="center" colspan="4">紀錄內容</td></tr>
            {$Medical}
            {$Nurse}
            {$Rights}
            <tr><td colspan="4">{$rowdata->R_note}</td></tr>
        </table>
        EOD;
        break;

    case 'nutrition':
        $Aim = findCheck($rowdata, $printDef[$recType], 'Aim');
        $Provide = findCheck($rowdata, $printDef[$recType], 'Provide');
        $Surgery = findCheck($rowdata, $printDef[$recType], 'Surgery');
        $Cure = findCheck($rowdata, $printDef[$recType], 'Cure');
        $Effect = findCheck($rowdata, $printDef[$recType], 'Effect');
        $Surgery = findCheck($rowdata, $printDef[$recType], 'Surgery');
        $Cure = findCheck($rowdata, $printDef[$recType], 'Cure');
        $Effect = findCheck($rowdata, $printDef[$recType], 'Effect');
        $Diet = findCheck($rowdata, $printDef[$recType], 'Diet');
        $Nutrition = findCheck($rowdata, $printDef[$recType], 'Nutrition');
        $Eat = findCheck($rowdata, $printDef[$recType], 'Eat');
        $Special = findCheck($rowdata, $printDef[$recType], 'Special');
        $rowdata->R_note = nl2br($rowdata->R_note);
        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>個案編號</td><td>{$rowdata->MemberID}</td><td>個案姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>紀錄人員</td><td>{$rowdata->AdName}</td><td>紀錄日期</td><td>{$rowdata->R_date}</td></tr>
            <tr><td>服務方式</td><td>{$service[$rowdata->R_way]}</td><td>地點</td><td>{$rowdata->R_loc}</td></tr>
            <tr><td>服務時間</td><td>{$serviceTime}</td><td colspan="2"></td></tr>
            <tr><td align="center" colspan="4">紀錄內容</td></tr>
            <tr><td>身高</td><td>{$rowdata->R_height}</td><td>體重</td><td>{$rowdata->R_weight}</td></tr>
            <tr><td>平常體重</td><td>{$rowdata->general_weight}</td><td>BMI</td><td>{$rowdata->R_BMI}</td></tr>
            <tr><td>體態</td><td colspan="3">{$rowdata->R_analysis}</td></tr>
            {$Aim}
            {$Provide}
            {$Surgery}
            {$Cure}
            {$Effect}
            <tr><td>熱量需求</td><td>{$rowdata->Caloric}</td><td>壓力因子</td><td>{$rowdata->Cal_pressure}</td></tr>
            <tr><td>活動因子</td><td>{$Cal_activity[floatval($rowdata->Cal_activity)]}</td><td>蛋白質需求</td><td>{$rowdata->Protein}</td></tr>
            <tr><td>需求量</td><td>{$rowdata->Protein_factor}</td><td colspan="2"></td></tr>
            {$Surgery}
            {$Cure}
            {$Effect}
            {$Diet}
            {$Nutrition}
            {$Eat}
            {$Special}
            <tr><td colspan="4">{$rowdata->R_note}</td></tr>
        </table>
        EOD;
        break;

    case 'social':
        $Aim = findCheck($rowdata, $printDef[$recType], 'Aim');
        $Grants = findCheck($rowdata, $printDef[$recType], 'Grants');
        $rowdata->R_note = nl2br($rowdata->R_note);
        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>個案編號</td><td>{$rowdata->MemberID}</td><td>個案姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>紀錄人員</td><td>{$rowdata->AdName}</td><td>紀錄日期</td><td>{$rowdata->R_date}</td></tr>
            <tr><td>服務方式</td><td>{$service[$rowdata->R_way]}</td><td>地點</td><td>{$rowdata->R_loc}</td></tr>
            <tr><td>服務時間</td><td>{$serviceTime}</td><td colspan="2"></td></tr>
            <tr><td align="center" colspan="4">紀錄內容</td></tr>
            {$Aim}
            {$Grants}
            <tr><td colspan="4">{$rowdata->R_note}</td></tr>
        </table>
        EOD;
        break;

    case 'mental':
        $Mind = findCheck($rowdata, $printDef[$recType], 'Mind');
        $rowdata->R_note = nl2br($rowdata->R_note);
        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>個案編號</td><td>{$rowdata->MemberID}</td><td>個案姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>紀錄人員</td><td>{$rowdata->AdName}</td><td>紀錄日期</td><td>{$rowdata->R_date}</td></tr>
            <tr><td>服務方式</td><td>{$service[$rowdata->R_way]}</td><td>地點</td><td>{$rowdata->R_loc}</td></tr>
            <tr><td>服務時間</td><td>{$serviceTime}</td><td colspan="2"></td></tr>
            <tr><td align="center" colspan="4">紀錄內容</td></tr>
            {$Mind}
            <tr><td colspan="4">{$rowdata->R_note}</td></tr>
        </table>
        EOD;
        break;

    case 'insurance':
        $Ins_med = findCheck($rowdata, $printDef[$recType], 'Ins_med');
        $Ins_policy = findCheck($rowdata, $printDef[$recType], 'Ins_policy');
        $rowdata->R_note = nl2br($rowdata->R_note);
        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>個案編號</td><td>{$rowdata->MemberID}</td><td>個案姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>紀錄人員</td><td>{$rowdata->AdName}</td><td>紀錄日期</td><td>{$rowdata->R_date}</td></tr>
            <tr><td>服務方式</td><td>{$service[$rowdata->R_way]}</td><td>地點</td><td>{$rowdata->R_loc}</td></tr>
            <tr><td>服務時間</td><td>{$serviceTime}</td><td colspan="2"></td></tr>
            <tr><td align="center" colspan="4">紀錄內容</td></tr>
            {$Ins_med}
            {$Ins_policy}
            <tr><td colspan="4">{$rowdata->R_note}</td></tr>
        </table>
        EOD;
        break;

    case 'care':
        $Act = findCheck($rowdata, $printDef[$recType], 'Act');
        $Transfer = findCheck($rowdata, $printDef[$recType], 'Transfer');
        $rowdata->R_note = nl2br($rowdata->R_note);
        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>個案編號</td><td>{$rowdata->MemberID}</td><td>個案姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>紀錄人員</td><td>{$rowdata->AdName}</td><td>紀錄日期</td><td>{$rowdata->R_date}</td></tr>
            <tr><td>服務方式</td><td>{$service[$rowdata->R_way]}</td><td>地點</td><td>{$rowdata->R_loc}</td></tr>
            <tr><td>服務時間</td><td>{$serviceTime}</td><td>關懷者姓名</td><td>{$rowdata->Contactpeop}</td></tr>
            <tr><td align="center" colspan="4">紀錄內容</td></tr>
            {$Act}
            {$Transfer}
            <tr><td colspan="4">{$rowdata->R_note}</td></tr>
        </table>
        EOD;
        break;

    case 'transfer':
        $rowdata->refComment = nl2br($rowdata->refComment);
        $rowdata->refReply = nl2br($rowdata->refReply);
        $printTable = <<<EOD
        <table cellspacing="0" cellpadding="4" border="1">
            <tr><td>個案編號</td><td>{$rowdata->MemberID}</td><td>個案姓名</td><td><font color="#FF0000">{$rowdata->C_name}</font></td></tr>
            <tr><td>轉介人</td><td>{$rowdata->AdName}</td><td>紀錄日期</td><td>{$rowdata->R_date}</td></tr>
            <tr><td>被轉介人</td><td>{$rowdata->Acc_name}</td><td colspan="2"></td></tr>
            <tr><td align="center" colspan="4">轉介說明</td></tr>
            <tr><td colspan="4">{$rowdata->refComment}</td></tr>
            <tr><td align="center" colspan="4">被轉介人回覆說明</td></tr>
            <tr><td colspan="4">{$rowdata->refReply}</td></tr>
        </table>
        EOD;
        break;

    default:
        $printTable = '';
        break;
}

$pdf->writeHTML($printTable, true, false, false, false, '');

if ($recType === 'basicData' && !empty($cancerHistoryStr)) {
    $pdf->AddPage();
    $pdf->SetFont('taipeisans', 'B', 16);
    $pdf->Write(0, '疾病史', '', 0, 'C', true, 0, false, false, 0);
    $pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);
    $pdf->SetFont('taipeisans', '', 12);
    $pdf->writeHTML($cancerHistoryStr, true, false, false, false, '');
}

//Close and output PDF document
$pdf->Output('print.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
