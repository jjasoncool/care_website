<?php
/**
 * 修改個案狀態
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param array $inputData 使用者輸入的資料
 * @param array $basicData 接受的欄位資料
 * @param string $action 對資料做的動作
 */

$inputData = [];
$basicData = [
    'page' => 'string',
    'dataID' => 'int',
];

// 基本資料
foreach ($basicData as $inputName => $inputType) {
    $inputData[$inputName] = reqParam($inputName, 'post', $inputType);
}

if ($inputData['page'] === 'closeCase') {
    $recData = [
        'C_status' => -1
    ];
    $_GET['sub'] = 'closeCase';
} else {
    $row = $db->row('SELECT caseClosed FROM FCF_careservice.Memberdata WHERE IDno=?', [$inputData['dataID']]);
    $recData = [
        'caseClosed' => intval(!boolval($row['caseClosed']))
    ];
}

if (isset($recData)) {
    // 更新個案狀態
    $updColArray = $recData;
    $updColStr = '';
    foreach ($updColArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    $dbQuery = "UPDATE FCF_careservice.Memberdata
                SET {$updColStr}
                WHERE IDno=?";
    $db->query($dbQuery, array_merge(array_values($updColArray), [$inputData['dataID']]));
}

$_GET['action'] = 'view';
header("Location:?" . http_build_query($_GET));