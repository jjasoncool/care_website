<?php
/**
 * 新增/編輯基本資料 (含疾病史)
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param array $inputData 使用者輸入的資料
 * @param array $basicData 接受的欄位資料
 * @param string $subPage 子類別
 * @param boolean $edit 編輯資料
 * @param int $id 個案編號
 */

$edit = boolval(reqParam('edit', 'get'));
$id = intval(reqParam('id', 'get'));

$date = new DateTime();
$inputData = [];
$inputCancerData = [];
$basicData = [
    'caseName' => 'string',
    'IDnumber' => 'string',
    'C_member' => 'int',
    'FCFvolunte' => 'boolean',
    'sex' => 'string',
    'birthday' => 'string',
    'C_opendate' => 'date',
    'comefrom' => 'string',
    'city' => 'string',
    'zone' => 'string',
    'zip' => 'int',
    'addr' => 'string',
    'tel' => 'string',
    'email' => 'string',
    'mobile1' => 'string',
    'mobile2' => 'string',
    'contact' => 'string',
    'relation' => 'string',
    'reltele' => 'string',
    'maritalStatus' => 'string',
    'haveChilds' => 'int',
    'edu' => 'string',
    'job' => 'int',
    'id_normal' => 'boolean',
    'id_lowincome' => 'boolean',
    'id_weakincome' => 'boolean',
    'id_oldman' => 'boolean',
    'id_handicapped' => 'boolean',
    'id_indigenous' => 'boolean',
    'id_foreign' => 'boolean',
    'id_singlemon' => 'boolean',
    'id_specstatus' => 'boolean',
    'Ins_none' => 'boolean',
    'Ins_health' => 'boolean',
    'Ins_social' => 'boolean',
    'Ins_work' => 'boolean',
    'Ins_gov' => 'boolean',
    'Ins_fisher' => 'boolean',
    'Ins_farmer' => 'boolean',
    'Ins_soldier' => 'boolean',
    'Ins_commerce' => 'boolean',
    'Ins_others' => 'string',
    'life_ability' => 'int',
    'caregiver' => 'int',
    'houseown' => 'int',
    'residence' => 'int',
    'cohabitation' => 'string',
    'payMonth' => 'int',
    'ecoStatus' => 'string',
    'monthIncome' => 'int',
    'chronic' => 'string',
    'otherChronic' => 'string',
    'C_note' => 'string',
    'count' => 'int',
    'privacy' => 'boolean',
    'getSMS' => 'boolean',
    'C_subscribe' => 'int',
    'assessForm' => 'int',
    'handSignature' => 'string',
];
// 基本資料
foreach ($basicData as $inputName => $inputType) {
    $inputData[$inputName] = reqParam($inputName, 'post', $inputType);
}
$medHistory = [
    'cancerID' => 'int',
    'cancerName' => 'string',
    'cancerStage' => 'int',
    'cancerTrans' => 'string',
    'Find_date' => 'date',
    'hospital' => 'string',
    'gene' => 'string',
    'Surgery' => 'boolean',
    'Surgery_date' => 'date',
    'cancer_type' => 'string',
    'recurrence' => 'date',
    'Chemical' => 'boolean',
    'Chemical_date_S' => 'string',
    'Chemical_date_E' => 'string',
    'Chemical_name' => 'string',
    'Target' => 'boolean',
    'Target_date_S' => 'string',
    'Target_date_E' => 'string',
    'Target_name' => 'string',
    'Immune' => 'boolean',
    'Immune_date_S' => 'string',
    'Immune_date_E' => 'string',
    'Immune_name' => 'string',
    'Radiation' => 'boolean',
    'Radiat_date_S' => 'string',
    'Radiat_date_E' => 'string',
    'Rad_time' => 'int',
    'Rad_part' => 'string',
    'hormone' => 'boolean',
    'hormone_date_S' => 'string',
    'hormone_date_E' => 'string',
    'hormone_name' => 'string',
    'integrated' => 'boolean',
    'tracking' => 'boolean',
    'peace' => 'boolean',
    'other' => 'boolean',
    'R_note' => 'string',
];
// 疾病史資料
foreach ($medHistory as $inputName => $inputType) {
    if ($inputName == 'Surgery') {
        echo '';
    }
    $inputCancerData[$inputName] = reqParam($inputName, 'post', $inputType);
    // checkbox
    if ($inputType == 'boolean') {
        if (is_array($inputCancerData[$inputName])) {
            $inputCancerData[$inputName] += array_fill(0, $inputData['count'], 0);
            ksort($inputCancerData[$inputName]);
        } else {
            $inputCancerData[$inputName] = array_fill(0, $inputData['count'], 0);
        }
    }
}

// 生日須拆資料
$BD_yy = 0;
$BD_mm = 0;
$BD_dd = 0;
if (isset($inputData['birthday'])) {
    $temp = explode('-', $inputData['birthday']);
    $BD_yy = $temp[0];
    $BD_mm = isset($temp[1]) ? $temp[1] : 1;
    $BD_dd = isset($temp[2]) ? $temp[2] : 1;
}

// 經濟狀況
$ecoStatusToSQL = '';
if (is_array($inputData['ecoStatus'])) {
    $ecoStatusToSQL = implode(',', $inputData['ecoStatus']);
}
// 慢性疾病史
$chronicToSQL = '';
if (is_array($inputData['chronic'])) {
    $chronicToSQL = implode(',', $inputData['chronic']);
}

// 先確認是否有重複身分證字號
$dbQuery = "SELECT IDno, C_idno, C_member FROM FCF_careservice.Memberdata where C_idno=? AND IDno!=?";
$result = $db->query($dbQuery, [$inputData['IDnumber'], $id]);
if (count($result) > 0) {
    $subPage = checkSubPage($result[0]['C_member']);
    toViewPage($result[0]['IDno']);
}

// 看是新增資料還是修改資料
if (empty($id) && empty($edit)) {
    // 新增個案基本資料
    $dbQuery = "INSERT INTO FCF_careservice.Memberdata (
                    `Keydate`, `AdminID`, `AdName`,
                    `C_opendate`, `C_status`, `C_comefrom`, `C_name`, `C_sex`,
                    `C_marriage`, `haveChilds`, `C_member`, `caseClosed`, `C_idno`,
                    `C_job`, `C_edu`, `BD_yy`, `BD_mm`, `BD_dd`,
                    `C_file`, `C_subscribe`, `C_tele`, `C_mail`, `C_mobile1`,
                    `C_mobile2`, `getSMS`, `C_area`, `C_zip`, `C_city`,
                    `C_zone`, `C_address`, `C_contact`, `C_relation`, `C_reltele`,
                    `FCFvolunte`, `C_note`, `id_normal`, `id_lowincome`, `id_weakincome`,
                    `id_oldman`, `id_handicapped`, `id_indigenous`, `id_foreign`, `id_singlemon`,
                    `id_specstatus`, `Ins_none`, `Ins_health`, `Ins_social`, `Ins_work`,
                    `Ins_gov`, `Ins_fisher`, `Ins_farmer`, `Ins_soldier`, `Ins_commerce`,
                    `Ins_others`, `life_ability`, `residence`, `cohabitation`, `houseown`,
                    `payMonth`, `caregiver`, `ecoStatus`, `monthIncome`, `chronic`,
                    `otherChronic`, `privacy`, `assessForm`, `Last_update`, `Close_reason`,
                    `Lastcare`, `handSignature`
                ) VALUES (
                    NOW(), ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, ?, ?,
                    ?, ?, ?, NOW(), NULL,
                    '', ?
                )";
    $SQLparam = [
        $generalData['userid'], $generalData['username'],
        $inputData['C_opendate'], 1, $inputData['comefrom'], $inputData['caseName'], $inputData['sex'],
        $inputData['maritalStatus'], $inputData['haveChilds'], $inputData['C_member'], 0, $inputData['IDnumber'],
        $inputData['job'], $inputData['edu'], $BD_yy, $BD_mm, $BD_dd,
        '', $inputData['C_subscribe'], $inputData['tel'], $inputData['email'], $inputData['mobile1'],
        $inputData['mobile2'], 1, $generalData['serviceArea'], $inputData['zip'], $inputData['city'],
        $inputData['zone'], $inputData['addr'], $inputData['contact'], $inputData['relation'], $inputData['reltele'],
        $inputData['FCFvolunte'], $inputData['C_note'], $inputData['id_normal'], $inputData['id_lowincome'], $inputData['id_weakincome'],
        $inputData['id_oldman'], $inputData['id_handicapped'], $inputData['id_indigenous'], $inputData['id_foreign'], $inputData['id_singlemon'],
        $inputData['id_specstatus'], $inputData['Ins_none'], $inputData['Ins_health'], $inputData['Ins_social'], $inputData['Ins_work'],
        $inputData['Ins_gov'], $inputData['Ins_fisher'], $inputData['Ins_farmer'], $inputData['Ins_soldier'], $inputData['Ins_commerce'],
        $inputData['Ins_others'], $inputData['life_ability'], $inputData['residence'], $inputData['cohabitation'], $inputData['houseown'],
        $inputData['payMonth'], $inputData['caregiver'], $ecoStatusToSQL, $inputData['monthIncome'], $chronicToSQL,
        $inputData['otherChronic'], $inputData['privacy'], $inputData['assessForm'], $inputData['handSignature']
    ];
    $db->query($dbQuery, $SQLparam);
    $id = $lastID = $db->lastInsertId();

    if ($subPage == 'personalCase') {
        // 新增疾病史 (個案)
        $dbQuery = "INSERT INTO FCF_careservice.Rec_cancer (
                        `Keydate`, `MemberID`, `AdminID`, `AdName`, `Find_date`,
                        `hospital`, `genetic_test`, `Cancer_type`, `recurrence`, `Cancer_name`,
                        `Cancer_level`, `Cancer_trans`, `Surgery`, `Surgery_date`, `Chemical`,
                        `Chemical_date`, `Chemical_name`, `Target`, `Target_date`, `Target_name`,
                        `Immune`, `Immune_date`, `Immune_medicine`, `Radiation`, `Radiat_date`,
                        `Rad_time`, `Rad_part`, `hormone`, `hormone_date`, `hormone_medicine`,
                        `integrated`, `tracking`, `peace`, `other`, `R_note`, `AdmincheckID`

                    ) VALUES (
                        NOW(), ?, ?, ?, ?,
                        ?, ?, ?, ?, ?,
                        ?, ?, ?, ?, ?,
                        ?, ?, ?, ?, ?,
                        ?, ?, ?, ?, ?,
                        ?, ?, ?, ?, ?,
                        ?, ?, ?, ?, ?, ?
                    )";

        // 有幾個疾病史
        for ($i = 0; $i < $inputData['count']; $i++) {
            $cancerDate = cancerDateChanger($inputCancerData, $i);
            $SQLparam = [
                $lastID, $generalData['userid'], $generalData['username'], $cancerDate['Find_date'],
                $inputCancerData['hospital'][$i], $inputCancerData['gene'][$i], $inputCancerData['cancer_type'][$i], $cancerDate['recurrence'], $inputCancerData['cancerName'][$i],
                $inputCancerData['cancerStage'][$i], $inputCancerData['cancerTrans'][$i], $inputCancerData['Surgery'][$i], $cancerDate['Surgery_date'], $inputCancerData['Chemical'][$i],
                $cancerDate['Chemical_date'], $inputCancerData['Chemical_name'][$i], $inputCancerData['Target'][$i], $cancerDate['Target_date'], $inputCancerData['Target_name'][$i],
                $inputCancerData['Immune'][$i], $cancerDate['Immune_date'], $inputCancerData['Immune_name'][$i], $inputCancerData['Radiation'][$i], $cancerDate['Radiat_date'],
                $inputCancerData['Rad_time'][$i], $inputCancerData['Rad_part'][$i], $inputCancerData['hormone'][$i], $cancerDate['hormone_date'], $inputCancerData['hormone_name'][$i],
                $inputCancerData['integrated'][$i], $inputCancerData['tracking'][$i], $inputCancerData['peace'][$i], $inputCancerData['other'][$i], $inputCancerData['R_note'][$i],
                $generalData['managerID'],
            ];
            $db->query($dbQuery, $SQLparam);
        }
    } elseif ($subPage == 'family') {
        // 新增疾病史 (家屬)
        $familyCancer = reqParam('familyCancer', 'post');
        foreach ($familyCancer as $cancerName) {
            $dbQuery = "INSERT INTO FCF_careservice.Rec_cancer (
                            `Keydate`, `MemberID`, `AdminID`, `AdName`, `Cancer_name`, `R_note`, `AdmincheckID`
                        ) VALUES (
                            NOW(), ?, ?, ?, ?, ?, ?
                        )";
            $SQLparam = [
                $id, $generalData['userid'], $generalData['username'], $cancerName, '家屬疾病史', $generalData['managerID'],
            ];
            $db->query($dbQuery, $SQLparam);
        }
    }

} elseif (!empty($id) && !empty($edit)) {
    // 修改個案基本資料
    $dbQuery = "UPDATE FCF_careservice.Memberdata
                    SET `C_opendate`=?, `C_status`=?, `C_comefrom`=?, `C_name`=?,
                    `C_sex`=?, `C_marriage`=?, `haveChilds`=?, `C_member`=?, `caseClosed`=?,
                    `C_idno`=?, `C_job`=?, `C_edu`=?, `BD_yy`=?, `BD_mm`=?, `BD_dd`=?,
                    `C_file`=?, `C_subscribe`=?, `C_tele`=?, `C_mail`=?, `C_mobile1`=?,
                    `C_mobile2`=?, `getSMS`=?, `C_area`=?, `C_zip`=?, `C_city`=?,
                    `C_zone`=?, `C_address`=?, `C_contact`=?, `C_relation`=?, `C_reltele`=?,
                    `FCFvolunte`=?, `C_note`=?, `id_normal`=?, `id_lowincome`=?, `id_weakincome`=?,
                    `id_oldman`=?, `id_handicapped`=?, `id_indigenous`=?, `id_foreign`=?, `id_singlemon`=?,
                    `id_specstatus`=?, `Ins_none`=?, `Ins_health`=?, `Ins_social`=?, `Ins_work`=?,
                    `Ins_gov`=?, `Ins_fisher`=?, `Ins_farmer`=?, `Ins_soldier`=?, `Ins_commerce`=?,
                    `Ins_others`=?, `life_ability`=?, `residence`=?, `cohabitation`=?, `houseown`=?,
                    `payMonth`=?, `caregiver`=?, `ecoStatus`=?, `monthIncome`=?, `chronic`=?,
                    `otherChronic`=?, assessForm=?, handSignature=?, `Last_update`=NOW()
                WHERE IDno = ?";
    $SQLparam = [
        $inputData['C_opendate'], 1, $inputData['comefrom'], $inputData['caseName'],
        $inputData['sex'], $inputData['maritalStatus'], $inputData['haveChilds'], $inputData['C_member'], 0,
        $inputData['IDnumber'], $inputData['job'], $inputData['edu'], $BD_yy, $BD_mm, $BD_dd,
        '', $inputData['C_subscribe'], $inputData['tel'], $inputData['email'], $inputData['mobile1'],
        $inputData['mobile2'], 1, $generalData['serviceArea'], $inputData['zip'], $inputData['city'],
        $inputData['zone'], $inputData['addr'], $inputData['contact'], $inputData['relation'], $inputData['reltele'],
        $inputData['FCFvolunte'], $inputData['C_note'], $inputData['id_normal'], $inputData['id_lowincome'], $inputData['id_weakincome'],
        $inputData['id_oldman'], $inputData['id_handicapped'], $inputData['id_indigenous'], $inputData['id_foreign'], $inputData['id_singlemon'],
        $inputData['id_specstatus'], $inputData['Ins_none'], $inputData['Ins_health'], $inputData['Ins_social'], $inputData['Ins_work'],
        $inputData['Ins_gov'], $inputData['Ins_fisher'], $inputData['Ins_farmer'], $inputData['Ins_soldier'], $inputData['Ins_commerce'],
        $inputData['Ins_others'], $inputData['life_ability'], $inputData['residence'], $inputData['cohabitation'], $inputData['houseown'],
        $inputData['payMonth'], $inputData['caregiver'], $ecoStatusToSQL, $inputData['monthIncome'], $chronicToSQL,
        $inputData['otherChronic'], $inputData['assessForm'], $inputData['handSignature'], $id,
    ];
    $db->query($dbQuery, $SQLparam);

    // 先撈目前疾病史在資料庫的ID
    $dbQuery = "SELECT IDno, Cancer_name FROM FCF_careservice.Rec_cancer WHERE MemberID=? AND `status`=1";
    $result = $db->query($dbQuery, [$id]);
    $cancerIDArray = [];
    $cancerTypeArray = [];
    foreach ($result as $key => $value) {
        $cancerIDArray[] = $value['IDno'];
        $cancerTypeArray[] = $value['Cancer_name'];
    }

    // 更新身分時，疾病史資料一併要用不同做法
    $subPage = checkSubPage($inputData['C_member']);

    if ($subPage == 'personalCase') {
        // 更新疾病史(個案)，從送過來的資料與ID逐筆更新
        if (is_array($inputCancerData['cancerID'])) {
            foreach ($inputCancerData['cancerID'] as $num => $updateID) {
                $cancerDate = cancerDateChanger($inputCancerData, $num);
                if (in_array($updateID, $cancerIDArray, true)) {
                    // ID存在於原有資料庫內的話，進行更新
                    $dbQuery = "UPDATE FCF_careservice.Rec_cancer
                                SET `Find_date`=?, `hospital`=?, `genetic_test`=?, `Cancer_type`=?, `recurrence`=?,
                                    `Cancer_name`=?, `Cancer_level`=?, `Cancer_trans`=?, `Surgery`=?, `Surgery_date`=?,
                                    `Chemical`=?, `Chemical_date`=?, `Chemical_name`=?, `Target`=?, `Target_date`=?,
                                    `Target_name`=?, `Immune`=?, `Immune_date`=?, `Immune_medicine`=?, `Radiation`=?,
                                    `Radiat_date`=?, `Rad_time`=?, `Rad_part`=?, `hormone`=?, `hormone_date`=?,
                                    `hormone_medicine`=?, `integrated`=?, `tracking`=?, `peace`=?, `other`=?,
                                    `R_note`=?, `AdmincheckID`=?
                                WHERE IDno=?";
                    $SQLparam = [
                        $cancerDate['Find_date'], $inputCancerData['hospital'][$num], $inputCancerData['gene'][$num], $inputCancerData['cancer_type'][$num], $cancerDate['recurrence'],
                        $inputCancerData['cancerName'][$num], $inputCancerData['cancerStage'][$num], $inputCancerData['cancerTrans'][$num], $inputCancerData['Surgery'][$num], $cancerDate['Surgery_date'],
                        $inputCancerData['Chemical'][$num], $cancerDate['Chemical_date'], $inputCancerData['Chemical_name'][$num], $inputCancerData['Target'][$num], $cancerDate['Target_date'],
                        $inputCancerData['Target_name'][$num], $inputCancerData['Immune'][$num], $cancerDate['Immune_date'], $inputCancerData['Immune_name'][$num], $inputCancerData['Radiation'][$num],
                        $cancerDate['Radiat_date'], $inputCancerData['Rad_time'][$num], $inputCancerData['Rad_part'][$num], $inputCancerData['hormone'][$num], $cancerDate['hormone_date'],
                        $inputCancerData['hormone_name'][$num], $inputCancerData['integrated'][$num], $inputCancerData['tracking'][$num], $inputCancerData['peace'][$num], $inputCancerData['other'][$num],
                        $inputCancerData['R_note'][$num], $generalData['managerID'], $updateID,
                    ];
                } elseif ($updateID == 0) {
                    // ID若等於0，進行新增疾病史(個案)
                    $dbQuery = "INSERT INTO FCF_careservice.Rec_cancer (
                                    `Keydate`, `MemberID`, `AdminID`, `AdName`, `Find_date`,
                                    `hospital`, `genetic_test`, `Cancer_type`, `recurrence`, `Cancer_name`,
                                    `Cancer_level`, `Cancer_trans`, `Surgery`, `Surgery_date`, `Chemical`,
                                    `Chemical_date`, `Chemical_name`, `Target`, `Target_date`, `Target_name`,
                                    `Immune`, `Immune_date`, `Immune_medicine`, `Radiation`, `Radiat_date`,
                                    `Rad_time`, `Rad_part`, `hormone`, `hormone_date`, `hormone_medicine`,
                                    `integrated`, `tracking`, `peace`, `other`, `R_note`, `AdmincheckID`
                                ) VALUES (
                                    NOW(), ?, ?, ?, ?,
                                    ?, ?, ?, ?, ?,
                                    ?, ?, ?, ?, ?,
                                    ?, ?, ?, ?, ?,
                                    ?, ?, ?, ?, ?,
                                    ?, ?, ?, ?, ?,
                                    ?, ?, ?, ?, ?, ?
                                )";
                    $SQLparam = [
                        $id, $generalData['userid'], $generalData['username'], $cancerDate['Find_date'],
                        $inputCancerData['hospital'][$num], $inputCancerData['gene'][$num], $inputCancerData['cancer_type'][$num], $cancerDate['recurrence'], $inputCancerData['cancerName'][$num],
                        $inputCancerData['cancerStage'][$num], $inputCancerData['cancerTrans'][$num], $inputCancerData['Surgery'][$num], $cancerDate['Surgery_date'], $inputCancerData['Chemical'][$num],
                        $cancerDate['Chemical_date'], $inputCancerData['Chemical_name'][$num], $inputCancerData['Target'][$num], $cancerDate['Target_date'], $inputCancerData['Target_name'][$num],
                        $inputCancerData['Immune'][$num], $cancerDate['Immune_date'], $inputCancerData['Immune_name'][$num], $inputCancerData['Radiation'][$num], $cancerDate['Radiat_date'],
                        $inputCancerData['Rad_time'][$num], $inputCancerData['Rad_part'][$num], $inputCancerData['hormone'][$num], $cancerDate['hormone_date'], $inputCancerData['hormone_name'][$num],
                        $inputCancerData['integrated'][$num], $inputCancerData['tracking'][$num], $inputCancerData['peace'][$num], $inputCancerData['other'][$num], $inputCancerData['R_note'][$num],
                        $generalData['managerID'],
                    ];
                }
                $db->query($dbQuery, $SQLparam);
            }

            // 疾病史(個案)送出ID若不包含資料庫原有ID，代表刪除該筆資料
            $deleteIDArray = array_diff($cancerIDArray, array_filter($inputCancerData['cancerID']));
            foreach ($deleteIDArray as $deleteID) {
                $dbQuery = "UPDATE FCF_careservice.Rec_cancer
                                SET `status`=0
                            WHERE IDno=?";
                $db->query($dbQuery, [$deleteID]);
            }
        }
    } elseif ($subPage == 'family') {
        // 更新家屬疾病史
        $familyCancer = reqParam('familyCancer', 'post');
        if (is_array($familyCancer)) {
            foreach ($familyCancer as $cancerName) {
                if (!in_array($cancerName, $cancerTypeArray, true)) {
                    $dbQuery = "INSERT INTO FCF_careservice.Rec_cancer (
                                    `Keydate`, `MemberID`, `AdminID`, `AdName`, `Cancer_name`, `R_note`, `AdmincheckID`
                                ) VALUES (
                                    NOW(), ?, ?, ?, ?, ?, ?
                                )";
                    $SQLparam = [
                        $id, $generalData['userid'], $generalData['username'], $cancerName, '家屬疾病史', $generalData['managerID'],
                    ];
                    $db->query($dbQuery, $SQLparam);
                }
            }
            // 刪除家族疾病史
            $deleteTypeArray = array_diff($cancerTypeArray, array_filter($familyCancer));
            foreach ($deleteTypeArray as $deleteCancer) {
                $dbQuery = "UPDATE FCF_careservice.Rec_cancer
                                SET `status`=0
                            WHERE Cancer_name=? AND MemberID=?";
                $db->query($dbQuery, [$deleteCancer, $id]);
            }
        }
    }
}

// 若有勾選志工，則新增資料到志工資料庫，否則更新為休息狀態
if (boolval($inputData['FCFvolunte'])) {
    $recArray = [
        "MemberID" => $id,
        "V_name" => $inputData['caseName'],
        "V_sex" => $inputData['sex'],
        "V_idno" => $inputData['IDnumber'],
        "V_joindate" => $date->format('Y-m-d H:i:s'),
        "V_joincity" => $inputData['city'],
        "V_status" => 1,
    ];
    // 插入資料
    $insColStr = implode('`,`', array_keys($recArray));
    $insParaStr = implode(",", array_fill(0, count($recArray), "?"));
    // 若重複ID，則更新
    $updColStr = '';
    foreach ($recArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    // SQL 用基本資料產出
    $dbQuery = "INSERT INTO FCF_careservice.Volunteer (
                    `{$insColStr}`
                ) VALUES (
                    {$insParaStr}
                ) ON DUPLICATE KEY UPDATE
                    {$updColStr}";
    $recArray = array_merge(array_values($recArray), array_values($recArray));
} else {
    $recArray = [$id];
    $dbQuery = "UPDATE FCF_careservice.Volunteer SET V_status=0 where MemberID=?";
}
$db->query($dbQuery, array_values($recArray));

// 填評估量表
if ($inputData['assessForm']) {
    // 若有沒填寫的就轉過去
    toAssessPage();
}

// 一般個案填寫完成，回登入頁
if ($generalData['userLevel'] > 2) {
    header("Location:/login.php?thank=1");
} else {
    toViewPage();
}

function cancerDateChanger($dataArray, $i)
{
    $returnArray = [];
    $kindArray = ['Chemical_date', 'Target_date', 'Immune_date', 'Radiat_date', 'hormone_date'];

    // 日期轉換並驗證
    $returnArray['Find_date'] = (validateDate($dataArray['Find_date'][$i], 'Y-m') ? $dataArray['Find_date'][$i] . '-01' : null);
    $returnArray['recurrence'] = (validateDate($dataArray['recurrence'][$i], 'Y-m') ? $dataArray['recurrence'][$i] . '-01' : null);
    $returnArray['Surgery_date'] = (validateDate($dataArray['Surgery_date'][$i], 'Y-m') ? $dataArray['Surgery_date'][$i] : '');

    // 串接日期
    foreach ($kindArray as $dateName) {
        $date_S = (validateDate($dataArray["{$dateName}_S"][$i], 'Y-m-d') ? $dataArray["{$dateName}_S"][$i] : '');
        $date_E = (validateDate($dataArray["{$dateName}_E"][$i], 'Y-m-d') ? $dataArray["{$dateName}_E"][$i] : '');
        if (empty($date_S) && empty($date_E)) {
            $returnArray["{$dateName}"] = '';
        } else {
            $returnArray["{$dateName}"] = implode('~', array($date_S, $date_E));
        }
    }

    return $returnArray;
}

/**
 * 回到 view 頁面，若有 goID 代表有重複身分證
 *
 * @param string $goID
 * @return void
 */
function toViewPage($goID = '')
{
    global $id, $edit, $subPage;
    if (!empty($goID)) {
        // goID 有值代表有重複身分證，優先判斷
        $_GET['id'] = $goID;
        $_GET['dulplicate'] = true;
    } elseif (!empty($id)) {
        // 一般修改
        $_GET['id'] = $id;
    }
    // 個案變為家屬，或家屬變為個案
    $_GET['sub'] = $subPage;
    // 改完資料回到view頁面
    $_GET['action'] = 'view';
    unset($_GET['edit']);
    header("Location:?" . http_build_query($_GET));
    exit();
}


/**
 * 轉至評估量表，若都有填寫過一次則不會再轉
 *
 * @return void
 */
function toAssessPage()
{
    global $id, $db;
    if (!empty($id)) {
        // 一般修改
        $_GET['id'] = $id;
    }

    $dbQuery = "SELECT COUNT(DISTINCT ns.IDno) AS nurse, COUNT(DISTINCT nt.IDno) AS nutrition, COUNT(DISTINCT s.IDno) AS social, COUNT(DISTINCT mt.IDno) AS mental
                FROM FCF_careservice.Memberdata m
                LEFT JOIN FCF_careservice.assess_nursing ns ON m.IDno=ns.MemberID
                LEFT JOIN FCF_careservice.assess_nutrition nt ON m.IDno=nt.MemberID
                LEFT JOIN FCF_careservice.assess_social s ON m.IDno=s.MemberID
                LEFT JOIN FCF_careservice.assess_mental mt ON m.IDno=mt.MemberID
                WHERE m.IDno=?";
    $result = $db->row($dbQuery, [$id]);
    $assessType = array_search(0, $result);

    if ($assessType != false) {
        $_GET['formType'] = $assessType;
        $_GET['page'] = 'assessment';
        unset($_GET['edit'], $_GET['sub']);
        header("Location:?" . http_build_query($_GET));
        exit();
    }
}

function checkSubPage($C_member)
{
    switch ($C_member) {
        case '0':
            return 'nonClient';
            break;
        case '1':
            return 'personalCase';
            break;
        case '2':
            return 'family';
            break;
    }
}
