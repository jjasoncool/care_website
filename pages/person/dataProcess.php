<?php
/**
 * 資料處理層，將php分配到不同頁面處理
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */
require_once 'vendor/autoload.php';
require 'settings/loginCheck.php';
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
$pageAction = reqParam('page', 'post', 'string');
unset($_GET["dulplicate"]);

switch ($pageAction) {
    case 'caseinfo':
        // 個案表單送出
        require_once get_relative_path("pages/{$page}/caseDataManage.php");
        break;

    case 'viewModal':
        // 個案諮詢紀錄 新增/修改/刪除
        require_once get_relative_path("pages/{$page}/consult/dataManage.php");
        break;

    case 'uploadFile':
        // 上傳檔案
        require_once get_relative_path("pages/{$page}/consult/uploadFile.php");
        break;

    case 'printPDF':
        // 列印諮詢檔案
        require_once get_relative_path("pages/{$page}/print/print.php");
        break;

    case 'closeCase':
        // 結案
        require_once get_relative_path("pages/{$page}/chgStatus/personStatus.php");
        break;

    case 'caseStatus':
        // 開/關檔
        require_once get_relative_path("pages/{$page}/chgStatus/personStatus.php");
        break;

    default:
        break;
}

exit();