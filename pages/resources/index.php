<?php
/**
 * 新增/以及編輯資料使用表單
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 */
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
$tableTitle = "資源清單";
// 資料定義
require_once get_relative_path("pages/{$page}/dataDefinition.php");
// 不存在頁面處理
if (!array_key_exists($subPage, $definition)) {
    header("Location: " . get_relative_path("pages/404.php"));
    exit();
}

?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition[$subPage]['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['subname'][$subPage]?></a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title"><?=$tableTitle?></h4>
                                <div class="ml-auto">
                                    <!-- <a class="btn btn-outline-success btn-round btn-sm" href="#">
                                        <span class="btn-label"><i class="fas fa-plus"></i>新增</span>
                                    </a> -->
                                    <button type="button" class="btn btn-outline-info btn-round btn-sm mx-2" data-toggle="modal" data-target="#importFile">
                                        <span class="btn-label"><i class="fas fa-upload"></i>匯入資源檔案</span>
                                    </button>
                                    <a class="btn btn-outline-secondary btn-round btn-sm" href="?<?=http_build_query($_GET)?>&action=download">
                                        <span class="btn-label"><i class="fas fa-download"></i>下載資料</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <table id="basic-datatables" class="display table table-striped table-hover dt-responsive nowrap" width="100%">
                                    <thead>
                                        <tr>
                                            <?php
                                            foreach ($definition[$subPage]['colhead'] as $head) {
                                                echo "<th>{$head}</th>";
                                            }
                                            ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        // 各種清單依據不同dataType顯示
                                        $dbQuery = "SELECT * FROM FCF_careservice.Resource WHERE dataType={$definition[$subPage]['dataType']}";
                                        $result = $db->query($dbQuery);
                                        foreach ($result as $row) {
                                            $columnContent = '';
                                            $rowdata = json_encode($row, JSON_UNESCAPED_UNICODE);
                                            foreach ($definition[$subPage]['col'] as $colname) {
                                                if ($colname == 'hospitalLevel') {
                                                    // 醫院等級
                                                    $hospitalLevel = '';
                                                    switch ($row[$colname]) {
                                                        case '1':
                                                            $hospitalLevel = '地區醫院';
                                                            break;
                                                        case '2':
                                                            $hospitalLevel = '區域醫院';
                                                            break;
                                                        case '3':
                                                            $hospitalLevel = '醫學中心';
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                    $columnContent .= "<td>{$hospitalLevel}</td>";
                                                } else {
                                                    $columnContent .= "<td>{$row[$colname]}</td>";
                                                }
                                            }
                                            echo nl2br("<tr>{$columnContent}</tr>");
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="importFile" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">匯入資料</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div id="dropzoneArea" style="display: flex; justify-content: center;">
                    <div class="dz-message" data-dz-message>
                        <div class="icon">
                            <i class="flaticon-file"></i>
                        </div>
                        <h4 class="message">Drag and Drop files here</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
var params = new URLSearchParams(location.search);
var dataType = <?=$definition[$subPage]['dataType']?>;

function init() {
    $('#basic-datatables').DataTable({
        stateSave: true,
        lengthChange: false,
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Details for '+data[2]+' '+data[3];
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table'
                } )
            }
        }
    });

    params.set("action", "upload");

    var myDropzone = new Dropzone("div#dropzoneArea", {
        url: "./?" + params.toString(),
        method: 'post',
        acceptedFiles: ".xlsx,.xls,.ods",
        // addRemoveLinks: true,
        maxFilesize: 100, // MB
        uploadMultiple: false,
        maxFiles: 1,
        parallelUploads: 1,
        filesizeBase: 1024,
        sending: function(file, xhr, formData) {
            formData.append("page", "import");
            formData.append("dataType", dataType);
        },
        success: function (file, response, e) {
            if (response.success) {
                file.previewElement.classList.add("dz-success");
                swal({
                    title: response.success,
                    icon: "success"
                }).then((value) => {
                    location.reload();
                });
                $("#importFile").modal('hide');
            }
        }
    });

    myDropzone.on("maxfilesexceeded", function(file) {
        this.removeFile(file);
        swal({
            title: "僅能上傳一個檔案",
            icon: "error"
        })
    });

    // 添加樣式
    $("#dropzoneArea").addClass("dropzone");

    // modal
    // 關閉上傳modal 清除 dropzone 檔案
    $("#importFile").on("hidden.bs.modal", function () {
        myDropzone.removeAllFiles();
    });
}
window.onload = init;
</script>