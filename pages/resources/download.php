<?php
/**
 * 將資料庫的 EXCEL 資料匯出
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $subPage 子類別
 * @param string $action 執行的動作
 */

// 清除先前頁面要顯示的快取
ob_end_clean();

// 資料庫連線
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
require_once get_relative_path("pages/{$page}/dataDefinition.php");

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

// Create new Spreadsheet object
$spreadsheet = new Spreadsheet();

// Set document properties
$spreadsheet->getProperties()->setCreator('台灣癌症基金會')
    ->setLastModifiedBy('台灣癌症基金會')
    ->setTitle('匯出資料')
    ->setSubject('資源清單')
    ->setDescription('資料屬於基金會所有，請勿用於未授權之用途')
    ->setKeywords('canceraway')
    ->setCategory($definition[$subPage]['title']);


$dbQuery = "SELECT * FROM FCF_careservice.Resource WHERE dataType=?";
$result = $db->query($dbQuery, [$definition[$subPage]['dataType']]);

$titleStyle = [
    'font' => ['bold' => true],
];

// Add some data
$rownum = 1;
$spreadsheet->setActiveSheetIndex(0);
array_unshift($definition[$subPage]['colhead'], '編號');
foreach ($definition[$subPage]['colhead'] as $key => $colhead) {
    $column = $key + 1;
    $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $rownum, $colhead);
}
$rownum++;

foreach ($result as $row) {
    // 編號
    $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, $rownum, $rownum-1);
    foreach ($definition[$subPage]['col'] as $key => $columnName) {
        // 內容欄位從第2欄開始
        $column = $key + 2;
        $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow($column, $rownum, $row[$columnName]);
    }
    $rownum++;
}

// Rename worksheet
$spreadsheet->getActiveSheet()->setTitle($definition[$subPage]['title']);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$spreadsheet->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Xlsx)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header("Content-Disposition: attachment;filename=\"{$definition[$subPage]['title']}.xlsx\"");
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header('Pragma: public'); // HTTP/1.0

$writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
$writer->save('php://output');
exit;