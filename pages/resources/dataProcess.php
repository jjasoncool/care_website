<?php
/**
 * 資料處理層，POST的資料都會跑到這裡來，從這邊將php分配到不同頁面處理
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */
require_once 'vendor/autoload.php';
require 'settings/loginCheck.php';
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
$pageAction = reqParam('page', 'post', 'string');
unset($_GET["dulplicate"]);

switch ($pageAction) {
    case 'import':
        // 匯入資料表EXCEL
        require_once get_relative_path("pages/{$page}/importData.php");
        break;

    default:
        break;
}

exit();