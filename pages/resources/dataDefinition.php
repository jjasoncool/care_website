<?php
/**
 * 定義資源清單使用資料庫欄位以及項目
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

$definition = [
    'center' => [
        'title' => '癌症資源中心',
        'colhead' => ['區域', '縣市', '醫院', '姓名', '職稱', '電話', '手機', '傳真', 'email', '地址', '備註'],
        'col' => ['region', 'city', 'hospital', 'contact', 'jobTitle', 'tel', 'mobile', 'fax', 'email', 'address', 'memo'],
        'dataType'=> 1
    ],
    'casemanager' => [
        'title' => '專科個管師',
        'colhead' => ['區域', '醫院', '單位', '窗口聯絡人', '主治醫師', '電話', '手機', 'email', '地址', '備註'],
        'col' => ['region', 'hospital', 'unit', 'contact', 'doctor', 'tel', 'mobile', 'email', 'address', 'memo'],
        'dataType'=> 2
    ],
    'hospitalsocial' => [
        'title' => '醫院社工室',
        'colhead' => ['區域', '醫院', '單位', '姓名', '職稱', '電話', '手機', '傳真', 'email', '地址', '備註'],
        'col' => ['region', 'hospital', 'unit', 'contact', 'jobTitle', 'tel', 'mobile', 'fax', 'email', 'address', 'memo'],
        'dataType'=> 3
    ],
    'northHospital' => [
        'title' => '北區醫院',
        'colhead' => ['縣市', '醫院名稱', '醫院等級', '電話', '地址'],
        'col' => ['city', 'hospital', 'hospitalLevel', 'tel', 'address'],
        'dataType'=> 4
    ],
    'government' => [
        'title' => '政府單位',
        'colhead' => ['區域', '類別', '單位', '姓名', '職稱', '電話', '手機', '傳真', 'email', '地址', '資源', '備註'],
        'col' => ['region', 'category', 'unit', 'contact', 'jobTitle', 'tel', 'mobile', 'fax', 'email', 'address', 'resource', 'memo'],
        'dataType'=> 5
    ],
    'socialGroup' => [
        'title' => '社福團體',
        'colhead' => ['區域', '類別', '單位', '姓名', '職稱', '電話', '手機', '傳真', 'email', '地址', '資源', '備註'],
        'col' => ['region', 'category', 'unit', 'contact', 'jobTitle', 'tel', 'mobile', 'fax', 'email', 'address', 'resource', 'memo'],
        'dataType'=> 6
    ],
    'group' => [
        'title' => '病友團體',
        'colhead' => ['區域', '醫院', '病友團體', '醫院窗口', '電話', '手機', 'email', '備註'],
        'col' => ['region', 'hospital', 'contact', 'patientGroup', 'tel', 'mobile', 'email', 'memo'],
        'dataType'=> 7
    ],
    'cooperation' => [
        'title' => '合作企業',
        'colhead' => ['區域', '類別', '單位', '姓名', '職稱', '電話', '手機', '傳真', 'email', '地址', '資源', '備註'],
        'col' => ['region', 'category', 'unit', 'contact', 'jobTitle', 'tel', 'mobile', 'fax', 'email', 'address', 'resource', 'memo'],
        'dataType'=> 8
    ],
];