<?php
/**
 * 新增/以及編輯資料使用表單
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $action 路由到這個頁面的參數
 */
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);

// 資料定義
require_once get_relative_path("pages/{$page}/dataDefinition.php");
?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$listItems[$page]['name']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <?php
                foreach ($definition['category']['dataType'] as $area => $detail) {
                ?>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <div class="d-flex align-items-center">
                                    <h4 class="card-title"><?=$detail['unit']?></h4>
                                    <div class="ml-auto">
                                        <button type="button" class="btn btn-outline-success btn-sm mx-2" data-toggle="modal" data-target="#<?=$area?>Modal" data-act="add">
                                            <span class="btn-label"><i class="fas fa-plus"></i>新增類別</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                <table id="<?=$area?>Course" class="display table table-striped table-hover dt-responsive nowrap" width="100%">
                                        <thead>
                                            <tr>
                                                <?php
                                                foreach ($definition['category']['colhead'] as $head) {
                                                    echo "<th>{$head}</th>";
                                                }
                                                ?>
                                                <th>編輯</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            // 各種清單依據不同dataType顯示
                                            $dbQuery = "SELECT * FROM FCF_careservice.EventCategory WHERE cityLocation=?
                                                        AND C_kind='{$definition['category']['kind']}' AND Del=0";
                                            $result = $db->query($dbQuery, [$detail['unit']]);
                                            foreach ($result as $row) {
                                                $columnContent = '';
                                                $rowdata = htmlspecialchars(json_encode($row, JSON_UNESCAPED_UNICODE), ENT_QUOTES);
                                                foreach ($definition['category']['col'] as $colname) {
                                                    $link = '';
                                                    $linkEnd = '';
                                                    if ($definition['category']['colLink'][$colname]) {
                                                        $link = "<a href=\"./?" . http_build_query($_GET) . "&action=view&id={$row['IDno']}\">";
                                                        $linkEnd = '</a>';
                                                    }
                                                    $columnContent .= "<td>{$link}{$row[$colname]}{$linkEnd}</td>";
                                                }
                                                $columnContent = nl2br($columnContent);
                                                $modBtn = "<button type=\"button\" title=\"修改\" class=\"btn btn-link btn-primary py-0 px-2\"
                                                                data-toggle=\"modal\" data-target=\"#{$area}Modal\" data-act=\"mod\">
                                                                <i class=\"fa fa-edit fa-lg\"></i>
                                                            </button>";
                                                // 管理者才可以刪除類別
                                                if ($generalData['userLevel'] == 1) {
                                                    $delBtn = "<button type=\"button\" title=\"刪除\" class=\"btn btn-link btn-danger py-0 pl-2\"
                                                                data-toggle=\"modal\" data-target=\"#del{$area}\">
                                                                    <i class=\"fa fa-times fa-lg\"></i>
                                                                </button>";
                                                } else {
                                                    $delBtn = '';
                                                }
                                                echo
                                                "<tr>
                                                    {$columnContent}
                                                    <td style=\"width: 10%\">
                                                        <div class=\"form-button-action\">
                                                            <input type=\"hidden\" class=\"rowdata\" value='{$rowdata}'>
                                                            {$modBtn}
                                                            {$delBtn}
                                                        </div>
                                                    </td>
                                                </tr>";
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="<?=$area?>Modal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">課程類別 (<?=$detail['unit']?>)</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <form method="post" action="./?<?=http_build_query($_GET)?>&action=dataForm">
                                    <div class="modal-body">
                                        <input type="hidden" name="dataID">
                                        <input type="hidden" name="action">
                                        <input type="hidden" name="page" value="category">
                                        <input type="hidden" name="Area" value="<?=$detail['unit']?>">
                                        <div class="col-md-12">
                                            <label><b>類別名稱</b></label>
                                            <input type="text" class="form-control form-control-sm my-2" name="category" field="category">
                                        </div>
                                    </div>
                                    <div class="modal-footer no-bd">
                                        <button type="submit" class="btn btn-primary"></button>
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <?php
                    if ($generalData['userLevel'] == 1) {
                    ?>
                        <div class="modal fade" id="del<?=$area?>" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">刪除類別 (<?=$detail['unit']?>)</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="./?<?=http_build_query($_GET)?>&action=dataForm">
                                        <div class="modal-body">
                                            <input type="hidden" name="dataID">
                                            <input type="hidden" name="action" value="del">
                                            <input type="hidden" name="page" value="category">
                                            <h6>請問確定要<span class="text-danger"><b>刪除</b></span>此資料嗎?</h6>
                                            <b><p class="my-0"></p></b>
                                        </div>
                                        <div class="modal-footer no-bd">
                                            <button type="submit" class="btn btn-danger">確認刪除</button>
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">取消</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                }
                ?>
            </div>
        </div>
    </div>
</div>

<script>
var params = new URLSearchParams(location.search);
var recArray = ["<?= implode("\",\"", array_keys($definition['category']['dataType'])) ?>"];
var tableStr = "";
var modalStr = "";
var delModalStr = "";

function init() {
    // 取得每個modal ID字串
    $.each(recArray, function (i, v) {
        tableStr += "#" + v + "Course,";
        modalStr += "#" + v + "Modal,";
        delModalStr += "#del" + v + ",";
    });
    tableStr = tableStr.slice(0,-1);
    modalStr = modalStr.slice(0,-1);
    delModalStr = delModalStr.slice(0,-1);

    $(tableStr).DataTable({
        stateSave: true,
        lengthChange: false,
        columnDefs: [{
            targets: [-1],
            orderable: false,
        }],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Details for '+data[2]+' '+data[3];
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table'
                } )
            }
        }
    });

    $(modalStr).on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        // Extract info from data-* attributes
        let action = button.data('act');
        let titleText = "";
        // 這個modal
        let modal = $(this);
        if (action == 'mod') {
            titleText = "修改";
            let rowdata = JSON.parse(decodeEntities(button.siblings(".rowdata").val()));

            modal.find("input[name='dataID']").val(rowdata["IDno"]);
            modal.find("input[name='category']").val(rowdata["C_name"]);
        } else {
            titleText = "新增";
        }

        modal.find("[name='action']").val(action);
        modal.find(".modal-title").prepend(titleText);
        modal.find("button[type='submit']").text("確認" + titleText);
    });

    $(modalStr).on('hidden.bs.modal', function (event) {
        let modal = $(this);
        let title = modal.find(".modal-title").text().slice(2);
        modal.find(".modal-title").text(title);
    });

    $(delModalStr).on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        // Extract info from data-* attributes
        let titleText = "";
        let date = "";
        // 這個modal
        let modal = $(this);
        let rowdata = JSON.parse(decodeEntities(button.siblings(".rowdata").val()));

        modal.find("input[name='dataID']").val(rowdata["IDno"]);
        modal.find(".modal-body p").text(rowdata["C_name"]);
    });

    $(delModalStr).on('hidden.bs.modal', function (event) {
        let modal = $(this);
        modal.find(".modal-body p").text('');
    });
}
window.onload = init;
</script>