<?php
/**
 * 新增/以及編輯資料使用表單
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $action 路由到這個頁面的參數
 * @param int $cid 課程類別ID
 * @param int $id 課程ID
 */

$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
// 資料定義
require_once get_relative_path("pages/{$page}/dataDefinition.php");
// 如果這邊有帶入ID 必須先撈資料
$id = intval(reqParam('id', 'get'));
$cid = intval(reqParam('cid', 'get'));

$dbQuery = "SELECT * FROM FCF_careservice.EventCategory WHERE IDno=?";
$result = $db->row($dbQuery, [$cid]);
$CTitle = "{$result['cityLocation']}-{$result['C_name']}";

$attendID = array();
if ($id > 0) {
    $status = 'mod';
    $pageStatus = '編輯';
    // 課程基本資料
    $dbQuery = "SELECT *, CONCAT(C_startday, '~', C_endday) AS eventdate
                FROM FCF_careservice.{$definition['activities']['tableName']} WHERE IDno=? AND `status`=1";
    $courseData = $db->row($dbQuery, [$id]);
    $cardTitle = $courseData['C_name'];
    $courseDataJSON = json_encode($courseData, JSON_UNESCAPED_UNICODE);
    // 參加課程個案
    $dbQuery = "SELECT rc.*, m.C_name, m.C_idno, m.C_sex, m.C_member, m.C_tele, m.C_mobile1
                FROM FCF_careservice.Rec_courses rc
                INNER JOIN FCF_careservice.Memberdata m ON rc.MemberID=m.IDno
                WHERE ClassID=?";
    $attendData = $db->query($dbQuery, [$id]);
    foreach ($attendData as $key => $row) {
        $attendID[$key]['id'] = $row['MemberID'];
        $attendID[$key]['name'] = $row['C_name'];
    }
    $attendIDStr = json_encode($attendID, JSON_UNESCAPED_UNICODE);
} else {
    $status = 'add';
    $pageStatus = '新增';
    $cardTitle = '新增課程基本資料';
    $courseDataJSON = json_encode('');
    $attendData = array();
    $attendIDStr = json_encode("", JSON_UNESCAPED_UNICODE);
}

?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$listItems[$page]['name']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="?<?="page={$page}"?>"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="?<?="page={$page}&action=view&id={$cid}"?>"><?=$CTitle?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$pageStatus?>課程資料</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form class="needs-validation" method="post" id="inputDataForm">
                        <input type="hidden" name="page" value="courseinfo">
                        <input type="hidden" name="formAction" value="<?=$status?>">
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    <b><?=$cardTitle?></b>
                                    <div class="print d-none"></div>
                                    <button type="button" title="列印" class="btn btn-outline-primary btn-round btn-sm float-sm-right mx-2 printBtn">
                                        <i class="fas fa-print fa-lg"></i> 列印
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-row px-2">
                                    <div class="form-group col-md-6">
                                        <label for="C_name">課程名稱</label>
                                        <input type="text" class="form-control" id="C_name" name="C_name" field="C_name" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="C_location">地點</label>
                                        <input type="text" class="form-control" id="C_location" name="C_location" field="C_location" value="<?=$result['cityLocation']?>" required>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="C_startday">開始日期</label>
                                        <div class="input-group date" id="C_startday" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="C_startday" data-target="#C_startday" field="C_startday" required>
                                            <div class="input-group-append" data-target="#C_startday" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="C_endday">至</label>
                                        <div class="input-group date" id="C_endday" data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input" maxlength="10" name="C_endday" data-target="#C_endday" field="C_endday" required>
                                            <div class="input-group-append" data-target="#C_endday" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="C_No">期別</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">第</div>
                                            </div>
                                            <input type="number" class="form-control numberic" id="C_No" name="C_No" step="1" min="0" field="C_No" required>
                                            <div class="input-group-append">
                                                <div class="input-group-text">期</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>課程日</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">共</div>
                                            </div>
                                            <select class="form-control py-1" id="C_num" name="C_num" field="C_num" required>
                                                <?php
                                                for ($i=1; $i <= 20; $i++) {
                                                    echo "<option value=\"{$i}\">{$i}</option>";
                                                }
                                                ?>
                                            </select>
                                            <div class="input-group-append">
                                                <div class="input-group-text">次</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="C_partner">合作單位</label>
                                        <input type="text" class="form-control" id="C_partner" name="C_partner" field="C_partner">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="C_manager">主責人員</label>
                                        <input type="text" class="form-control" id="C_manager" name="C_manager" field="C_manager" value="<?=$generalData['username']?>" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="C_teacher">講師</label>
                                        <input type="text" class="form-control" id="C_teacher" name="C_teacher" field="C_teacher" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="C_assistant">助教</label>
                                        <input type="text" class="form-control" id="C_assistant" name="C_assistant" field="C_assistant">
                                    </div>
                                </div>
                                <div class="form-row px-2">
                                    <div class="form-group col-md-3">
                                        <label for="C_joinnum">參與人數</label>
                                        <input type="number" class="form-control numberic" id="C_joinnum" name="C_joinnum" field="C_joinnum" step="1" min="0" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="C_servicenum">服務總人次</label>
                                        <input type="number" class="form-control numberic" id="C_servicenum" name="C_servicenum" field="C_servicenum" step="1" min="0" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="C_leavenum">請假人次</label>
                                        <input type="number" class="form-control numberic" id="C_leavenum" name="C_leavenum" field="C_leavenum" step="1" min="0" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="C_note">身心靈課程內容記錄</label>
                                    <textarea class="form-control" id="C_note" name="C_note" rows="5" field="C_note" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="C_specrec">特殊情形</label>
                                    <textarea class="form-control" id="C_specrec" name="C_specrec" rows="5" field="C_specrec" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="C_comment">相關評值</label>
                                    <textarea class="form-control" id="C_comment" name="C_comment" rows="5" field="C_comment" required></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="C_issue">檢討與建議</label>
                                    <textarea class="form-control" id="C_issue" name="C_issue" rows="5" field="C_issue" required></textarea>
                                </div>
                            </div>
                            <div class="card-header">
                                <div class="card-title"><b>參與紀錄</b></div>
                            </div>
                            <div class="card-body">
                                <div class="form-group" style="border: 1px solid #ebedf2;">
                                    <label for="attendPeop">參與人員</label><br>
                                    <input type="text" id="attendPeop" name="attendPeop" field="attendPeop" placeholder="點此新增人員      ">
                                </div>
                                <?php
                                if (!empty($attendData)) {
                                ?>
                                    <div class="form-group">
                                        <table class="table table-hover">
                                            <tr>
                                                <th>序號</th>
                                                <th>姓名</th>
                                                <th>身分證</th>
                                                <th>性別</th>
                                                <th>電話</th>
                                                <th>手機</th>
                                            </tr>
                                        <?php
                                            foreach ($attendData as $index => $row) {
                                                if ($row['C_member'] == 1) {
                                                    $caseSubPage = 'personalCase';
                                                } elseif ($row['C_member'] == 2) {
                                                    $caseSubPage = 'family';
                                                }
                                                $linkHead = "";
                                                $linkEnd = "";
                                                // 連結至個案資訊
                                                if (!empty($caseSubPage)) {
                                                    $linkHead = "<a href=\"./?page=person&sub={$caseSubPage}&action=view&id={$row['MemberID']}\">";
                                                    $linkEnd = "</a>";
                                                }
                                                // 性別
                                                if ($row['C_sex'] == 'male') {
                                                    $showSex = '男';
                                                } elseif ($row['C_sex'] == 'female') {
                                                    $showSex = '女';
                                                }
                                                echo "
                                                <tr>
                                                    <td>{$index}</td>
                                                    <td>{$linkHead}{$row['C_name']}{$linkEnd}</td>
                                                    <td>{$row['C_idno']}</td>
                                                    <td>{$showSex}</td>
                                                    <td>{$row['C_tele']}</td>
                                                    <td>{$row['C_mobile1']}</td>
                                                </tr>";
                                            }
                                        ?>
                                        </table>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>
                            <?php
                            if (!empty($attendData)) {
                            ?>
                            <div class="card-header">
                                <div class="d-flex align-items-center">
                                <div class="card-title"><b>出席紀錄</b></div>
                                    <div class="ml-auto">
                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#editAttend" data-act="add">
                                            <span class="btn-label"><i class="fa fa-plus"></i></span>新增紀錄
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <table class="table table-hover">
                                        <tr>
                                            <th>時間</th>
                                            <th>出席人數</th>
                                            <th>紀錄者</th>
                                            <th></th>
                                        </tr>
                                    <?php
                                        $dbQuery = "SELECT * FROM FCF_careservice.EventAttend WHERE ClassID=?";
                                        $attendHistory = $db->query($dbQuery, [$id]);
                                        foreach ($attendHistory as $index => $row) {
                                            $rowdata = json_encode($row, JSON_UNESCAPED_UNICODE);
                                            echo "
                                            <tr>
                                                <td>{$row['Eventime']}</td>
                                                <td>{$row['PeopNum']}</td>
                                                <td>{$row['ADname']}</td>
                                                <td style=\"width: 15%\">
                                                    <input type=\"hidden\" class=\"rowdata\" value='{$rowdata}'>
                                                    <button type=\"button\" title=\"修改\" class=\"btn btn-link btn-primary py-0 px-2\"
                                                        data-toggle=\"modal\" data-target=\"#editAttend\" data-act=\"mod\">
                                                        <i class=\"fa fa-edit fa-lg\"></i>
                                                    </button>
                                                    <button type=\"button\" title=\"刪除\" class=\"btn btn-link btn-danger py-0 px-2\"
                                                        data-toggle=\"modal\" data-target=\"#delAttend\" data-act=\"del\">
                                                        <i class=\"fa fa-times fa-lg\"></i>
                                                    </button>
                                                </td>
                                            </tr>";
                                        }
                                    ?>
                                    </table>
                                </div>
                            </div>
                            <?php
                            }
                            if (!empty($id)) {
                            ?>
                            <div class="card-header">
                                <div class="d-flex align-items-center">
                                    <div class="card-title"><b>活動照片</b></div>
                                    <div class="ml-auto">
                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#uploadAttend">
                                            <span class="btn-label"><i class="fas fa-images"></i></span>新增相片
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <style>
                            #attendPhoto {
                                padding: 20px;
                            }

                            #attendPhoto .lightbox img {
                                width: 100%;
                                margin-bottom: 30px;
                                transition: 0.2s ease-in-out;
                                box-shadow: 0 2px 3px rgba(0,0,0,0.2);
                            }

                            #attendPhoto .lightbox img:hover {
                                transform: scale(1.05);
                                box-shadow: 0 8px 15px rgba(0,0,0,0.3);
                            }

                            #attendPhoto img {
                                border-radius: 4px;
                            }

                            .baguetteBox-button {
                                background-color: transparent !important;
                            }

                            .image-remove {
                                display: block;
                                position: absolute;
                                background-color: white;
                                color: black;
                                font-size: 20px;
                                width: 30px;
                                height: 30px;
                                text-align: center;
                                border-radius: 100%;
                                transform: rotate(45deg);
                                cursor:pointer;
                                transition: 0.2s;
                                opacity: 0.5;
                                top:5px;
                                right:20px;
                            }

                            .image-remove:hover {
                                transition: 0.2s;
                                opacity: 0.8;
                            }
                            </style>
                            <div class="card-body">
                                <div class="row" id="attendPhoto">
                                    <?php
                                    $dbQuery = "SELECT * FROM FCF_careservice.EventArchives WHERE ClassID=?";
                                    $attendPhoto = $db->query($dbQuery, [$id]);
                                    foreach ($attendPhoto as $key => $row) {
                                        echo "
                                        <div class=\"col-md-2\">
                                            <a class=\"lightbox\" href=\"/uploads/photos/{$row['Fname']}\" data-caption=\"{$row['Ftitle']}\">
                                                <img class=\"img-thumbnail\" src=\"/uploads/photos/{$row['Fname']}\" alt=\"{$row['Ftitle']}\">
                                                <input type=\"hidden\" class=\"imageID\" value=\"{$row['IDno']}\">
                                                <span class=\"image-remove\">+</span>
                                            </a>
                                        </div>";
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                            <div class="card-action">
                                <button type="button" class="btn btn-success" id="submitForm">儲存</button>
                                <button type="button" class="btn btn-danger" id="goBack">回上頁</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="editAttend" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">出席紀錄</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post">
                <div class="modal-body">
                    <input type="hidden" name="dataID">
                    <input type="hidden" name="action">
                    <input type="hidden" name="page" value="attendList">
                    <div class="form-group col-md-4">
                        <label><b>日期</b></label>
                        <div class="input-group date" id="Eventime" data-target-input="nearest">
                            <input type="text" class="form-control form-control-sm datetimepicker-input" maxlength="10" name="Eventime" field="Eventime" data-target="#Eventime" required>
                            <div class="input-group-append" data-target="#Eventime" data-toggle="datetimepicker">
                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <table class="table table-hover">
                            <tr>
                                <th><input class="align-middle" type="checkbox" id="checkAll"> 全選</th>
                                <th>姓名</th>
                                <th>身分證</th>
                                <th>性別</th>
                                <th>電話</th>
                                <th>手機</th>
                            </tr>
                            <?php
                            foreach ($attendData as $key => $row) {
                                echo "
                                <tr>
                                    <td><input type=\"checkbox\" name=\"attendPeop[]\" value=\"{$row['MemberID']}\"></td>
                                    <td>{$row['C_name']}</td>
                                    <td>{$row['C_idno']}</td>
                                    <td>{$showSex}</td>
                                    <td>{$row['C_tele']}</td>
                                    <td>{$row['C_mobile1']}</td>
                                </tr>";
                            }
                            ?>
                        </table>
                    </div>
                </div>
                <div class="modal-footer no-bd">
                    <button type="submit" class="btn btn-primary"></button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="delAttend" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">刪除出席紀錄</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post">
                <div class="modal-body">
                    <input type="hidden" name="dataID">
                    <input type="hidden" name="action" value="del">
                    <input type="hidden" name="page" value="attendList">
                    <h6>請問確定要<span class="text-danger"><b>刪除</b></span>此資料嗎?</h6>
                    <b><p class="my-0"></p></b>
                </div>
                <div class="modal-footer no-bd">
                    <button type="submit" class="btn btn-danger">確認刪除</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade" id="uploadAttend" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">上傳相片</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
            <div class="modal-body">
                <div id="dropzoneArea">
                    <div class="dz-message" data-dz-message>
                        <div class="icon">
                            <i class="flaticon-file"></i>
                        </div>
                        <h4 class="message">Drag and Drop files here</h4>
                    </div>
                </div>
            </div>
            <div class="modal-footer no-bd">
                <button type="button" class="btn btn-primary" id="dropzoneUpload">確認上傳</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

<script>
    "use strict";
    var params = new URLSearchParams(location.search);
    var dataArray = <?=$courseDataJSON?>;
    var attendArray = <?=$attendIDStr?>;
    var attendDataArray = <?=json_encode($attendData)?>;

    function init() {
        // 列印
        $(".printBtn").on("click", function() {
            let rowdata = dataArray;
            rowdata.attend = attendDataArray;
            let form = $(this).siblings("div .print");
            let virtualForm = $("<form target=\"_blank\" method=\"POST\"></form>");
            virtualForm.attr("action", "?" + params.toString());
            virtualForm.append("<input type=\"hidden\" name=\"page\" value=\"printPDF\">");
            virtualForm.append("<input type=\"hidden\" name=\"rowdata\">");
            virtualForm.find("[name='rowdata']").val(encodeURI(JSON.stringify(rowdata)));
            virtualForm.appendTo(form).submit();
            form.html("");
        });

        // 送出
        $("#submitForm").on("click", function (e) {
            if (valid(e)) {
                $(this).closest("form").submit();
            }
        });

        // 回上一頁
        $("#goBack").on("click", function () {
            let cid = params.get("cid");
            params.set("id", cid);
            params.set("action", "view");
            params.delete("cid");
            location.search = params.toString();
        });

        $('#C_startday, #C_endday').datetimepicker({
            format: 'YYYY-MM-DD',
            allowInputToggle: true,
            useStrict: true,
            debug: false
        });

        // 把每個欄位都填資料
        $.each(dataArray, function(field, value) {
            let inputField = $("[field='" + field + "']");
            let inputType = $(inputField).attr("type") || $(inputField).prop("tagName");

            if (inputType != undefined) {
                inputType = inputType.toLowerCase();
            }

            // 依據不同的 input 類型，做不同的塞值動作(單個欄位對應單筆資料)
            if (inputType == "text" || inputType == "textarea" || inputType == "number") {
                $(inputField).val(value);
            } else if (inputType == "checkbox" && Boolean(value)) {
                $(inputField).prop("checked", true);
            } else if (inputType == "radio") {
                $.each($(inputField), function(i) {
                    if ($(this).val() == value) {
                        $(this).prop("checked", true);
                    }
                });
            } else if (inputType == "select") {
                $(inputField).val(value).trigger("change");
            }
        });

        // 參加人員
        $('#attendPeop').tagsinput({
            tagClass: 'badge-info',
            freeInput: false,
            allowDuplicates: false,
            itemValue: 'id',
            itemText: 'name',
            typeahead: {
                afterSelect: function (item) {
                    // typeahead的物件需要使用this.$element才抓的到
                    $(this.$element).val("");
                },
                source: function(query) {
                    params.set("action", "upload");
                    return $.ajax({
                        url: "./?" + params.toString(),
                        method: "POST",
                        data: {query: query, page: "searchList"},
                        dataType: "json"
                    }).done(function(res) {
                    });
                }
            }
        });

        // 填入參與人員資料
        if (attendArray.length > 0) {
            attendArray.forEach(function (value, index) {
                $('#attendPeop').tagsinput("add", attendArray[index]);
            });
        }

        $("#Eventime").datetimepicker({
            format: 'YYYY-MM-DD',
            allowInputToggle: true,
            useStrict: true,
            debug: false
        });

        // 出席紀錄Modal
        $("#editAttend").on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            // Extract info from data-* attributes
            let action = button.data('act');
            let titleText = "";
            // 這個modal
            let modal = $(this);
            if (action == 'mod') {
                titleText = "修改";
                let rowdata = JSON.parse(button.siblings(".rowdata").val());
                let checkItems = rowdata["PeopID"].split(",").map((item)=>item.trim());
                checkItems.forEach(function (value, index) {
                    modal.find("input:checkbox[value='" + value + "']").prop("checked", true);
                });

                modal.find("input[field='Eventime']").val(rowdata["Eventime"]);
                modal.find("input[name='dataID']").val(rowdata["IDno"]);
            } else {
                titleText = "新增";
                modal.find("input[field='Eventime']").val(moment().format("YYYY-MM-DD"));
            }

            // 全選
            if (modal.find('input:checkbox:checked').not("#checkAll").length == modal.find('input:checkbox').not("#checkAll").length) {
                $("#checkAll").prop("checked", true);
            }

            $("#checkAll").on("click", function(){
                modal.find('input:checkbox').not(this).prop('checked', this.checked);
            });

            modal.find("[name='action']").val(action);
            modal.find(".modal-title").prepend(titleText);
            modal.find("button[type='submit']").text("確認" + titleText);
        });

        $("#editAttend").on('hidden.bs.modal', function (event) {
            let modal = $(this);
            let title = modal.find(".modal-title").text().slice(2);
            $("#checkAll").off("click");
            modal.find("form").get(0).reset();
            modal.find(".modal-title").text(title);
        });

        $("#delAttend").on('show.bs.modal', function (event) {
            let button = $(event.relatedTarget);
            // Extract info from data-* attributes
            let titleText = "";
            let date = "";
            // 這個modal
            let modal = $(this);
            let rowdata = JSON.parse(button.siblings(".rowdata").val());

            modal.find("input[name='dataID']").val(rowdata["IDno"]);
            modal.find(".modal-body p").text("日期: " + rowdata["Eventime"] + " 出席人數: " + rowdata["PeopNum"]);
        });

        if (params.get("attendEmpty") == true) {
            swal ("輸入錯誤" ,  "請勾選出席人員", "error", {
                button: "我知道了"
            });
        }

        // 活動照片
        baguetteBox.run("#attendPhoto");
        // 新增照片
        var myDropzone = new Dropzone("#uploadAttend div#dropzoneArea", {
            url: "./?" + params.toString(),
            method: 'post',
            acceptedFiles: "image/*",
            addRemoveLinks: true,
            autoProcessQueue : false,
            maxFilesize: 100, // MB
            uploadMultiple: false,
            filesizeBase: 1024,
            parallelUploads: 30,
            maxFiles: 30,
            sending: function(file, xhr, formData) {
                formData.append("page", "uploadFile");
            },
            success: function (file, response, e) {
                if (response.success) {
                    file.previewElement.classList.add("dz-success");
                }
            },
            queuecomplete: function() {
                $("#uploadAttend").modal('hide');
                swal({
                    title: "上傳成功",
                    icon: "success"
                }).then((value) => {
                    location.reload();
                });
            }
        });

        // 添加樣式
        $("#dropzoneArea").addClass("dropzone");
        // 按鈕上傳
        $("#dropzoneUpload").on("click", function () {
            myDropzone.processQueue();
        });

        // 關閉上傳modal 清除 dropzone 檔案
        $("#uploadAttend").on("hidden.bs.modal", function () {
            myDropzone.removeAllFiles();
        });

        // 刪除照片
        $(".image-remove").on("click", function(e) {
            let picID = $(this).siblings("input").val();
            swal({
                title: "確定刪除此相片?",
                text: "相片刪除後，將無法復原!",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        url: "./?" + params.toString(),
                        method: "POST",
                        data: {delID: picID, page: "deleteFile"},
                        dataType: "json"
                    }).done(function(res) {
                        console.log(res);
                        swal({
                            title: res.deleted,
                            icon: "success"
                        }).then((value) => {
                            location.reload();
                        });
                    });
                }
            });

            e.preventDefault();
            e.stopPropagation();
        })

    }

    window.onload = init;
</script>