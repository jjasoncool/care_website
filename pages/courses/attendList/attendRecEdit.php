<?php
/**
 * 新增/編輯/刪除 參與紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param array $inputData 使用者輸入的資料
 * @param array $basicData 接受的欄位資料
 * @param string $action 對資料做的動作
 * @param int $dataID 該修改或刪除紀錄的編號
 * @param int $id 課程編號
 */

$id = intval(reqParam('id', 'get'));
$dataID = intval(reqParam('dataID', 'post'));
$formAction = reqParam('action', 'post');

$dataTable = 'EventAttend';
$basicData = [
    'Eventime' => 'string',
    'attendPeop' => 'int',
];

// 基本資料
foreach ($basicData as $inputName => $inputType) {
    $inputData[$inputName] = reqParam($inputName, 'post', $inputType);
}

if (empty($inputData['attendPeop']) && $formAction != 'del') {
    toViewPage("error");
} elseif ($formAction == 'del') {
    $inputData['attendPeop'] = array();
}

// 紀錄基本資料
$dt = new dateTime();
$recArray = [
    "ClassID" => $id,
    "Eventime" => $inputData["Eventime"],
    "PeopNum" => count($inputData['attendPeop']),
    "PeopID" => implode(', ', $inputData['attendPeop']),
    "AdminID" => $generalData['userid'],
    "AdName" => $generalData['username'],
    "Keydate" => $dt->format('Y-m-d H:i:s')
];

// 判斷資料類型
if ($formAction === 'add') {
    $insColStr = implode('`,`', array_keys($recArray));
    $insParaStr = implode(",", array_fill(0, count($recArray), "?"));
    // SQL 用基本資料產出
    $dbQuery = "INSERT INTO FCF_careservice.{$dataTable} (
        `$insColStr`
    ) VALUES (
        $insParaStr
    )";
    $db->query($dbQuery, array_values($recArray));
} elseif ($formAction === 'mod' && !empty($dataID)) {
    // 更新時，不可變更課程ID
    unset($recArray["ClassID"]);
    $updColStr = '';
    foreach ($recArray as $key => $value) {
        $updColStr .= "{$key}=?,";
    }
    $updColStr = substr($updColStr, 0, -1);
    $dbQuery = "UPDATE FCF_careservice.{$dataTable}
                SET {$updColStr}
                WHERE IDno=?";
    $db->query($dbQuery, array_merge(array_values($recArray), [$dataID]));
} elseif ($formAction === 'del' && !empty($dataID)) {
    // 刪除，取消此個案與此紀錄之關聯
    $dbQuery = "DELETE FROM FCF_careservice.{$dataTable}
                WHERE IDno=? AND ClassID=?";
    $db->query($dbQuery, [$dataID, $id]);
}

toViewPage();

function toViewPage($error = '') {
    // 回到上一個瀏覽頁面
    if (!empty($error)) {
        $_GET['attendEmpty'] = true;
    }
    header("Location:?" . http_build_query($_GET));
    exit();
}