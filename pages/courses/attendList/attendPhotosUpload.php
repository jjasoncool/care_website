<?php
/**
 * 上傳檔案，依據諮詢紀錄新增檔案
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param int $id 課程編號
 */

$id = intval(reqParam('id', 'get'));
// 錯誤或成功訊息
$msg = [];
// 拖拉檔案上傳處
if (empty($_FILES)) {
    exit();
} else {
    // 檔案處理
    $maxMB = filter_var(ini_get('upload_max_filesize'), FILTER_SANITIZE_NUMBER_INT);
    $acceptFormat = [
        'image/bmp',
        'image/jpeg',
        'image/gif',
        'image/png',
    ];

    $f_temp = $_FILES['file']['tmp_name'];
    $f_name = $_FILES['file']['name'];
    $f_type = $_FILES['file']['type'];
    $f_size = round(($_FILES['file']['size'] / 1048576), 3); //MB
    $f_error = intval($_FILES['file']['error']);

    if ($f_error != 0) {
        $msg['error'] = "上傳發生錯誤";
    }

    // 檔案大小限制
    if ($f_size > $maxMB) {
        $msg['error'] = "超過容許大小";
    }

    if (!in_array($f_type, $acceptFormat, true)) {
        $msg['error'] = "不接受此檔案格式";
    }

    $ext = strtolower(pathinfo($f_name)['extension']);
    $fileTitle = pathinfo($f_name)['filename'];
}


//return right HTTP code
if (!empty($msg['error'])) {
    header('http/1.1 500 internal server error');
} else {
    // 檔案先放入暫存區
    $date = new DateTime();
    $dateString = $date->format('YmdHis');
    $dateString .= '_' . mt_rand();

    // 檔名
    $NewName = "IMG_{$dateString}";
    $target_path = "{$_SERVER['DOCUMENT_ROOT']}/uploads/photos/{$NewName}.{$ext}";

    if (!file_exists(dirname($target_path))) {
        mkdir(dirname($target_path), '0777', true);
    }

    if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
    } else {
        $msg['error'] = "There was an error uploading the file" . basename($_FILES['file']['name']) . ", please try again!";
    }

    // 有錯誤即退出
    if (empty($msg['error'])) {
        // 最後的成功訊息
        header("HTTP/1.1 200 OK");
        $msg['filename'] = "{$NewName}.{$ext}";
        $msg['success'] = '上傳檔案成功!';

        // 新增相片資訊到資料庫內
        $recArray = [
            "Fdate" => $date->format('Y-m-d H:i:s'),
            "Fname" => $msg['filename'],
            "Ftitle" => $fileTitle,
            "Ftype" => 1,
            "ClassID" => $id
        ];
        $dataTable = 'EventArchives';
        $insColStr = implode('`,`', array_keys($recArray));
        $insParaStr = implode(",", array_fill(0, count($recArray), "?"));
        // SQL 用基本資料產出
        $dbQuery = "INSERT INTO FCF_careservice.{$dataTable} (
            `$insColStr`
        ) VALUES (
            $insParaStr
        )";
        $db->query($dbQuery, array_values($recArray));
    }
}

//set Content-Type to JSON
header('Content-Type: application/json; charset=utf-8');
// 清除先前頁面要顯示的快取(一開始走router前面的頁面等等)
ob_end_clean();
//echo error message as JSON
echo json_encode($msg, JSON_UNESCAPED_UNICODE);
