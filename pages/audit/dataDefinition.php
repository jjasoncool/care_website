<?php
/**
 * 定義資源清單使用資料庫欄位以及項目
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

$definition = [
    'title' => '審核',
    'audit' => [
        'colhead' => ['類別', '日期', '地點', '紀錄人員', '方式', '諮詢者', '附加檔案', '審核者'],
        'col' => ['recType', 'R_date', 'R_loc', 'AdName', 'anonym', 'memberName', 'filePath', 'auditor'],
    ],
];

$recTypeArray = [
    'medical' => 'Rec_medical',
    'nutrition' => 'Rec_nutrition',
    'social' => 'Rec_social',
    'mental' => 'Rec_mental',
    'insurance' => 'Rec_insurance',
    'care' => 'Rec_care',
    'general' => 'Rec_ask',
    'projects' => 'Projects',
    'course' => 'MindBodyCourse',
    'train' => 'VolunteerTrain',
];
