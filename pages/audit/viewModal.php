<?php
// 醫療紀錄
ob_start();
?>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>醫療需求</b></label><br>
        <input type="hidden" field="Medical">
        <?php
        $optArray = [
            'Med_cure' => '癌症治療相關問題',
            'Med_check' => '癌症檢查相關問題',
            'Med_drug' => '藥品健保相關問題',
            'Med_clinical' => '臨床試驗',
            'Med_suggest' => '就醫(跨科/院/國)建議',
            'Med_assist' => '輔助療法',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline m-0\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>護理需求</b></label><br>
        <input type="hidden" field="Nurse">
        <?php
        $optArray = [
            'Nur_surgery' => '術前術後衛教',
            'Nur_chemical' => '化學治療衛教',
            'Nur_target' => '標靶治療衛教',
            'Nur_radiation' => '放射線治療衛教',
            'Nur_drug' => '其他藥物衛教',
            'Nur_home' => '居家照護衛教',
            'Nur_assuage' => '緩和照護衛教',
            'Nur_hospital' => '住院期間其他疑問之衛教',
            'Nur_food' => '原則性飲食衛教',
            'Nur_mood' => '情緒支持',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline m-0\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>權利倡議</b></label><br>
        <input type="hidden" field="Rights">
        <?php
        $optArray = [
            'Right_drugpay' => '健保藥品給付',
            'Right_newdrug' => '新藥審查及使用條件',
            'Right_buydrug' => '自購藥物管道',
            'Right_clinical' => '臨床試驗',
            'Right_doctor' => '醫病關係',
            'Right_quality' => '醫療品質',
            'Right_other' => '其他',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline m-0\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control-plaintext form-control-sm" field="Right_othertxt">
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>備註</b></label>
        <textarea class="form-control-plaintext" field="R_note" rows="6" readonly></textarea>
    </div>
</div>
<?php
$medicalContent = ob_get_contents();
ob_end_clean();

// 營養紀錄
ob_start();
?>
<div class="form-group row">
    <div class="col-md-4">
        <label><b>身高</b></label><br>
        <input type="number" class="form-control-plaintext form-control-sm" field="R_height" placeholder="公分" readonly>
    </div>
    <div class="col-md-4">
        <label><b>體重</b></label><br>
        <input type="number" class="form-control-plaintext form-control-sm" field="R_weight" placeholder="公斤" readonly>
    </div>
    <div class="col-md-4">
        <label><b>平常體重</b></label><br>
        <input type="number" class="form-control-plaintext form-control-sm" field="general_weight" readonly>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-4">
        <label><b>BMI</b></label><br>
        <input type="text" class="form-control-plaintext" field="R_BMI" value="0" readonly>
    </div>
    <div class="col-md-4">
        <label><b>體態</b></label><br>
        <input type="text" class="form-control-plaintext" field="R_analysis" value="---" readonly>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>諮詢目的</b></label><br>
        <input type="hidden" field="Aim">
        <?php
        $optArray = [
            'Aim_therapeutic' => '治療飲食',
            'Aim_nutrition' => '營養品補助',
            'Aim_balance' => '均衡營養',
            'Aim_countermeasures' => '生理狀況之飲食對策',
            'Aim_other' => '其他',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline m-0\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control-plaintext form-control-sm" field="Aim_othertxt" readonly>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>進食方式</b></label><br>
        <input type="hidden" field="Provide">
        <?php
        $optArray = [
            'Pv_self' => '由口進食',
            'Pv_ng' => '鼻胃管餵食',
            'Pv_gastric' => '胃管餵食',
            'Pv_intestinal' => '腸管餵食',
            'Pv_ppn' => '週邊靜脈營養',
            'Pv_tpn' => '全靜脈營養',
            'Pv_other' => '其他',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline m-0\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control-plaintext form-control-sm" field="Pv_othertxt" readonly>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-4">
        <label><b>熱量需求</b></label><br>
        <input type="text" class="form-control-plaintext form-control-sm" field="Caloric" readonly>
    </div>
    <div class="col-md-4">
        <label><b>壓力因子</b></label><br>
        <select class="form-control-plaintext form-control-sm" field="Cal_pressure" disabled>
            <?php
            foreach (['1', '1.3', '1.5', '1.8', '2'] as $i) {
                echo "<option value=\"{$i}\">{$i}</option>";
            }
            ?>
        </select>
    </div>
    <div class="col-md-4">
        <label><b>活動因子</b></label><br>
        <select class="form-control-plaintext form-control-sm" field="Cal_activity" disabled>
            <?php
            foreach (['1.2' => '臥床', '1.3' => '輕度活動', '1.4' => '中度活動'] as $i => $v) {
                echo "<option value=\"{$i}\">{$v}</option>";
            }
            ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-4">
        <label><b>蛋白質需求</b></label><br>
        <input type="text" class="form-control-plaintext form-control-sm" field="Protein" readonly>
    </div>
    <div class="col-md-4">
        <label><b>需求量</b></label><br>
        <select class="form-control-plaintext form-control-sm" field="Protein_factor" disabled>
            <?php
            foreach (['0.8', '1', '1.3', '1.5', '1.8', '2'] as $i) {
                echo "<option value=\"{$i}\">{$i}</option>";
            }
            ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>手術前後飲食衛教</b></label><br>
        <?php
        $optArray = [
            'beforeSurgery' => '手術前飲食衛教',
            'afterSurgery' => '手術後飲食衛教',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline m-0\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>治療期間飲食衛教</b></label><br>
        <input type="hidden" field="Cure">
        <?php
        $optArray = [
            'Cure_calorie' => '熱量需求',
            'Cure_protein' => '蛋白質需求與估算',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline m-0\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>生理狀況之飲食對策</b></label><br>
        <input type="hidden" field="Effect">
        <?php
        $optArray = [
            'Effect_vomit' => '噁心嘔吐 ',
            'Effect_oral' => '口腔破損',
            'Effect_full' => '飽脹感',
            'Effect_flatulence' => '脹氣',
            'Effect_diarrhea' => '腹瀉',
            'Effect_constipation' => '便秘',
            'Effect_lowHeme' => '血色素不足',
            'Effect_lowLeukocyte' => '白血球低下',
            'Effect_appetiteLoss' => '食慾不振',
            'Effect_weightLoss' => '體重下降',
            'Effect_gainWeight' => '體重增加',
            'Effect_other' => '其他',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control-plaintext form-control-sm" field="Effect_othertxt">
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>飲食原則</b></label><br>
        <input type="hidden" field="Diet">
        <?php
        $optArray = [
            'Diet_blance' => '均衡飲食',
            'Diet_diabetes' => '糖尿病飲食',
            'Diet_renalFailure' => '慢性腎衰竭飲食',
            'Diet_dialysis' => '透析飲食',
            'Diet_liver' => '肝病飲食',
            'Diet_other' => '其他',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline m-0\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control-plaintext form-control-sm" field="Diet_othertxt">
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>保健營養品相關資訊</b></label><br>
        <input type="hidden" field="Nutrition">
        <?php
        $optArray = [
            'Nu_panax' => '蔘類',
            'Nu_algae' => '藻類',
            'Nu_vitamins' => '維生素類',
            'Nu_calcium' => '鈣片',
            'Nu_mushroom' => '菇菌類保健品',
            'Nu_phytochemicals' => '植化素萃取物',
            'Nu_other' => '其他',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline m-0\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control-plaintext form-control-sm" field="Nu_othertxt">
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>飲食製備與禁忌資訊</b></label><br>
        <input type="hidden" field="Eat">
        <?php
        $optArray = [
            'Eat_normal' => '普通飲食',
            'Eat_liquid' => '流質飲食',
            'Eat_bland' => '溫和飲食',
            'Eat_lowResidue' => '低渣飲食',
            'Eat_highFiber' => '高纖飲食',
            'Eat_tube' => '管灌飲食',
            'Eat_elemental' => '元素飲',
            'Eat_other' => '其他',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline m-0\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control-plaintext form-control-sm" field="Eat_othertxt">
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>特殊營養品需求</b></label><br>
        <input type="hidden" field="Special">
        <?php
        $optArray = [
            'Sp_glutamine' => '麩醯胺酸',
            'Sp_balanced' => '均衡配方',
            'Sp_tumor' => '腫瘤配方',
            'Sp_pneumonia' => '肺病配方',
            'Sp_diabetes' => '糖尿病配方',
            'Sp_nephropathy' => '腎病配方',
            'Sp_other' => '其他',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline m-0\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control-plaintext form-control-sm" field="Sp_othertxt">
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>備註</b></label>
        <textarea class="form-control-plaintext" field="R_note" rows="6" readonly></textarea>
    </div>
</div>
<?php
$nutritionContent = ob_get_contents();
ob_end_clean();

// 社工紀錄
ob_start();
?>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>諮詢目的</b></label><br>
        <?php
        $optArray = [
            'Aim_wig' => '假髮租借',
            'Aim_welfare' => '福利諮詢',
            'Aim_goods' => '物資需求',
            'Aim_volcare' => '志工關懷需求',
            'Aim_other' => '其他',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control-plaintext form-control-sm" field="Aim_othertxt">
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>補助評估</b></label><br>
        <input type="hidden" field="Grants">
        <?php
        $optArray = [
            'Grant_help' => '急難救助',
            'Grant_nutrition' => '營養品補助',
            'Grant_traffic' => '交通補助',
            'Grant_respitecare' => '喘息服務',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>備註</b></label>
        <textarea class="form-control-plaintext" field="R_note" rows="6" readonly></textarea>
    </div>
</div>
<?php
$socialContent = ob_get_contents();
ob_end_clean();

// 心理紀錄
ob_start();
?>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>諮詢目的</b></label><br>
        <input type="hidden" field="Mind">
        <?php
        $optArray = [
            'Mind_feeling' => '情緒議題',
            'Mind_self' => '自我概念',
            'Mind_adapt' => '生活適應',
            'Mind_friends' => '人際關係',
            'Mind_family' => '家庭議題',
            'Mind_sickbed' => '疾病末期',
            'Mind_suicide' => '自殺意念',
            'Mind_selfcare' => '自我照顧',
            'Mind_lost' => '失落經驗',
            'Mind_other' => '其他',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control-plaintext form-control-sm" field="Mind_othertxt">
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>備註</b></label>
        <textarea class="form-control-plaintext" field="R_note" rows="6" readonly></textarea>
    </div>
</div>
<?php
$mentalContent = ob_get_contents();
ob_end_clean();

// 保險紀錄
ob_start();
?>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>醫療相關</b></label><br>
        <input type="hidden" field="Ins_med">
        <?php
        $optArray = [
            'Ins_admission' => '住院給付',
            'Ins_clinic' => '門診給付',
            'Ins_surgery' => '手術給付',
            'Ins_expenses' => '自費醫材給付',
            'Ins_targeted' => '標靶藥物給付',
            'Ins_radiology' => '放射治療給付',
            'Ins_chemotherapy' => '化學治療給付',
            'Ins_immunotherapy' => '免疫治療給付',
            'Ins_discharged' => '出院帶藥給付',
            'Ins_tcm' => '中醫門診',
            'Ins_hospice' => '安寧病房給付',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>保單相關</b></label><br>
        <input type="hidden" field="Ins_policy">
        <?php
        $optArray = [
            'Ins_view' => '保單檢視',
            'Ins_plan' => '保險規劃',
            'Ins_law' => '保險法律',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label m-0\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>備註</b></label>
        <textarea class="form-control-plaintext" field="R_note" rows="6" readonly></textarea>
    </div>
</div>
<?php
$insuranceContent = ob_get_contents();
ob_end_clean();

// 志工關懷
ob_start();
?>
<div class="form-group row">
    <div class="col-md-4">
        <label><b>關懷者姓名</b></label><br>
        <input type="text" class="form-control-plaintext form-control-sm" field="Contactpeop">
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>提供服務</b></label><br>
        <?php
        $optArray = [
            'Act_care' => '近況瞭解及關懷',
            'Act_expshare' => '療程經驗分享',
            'Act_support' => '情緒支持',
            'Act_other' => '其他',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control-plaintext form-control-sm" field="Act_othertxt">
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>轉介專業人員</b></label><br>
        <input type="hidden" field="Transfer">
        <?php
        $optArray = [
            'Trans_medical' => '護理師',
            'Trans_nutrition' => '營養師',
            'Trans_social' => '社工師',
            'Trans_mental' => '心理師',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>備註</b></label>
        <textarea class="form-control-plaintext" field="R_note" rows="6" readonly></textarea>
    </div>
</div>
<?php
$careContent = ob_get_contents();
ob_end_clean();

// 身心靈課程
ob_start();
?>
<div class="form-group row">
    <div class="select2-input col-md-12">
        <label><b>身心靈課程</b></label>
        <select class="form-control-plaintext form-control-sm" field="ClassID">
            <?php
            // 撈取課程資料
            $dbQuery = "SELECT * FROM FCF_careservice.MindBodyCourse ORDER BY categoryID DESC, Keydate DESC";
            $result = $db->query($dbQuery);
            foreach ($result as $rownum => $row) {
                if (!empty($row['C_No'])) {
                    $C_No = "第{$row['C_No']}期 ";
                } else {
                    $C_No = "";
                }
                echo "<option value=\"{$row['IDno']}\">{$C_No}{$row['C_name']} ({$row['C_startday']}～{$row['C_endday']})</option>";
            }
            ?>
        </select>
    </div>
</div>
<?php
$coursesContent = ob_get_contents();
ob_end_clean();

// 活動專案
ob_start();
?>
<div class="form-group row">
    <div class="select2-input col-md-12">
        <label><b>活動專案</b></label>
        <select class="form-control-plaintext form-control-sm" field="ClassID">
            <?php
            // 撈取課程資料
            $dbQuery = "SELECT * FROM FCF_careservice.Projects ORDER BY categoryID DESC, Keydate DESC";
            $result = $db->query($dbQuery);
            foreach ($result as $rownum => $row) {
                if (!empty($row['C_No'])) {
                    $C_No = "第{$row['C_No']}期 ";
                } else {
                    $C_No = "";
                }
                echo "<option value=\"{$row['IDno']}\">{$C_No}{$row['C_name']} ({$row['C_startday']}～{$row['C_endday']})</option>";
            }
            ?>
        </select>
    </div>
</div>
<?php
$projectsContent = ob_get_contents();
ob_end_clean();

// 轉介單
ob_start();
?>
<div class="form-group row">
    <div class="col-md-4">
        <label><b>轉介人</b></label><br>
        <input type="hidden" field="AdminID" value="<?=$generalData['userid']?>">
        <input type="text" class="form-control-plaintext form-control-sm" field="AdName" value="<?=$generalData['username']?>" readonly>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>轉介說明</b></label>
        <textarea class="form-control-plaintext" field="refComment" rows="5" readonly></textarea>
    </div>
</div>
<div class="form-group row">
    <div class="select2-input col-md-4">
        <label><b>通知被轉介人</b></label>
        <select class="form-control-plaintext form-control-sm" field="refID">
            <option value="0"></option>
            <?php
            // 撈取課程資料
            $dbQuery = "SELECT * FROM FCF_careservice.Accuser WHERE Acc_status=1 AND Acc_level=2";
            $result = $db->query($dbQuery);
            foreach ($result as $rownum => $row) {
                echo "<option value=\"{$row['IDno']}\">{$row['Acc_name']}</option>";
            }
            ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>被轉介人回覆說明</b></label>
        <textarea class="form-control-plaintext" field="refReply" rows="5" readonly></textarea>
    </div>
</div>
<?php
$transferContent = ob_get_contents();
ob_end_clean();

// 一般諮詢
ob_start();
?>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>蔬果防癌觀念</b></label><br>
        <input type="hidden" field="vegetarian">
        <?php
        $optArray = [
            'Vege_1' => '蔬果彩虹579',
            'Vege_2' => '高纖蔬果',
            'Vege_3' => '蔬果的農藥清洗',
            'Vege_4' => '減脂防癌',
            'Vege_5' => '食物保存',
            'Vege_6' => '食品安全',
            'Vege_other' => '其他',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline mr-0\">
                <label class=\"form-check-label\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
        <div class="form-check-inline">
            <input type="text" class="form-control-plaintext form-control-sm" field="Vege_othertxt">
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>癌症預防觀念</b></label><br>
        <input type="hidden" field="prevent">
        <?php
        $optArray = [
            'prev_1' => '子宮頸癌疫苗問題及衛教',
            'prev_2' => '健康檢查',
            'prev_3' => '生活防癌全民練5功衛教',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>疾病的認識</b></label><br>
        <input type="hidden" field="Knowledge">
        <?php
        $optArray = [
            'Konw_1' => '癌症的認識',
            'Konw_2' => '治療的認識',
            'Konw_3' => '其他疾病的認識',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>其他</b></label><br>
        <input type="hidden" field="Other">
        <?php
        $optArray = [
            'Oth_1' => '陳情及建議',
            'Oth_2' => '感謝',
            'Oth_3' => '異業結盟',
            'hair' => '捐贈頭髮',
        ];

        foreach ($optArray as $name => $label) {
            echo "
            <div class=\"form-check form-check-inline\">
                <label class=\"form-check-label\">
                    <input class=\"form-check-input\" type=\"checkbox\" value=\"1\" field=\"{$name}\">
                    <span class=\"form-check-sign unselectable\">{$label}</span>
                </label>
            </div>";
        }
        ?>
    </div>
</div>
<div class="form-group row">
    <div class="col-md-12">
        <label><b>備註</b></label>
        <textarea class="form-control-plaintext" field="R_note" rows="6" readonly></textarea>
    </div>
</div>
<?php
$generalContent = ob_get_contents();
ob_end_clean();

unset($recTypeArray['projects'], $recTypeArray['course'], $recTypeArray['train']);
// 新增，修改資料Modal
foreach ($recTypeArray as $recType => $tables) {
?>
    <div class="modal fade" id="data_<?=$recType?>" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title"></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="dataID">
                    <input type="hidden" name="recType">
                    <input type="hidden" name="action">
                    <input type="hidden" value="viewModal">
                    <div class="form-group row">
                    <?php
                    if (!in_array($recType, ['courses','projects'], true)) {
                    ?>
                        <div class="col-md-3">
                            <label><b>日期</b></label>
                            <input type="text" class="form-control-plaintext form-control-sm" maxlength="10" field="R_date" readonly>
                        </div>
                        <?php
                        if (!in_array($recType, ['transfer'], true)) {
                        ?>
                        <div class="col-md-3">
                            <label><b>共服務時間</b></label>
                            <select class="form-control-plaintext form-control-sm" field="R_time" disabled>
                                <?php
                                for ($i=0; $i <120 ; $i+=10) {
                                    echo "<option value=\"{$i}\">{$i}~" . ($i+10) . " 分鐘</option>";
                                }
                                ?>
                                <option value="<?=$i?>"><?=$i?> 分鐘以上</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label><b>服務方式</b></label>
                            <select class="form-control-plaintext form-control-sm" field="R_way" disabled>
                                <?php
                                    $optArray = [
                                        'tel' => '電話',
                                        'face' => '面談',
                                        'web' => '網路',
                                        'email' => '電子郵件',
                                    ];
                                    foreach ($optArray as $key => $value) {
                                        echo "<option value=\"{$key}\">{$value}</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label><b>地點</b></label>
                            <input type="text" class="form-control-plaintext form-control-sm" field="R_loc" maxlength="50" readonly>
                        </div>
                        <?php
                        }
                    }
                        ?>
                    </div>
                    <?=${"{$recType}Content"}?>
                </div>
            </div>
        </div>
    </div>
<?php
}
