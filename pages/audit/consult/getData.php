<?php
/**
 * 搜尋待審核的資料
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $rowid 搜尋的資料ID
 * @param string $rowType 搜尋的資料類型
 */
$rowid = intval(reqParam('rowid', 'post'));
$rowType = trim(strval(reqParam('rowType', 'post')));
$tableName = $recTypeArray[$rowType];

$dbQuery = "SELECT * FROM FCF_careservice.{$tableName} WHERE IDno=?";
$response = $db->row($dbQuery, [$rowid]);

//set Content-Type to JSON
header('Content-Type: application/json; charset=utf-8');
// 清除先前頁面要顯示的快取
ob_end_clean();
//echo error message as JSON
echo json_encode($response, JSON_UNESCAPED_UNICODE);