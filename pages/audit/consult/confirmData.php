<?php
/**
 * 審核待審資料
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param array $auditItems 使用者勾選待審核人員
 */

$auditItems = reqParam('selectAudit', 'post', 'string');

$dt = new DateTime();
$now = $dt->format('Y-m-d H:i:s');

if (is_array($auditItems)) {
    foreach ($auditItems as $key => $item) {
        $itemArray = explode('-', $item);
        $dataTable = $recTypeArray[$itemArray[0]];
        $dbQuery = "UPDATE FCF_careservice.{$dataTable} SET Admincheck=1, checktime=? WHERE IDno=? AND AdmincheckID=?";
        $db->query($dbQuery, [$now, $itemArray[1], $generalData['userid']]);
    }
}

unset($_GET['action']);
header("Location:?" . http_build_query($_GET));