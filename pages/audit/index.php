<?php
/**
 * 新增/以及編輯資料使用表單
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 */
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
$tableTitle = '待審核清單';
// 資料定義
require_once get_relative_path("pages/{$page}/dataDefinition.php");
$dbQuery = "SELECT t.*, CASE t.anonym WHEN 0 THEN m.C_name WHEN 1 THEN an.C_name END AS memberName, a.Acc_name AS auditor
            FROM (
                SELECT concat('general-', IDno) AS auditID, '一般諮詢' AS recType, R_date, R_loc, AdName, anonym, MemberID, '' AS filePath, 0 AS categoryID, AdmincheckID FROM FCF_careservice.Rec_ask WHERE MemberID>0 AND Admincheck=0
                UNION ALL
                SELECT concat('medical-', IDno) AS auditID, '醫護紀錄' AS recType, R_date, R_loc, AdName, anonym, MemberID, filePath, 0 AS categoryID, AdmincheckID FROM FCF_careservice.Rec_medical WHERE MemberID>0 AND Admincheck=0
                UNION ALL
                SELECT concat('nutrition-', IDno) AS auditID, '營養紀錄' AS recType, R_date, R_loc, AdName, anonym, MemberID, filePath, 0 AS categoryID, AdmincheckID FROM FCF_careservice.Rec_nutrition WHERE MemberID>0 AND Admincheck=0
                UNION ALL
                SELECT concat('social-', IDno) AS auditID, '社工紀錄' AS recType, R_date, R_loc, AdName, anonym, MemberID, filePath, 0 AS categoryID, AdmincheckID FROM FCF_careservice.Rec_social WHERE MemberID>0 AND Admincheck=0
                UNION ALL
                SELECT concat('mental-', IDno) AS auditID, '心理紀錄' AS recType, R_date, R_loc, AdName, anonym, MemberID, filePath, 0 AS categoryID, AdmincheckID FROM FCF_careservice.Rec_mental WHERE MemberID>0 AND Admincheck=0
                UNION ALL
                SELECT concat('insurance-', IDno) AS auditID, '保險紀錄' AS recType, R_date, R_loc, AdName, anonym, MemberID, filePath, 0 AS categoryID, AdmincheckID FROM FCF_careservice.Rec_insurance WHERE MemberID>0 AND Admincheck=0
                UNION ALL
                SELECT concat('care-', IDno) AS auditID, '志工關懷' AS recType, R_date, R_loc, AdName, anonym, MemberID, '' AS filePath, 0 AS categoryID, AdmincheckID FROM FCF_careservice.Rec_care WHERE MemberID>0 AND Admincheck=0
                UNION ALL
                SELECT concat('projects-', IDno) AS auditID, '專案' AS recType, DATE(Keydate) AS R_date, C_location AS R_loc, AdName, 99 AS anonym, 0 AS MemberID, '' AS filePath, categoryID, AdmincheckID FROM FCF_careservice.Projects WHERE Admincheck=0
                UNION ALL
                SELECT concat('course-', IDno) AS auditID, '身心靈課程' AS recType, DATE(Keydate) AS R_date, C_location AS R_loc, AdName, 99 AS anonym, 0 AS MemberID, '' AS filePath, categoryID, AdmincheckID FROM FCF_careservice.MindBodyCourse WHERE Admincheck=0
                UNION ALL
                SELECT concat('train-', IDno) AS auditID, '教育訓練' AS recType, DATE(Keydate) AS R_date, C_location AS R_loc, AdName, 99 AS anonym, 0 AS MemberID, '' AS filePath, categoryID, AdmincheckID FROM FCF_careservice.VolunteerTrain WHERE Admincheck=0
            ) t
            LEFT JOIN FCF_careservice.Memberdata m ON m.IDno=t.MemberID AND m.C_status=1
            LEFT JOIN FCF_careservice.Anonymous an ON an.IDno=t.MemberID
            LEFT JOIN FCF_careservice.Accuser a ON a.IDno=t.AdmincheckID
            WHERE t.AdmincheckID=?
            ORDER BY t.R_date DESC";
$result = $db->query($dbQuery, [$generalData['userid']]);
?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$definition['title']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$listItems[$page]['name']?></a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <form method="post" id="auditForm">
                        <input type="hidden" name="page" value="audit">
                        <div class="card">
                            <div class="card-header">
                                <div class="d-flex align-items-center">
                                    <h4 class="card-title"><?=$tableTitle?></h4>
                                    <button type="submit" class="btn btn-outline-info btn-round btn-sm mx-3">
                                        <span class="btn-label"><i class="fas fa-tasks"></i>確認審核</span>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                <table id="basic-datatables" class="display table table-striped table-hover dt-responsive nowrap" width="100%">
                                        <thead>
                                            <tr>
                                                <th style="width:50px;"><input class="align-middle" type="checkbox" id="checkAll"> 全選</th>
                                                <?php
                                                foreach ($definition[$page]['colhead'] as $head) {
                                                    echo "<th>{$head}</th>";
                                                }
                                                ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            // 待審核清單
                                            foreach ($result as $row) {
                                                $columnContent = '';
                                                $columnContent .= "<td><input type=\"checkbox\" name=\"selectAudit[]\" value=\"{$row['auditID']}\"></td>";
                                                foreach ($definition[$page]['col'] as $colname) {
                                                    if ($colname === 'recType') {
                                                        $dataArray = explode('-', $row['auditID']);
                                                        // 活動類型的直接導向該活動頁面
                                                        if (in_array($row[$colname], ['專案', '身心靈課程', '教育訓練'])) {
                                                            switch ($row[$colname]) {
                                                                case '專案':
                                                                    $columnContent .= "<td><a class=\"py-0 px-2\" href=\"?page=projects&id={$dataArray[1]}&cid={$row['categoryID']}&action=dataForm\" target=\"_blank\">{$row[$colname]}</a></td>";
                                                                    break;
                                                                case '身心靈課程':
                                                                    $columnContent .= "<td><a class=\"py-0 px-2\" href=\"?page=courses&id={$dataArray[1]}&cid={$row['categoryID']}&action=dataForm\" target=\"_blank\">{$row[$colname]}</a></td>";
                                                                    break;
                                                                case '教育訓練':
                                                                    $columnContent .= "<td><a class=\"py-0 px-2\" href=\"?page=volunteer&id={$dataArray[1]}&cid={$row['categoryID']}&sub=vol_train&action=dataForm\" target=\"_blank\">{$row[$colname]}</a></td>";
                                                                    break;
                                                            }
                                                        } else {
                                                            $columnContent .= "<td><input class=\"rowID\" type=\"hidden\" value='{$dataArray[1]}'>
                                                            <button type=\"button\" class=\"btn btn-link py-0 px-2\" data-toggle=\"modal\"
                                                            data-target=\"#data_{$dataArray[0]}\" data-act=\"mod\">{$row[$colname]}</button></td>";
                                                        }
                                                    }elseif ($colname === 'anonym') {
                                                        if ($row[$colname] == 1) {
                                                            $columnContent .= '<td>線上諮詢</td>';
                                                        } elseif ($row[$colname] == 0) {
                                                            $columnContent .= '<td>個案諮詢</td>';
                                                        } else {
                                                            $columnContent .= '<td>活動項目</td>';
                                                        }
                                                    } elseif ($colname === 'filePath') {
                                                        if (empty($row[$colname])) {
                                                            $columnContent .= "<td>無檔案</td>";
                                                        } else {
                                                            $columnContent .= "<td><a href=\"/uploads/files/{$row[$colname]}\" target=\"_blank\">檔案連結</a></td>";
                                                        }
                                                    } else {
                                                        $columnContent .= "<td>{$row[$colname]}</td>";
                                                    }
                                                }
                                                echo "<tr>{$columnContent}</tr>";
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
require 'viewModal.php';
?>

<script>
var params = new URLSearchParams(location.search);

function init() {
    $('#basic-datatables').DataTable({
        order: [[ 2, "desc" ]],
        columnDefs: [{
            targets: [-1, 0],
            orderable: false,
        }],
    });

    // 全選
    if ($('#basic-datatables').find('input:checkbox:checked').not("#checkAll").length == $('#basic-datatables').find('input:checkbox').not("#checkAll").length) {
        $("#checkAll").prop("checked", true);
    }

    $("#checkAll").on("click", function(){
        $('#basic-datatables').find('input:checkbox').not(this).prop('checked', this.checked);
    });

    $("#auditForm").on("submit", function (e) {
        if ($(this).find(":checkbox:checked").length === 0) {
            swal({
                title: "請勾選需審核項目",
                icon: "error"
            });
            e.preventDefault();
        } else {
            params.set("action", "dataForm");
            $(this).attr("action", "?" + params.toString());
        }
    })

    // 以下為 Modal 使用之 script
    let recArray = ["<?=implode("\",\"", array_keys($recTypeArray))?>"];
    let dataModalStr = "";

    // 取得每個modalID
    $.each(recArray, function (i, v) {
        dataModalStr += "#data_" + v + ",";
    });
    dataModalStr = dataModalStr.slice(0,-1);

    $(dataModalStr).on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        // Extract info from data-* attributes
        let rowdata = "";
        // 這個modal
        let modal = $(this);
        let recType = modal.attr("id").replace("data_", "");

        let rowID = JSON.parse(button.siblings(".rowID").val());

        params.set("action", "view");
        $.ajax({
            url: "./?" + params.toString(),
            method: "POST",
            data: {rowid: rowID, rowType: recType, page: "getdata"},
            dataType: "json"
        }).done(function(rowdata) {
            let fields = modal.find("[field]");
            // 填欄位
            $.each(fields, function(index, fieldNode) {
                let field = $(fieldNode).attr("field");
                let inputType = $(fieldNode).attr("type") || $(fieldNode).prop("tagName");
                if (inputType != undefined) {
                    inputType = inputType.toLowerCase();
                }
                // 依據不同的 input 類型，做不同的塞值動作(單個欄位對應單筆資料)
                if (inputType == "text" || inputType == "textarea" || inputType == "number" || inputType == "hidden") {
                    $(fieldNode).val(rowdata[field]);
                } else if (inputType == "checkbox" && Boolean(rowdata[field])) {
                    $(fieldNode).prop("checked", true);
                } else if (inputType == "select") {
                    $(fieldNode).val(rowdata[field]).trigger("change");
                }
            });
        });


        modal.find(".modal-title").text("瀏覽諮詢紀錄");
        modal.find(":checkbox").on("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
        });
    });
}
window.onload = init;
</script>