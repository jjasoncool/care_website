<?php
/**
 * 資料處理層，將php分配到不同頁面處理
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */
require_once 'vendor/autoload.php';
require 'settings/loginCheck.php';
require_once get_relative_path("pages/{$page}/dataDefinition.php");
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
$pageAction = reqParam('page', 'post', 'string');

switch ($pageAction) {
    case 'getdata':
        // 查詢該諮詢紀錄資料
        require_once get_relative_path("pages/{$page}/consult/getData.php");
        break;

    case 'audit':
        // 審核資料
        require_once get_relative_path("pages/{$page}/consult/confirmData.php");
        break;

    default:
        break;
}

exit();