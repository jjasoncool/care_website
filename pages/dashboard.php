<?php
/**
 * 首頁面板
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);

$dbQuery = "SELECT COUNT(1) as num FROM FCF_careservice.Memberdata";
$result = $db->row($dbQuery);
$allCaseNum = number_format($result['num']);

$dbQuery = "SELECT
                SUM(CASE WHEN a.Acc_area='Taipei' THEN 1 ELSE 0 END) AS Taipei,
                SUM(CASE WHEN a.Acc_area='Kaohsiung' THEN 1 ELSE 0 END) AS Kaohsiung
            FROM FCF_careservice.Memberdata m
            INNER JOIN FCF_careservice.Accuser a ON m.AdminID=a.IDno
            WHERE YEAR(m.Keydate) = YEAR(CURRENT_DATE) AND MONTH(m.Keydate) = MONTH(CURRENT_DATE);";
$result = $db->row($dbQuery);
$monthCaseNum = number_format($result['Taipei'] + $result['Kaohsiung']);
$TPmonth = number_format($result['Taipei']);
$KHmonth = number_format($result['Kaohsiung']);

// 行事曆顯示
if ($generalData['trackJudge']) {
    $filter = '';
} else {
    $filter = ' AND AdminID=?';
}
$dbQuery = "SELECT CONCAT(a.Acc_name, '追蹤個案:',t.T_name) AS title, t.T_date AS 'start',
            CASE t.assess_type WHEN 0 THEN 'fc-default' WHEN 1 THEN 'fc-danger' WHEN 2 THEN 'fc-warning' WHEN 3 THEN 'fc-info' WHEN 4 THEN 'fc-primary' END AS className,
            CONCAT('?page=assessment&action=view&id=', t.MemberID) AS url
            FROM FCF_careservice.Tracklist t
            INNER JOIN FCF_careservice.Accuser a ON t.AdminID=a.IDno
            WHERE T_date IS NOT NULL {$filter} AND MemberID>0";
$result = $db->query($dbQuery, [$generalData['userid']]);
$calendarJSON = htmlspecialchars(json_encode($result, JSON_UNESCAPED_UNICODE));

?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">Dashboard</h4>
                <div class="btn-group btn-group-page-header ml-auto">
                    <button type="button" class="btn btn-light btn-round btn-page-header-dropdown dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-ellipsis-h"></i>
                    </button>
                    <div class="dropdown-menu">
                        <div class="arrow"></div>
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Separated link</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="card card-stats card-round">
                        <div class="card-body ">
                            <div class="row align-items-center">
                                <div class="col-icon">
                                    <div class="icon-big text-center icon-primary bubble-shadow-small">
                                        <i class="fas fa-users"></i>
                                    </div>
                                </div>
                                <div class="col col-stats ml-3 ml-sm-0">
                                    <div class="numbers">
                                        <p class="card-category">總個案數</p>
                                        <h4 class="card-title"><?=$allCaseNum?></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="card card-stats card-round">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-icon">
                                    <div class="icon-big text-center icon-info bubble-shadow-small">
                                        <i class="far fa-calendar-alt"></i>
                                    </div>
                                </div>
                                <div class="col col-stats ml-3 ml-sm-0">
                                    <div class="numbers">
                                        <p class="card-category">本月新增個案</p>
                                        <h4 class="card-title"><?=$monthCaseNum?></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="card card-stats card-round">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-icon">
                                    <div class="icon-big text-center icon-success bubble-shadow-small">
                                        <i class="fas fa-building"></i>
                                    </div>
                                </div>
                                <div class="col col-stats ml-3 ml-sm-0">
                                    <div class="numbers">
                                        <p class="card-category">台北總會本月個案數</p>
                                        <h4 class="card-title"><?=$TPmonth?></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="card card-stats card-round">
                        <div class="card-body">
                            <div class="row align-items-center">
                                <div class="col-icon">
                                    <div class="icon-big text-center icon-secondary bubble-shadow-small">
                                        <i class="fab fa-kickstarter-k"></i>
                                    </div>
                                </div>
                                <div class="col col-stats ml-3 ml-sm-0">
                                    <div class="numbers">
                                        <p class="card-category">南部分會本月個案數</p>
                                        <h4 class="card-title"><?=$KHmonth?></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">追蹤時程</div>
                            <ul class="list-inline my-0">
                                <li class="list-inline-item"><i class="fas fa-square text-default"></i> 未指派</li>
                                <li class="list-inline-item"><i class="fas fa-square text-danger"></i> 護理</li>
                                <li class="list-inline-item"><i class="fas fa-square text-warning"></i> 營養</li>
                                <li class="list-inline-item"><i class="fas fa-square text-info"></i> 社工</li>
                                <li class="list-inline-item"><i class="fas fa-square text-success"></i> 心理</li>
                            </ul>
                        </div>
                        <div class="card-body">
                            <div id="calendar"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-head-row">
                                <div class="card-title">APP 會員諮詢</div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="d-flex">
                                <div class="avatar">
                                    <span class="avatar-title rounded-circle border border-white bg-info"><i class="fas fa-search"></i></span>
                                </div>
                                <div class="flex-1 ml-3 my-auto pt-1">
                                    <h5 class="text-uppercase fw-bold mb-1">
                                        <a href="https://www.canceraway.org.tw/cms/member-list.asp?mm=10" target="_blank">會員查詢管理</a>
                                    </h5>
                                </div>
                            </div>
                            <div class="separator-dashed"></div>
                            <div class="d-flex">
                                <div class="avatar">
                                    <span class="avatar-title rounded-circle border border-white bg-secondary"><i class="la flaticon-alarm-1"></i></span>
                                </div>
                                <div class="flex-1 ml-3 my-auto pt-1">
                                    <h5 class="text-uppercase fw-bold mb-1">
                                        <a href="https://www.canceraway.org.tw/cms/member-QAframe.asp" target="_blank">最新諮詢</a>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var className = Array('fc-primary', 'fc-danger', 'fc-default', 'fc-success', 'fc-info', 'fc-warning', 'fc-danger-solid', 'fc-warning-solid', 'fc-success-solid', 'fc-default-solid', 'fc-success-solid', 'fc-primary-solid');

    function init() {
        let calendarJSON = JSON.parse(decodeEntities("<?=$calendarJSON?>"));
        $('#calendar').fullCalendar({
            locale: 'zh-tw',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay,listWeek'
            },
            events: calendarJSON
        });
    }

    window.onload = init;

</script>