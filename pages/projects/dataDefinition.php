<?php
/**
 * 定義資源清單使用資料庫欄位以及項目
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

$definition = [
    'title' => '專案清單',
    'category' => [
        'kind' => 'event',
        'colhead' => ['類別名稱'],
        'col' => ['C_name'],
        'colLink' => ['C_name' => true],
        'dataType' => [
            'TP' => ['unit' => '臺北總會'],
            'KH' => ['unit' => '高雄分會']
        ],
    ],
    'activities' => [
        'tableName' => 'Projects',
        'colhead' => ['日期', '項目', '期別', '合作單位' ,'負責人', '講師', '審核'],
        'col' => ['eventdate', 'C_name', 'C_No', 'C_partner', 'C_manager', 'C_teacher','Admincheck'],
    ]
];