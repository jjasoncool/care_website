<?php
/**
 * 上傳檔案，依據諮詢紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param int $id 專案編號
 */

$id = intval(reqParam('delID', 'post'));
$classid = intval(reqParam('id', 'get'));
// 錯誤或成功訊息
$msg = [];
$dataTable = 'EventArchives';
$dbQuery = "SELECT * FROM FCF_careservice.{$dataTable} WHERE IDno=? AND ClassID=?";
$result = $db->row($dbQuery, [$id, $classid]);

// 刪除檔案
$fileName = $result['Fname'];
$target_path = "{$_SERVER['DOCUMENT_ROOT']}/uploads/photos/{$fileName}";
unlink($target_path);

// 更新資料庫
$dbQuery = "DELETE FROM FCF_careservice.{$dataTable} WHERE IDno=? AND ClassID=?";
$db->query($dbQuery, [$id, $classid]);

// 最後的成功訊息
header("HTTP/1.1 200 OK");
$msg['filename'] = $fileName;
$msg['deleted'] = '刪除檔案成功!';

//set Content-Type to JSON
header('Content-Type: application/json; charset=utf-8');
// 清除先前頁面要顯示的快取(一開始走router前面的頁面等等)
ob_end_clean();
//echo error message as JSON
echo json_encode($msg, JSON_UNESCAPED_UNICODE);
