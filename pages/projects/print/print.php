<?php
/**
 * 列印諮詢紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */

// 清除先前頁面要顯示的快取
ob_end_clean();

require_once 'printDefinition.php';

// 準備資料
$rowdata = json_decode(urldecode(reqParam('rowdata', 'post')));

/**
 * 將勾選項目轉換成文字輸出
 *
 * @param object $rowdata
 * @param array $categoryArray 該諮詢紀錄定義的陣列
 * @param string $defType 陣列中的勾選類型
 * @return void
 */
function findCheck($rowdata, $categoryArray, $defType)
{
    $returnText = '';
    $title = $categoryArray[$defType]['categoryTitle'];
    unset($categoryArray[$defType]['categoryTitle']);
    // 找尋勾選項目
    foreach ($categoryArray[$defType] as $col => $label) {
        if ($rowdata->$col == 1) {
            $returnText .= "{$label},";
        }
    }
    if (!empty($returnText)) {
        $returnText = '<tr><td>' . $title . '</td><td colspan="3">' . substr($returnText, 0, -1) . '</td></tr>';
    }

    return $returnText;
}

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF
{

    //Page header
    public function Header()
    {
        // Logo
        $image_file = "{$_SERVER['DOCUMENT_ROOT']}uploads/images/logo.png";
        $this->Image($image_file, '', '', 50, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
    }

}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('skjan');
$pdf->SetTitle("{$printDef['name']}列印");

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
    require_once dirname(__FILE__) . '/lang/eng.php';
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('taipeisans', 'B', 16);

// add a page
$pdf->AddPage();

$pdf->Write(0, $printDef['name'], '', 0, 'C', true, 0, false, false, 0);
$pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);

// 表格字體
$pdf->SetFont('taipeisans', '', 12);

// -----------------------------------------------------------------------------

$printTable = <<<EOD
<table cellspacing="0" cellpadding="4" border="1">
    <tr><td>課程名稱</td><td colspan="2"><font color="#FF0000">{$rowdata->C_name}</font></td><td>地點</td><td colspan="2">{$rowdata->C_location}</td></tr>
    <tr><td>開始日期</td><td colspan="2">{$rowdata->C_startday}</td><td>至</td><td colspan="2">{$rowdata->C_endday}</td></tr>
    <tr><td>期別</td><td colspan="2">第{$rowdata->C_No}期</td><td>課程日</td><td colspan="2">共{$rowdata->C_num}次</td></tr>
    <tr><td>合作單位</td><td colspan="2">{$rowdata->C_partner}</td><td>主責人員</td><td colspan="2">{$rowdata->C_manager}</td></tr>
    <tr><td>參與人數</td><td colspan="2">{$rowdata->C_joinnum}</td><td>服務總人次</td><td colspan="2">{$rowdata->C_servicenum}</td></tr>
    <tr><td>請假人次</td><td colspan="2">{$rowdata->C_leavenum}</td><td colspan="3"></td></tr>
    <tr><td>{$printDef['name']}\n內容記錄</td><td colspan="5">{$rowdata->C_note}</td></tr>
    <tr><td>特殊情形</td><td colspan="5">{$rowdata->C_specrec}</td></tr>
    <tr><td>相關評值</td><td colspan="5">{$rowdata->C_comment}</td></tr>
    <tr><td>檢討與建議</td><td colspan="5">{$rowdata->C_issue}</td></tr>
</table>
EOD;

$pdf->writeHTML($printTable, true, false, false, false, '');

// ---------------------------------------------------------

// set font
$pdf->SetFont('taipeisans', 'B', 16);

// add a page
$pdf->AddPage();

$pdf->Write(0, '出席紀錄', '', 0, 'C', true, 0, false, false, 0);
$pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);

// 表格字體
$pdf->SetFont('taipeisans', '', 12);

// -----------------------------------------------------------------------------

$attendPeople = <<<EOD
<table cellspacing="0" cellpadding="4" border="1">
<tr><th>姓名</th><th>身分證</th><th>性別</th><th>電話</th><th>手機</th></tr>
EOD;
foreach ($rowdata->attend as $num => $peop) {
    $attendPeople .= <<<EOD
    <tr><td>{$peop->C_name}</td><td>{$peop->C_idno}</td><td>{$peop->C_sex}</td><td>{$peop->C_tele}</td><td>{$peop->C_mobile1}</td></tr>
    EOD;
}
$attendPeople .= '</table>';

$pdf->writeHTML($attendPeople, true, false, false, false, '');

$id = intval(reqParam('id', 'get'));
$dbQuery = "SELECT * FROM FCF_careservice.EventArchives WHERE ClassID=?";
$attendPhoto = $db->query($dbQuery, [$id]);

if (count($attendPhoto) > 0) {
    // -----------------------------------------------------------------------------

    // set font
    $pdf->SetFont('taipeisans', 'B', 16);

    // add a page
    $pdf->AddPage();

    $pdf->Write(0, '活動照片', '', 0, 'C', true, 0, false, false, 0);
    $pdf->Write(0, '', '', 0, 'C', true, 0, false, false, 0);

    // 表格字體
    $pdf->SetFont('taipeisans', '', 12);

    // -----------------------------------------------------------------------------

    $showPhoto = <<<EOD
    <table cellspacing="0" cellpadding="4" border="1">
    EOD;
    foreach ($attendPhoto as $num => $row) {
        if ($num % 3 == 0) {
            $showPhoto .= '<tr>';
        }
        $showPhoto .= <<<EOD
        <td><img src="uploads/photos/{$row['Fname']}"></td>
        EOD;
        if ($num % 3 == 2) {
            $showPhoto .= '</tr>';
        }
    }

    $pdf->writeHTML($showPhoto, true, false, false, false, '');
}

// Close and output PDF document
$pdf->Output('print.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
