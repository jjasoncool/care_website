<?php
/**
 * 資料處理層，將php分配到不同頁面處理
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */
require_once 'vendor/autoload.php';
require 'settings/loginCheck.php';
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
$pageAction = reqParam('page', 'post', 'string');
unset($_GET["attendEmpty"]);
require_once get_relative_path("pages/{$page}/dataDefinition.php");

switch ($pageAction) {
    case 'category':
        // 類別 新增,修改,刪除
        require_once get_relative_path("pages/{$page}/categoryManage.php");
        break;

    case 'courseinfo':
        // 課程資料修改
        require_once get_relative_path("pages/{$page}/dataManage.php");
        break;

    case 'searchList':
        // 專案名單搜尋
        require_once get_relative_path("pages/{$page}/attendList/searchList.php");
        break;

    case 'attendList':
        // 參加名單修改
        require_once get_relative_path("pages/{$page}/attendList/attendRecEdit.php");
        break;

    case 'uploadFile':
        // 上傳檔案
        require_once get_relative_path("pages/{$page}/attendList/attendPhotosUpload.php");
        break;

    case 'deleteFile':
        // 刪除相片檔案
        require_once get_relative_path("pages/{$page}/attendList/attendPhotoDel.php");
        break;

    case 'printPDF':
        // 列印課程/專案
        require_once get_relative_path("pages/{$page}/print/print.php");
        break;

    default:
        break;
}

exit();