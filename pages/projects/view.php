<?php
/**
 * 查看類別內的專案紀錄
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $action 路由到這個頁面的參數
 * @param int $id 專案類別編號
 */

$cid = intval(reqParam('id', 'get'));
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
// 資料定義
require_once get_relative_path("pages/{$page}/dataDefinition.php");
// 直接撈出專案清單
$dbQuery = "SELECT * FROM FCF_careservice.EventCategory WHERE IDno=?";
$result = $db->row($dbQuery, [$cid]);
$tableTitle = "{$result['cityLocation']}-{$result['C_name']}";

// 資料定義
require_once get_relative_path("pages/{$page}/dataDefinition.php");
?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title"><?=$listItems[$page]['name']?></h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="?<?="page={$page}"?>"><?=$listItems[$page]['name']?></a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#"><?=$tableTitle?></a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="d-flex align-items-center">
                                <h4 class="card-title"><?=$tableTitle?></h4>
                                <div class="ml-auto">
                                    <a class="btn btn-outline-success btn-sm mr-3" href="?page=<?=$page?>&cid=<?=$cid?>&action=dataForm">
                                        <span class="btn-label"><i class="fas fa-plus"></i>新增專案</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="basic-datatables" class="display table table-striped table-hover dt-responsive nowrap" width="100%">
                                    <thead>
                                        <tr>
                                            <?php
                                            foreach ($definition['activities']['colhead'] as $head) {
                                                echo "<th>{$head}</th>";
                                            }
                                            ?>
                                            <th>刪除</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $delBtn = "<button type=\"button\" title=\"刪除\" class=\"btn btn-link btn-danger py-0 pl-2\"
                                                    data-toggle=\"modal\" data-target=\"#delEvent\">
                                                        <i class=\"fa fa-times fa-lg\"></i>
                                                    </button>";
                                        $dbQuery = "SELECT m.*, CONCAT(C_startday, '~', C_endday) AS eventdate, a.Acc_name AS checkName
                                                    FROM FCF_careservice.{$definition['activities']['tableName']} m
                                                    LEFT JOIN FCF_careservice.Accuser a ON m.AdmincheckID=a.IDno
                                                    WHERE m.categoryID=? AND m.`status`=1";
                                        $result = $db->query($dbQuery, [$cid]);
                                        foreach ($result as $row) {
                                            $columnContent = '';
                                            $rowdata = json_encode($row, JSON_UNESCAPED_UNICODE);
                                            foreach ($definition['activities']['col'] as $colname) {
                                                if ($colname === 'Admincheck') {
                                                    if ($row[$colname] == 1) {
                                                        $columnContent .= "<td><i class=\"fas fa-lg fa-check\"></i> {$row['checkName']}</td>";
                                                    } else {
                                                        $columnContent .= "<td><i class=\"fas fa-lg fa-book-reader\"></i> 審核中</td>";
                                                    }
                                                } elseif ($colname === 'C_name') {
                                                    $columnContent .= "<td><a href=\"?page={$page}&id={$row['IDno']}&cid={$cid}&action=dataForm\">{$row[$colname]}</a></td>";
                                                } else {
                                                    $columnContent .= "<td>{$row[$colname]}</td>";
                                                }
                                            }
                                            echo
                                                "<tr>
                                                    {$columnContent}
                                                    <td width=\"50\">
                                                        <div class=\"form-button-action\">
                                                            <input type=\"hidden\" class=\"rowdata\" value='{$rowdata}'>
                                                            {$delBtn}
                                                        </div>
                                                    </td>
                                                </tr>";
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delEvent" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">刪除<?=$listItems[$page]['name']?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post">
                <div class="modal-body">
                    <input type="hidden" name="dataID">
                    <input type="hidden" name="formAction" value="del">
                    <input type="hidden" name="page" value="courseinfo">
                    <h6>請問確定要<span class="text-danger"><b>刪除</b></span>此資料嗎?</h6>
                    <b><p class="my-0"></p></b>
                </div>
                <div class="modal-footer no-bd">
                    <button type="submit" class="btn btn-danger">確認刪除</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
function init() {
    $('#basic-datatables').DataTable({
        lengthChange: false,
        order: [[ 0, "desc" ]],
        columnDefs: [{
            targets: [-1],
            orderable: false,
        }],
        responsive: {
            details: {
                display: $.fn.dataTable.Responsive.display.modal( {
                    header: function ( row ) {
                        var data = row.data();
                        return 'Details for '+data[2]+' '+data[3];
                    }
                } ),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                    tableClass: 'table'
                } )
            }
        }
    });

    // 刪除課程/專案
    $("#delEvent").on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget);
        // Extract info from data-* attributes
        let titleText = "";
        let date = "";
        // 這個modal
        let modal = $(this);
        let rowdata = JSON.parse(button.siblings(".rowdata").val());

        modal.find("input[name='dataID']").val(rowdata["IDno"]);
        modal.find(".modal-body p").html("日期: " + rowdata["eventdate"] + "<br>項目: " + rowdata["C_name"]);
    });
}
window.onload = init;
</script>