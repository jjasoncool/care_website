<?php
/**
 * 進入評估量表個案
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $page 哪個類別
 * @param string $subPage 子類別
 * @param string $action 路由到這個頁面的參數
 */

$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
$start = strval(reqParam('start', 'get'));
$end = strval(reqParam('end', 'get'));
$date = new DateTime();
if (empty($end)) {
    $end = $date->format('Y-m-d');
}
if (empty($start)) {
    $date->sub(new DateInterval('P1M'));
    $start = $date->format('Y-m-d');
}

$dbQuery = "SELECT * FROM FCF_careservice.notification WHERE noticeACC=? AND DATE(noticeTime) BETWEEN ? AND ?";
$result = $db->query($dbQuery, [$generalData['userid'], $start, $end]);
$tableContent = '';
foreach ($result as $row) {
    if (empty($row['checktime'])) {
        $read = '未讀取';
    } else {
        $read = '已讀取';
    }
    $tableContent .= "<tr><td>{$row['noticeTime']}</td><td>{$read}</td><td><a href=\"{$row['noticeLink']}\">{$row['noticeMsg']}</a></td></tr>";
}

?>
<div class="main-panel">
    <div class="content">
        <div class="page-inner">
            <div class="page-header">
                <h4 class="page-title">通知清單</h4>
                <ul class="breadcrumbs">
                    <li class="nav-home">
                        <a href="<?=serverURL?>">
                            <i class="flaticon-home"></i>
                        </a>
                    </li>
                    <li class="separator">
                        <i class="flaticon-right-arrow"></i>
                    </li>
                    <li class="nav-item">
                        <a href="#">通知清單</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <form method="get">
                                <input type="hidden" name="page" value="notice">
                                <div class="row">
                                    <div class="col-md-4">
                                        <h4 class="card-title">通知清單</h4>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group date" id="start" data-target-input="nearest">
                                            <input type="text" class="form-control form-control-sm datetimepicker-input" maxlength="10" name="start" value="<?=$start?>" data-target="#start" required>
                                            <div class="input-group-append" data-target="#start" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="input-group date" id="end" data-target-input="nearest">
                                            <input type="text" class="form-control form-control-sm datetimepicker-input" maxlength="10" name="end" value="<?=$end?>" data-target="#end" required>
                                            <div class="input-group-append" data-target="#end" data-toggle="datetimepicker">
                                                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="submit" title="搜尋" class="btn btn-outline-primary btn-round btn-sm float-md-left mx-2">
                                            <i class="fas fa-search fa-lg"></i> 搜尋
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-12">
                                    <div class="table-responsive">
                                        <table id="memberTable" class="display table table-striped table-hover" >
                                            <thead>
                                                <tr>
                                                    <th width="15%">通知時間</th>
                                                    <th width="10%">狀態</th>
                                                    <th>通知訊息</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?=$tableContent?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
function init() {
    $("#start, #end").datetimepicker({
        format: 'YYYY-MM-DD',
        allowInputToggle: true,
        useStrict: true,
        debug: false
    });
}

window.onload = init;
</script>