<?php
/**
 * 新增人員名單 typeahead
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 * @param string $query 搜尋的字串
 */
$id = intval(reqParam('id', 'post'));

$dbQuery = "UPDATE FCF_careservice.notification SET checktime=NOW() WHERE IDno=?";
$result = $db->query($dbQuery, [$id]);

//set Content-Type to JSON
header('Content-Type: application/json; charset=utf-8');
// 清除先前頁面要顯示的快取
ob_end_clean();

echo 'success';