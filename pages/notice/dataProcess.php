<?php
/**
 * 資料處理層，將php分配到不同頁面處理
 * @author skjan <forwork.jan@gmail.com>
 * @version 1.0.0
 */
require_once 'vendor/autoload.php';
require 'settings/loginCheck.php';
$db = new MysqlDB(DB_HOST, DB_PORT, 'FCF_careservice', DB_USER, DB_PASS);
$pageAction = reqParam('page', 'post', 'string');

switch ($pageAction) {
    case 'read':
        // 類別 新增,修改,刪除
        require_once get_relative_path("pages/{$page}/read_notice.php");
        break;

    default:
        break;
}

exit();