-- ===================================== --
--           個案管理編輯
-- ===================================== --
-- 資料庫：FCF_careservice
-- 關聯物件:Memberdata,Rec_cancer
-- 頁面程式:pages\person\index.php
USE FCF_careservice;

-- 自動生成IDNO
ALTER TABLE FCF_careservice.`Accuser` CHANGE COLUMN `IDno` `IDno` INT NOT NULL AUTO_INCREMENT FIRST;
ALTER TABLE FCF_careservice.`Rec_cancer` CHANGE COLUMN `IDno` `IDno` INT(11) NOT NULL AUTO_INCREMENT FIRST;
ALTER TABLE FCF_careservice.`Memberdata` CHANGE COLUMN `IDno` `IDno` INT(11) NOT NULL AUTO_INCREMENT FIRST;

-- 無用欄位刪除，並修改欄位名稱
ALTER TABLE FCF_careservice.Accuser
    DROP COLUMN FFFF,
	CHANGE COLUMN `Acc_status` `Acc_status` INT NOT NULL DEFAULT 1 COMMENT '1: 啟用 0:停用',
    MODIFY COLUMN `Acc_level` INT COMMENT '1:管理者 2:一般使用者 3:志工 4:保險顧問 5:個案',
    MODIFY COLUMN `Acc_area` VARCHAR(20) COMMENT 'Taipei / Kaohsiung',
    ADD COLUMN `Acc_judge` BOOLEAN DEFAULT '0' COMMENT '評估量表窗口 0:否 1:是' AFTER `Acc_status`,
    ADD UNIQUE INDEX `Acc_mail` (`Acc_mail`);

-- 個人大頭貼
UPDATE FCF_careservice.Accuser a SET a.acc_stmapimg='';

-- SELECT * FROM FCF_careservice.Memberdata;
ALTER TABLE FCF_careservice.Memberdata
	CHANGE COLUMN `keydate` `Keydate` DATETIME NULL DEFAULT NULL AFTER `IDno`,
    CHANGE COLUMN `C_opendate` `C_opendate` DATE NULL DEFAULT NULL AFTER `AdName`,
    CHANGE COLUMN `C_closedate` `C_closedate` DATE NULL DEFAULT NULL AFTER `C_opendate`;

-- C_status 看起來沒在用 之後拿來用當作"往生關檔" -1:往生 1:健在
-- 目前有沒有結案是看 C_member = 1:個案 0:一般民眾 -1:結案
-- 家屬欄位 C_family 1:家屬 0:非家屬
ALTER TABLE FCF_careservice.Memberdata
    CHANGE COLUMN `C_status` `C_status` INT(11) NULL DEFAULT 1 COMMENT '往生關檔 -1:往生 1:健在' AFTER `C_closedate`,
    CHANGE COLUMN `C_member` `C_member` INT(11) NULL DEFAULT 0 COMMENT '案件類型 0:一般民眾 1:個案 2:家屬 3:志工基本資料(不歸類在個案內)' AFTER `C_status`,
    CHANGE COLUMN `C_family` `C_family` INT(11) NULL DEFAULT 0 COMMENT '1:家屬 0:非家屬' AFTER `C_member`;

-- 新增關檔欄位
ALTER TABLE FCF_careservice.Memberdata ADD COLUMN caseClosed BOOLEAN DEFAULT 0 COMMENT '是否關檔 1:關檔 0:未關檔' AFTER `C_member`;
-- 更新 C_member 為所有資料類型
UPDATE FCF_careservice.Memberdata SET caseClosed=1, C_member=1 WHERE C_member=-1;
UPDATE FCF_careservice.Memberdata SET C_member=2 WHERE C_family=1;
-- 刪除不必要欄位
ALTER TABLE FCF_careservice.Memberdata DROP COLUMN C_care, DROP COLUMN C_group, DROP COLUMN MemberID, DROP COLUMN C_family;

-- SELECT * FROM FCF_careservice.Memberdata WHERE (Close_reason LIKE '%往生%' OR C_note LIKE '%往生%') AND C_member<=0;
-- SELECT * FROM FCF_careservice.Memberdata WHERE (Close_reason LIKE '%往生%' OR C_note LIKE '%個案已往生%') AND C_member>0;
UPDATE FCF_careservice.Memberdata SET C_status=-1 WHERE (Close_reason LIKE '%往生%' OR C_note LIKE '%往生%') AND C_member<=0;
UPDATE FCF_careservice.Memberdata SET C_status=-1 WHERE (Close_reason LIKE '%往生%' OR C_note LIKE '%個案已往生%') AND C_member>0;

-- 刪除原有家屬欄位，將個案狀態分為三類(2:家屬 1:個案 0:一般民眾)

ALTER TABLE FCF_careservice.Memberdata CHANGE COLUMN `C_file` `C_file` VARCHAR(50) NULL DEFAULT NULL COMMENT '家系圖位址' AFTER `BD_dd`;
ALTER TABLE FCF_careservice.Memberdata CHANGE COLUMN `FCFvolunte` `FCFvolunte` BOOLEAN NOT NULL DEFAULT 0 COMMENT '是否為志工 1:志工 0:非志工' AFTER `C_member`;
UPDATE FCF_careservice.Memberdata SET C_idno=NULL WHERE C_idno='';
ALTER TABLE FCF_careservice.Memberdata CHANGE COLUMN `C_idno` `C_idno` VARCHAR(10) NULL UNIQUE DEFAULT NULL COMMENT '身分證字號';
ALTER TABLE FCF_careservice.Memberdata CHANGE COLUMN `IDcode` `handSignature` LONGTEXT DEFAULT '' COMMENT '簽名';
UPDATE FCF_careservice.Memberdata SET handSignature='' WHERE handSignature!='';

-- 建立索引(不然找太慢)
CREATE INDEX memberIndex ON FCF_careservice.Rec_cancer(MemberID);

-- 職業 -> 更新新職業
UPDATE FCF_careservice.`Memberdata`
SET C_job=CASE C_job
WHEN '製造業' THEN 9
WHEN '大眾傳播廣告業' THEN 10
WHEN '中小學生' THEN 13
WHEN '中小學老師' THEN 13
WHEN '大專學生' THEN 13
WHEN '大專教師' THEN 13
WHEN '金融、證券與保險業' THEN 17
WHEN '休閒旅遊服務業' THEN 17
WHEN '其他服務業' THEN 17
WHEN '其他' THEN 18
WHEN '家庭主婦' THEN 18
WHEN '軍人' THEN 20
WHEN '資訊電腦業' THEN 21
WHEN '公務人員' THEN 23
WHEN '待業中' THEN 24
WHEN '退休' THEN 25
ELSE 0
END;

-- 修改欄位型別與註解
ALTER TABLE FCF_careservice.`Memberdata`
CHANGE COLUMN `C_job` `C_job` int DEFAULT '0'
COMMENT '1:一般職業(機關團體/公司行號) 2:農牧業 3:漁業 4:木材森林業 5:礦業採石業
6:交通運輸業 7:餐旅業 8:建築工程業 9:製造業 10:新聞廣告業
11:保健業(醫院人員/保健人員) 12:娛樂業 13:文教機關(教職員/學生) 14:宗教團體 15:公共事業
16:一般商業 17:服務業(銀行/保險/信託/租賃/自由業/殯葬業/其他) 18:家庭管理(家管/佣人/保母) 19:治安人員 20:軍人
21:資訊業 22:職業運動人員 23:公務人員 24:待業中 25:退休'
AFTER `C_idno`;

-- 教育程度欄位整理
UPDATE FCF_careservice.`Memberdata`
SET C_edu=CASE C_edu
WHEN '不識字' THEN '未受教育'
WHEN '高中職/專科' THEN '高中/高職'
WHEN '大學/研究所' THEN '大學'
WHEN '研究所' THEN '研究所(或以上)'
ELSE C_edu
END;

-- 婚姻子女
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN haveChilds INT DEFAULT 0 AFTER C_marriage;

-- 福利身分
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN id_normal INT DEFAULT 0 AFTER C_note;
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN id_indigenous INT DEFAULT 0 AFTER id_handicapped;

-- 保險情況
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN Ins_none INT DEFAULT 0 AFTER id_specstatus;
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN Ins_others VARCHAR(30) DEFAULT '' AFTER Ins_social;

-- 新增日常生活功能
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN life_ability INT DEFAULT 0 COMMENT '0:--- 1:正常 2:需要他人幫忙 3:需要輔助用具 4:完全無法自行活動' AFTER Ins_others;
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN residence INT DEFAULT 0 COMMENT '居住狀況 0:--- 1:與家人同住 2:獨居 3:醫療機構 4:安置機構 5:無固定住所 6:服刑中 7:其他' AFTER life_ability;
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN cohabitation VARCHAR(20) DEFAULT '' COMMENT '同住家人' AFTER residence;
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN houseown INT DEFAULT 0 COMMENT '現在住所 0:--- 1:自宅無貸款 2:自宅有貸款 3:租屋 4:借住 5:其他' AFTER cohabitation;
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN payMonth INT DEFAULT 0 COMMENT '居住月支出(貸款/租金)' AFTER houseown;
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN caregiver INT DEFAULT 0 COMMENT '主要照顧者 0:--- 1:父母 2:配偶 3:子女 4:同居人 5:朋友 6:其他' AFTER payMonth;
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN ecoStatus VARCHAR(30) DEFAULT '' COMMENT '經濟狀況 0:自己有工作 1:政府補助 2:父母扶養 3:子女提供 4:親友提供 5:其他' AFTER caregiver;
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN monthIncome INT DEFAULT 0 COMMENT '月收入' AFTER ecoStatus;
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN chronic VARCHAR(30) DEFAULT '' COMMENT '慢性疾病史 0:無 1:糖尿病 2:高血壓 3:心臟病 4:中風 5:COPD 6:CRF 7:其他' AFTER monthIncome;
ALTER TABLE FCF_careservice.`Memberdata` ADD COLUMN otherChronic VARCHAR(30) DEFAULT '' COMMENT '其他慢性疾病史' AFTER chronic;

-- 是否接收會訊(三個月一本)
ALTER TABLE FCF_careservice.Memberdata CHANGE COLUMN `C_mag` `C_subscribe` INT NOT NULL DEFAULT 0 COMMENT '訂閱會訊(三個月一次) 0:無訂閱 1:紙本 2:email';
UPDATE FCF_careservice.Memberdata
SET C_subscribe=CASE
WHEN LENGTH(C_mail)>0 AND C_subscribe=1 THEN 2
ELSE C_subscribe END;
-- 應該要沒資料
-- SELECT * FROM FCF_careservice.Memberdata WHERE LENGTH(C_mail)>0 AND C_subscribe=1;

-- 新增個資保密條款、最新消息
ALTER TABLE FCF_careservice.Memberdata ADD COLUMN `getSMS` BOOLEAN NOT NULL DEFAULT FALSE COMMENT '接收最新活動消息 1:是 0:否' AFTER `C_mobile2`;
ALTER TABLE FCF_careservice.Memberdata ADD COLUMN `privacy` BOOLEAN NOT NULL DEFAULT FALSE COMMENT '個資保密條款，基本上都要同意 1:同意 0:不同意' AFTER `otherChronic`;
UPDATE FCF_careservice.Memberdata SET privacy=1;
ALTER TABLE FCF_careservice.Memberdata ADD COLUMN `assessForm` BOOLEAN NOT NULL DEFAULT FALSE COMMENT '評估量表 1:已填寫過 0:未填寫' AFTER `privacy`;

-- 疾病史 (MemberID 關聯 Memberdata ID)
-- SELECT * FROM FCF_careservice.Rec_cancer;
-- SELECT * FROM FCF_careservice.`Memberdata` WHERE IDno=16113;
ALTER TABLE FCF_careservice.Rec_cancer ADD COLUMN `status` INT NOT NULL DEFAULT 1 COMMENT '資料狀態 0:已刪除 1:有效資料' AFTER AdName;
-- 疾病史須新增欄位
-- 診斷時間 就診醫院 基因檢測 手術切除(Y/N) 復發時間 免疫治療期間 免疫藥名 放射次數 放射部位 安寧療護 其他治療
ALTER TABLE FCF_careservice.Rec_cancer MODIFY COLUMN Find_date DATE NULL DEFAULT NULL COMMENT '診斷日期';
ALTER TABLE FCF_careservice.Rec_cancer MODIFY COLUMN Cancer_type VARCHAR(10) NULL DEFAULT 'New' COMMENT '癌症復發';
ALTER TABLE FCF_careservice.Rec_cancer ADD COLUMN recurrence DATE NULL DEFAULT NULL COMMENT '復發時間' AFTER Cancer_type;
ALTER TABLE FCF_careservice.Rec_cancer ADD COLUMN hospital VARCHAR(20) DEFAULT '' COMMENT '就診醫院' AFTER Find_date;
ALTER TABLE FCF_careservice.Rec_cancer ADD COLUMN genetic_test VARCHAR(50) DEFAULT '' COMMENT '基因檢測' AFTER hospital;
-- 手術切除(舊資料只要有做過手術，皆判定為有)
UPDATE FCF_careservice.Rec_cancer SET Surgery=IF(Surgery>0, 1, 0);
UPDATE FCF_careservice.Rec_cancer SET R_note=CONCAT(R_note, ' 手術時間:', Surgery_date) WHERE Surgery_date!='' AND Surgery_date IS NOT NULL;
UPDATE FCF_careservice.Rec_cancer SET R_note=CONCAT(R_note, ' 化療期間:', Chemical_date) WHERE Chemical_date!='' AND Chemical_date IS NOT NULL;
UPDATE FCF_careservice.Rec_cancer SET R_note=CONCAT(R_note, ' 標靶治療期間:', Target_date) WHERE Target_date!='' AND Target_date IS NOT NULL;
UPDATE FCF_careservice.Rec_cancer SET R_note=CONCAT(R_note, ' 放射治療期間:', Radiat_date) WHERE Radiat_date!='' AND Radiat_date IS NOT NULL;
ALTER TABLE FCF_careservice.Rec_cancer MODIFY COLUMN Surgery BOOLEAN DEFAULT FALSE COMMENT '手術切除(是/否)';
ALTER TABLE FCF_careservice.Rec_cancer ADD COLUMN Immune BOOLEAN DEFAULT 0 COMMENT '免疫治療' AFTER Target_name;
ALTER TABLE FCF_careservice.Rec_cancer ADD COLUMN Immune_date VARCHAR(50) DEFAULT '' COMMENT '免疫治療期間' AFTER Immune;
ALTER TABLE FCF_careservice.Rec_cancer ADD COLUMN Immune_medicine VARCHAR(50) DEFAULT '' COMMENT '免疫治療使用藥名' AFTER Immune_date;
ALTER TABLE FCF_careservice.Rec_cancer CHANGE COLUMN `Radiation` `Rad_time` INT NULL DEFAULT 0 COMMENT '放射治療次數' AFTER `Radiat_date`;
ALTER TABLE FCF_careservice.Rec_cancer ADD COLUMN Radiation BOOLEAN NULL DEFAULT 0 COMMENT '放射治療' AFTER Immune_medicine;

-- 標靶/化療目前狀況更新到現在治療方式
-- SELECT Chemical_name, Now_eatname FROM FCF_careservice.Rec_cancer WHERE Chemical_name != '' AND Now_eatname != '';
UPDATE FCF_careservice.Rec_cancer SET Chemical=IF(Now_chemical>0, 1, 0), Chemical_name=TRIM(CONCAT(Chemical_name, ' ', Now_eatname)), Target=IF(Now_target>0, 1, 0);

-- 有做過放射治療就判定有
UPDATE FCF_careservice.Rec_cancer SET Radiation=IF(Rad_time>0 OR Now_radio>0, 1, 0);
ALTER TABLE FCF_careservice.Rec_cancer ADD COLUMN Rad_part VARCHAR(50) DEFAULT '' COMMENT '放射治療部位' AFTER Rad_time;
ALTER TABLE FCF_careservice.Rec_cancer CHANGE COLUMN `Now_hormone` `hormone` BOOLEAN NOT NULL DEFAULT 0 COMMENT '賀爾蒙療法' AFTER Rad_part;
ALTER TABLE FCF_careservice.Rec_cancer ADD COLUMN hormone_date VARCHAR(50) DEFAULT '' COMMENT '賀爾蒙療法期間' AFTER hormone;
ALTER TABLE FCF_careservice.Rec_cancer CHANGE COLUMN `Now_horname` `hormone_medicine` VARCHAR(50) DEFAULT '' COMMENT '賀爾蒙療法使用藥名' AFTER hormone_date;
ALTER TABLE FCF_careservice.Rec_cancer CHANGE COLUMN `Now_mix` `integrated` BOOLEAN NOT NULL DEFAULT 0 COMMENT '中西醫整合療法' AFTER hormone_medicine;
ALTER TABLE FCF_careservice.Rec_cancer CHANGE COLUMN `Now_track` `tracking` BOOLEAN NOT NULL DEFAULT 0 COMMENT '定期門診追蹤' AFTER integrated;
ALTER TABLE FCF_careservice.Rec_cancer CHANGE COLUMN `Now_slow` `peace` BOOLEAN NOT NULL DEFAULT 0 COMMENT '安寧療護' AFTER tracking;
ALTER TABLE FCF_careservice.Rec_cancer ADD COLUMN other BOOLEAN DEFAULT 0 COMMENT '其他治療' AFTER peace;
ALTER TABLE FCF_careservice.Rec_cancer
DROP COLUMN Cancer_area, DROP COLUMN Now_chemical, DROP COLUMN Now_target,
DROP COLUMN Now_radio, DROP COLUMN Now_eat, DROP COLUMN Now_eatname;

-- 家屬疾病史資料移至個案基本資料備註
UPDATE FCF_careservice.Memberdata m
INNER JOIN FCF_careservice.Rec_cancer c ON m.IDno=c.MemberID
SET m.C_note=CONCAT_WS(CHAR(10 using UTF8), m.C_note, c.R_note)
WHERE m.C_member=2;

-- 一般民眾若有登記疾病史，轉為個案
UPDATE FCF_careservice.Memberdata m
INNER JOIN FCF_careservice.Rec_cancer c ON m.IDno=c.MemberID
SET m.C_member=1
WHERE m.C_member=0 AND m.C_status=1;

-- ===================================== --
--           個案諮詢紀錄
-- ===================================== --
-- 資料庫：FCF_careservice
-- 關聯物件:Rec_medical,Rec_nutrition,Rec_social,Rec_other,Eventdata
-- 頁面程式:pages\person\view.php

-- IDNO auto_increment
ALTER TABLE FCF_careservice.Rec_medical CHANGE COLUMN `IDno` `IDno` INT NOT NULL AUTO_INCREMENT FIRST; -- 醫護紀錄
ALTER TABLE FCF_careservice.Rec_nutrition CHANGE COLUMN `IDno` `IDno` INT NOT NULL AUTO_INCREMENT FIRST; -- 營養紀錄
ALTER TABLE FCF_careservice.Rec_social CHANGE COLUMN `IDno` `IDno` INT NOT NULL AUTO_INCREMENT FIRST; -- 社工紀錄
DROP TABLE IF EXISTS FCF_careservice.`Rec_care`;
RENAME TABLE FCF_careservice.Rec_other TO FCF_careservice.Rec_care;
ALTER TABLE FCF_careservice.Rec_care CHANGE COLUMN `IDno` `IDno` INT NOT NULL AUTO_INCREMENT FIRST; -- 志工關懷

-- 欄位定義變更
ALTER TABLE FCF_careservice.Rec_nutrition CHANGE COLUMN `R_date` `R_date` DATE NULL DEFAULT NULL;
ALTER TABLE FCF_careservice.Rec_medical CHANGE COLUMN `R_date` `R_date` DATE NULL DEFAULT NULL;
ALTER TABLE FCF_careservice.Rec_social CHANGE COLUMN `R_date` `R_date` DATE NULL DEFAULT NULL;
ALTER TABLE FCF_careservice.Rec_care CHANGE COLUMN `R_date` `R_date` DATE NULL DEFAULT NULL;

-- 資料表重新命名
DROP TABLE IF EXISTS FCF_careservice.`EventCategory`;
RENAME TABLE FCF_careservice.Eventlist TO FCF_careservice.EventCategory;
ALTER TABLE FCF_careservice.EventCategory CHANGE COLUMN `IDno` `IDno` INT NOT NULL AUTO_INCREMENT FIRST;
UPDATE FCF_careservice.EventCategory SET cityLocation='' WHERE cityLocation IS NULL;
ALTER TABLE FCF_careservice.EventCategory MODIFY COLUMN `cityLocation` VARCHAR(50) NOT NULL DEFAULT '';
ALTER TABLE FCF_careservice.EventCategory ADD COLUMN `Del` BOOLEAN NOT NULL DEFAULT 0 COMMENT '1:代表被刪除了';
DROP TABLE IF EXISTS FCF_careservice.`EventArchives`;
RENAME TABLE FCF_careservice.`Eventfiles` TO FCF_careservice.`EventArchives`;
-- 每個活動都有一個類別對應ID
ALTER TABLE FCF_careservice.Eventdata CHANGE COLUMN `C_ID` `categoryID` INT NOT NULL DEFAULT 0;

-- 專案
DROP TABLE IF EXISTS FCF_careservice.`Projects`;
RENAME TABLE FCF_careservice.Eventdata TO FCF_careservice.Projects;
UPDATE FCF_careservice.Projects SET `Oldid`=1;
ALTER TABLE FCF_careservice.Projects
    CHANGE COLUMN `IDno` `IDno` INT NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `keydate` `Keydate` DATETIME NULL DEFAULT NULL AFTER `IDno`,
    CHANGE COLUMN `C_startday` `C_startday` DATE DEFAULT NULL,
    CHANGE COLUMN `C_endday` `C_endday` DATE DEFAULT NULL,
    CHANGE COLUMN `Oldid` `status` BOOLEAN DEFAULT 1 COMMENT '0:刪除 1:有效',
    MODIFY COLUMN `C_No` VARCHAR(20) DEFAULT '';

-- 身心靈課程
DROP TABLE IF EXISTS FCF_careservice.`MindBodyCourse`;
CREATE TABLE FCF_careservice.`MindBodyCourse` AS SELECT * FROM FCF_careservice.Projects;
DELETE FROM FCF_careservice.MindBodyCourse WHERE C_kind!='class';
ALTER TABLE FCF_careservice.MindBodyCourse CHANGE COLUMN `IDno` `IDno` INT NOT NULL PRIMARY KEY AUTO_INCREMENT FIRST;

-- 志工訓練課程
DROP TABLE IF EXISTS FCF_careservice.`VolunteerTrain`;
CREATE TABLE FCF_careservice.`VolunteerTrain` AS SELECT * FROM FCF_careservice.Projects;
DELETE FROM FCF_careservice.VolunteerTrain WHERE C_kind!='train';
ALTER TABLE FCF_careservice.VolunteerTrain
    CHANGE COLUMN `IDno` `IDno` INT NOT NULL PRIMARY KEY AUTO_INCREMENT FIRST;

-- 志工服務時數
DROP TABLE IF EXISTS FCF_careservice.`VolunteerHours`;
RENAME TABLE FCF_careservice.Rec_Voltime TO FCF_careservice.VolunteerHours;
ALTER TABLE FCF_careservice.VolunteerHours COMMENT='志工服務時數';
ALTER TABLE FCF_careservice.`VolunteerHours`
    CHANGE COLUMN `IDno` `IDno` INT NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `MemberID` `VolunteerID` INT NULL DEFAULT NULL AFTER `Adname`,
    MODIFY COLUMN R_date DATE NULL DEFAULT NULL,
    ADD INDEX `VolunteerID` (`VolunteerID`);

-- 更新關聯ID為志工ID
UPDATE FCF_careservice.VolunteerHours h
LEFT JOIN FCF_careservice.Volunteer v ON h.VolunteerID=v.MemberID
SET h.VolunteerID=v.IDno;
-- 刪除無關聯的ID
DELETE h FROM FCF_careservice.VolunteerHours h
LEFT JOIN FCF_careservice.Volunteer v ON h.VolunteerID=v.IDno
WHERE v.IDno IS NULL;

-- 專案
DELETE FROM FCF_careservice.Projects WHERE C_kind!='event';

-- 社心紀錄權利欄位移到醫護紀錄
-- SELECT * FROM FCF_careservice.Rec_social;
-- SELECT * FROM FCF_careservice.Rec_social WHERE `Right`=1;
ALTER TABLE FCF_careservice.Rec_medical ADD COLUMN Rights BOOLEAN NOT NULL DEFAULT 0 COMMENT '權利倡議' AFTER Nur_mood;
ALTER TABLE FCF_careservice.Rec_medical ADD COLUMN Right_drugpay BOOLEAN NOT NULL DEFAULT 0 COMMENT '健保藥品給付' AFTER Rights;
ALTER TABLE FCF_careservice.Rec_medical ADD COLUMN Right_newdrug BOOLEAN NOT NULL DEFAULT 0 COMMENT '新藥審查及使用條件' AFTER Right_drugpay;
ALTER TABLE FCF_careservice.Rec_medical ADD COLUMN Right_buydrug BOOLEAN NOT NULL DEFAULT 0 COMMENT '自購藥物管道' AFTER Right_newdrug;
ALTER TABLE FCF_careservice.Rec_medical ADD COLUMN Right_clinical BOOLEAN NOT NULL DEFAULT 0 COMMENT '臨床試驗' AFTER Right_buydrug;
ALTER TABLE FCF_careservice.Rec_medical ADD COLUMN Right_doctor BOOLEAN NOT NULL DEFAULT 0 COMMENT '醫病關係' AFTER Right_clinical;
ALTER TABLE FCF_careservice.Rec_medical ADD COLUMN Right_quality BOOLEAN NOT NULL DEFAULT 0 COMMENT '醫療品質' AFTER Right_doctor;
ALTER TABLE FCF_careservice.Rec_medical ADD COLUMN Right_other BOOLEAN NOT NULL DEFAULT 0 COMMENT '其他議題' AFTER Right_quality;
ALTER TABLE FCF_careservice.Rec_medical ADD COLUMN Right_othertxt VARCHAR(50) DEFAULT '' COMMENT '其他議題文字輸入' AFTER Right_other;
ALTER TABLE FCF_careservice.Rec_medical ADD COLUMN filePath VARCHAR(100) DEFAULT '' COMMENT '上傳檔案名稱與路徑' AFTER R_note;
INSERT INTO FCF_careservice.Rec_medical (
    Keydate, MemberID, AdminID, AdName, R_date, R_time, R_loc, R_way,
    Rights, Right_drugpay, Right_newdrug, Right_buydrug, Right_clinical,
    Right_doctor, Right_quality, Right_other, Right_othertxt, AdmincheckID, Admincheck, checktime
)
SELECT s.Keydate, s.MemberID, s.AdminID, s.AdName, s.R_date, s.R_time, s.R_loc, s.R_way,
    s.`Right`, s.Right_drugpay, s.Right_newdrug, s.Right_buydrug, s.Right_clinical,
    s.Right_doctor, s.Right_quality, s.Right_other, s.Right_othertxt, s.AdmincheckID, s.Admincheck, s.checktime
FROM FCF_careservice.Rec_social s
WHERE s.`Right`=1;
ALTER TABLE FCF_careservice.Rec_social
    DROP COLUMN `Right_drugpay`,
    DROP COLUMN `Right_newdrug`,
    DROP COLUMN `Right_buydrug`,
    DROP COLUMN `Right_clinical`,
    DROP COLUMN `Right_doctor`,
    DROP COLUMN `Right_quality`,
    DROP COLUMN `Right_other`,
    DROP COLUMN `Right_othertxt`,
    DROP COLUMN `Right`;

-- 社心紀錄內的心理紀錄需創新的資料表
-- SELECT * FROM FCF_careservice.Rec_social WHERE `Mind`=1;
DROP TABLE IF EXISTS FCF_careservice.`Rec_mental`;
CREATE TABLE FCF_careservice.`Rec_mental` (
    `IDno` INT NOT NULL AUTO_INCREMENT,
    `Keydate` DATETIME NULL DEFAULT NULL,
    `MemberID` INT NULL DEFAULT NULL,
    `AdminID` INT NULL DEFAULT NULL,
    `AdName` VARCHAR(50) NULL DEFAULT NULL,
    `R_date` DATE NULL DEFAULT NULL,
    `R_time` INT NULL DEFAULT NULL,
    `R_loc` VARCHAR(20) NULL DEFAULT NULL,
    `R_way` VARCHAR(20) NULL DEFAULT NULL,
    `Mind` BOOLEAN NOT NULL DEFAULT 0 COMMENT '心理諮商',
    `Mind_feeling` BOOLEAN NOT NULL DEFAULT 0 COMMENT '情緒議題',
    `Mind_self` BOOLEAN NOT NULL DEFAULT 0 COMMENT '自我概念',
    `Mind_adapt` BOOLEAN NOT NULL DEFAULT 0 COMMENT '生活適應',
    `Mind_friends` BOOLEAN NOT NULL DEFAULT 0 COMMENT '人際關係',
    `Mind_family` BOOLEAN NOT NULL DEFAULT 0 COMMENT '家庭議題',
    `Mind_sickbed` BOOLEAN NOT NULL DEFAULT 0 COMMENT '疾病末期',
    `Mind_suicide` BOOLEAN NOT NULL DEFAULT 0 COMMENT '自殺意念',
    `Mind_selfcare` BOOLEAN NOT NULL DEFAULT 0 COMMENT '自我照顧',
    `Mind_lost` BOOLEAN NOT NULL DEFAULT 0 COMMENT '失落經驗',
    `Mind_other` BOOLEAN NOT NULL DEFAULT 0 COMMENT '其他',
    `Mind_othertxt` VARCHAR(50) NOT NULL DEFAULT '',
    `R_note` LONGTEXT NULL DEFAULT '' ,
    `filePath` VARCHAR(100) DEFAULT '' COMMENT '上傳檔案名稱與路徑',
    `AdmincheckID` INT NULL DEFAULT 0,
    `Admincheck` INT NULL DEFAULT 0,
    `checktime` DATETIME NULL DEFAULT NULL,
    PRIMARY KEY (`IDno`) USING BTREE,
    INDEX `memberIndex` (`MemberID`) USING BTREE
);

-- 社心紀錄將心理部分移到新的table
INSERT INTO FCF_careservice.Rec_mental (
    Keydate, MemberID, AdminID, AdName, R_date, R_time, R_loc, R_way,
    Mind, Mind_feeling, Mind_self, Mind_adapt, Mind_friends, Mind_family, Mind_sickbed, Mind_suicide, Mind_selfcare, Mind_lost,
    AdmincheckID, Admincheck, checktime
)
SELECT s.Keydate, s.MemberID, s.AdminID, s.AdName, s.R_date, s.R_time, s.R_loc, s.R_way,
    s.Mind, s.Mind_feeling, s.Mind_self, s.Mind_adapt, s.Mind_friends, s.Mind_family, s.Mind_sickbed, s.Mind_suicide, s.Mind_selfcare, s.Mind_lost,
    s.AdmincheckID, s.Admincheck, s.checktime
FROM FCF_careservice.Rec_social s
WHERE s.`Mind`=1;

ALTER TABLE FCF_careservice.Rec_social
    DROP COLUMN Mind,
    DROP COLUMN Mind_feeling,
    DROP COLUMN Mind_self,
    DROP COLUMN Mind_adapt,
    DROP COLUMN Mind_friends,
    DROP COLUMN Mind_family,
    DROP COLUMN Mind_sickbed,
    DROP COLUMN Mind_suicide,
    DROP COLUMN Mind_selfcare,
    DROP COLUMN Mind_lost;

-- 社工紀錄(社心移除心理) 欄位調整
ALTER TABLE FCF_careservice.Rec_social MODIFY COLUMN `R_note` LONGTEXT AFTER `Soc_volcare`;
ALTER TABLE FCF_careservice.Rec_social ADD COLUMN `Aim_wig` BOOLEAN NOT NULL DEFAULT 0 COMMENT '假髮租借' AFTER `R_way`;
ALTER TABLE FCF_careservice.Rec_social ADD COLUMN `Aim_welfare` BOOLEAN NOT NULL DEFAULT 0 COMMENT '福利諮詢(原就業/服務需求)' AFTER `Aim_wig`;
UPDATE FCF_careservice.Rec_social SET Aim_welfare=1 WHERE Soc_job=1 OR Soc_service=1;
ALTER TABLE FCF_careservice.Rec_social CHANGE COLUMN `Soc_goods` `Aim_goods` BOOLEAN NOT NULL DEFAULT 0 COMMENT '物資需求' AFTER `Aim_welfare`;
ALTER TABLE FCF_careservice.Rec_social CHANGE COLUMN `Soc_volcare` `Aim_volcare` BOOLEAN NOT NULL DEFAULT 0 COMMENT '志工關懷需求' AFTER `Aim_goods`;
ALTER TABLE FCF_careservice.Rec_social ADD COLUMN `Aim_other` BOOLEAN NOT NULL DEFAULT 0 COMMENT '其他' AFTER `Aim_volcare`;
ALTER TABLE FCF_careservice.Rec_social ADD COLUMN `Aim_othertxt` VARCHAR(20) DEFAULT '' AFTER `Aim_other`;
-- 補助評估
ALTER TABLE FCF_careservice.Rec_social ADD COLUMN `Grants` BOOLEAN NOT NULL DEFAULT 0 COMMENT '補助評估' AFTER `Aim_other`;
ALTER TABLE FCF_careservice.Rec_social CHANGE COLUMN `Soc_economy` `Grant_help` BOOLEAN NOT NULL DEFAULT 0 COMMENT '急難救助' AFTER `Grants`;
ALTER TABLE FCF_careservice.Rec_social ADD COLUMN `Grant_nutrition` BOOLEAN NOT NULL DEFAULT 0 COMMENT '營養品補助' AFTER `Grant_help`;
ALTER TABLE FCF_careservice.Rec_social ADD COLUMN `Grant_traffic` BOOLEAN NOT NULL DEFAULT 0 COMMENT '交通補助' AFTER `Grant_nutrition`;
ALTER TABLE FCF_careservice.Rec_social CHANGE COLUMN `Soc_care` `Grant_respitecare` BOOLEAN NOT NULL DEFAULT 0 COMMENT '喘息服務' AFTER `Grant_traffic`;
ALTER TABLE FCF_careservice.Rec_social ADD COLUMN `filePath` VARCHAR(100) DEFAULT '' COMMENT '上傳檔案名稱與路徑' AFTER `R_note`;
-- 刪除不要的欄位
ALTER TABLE FCF_careservice.Rec_social DROP COLUMN `Soc`, DROP COLUMN `Soc_job`, DROP COLUMN `Soc_service`;

-- 修改醫療紀錄
-- SELECT * FROM FCF_careservice.Rec_nutrition;
ALTER TABLE FCF_careservice.Rec_nutrition CHANGE COLUMN `Surgery` `afterSurgery` BOOLEAN NOT NULL DEFAULT 0 COMMENT '手術後飲食衛教';
-- 修改欄位位置、名稱以及型別
ALTER TABLE FCF_careservice.Rec_nutrition
	CHANGE COLUMN `R_height` `R_height` SMALLINT NULL DEFAULT 0 COMMENT '身高(cm)' AFTER `R_way`,
	CHANGE COLUMN `R_weight` `R_weight` SMALLINT NULL DEFAULT 0 COMMENT '體重(kg)' AFTER `R_height`,
	CHANGE COLUMN `R_BMI` `R_BMI` FLOAT(4,1) NULL DEFAULT 0 COMMENT 'BMI' AFTER `R_weight`,
	CHANGE COLUMN `R_analysis` `R_analysis` VARCHAR(20) NULL DEFAULT '' COMMENT '體態分析' AFTER `R_BMI`,
    CHANGE COLUMN `Cure` `Cure` BOOLEAN NOT NULL DEFAULT 0 COMMENT '治療期間飲食衛教' AFTER `afterSurgery`,
    CHANGE COLUMN `Cure_1` `Cure_calorie` BOOLEAN NOT NULL DEFAULT 0 COMMENT '熱量需求',
    CHANGE COLUMN `Cure_2` `Cure_protein` BOOLEAN NOT NULL DEFAULT 0 COMMENT '蛋白質需求與估算',
    CHANGE COLUMN `Effect` `Effect` BOOLEAN NOT NULL DEFAULT 0 COMMENT '生理狀況之飲食對策' AFTER `Cure_protein`,
    CHANGE COLUMN `Effect_1` `Effect_vomit` BOOLEAN NOT NULL DEFAULT 0 COMMENT '噁心嘔吐 ' AFTER `Effect`,
    CHANGE COLUMN `Effect_2` `Effect_oral` BOOLEAN NOT NULL DEFAULT 0 COMMENT '口腔破損' AFTER `Effect_vomit`,
    CHANGE COLUMN `Effect_3` `Effect_full` BOOLEAN NOT NULL DEFAULT 0 COMMENT '飽脹感' AFTER `Effect_oral`,
    CHANGE COLUMN `Effect_4` `Effect_flatulence` BOOLEAN NOT NULL DEFAULT 0 COMMENT '脹氣' AFTER `Effect_full`,
    CHANGE COLUMN `Effect_5` `Effect_diarrhea` BOOLEAN NOT NULL DEFAULT 0 COMMENT '腹瀉' AFTER `Effect_flatulence`,
    CHANGE COLUMN `Effect_6` `Effect_constipation` BOOLEAN NOT NULL DEFAULT 0 COMMENT '便秘' AFTER `Effect_diarrhea`,
    CHANGE COLUMN `Effect_7` `Effect_lowHeme` BOOLEAN NOT NULL DEFAULT 0 COMMENT '血色素不足' AFTER `Effect_constipation`,
    CHANGE COLUMN `Effect_8` `Effect_lowLeukocyte` BOOLEAN NOT NULL DEFAULT 0 COMMENT '白血球低下' AFTER `Effect_lowHeme`,
    CHANGE COLUMN `Effect_9` `Effect_appetiteLoss` BOOLEAN NOT NULL DEFAULT 0 COMMENT '食慾不振' AFTER `Effect_lowLeukocyte`,
    CHANGE COLUMN `Effect_10` `Effect_weightLoss` BOOLEAN NOT NULL DEFAULT 0 COMMENT '體重下降' AFTER `Effect_appetiteLoss`,
    CHANGE COLUMN `Effect_11` `Effect_gainWeight` BOOLEAN NOT NULL DEFAULT 0 COMMENT '體重增加' AFTER `Effect_weightLoss`,
    CHANGE COLUMN `Effect_other` `Effect_other` BOOLEAN NOT NULL DEFAULT 0 COMMENT '其他生理狀況' AFTER `Effect_gainWeight`,
    CHANGE COLUMN `Effect_othertxt` `Effect_othertxt` VARCHAR(50) NULL DEFAULT '' COMMENT '其他生理狀況文字說明' AFTER `Effect_other`,
    CHANGE COLUMN `Recover` `Diet` BOOLEAN NOT NULL DEFAULT 0 COMMENT '飲食原則' AFTER `Effect_othertxt`,
    CHANGE COLUMN `Re_food` `Diet_blance` BOOLEAN NOT NULL DEFAULT 0 COMMENT '均衡飲食' AFTER `Diet`,
    CHANGE COLUMN `Re_diabetes` `Diet_diabetes` BOOLEAN NOT NULL DEFAULT 0 COMMENT '糖尿病飲食' AFTER `Diet_blance`,
    CHANGE COLUMN `Re_kidney` `Diet_renalFailure` BOOLEAN NOT NULL DEFAULT 0 COMMENT '慢性腎衰竭飲食' AFTER `Diet_diabetes`,
    CHANGE COLUMN `Re_dialysis` `Diet_dialysis` BOOLEAN NOT NULL DEFAULT 0 COMMENT '透析飲食' AFTER `Diet_renalFailure`,
    CHANGE COLUMN `Re_liver` `Diet_liver` BOOLEAN NOT NULL DEFAULT 0 COMMENT '肝病飲食' AFTER `Diet_dialysis`,
    CHANGE COLUMN `Re_other` `Diet_other` BOOLEAN NOT NULL DEFAULT 0 COMMENT '其他飲食原則' AFTER `Diet_liver`,
    CHANGE COLUMN `Re_othertxt` `Diet_othertxt` VARCHAR(50) NULL DEFAULT '' COMMENT '其他飲食原則說明文字' AFTER `Diet_other`,
    DROP COLUMN `Re_fkeep`,
    DROP COLUMN `Re_fsafe`,
    DROP COLUMN `Re_weight`,
    DROP COLUMN `Re_579`,
    CHANGE COLUMN `Nutrition` `Nutrition` BOOLEAN NOT NULL DEFAULT 0 COMMENT '保健營養品相關資訊' AFTER `Diet_othertxt`,
    CHANGE COLUMN `Nu_1` `Nu_panax` BOOLEAN NOT NULL DEFAULT 0 COMMENT '蔘類' AFTER `Nutrition`,
    CHANGE COLUMN `Nu_2` `Nu_algae` BOOLEAN NOT NULL DEFAULT 0 COMMENT '藻類' AFTER `Nu_panax`,
    CHANGE COLUMN `Nu_3` `Nu_vitamins` BOOLEAN NOT NULL DEFAULT 0 COMMENT '維生素類' AFTER `Nu_algae`,
    CHANGE COLUMN `Nu_4` `Nu_calcium` BOOLEAN NOT NULL DEFAULT 0 COMMENT '鈣片' AFTER `Nu_vitamins`,
    CHANGE COLUMN `Nu_5` `Nu_mushroom` BOOLEAN NOT NULL DEFAULT 0 COMMENT '菇菌類保健品' AFTER `Nu_calcium`,
    CHANGE COLUMN `Nu_6` `Nu_phytochemicals` BOOLEAN NOT NULL DEFAULT 0 COMMENT '植化素萃取物' AFTER `Nu_mushroom`,
    DROP COLUMN `Nu_7`,
    CHANGE COLUMN `Nu_other` `Nu_other` BOOLEAN NULL DEFAULT '0' COMMENT '其他保健營養品' AFTER `Nu_phytochemicals`,
    CHANGE COLUMN `Nu_othertxt` `Nu_othertxt` VARCHAR(50) NULL DEFAULT '' COMMENT '保健營養品其他文字說明' AFTER `Nu_other`,
    CHANGE COLUMN `Makefood` `Eat` BOOLEAN NOT NULL DEFAULT 0 COMMENT '飲食製備方式' AFTER `Nu_othertxt`,
    CHANGE COLUMN `Mk_1` `Eat_normal` BOOLEAN NOT NULL DEFAULT 0 COMMENT '普通飲食' AFTER `Eat`,
    CHANGE COLUMN `Mk_2` `Eat_liquid` BOOLEAN NOT NULL DEFAULT 0 COMMENT '流質飲食' AFTER `Eat_normal`,
    CHANGE COLUMN `Mk_3` `Eat_bland` BOOLEAN NOT NULL DEFAULT 0 COMMENT '溫和飲食' AFTER `Eat_liquid`,
    CHANGE COLUMN `Mk_4` `Eat_lowResidue` BOOLEAN NOT NULL DEFAULT 0 COMMENT '低渣飲食' AFTER `Eat_bland`,
    CHANGE COLUMN `Mk_5` `Eat_highFiber` BOOLEAN NOT NULL DEFAULT 0 COMMENT '高纖飲食' AFTER `Eat_lowResidue`,
    CHANGE COLUMN `Mk_6` `Eat_tube` BOOLEAN NOT NULL DEFAULT 0 COMMENT '管灌飲食' AFTER `Eat_highFiber`,
    CHANGE COLUMN `Mk_7` `Eat_elemental` BOOLEAN NOT NULL DEFAULT 0 COMMENT '元素飲' AFTER `Eat_tube`,
    CHANGE COLUMN `MK_other` `Eat_other` BOOLEAN NOT NULL DEFAULT 0 COMMENT '其他飲食方法' AFTER `Eat_elemental`,
    CHANGE COLUMN `MK_othertxt` `Eat_othertxt` VARCHAR(50) NULL DEFAULT '' COMMENT '其他飲食方法文字說明' AFTER `Eat_other`,
    CHANGE COLUMN `Specfood` `Special` BOOLEAN NOT NULL DEFAULT 0 COMMENT '特殊營養品需求' AFTER `Eat_othertxt`,
    CHANGE COLUMN `Spec_1` `Sp_glutamine` BOOLEAN NOT NULL DEFAULT 0 COMMENT '麩醯胺酸' AFTER `Special`,
    CHANGE COLUMN `Spec_2` `Sp_balanced` BOOLEAN NOT NULL DEFAULT 0 COMMENT '均衡配方' AFTER `Sp_glutamine`,
    CHANGE COLUMN `Spec_3` `Sp_tumor` BOOLEAN NOT NULL DEFAULT 0 COMMENT '腫瘤配方' AFTER `Sp_balanced`,
    CHANGE COLUMN `Spec_4` `Sp_pneumonia` BOOLEAN NOT NULL DEFAULT 0 COMMENT '肺病配方' AFTER `Sp_tumor`,
    CHANGE COLUMN `Spec_5` `Sp_diabetes` BOOLEAN NOT NULL DEFAULT 0 COMMENT '糖尿病配方' AFTER `Sp_pneumonia`,
    CHANGE COLUMN `Spec_6` `Sp_nephropathy` BOOLEAN NOT NULL DEFAULT 0 COMMENT '腎病配方' AFTER `Sp_diabetes`,
    CHANGE COLUMN `Spec_other` `Sp_other` BOOLEAN NOT NULL DEFAULT 0 COMMENT '其他特殊營養品' AFTER `Sp_nephropathy`,
    CHANGE COLUMN `Spec_othertxt` `Sp_othertxt` VARCHAR(50) NULL DEFAULT '' COMMENT '其他特殊營養品文字說明' AFTER `Sp_other`;

-- 新增欄位
ALTER TABLE FCF_careservice.Rec_nutrition
    ADD COLUMN `general_weight` SMALLINT NULL DEFAULT 0 COMMENT '平常體重' AFTER `R_weight`,
    ADD COLUMN `Aim` BOOLEAN NOT NULL DEFAULT 0 COMMENT '諮詢目的' AFTER `R_analysis`,
    ADD COLUMN `Aim_therapeutic` BOOLEAN NOT NULL DEFAULT 0 COMMENT '治療飲食' AFTER `Aim`,
    ADD COLUMN `Aim_nutrition` BOOLEAN NOT NULL DEFAULT 0 COMMENT '營養品補助' AFTER `Aim_therapeutic`,
    ADD COLUMN `Aim_balance` BOOLEAN NOT NULL DEFAULT 0 COMMENT '均衡營養' AFTER `Aim_nutrition`,
    ADD COLUMN `Aim_countermeasures` BOOLEAN NOT NULL DEFAULT 0 COMMENT '生理狀況之飲食對策' AFTER `Aim_balance`,
    ADD COLUMN `Aim_other` BOOLEAN NOT NULL DEFAULT 0 COMMENT '其他諮詢目的' AFTER `Aim_countermeasures`,
    ADD COLUMN `Aim_othertxt` VARCHAR(50) NULL DEFAULT '' COMMENT '其他諮詢目的文字說明' AFTER `Aim_other`,
    ADD COLUMN `Provide` BOOLEAN NOT NULL DEFAULT 0 COMMENT '進食方式' AFTER `Aim_othertxt`,
    ADD COLUMN `Pv_self` BOOLEAN NOT NULL DEFAULT 0 COMMENT '由口進食' AFTER `Provide`,
    ADD COLUMN `Pv_ng` BOOLEAN NOT NULL DEFAULT 0 COMMENT '鼻胃管餵食' AFTER `Pv_self`,
    ADD COLUMN `Pv_gastric` BOOLEAN NOT NULL DEFAULT 0 COMMENT '胃管餵食' AFTER `Pv_ng`,
    ADD COLUMN `Pv_intestinal` BOOLEAN NOT NULL DEFAULT 0 COMMENT '腸管餵食' AFTER `Pv_gastric`,
    ADD COLUMN `Pv_ppn` BOOLEAN NOT NULL DEFAULT 0 COMMENT '週邊靜脈營養' AFTER `Pv_intestinal`,
    ADD COLUMN `Pv_tpn` BOOLEAN NOT NULL DEFAULT 0 COMMENT '全靜脈營養' AFTER `Pv_ppn`,
    ADD COLUMN `Pv_other` BOOLEAN NOT NULL DEFAULT 0 COMMENT '其他進食方式' AFTER `Pv_tpn`,
    ADD COLUMN `Pv_othertxt` VARCHAR(50) NULL DEFAULT '' COMMENT '其他進食方式文字說明' AFTER `Pv_other`,
    ADD COLUMN `Caloric` FLOAT(6,1) DEFAULT 0 COMMENT '熱量需求' AFTER `Pv_othertxt`,
    ADD COLUMN `Cal_pressure` FLOAT(3,1) DEFAULT 0 COMMENT '壓力因子' AFTER `Caloric`,
    ADD COLUMN `Cal_activity` FLOAT(3,1) DEFAULT 0 COMMENT '活動因子' AFTER `Cal_pressure`,
    ADD COLUMN `Protein` FLOAT(5,1) DEFAULT 0 COMMENT '蛋白質需求' AFTER `Cal_activity`,
    ADD COLUMN `Protein_factor` FLOAT(3,1) DEFAULT 0 COMMENT '需求量因子' AFTER `Protein`,
    ADD COLUMN `beforeSurgery` BOOLEAN NOT NULL DEFAULT 0 COMMENT '手術前飲食衛教' AFTER `Protein_factor`,
    ADD COLUMN `filePath` VARCHAR(100) DEFAULT '' COMMENT '上傳檔案名稱與路徑' AFTER `R_note`;


-- 保險紀錄
DROP TABLE IF EXISTS FCF_careservice.`Rec_insurance`;
CREATE TABLE FCF_careservice.`Rec_insurance` (
    `IDno` INT NOT NULL AUTO_INCREMENT,
    `Keydate` DATETIME NULL DEFAULT NULL,
    `MemberID` INT NULL DEFAULT NULL,
    `AdminID` INT NULL DEFAULT NULL,
    `AdName` VARCHAR(50) NULL DEFAULT NULL,
    `R_date` DATE NULL DEFAULT NULL,
    `R_time` INT NULL DEFAULT NULL,
    `R_loc` VARCHAR(20) NULL DEFAULT NULL,
    `R_way` VARCHAR(20) NULL DEFAULT NULL,
    `Ins_med` BOOLEAN NOT NULL DEFAULT 0 COMMENT '醫療相關',
    `Ins_admission` BOOLEAN NOT NULL DEFAULT 0 COMMENT '住院給付(醫療)',
    `Ins_clinic` BOOLEAN NOT NULL DEFAULT 0 COMMENT '門診給付(醫療)',
    `Ins_surgery` BOOLEAN NOT NULL DEFAULT 0 COMMENT '手術給付(醫療)',
    `Ins_expenses` BOOLEAN NOT NULL DEFAULT 0 COMMENT '自費醫材給付(醫療)',
    `Ins_targeted` BOOLEAN NOT NULL DEFAULT 0 COMMENT '標靶藥物給付(醫療)',
    `Ins_radiology` BOOLEAN NOT NULL DEFAULT 0 COMMENT '放射治療給付(醫療)',
    `Ins_chemotherapy` BOOLEAN NOT NULL DEFAULT 0 COMMENT '化學治療給付(醫療)',
    `Ins_immunotherapy` BOOLEAN NOT NULL DEFAULT 0 COMMENT '免疫治療給付(醫療)',
    `Ins_discharged` BOOLEAN NOT NULL DEFAULT 0 COMMENT '出院帶藥給付(醫療)',
    `Ins_tcm` BOOLEAN NOT NULL DEFAULT 0 COMMENT '中醫門診(醫療)',
    `Ins_hospice` BOOLEAN NOT NULL DEFAULT 0 COMMENT '安寧病房給付(醫療)',
    `Ins_policy` BOOLEAN NOT NULL DEFAULT 0 COMMENT '保單相關',
    `Ins_view` BOOLEAN NOT NULL DEFAULT 0 COMMENT '保單檢視(保單)',
    `Ins_plan` BOOLEAN NOT NULL DEFAULT 0 COMMENT '保險規劃(保單)',
    `Ins_law` BOOLEAN NOT NULL DEFAULT 0 COMMENT '保險法律(保單)',
    `R_note` LONGTEXT NULL DEFAULT '' ,
    `filePath` VARCHAR(100) DEFAULT '' COMMENT '上傳檔案名稱與路徑',
    `AdmincheckID` INT NULL DEFAULT 0,
    `Admincheck` INT NULL DEFAULT 0,
    `checktime` DATETIME NULL DEFAULT NULL,
    PRIMARY KEY (`IDno`) USING BTREE,
    INDEX `memberIndex` (`MemberID`) USING BTREE
);

-- 關懷紀錄
-- SELECT * FROM FCF_careservice.Rec_care c WHERE c.Hospclass_txt IS NOT NULL AND c.Hospclass_txt !='';
-- SELECT * FROM FCF_careservice.Rec_care c WHERE c.FCFclass_txt IS NOT NULL AND c.FCFclass_txt !='';
-- SELECT * FROM FCF_careservice.Rec_care c WHERE c.FCFmeet_txt IS NOT NULL AND c.FCFmeet_txt !='';
UPDATE FCF_careservice.Rec_care c SET c.R_note=CONCAT_WS(CHAR(10 using UTF8), c.Hospclass_txt, c.R_note) WHERE c.Hospclass_txt IS NOT NULL AND c.Hospclass_txt !='';
UPDATE FCF_careservice.Rec_care c SET c.R_note=CONCAT_WS(CHAR(10 using UTF8), c.FCFclass_txt, c.R_note) WHERE c.FCFclass_txt IS NOT NULL AND c.FCFclass_txt !='';
UPDATE FCF_careservice.Rec_care c SET c.R_note=CONCAT_WS(CHAR(10 using UTF8), c.FCFmeet_txt, c.R_note) WHERE c.FCFmeet_txt IS NOT NULL AND c.FCFmeet_txt !='';
ALTER TABLE FCF_careservice.Rec_care
MODIFY COLUMN `R_note` LONGTEXT DEFAULT '' AFTER `Trans_mental`,
DROP COLUMN Hospclass_txt,
DROP COLUMN FCFclass_txt,
DROP COLUMN FCFmeet_txt;

-- 身心靈課程紀錄
DROP TABLE IF EXISTS FCF_careservice.`Rec_courses`;
CREATE TABLE FCF_careservice.`Rec_courses` (
    `IDno` INT NOT NULL AUTO_INCREMENT,
    `Keydate` DATE NULL DEFAULT NULL COMMENT '資料建立日期',
    `MemberID` INT NULL DEFAULT NULL,
    `AdminID` INT NULL DEFAULT NULL,
    `AdName` VARCHAR(50) NULL DEFAULT NULL,
    `ClassID` INT NOT NULL COMMENT '課程ID',
    PRIMARY KEY (`IDno`) USING BTREE,
    INDEX `memberIndex` (`MemberID`) USING BTREE
)
COMMENT='身心靈課程紀錄';
-- 轉資料
INSERT INTO FCF_careservice.Rec_courses (
    Keydate, MemberID, AdminID, AdName, ClassID
)
SELECT NOW(), e.MemberID, m.AdminID, m.Adname, e.ClassID
FROM FCF_careservice.Eventpeop e
INNER JOIN FCF_careservice.MindBodyCourse m ON e.ClassID=m.IDno;
-- 刪除身心靈課程紀錄
DELETE FROM FCF_careservice.Eventpeop WHERE ClassID IN (SELECT IDno FROM FCF_careservice.MindBodyCourse);

-- 活動專案紀錄
DROP TABLE IF EXISTS FCF_careservice.`Rec_projects`;
CREATE TABLE FCF_careservice.`Rec_projects` (
    `IDno` INT NOT NULL AUTO_INCREMENT,
    `Keydate` DATE NULL DEFAULT NULL COMMENT '資料建立日期',
    `MemberID` INT NULL DEFAULT NULL,
    `AdminID` INT NULL DEFAULT NULL,
    `AdName` VARCHAR(50) NULL DEFAULT NULL,
    `ClassID` INT NOT NULL COMMENT '課程ID',
    PRIMARY KEY (`IDno`) USING BTREE,
    INDEX `memberIndex` (`MemberID`) USING BTREE
)
COMMENT='活動專案紀錄';
-- 轉資料
INSERT INTO FCF_careservice.Rec_projects (
    Keydate, MemberID, AdminID, AdName, ClassID
)
SELECT NOW(), e.MemberID, m.AdminID, m.Adname, e.ClassID
FROM FCF_careservice.Eventpeop e
INNER JOIN FCF_careservice.Projects m ON e.ClassID=m.IDno;
-- 刪除活動專案紀錄
DELETE FROM FCF_careservice.Eventpeop WHERE ClassID IN (SELECT IDno FROM FCF_careservice.Projects);

-- 志工
ALTER TABLE FCF_careservice.`Volunteer` CHANGE COLUMN `IDno` `IDno` INT NOT NULL AUTO_INCREMENT FIRST;
-- 重複ID
-- SELECT * FROM FCF_careservice.Volunteer WHERE MemberID IN (SELECT MemberID FROM FCF_careservice.Volunteer GROUP BY MemberID HAVING COUNT(1)>1);
DELETE FROM FCF_careservice.Volunteer WHERE IDno IN (SELECT DISTINCT n1.IDno FROM FCF_careservice.Volunteer n1, FCF_careservice.Volunteer n2 WHERE n1.IDno > n2.IDno AND n1.MemberID = n2.MemberID);
-- 志工相關資料欄位
ALTER TABLE FCF_careservice.`Volunteer`
    ADD UNIQUE INDEX `MemberID` (`MemberID`),
    ADD COLUMN `V_name` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '志工姓名' AFTER `MemberID`,
    ADD COLUMN `V_sex` VARCHAR(10) NOT NULL DEFAULT '' COMMENT '志工性別' AFTER `V_name`,
    ADD COLUMN `V_idno` VARCHAR(10) DEFAULT NULL COMMENT '志工身分證' AFTER `V_sex`,
    ADD UNIQUE INDEX `V_idno` (`V_idno`),
    MODIFY COLUMN `V_status` SMALLINT NULL DEFAULT 1 COMMENT '0:休息 1:參與' AFTER `V_joindate`,
    MODIFY COLUMN `MemberID` INT DEFAULT NULL COMMENT '志工與會員ID的關聯，無關聯為null' AFTER `IDno`,
    MODIFY COLUMN `V_recbook` BOOLEAN DEFAULT 0 COMMENT '是否有志願服務紀錄冊 0:無 1:有' AFTER `V_status`,
    MODIFY COLUMN `V_No` VARCHAR(30) DEFAULT '' COMMENT '志工手冊編號' AFTER `V_recbook`,
    MODIFY COLUMN `Skill_lang` BOOLEAN DEFAULT 0 COMMENT '語文能力' AFTER `V_major`,
    ADD COLUMN `lang_english` BOOLEAN DEFAULT 0 COMMENT '英文' AFTER `Skill_lang`,
    ADD COLUMN `lang_japanese` BOOLEAN DEFAULT 0 COMMENT '日文' AFTER `lang_english`,
    ADD COLUMN `lang_taiwanese` BOOLEAN DEFAULT 0 COMMENT '台語' AFTER `lang_japanese`,
    ADD COLUMN `lang_hakka` BOOLEAN DEFAULT 0 COMMENT '客語' AFTER `lang_taiwanese`,
    ADD COLUMN `lang_other` BOOLEAN DEFAULT 0 COMMENT '其他' AFTER `lang_hakka`,
    CHANGE COLUMN `Skill_langtxt` `lang_othertxt` VARCHAR(30) DEFAULT '' COMMENT '其他語言文字' AFTER `lang_other`,
    MODIFY COLUMN `Skill_medical` BOOLEAN DEFAULT 0 COMMENT '醫護背景' AFTER `lang_othertxt`,
    ADD COLUMN `med_hospital` BOOLEAN DEFAULT 0 COMMENT '醫護' AFTER `Skill_medical`,
    ADD COLUMN `med_social` BOOLEAN DEFAULT 0 COMMENT '社工' AFTER `med_hospital`,
    ADD COLUMN `med_mental` BOOLEAN DEFAULT 0 COMMENT '心理' AFTER `med_social`,
    ADD COLUMN `med_recover` BOOLEAN DEFAULT 0 COMMENT '復健' AFTER `med_mental`,
    ADD COLUMN `med_nutrition` BOOLEAN DEFAULT 0 COMMENT '營養' AFTER `med_recover`,
    ADD COLUMN `med_other` BOOLEAN DEFAULT 0 COMMENT '其他' AFTER `med_nutrition`,
    CHANGE COLUMN `Skill_medicaltxt` `med_othertxt` VARCHAR(30) DEFAULT '' COMMENT '其他醫護文字' AFTER `med_other`,
    MODIFY COLUMN `Skill_PC` BOOLEAN DEFAULT 0 COMMENT '電腦文書' AFTER `med_othertxt`,
    ADD COLUMN `PC_word` BOOLEAN DEFAULT 0 COMMENT 'word' AFTER `Skill_PC`,
    ADD COLUMN `PC_excel` BOOLEAN DEFAULT 0 COMMENT 'excel' AFTER `PC_word`,
    ADD COLUMN `PC_powerpoint` BOOLEAN DEFAULT 0 COMMENT 'power point' AFTER `PC_excel`,
    ADD COLUMN `PC_media` BOOLEAN DEFAULT 0 COMMENT '數位媒體' AFTER `PC_powerpoint`,
    ADD COLUMN `PC_other` BOOLEAN DEFAULT 0 COMMENT '其他' AFTER `PC_media`,
    CHANGE COLUMN `Skill_PCtxt` `PC_othertxt` VARCHAR(30) DEFAULT '' COMMENT '其他電腦技能' AFTER `PC_other`,
    MODIFY COLUMN `Skill_market` BOOLEAN DEFAULT 0 COMMENT '行銷企劃' AFTER `PC_othertxt`,
    MODIFY COLUMN `Skill_art` BOOLEAN DEFAULT 0 COMMENT '美工設計' AFTER `Skill_market`,
    ADD COLUMN `art_photo` BOOLEAN DEFAULT 0 COMMENT '攝影' AFTER `Skill_art`,
    ADD COLUMN `art_maker` BOOLEAN DEFAULT 0 COMMENT '手工藝' AFTER `art_photo`,
    ADD COLUMN `Skill_othertxt` VARCHAR(60) DEFAULT '' COMMENT '其他專長文字' AFTER `art_maker`;

-- 資料放到另外欄位
UPDATE FCF_careservice.`Volunteer` SET art_maker=Skill_art;
UPDATE FCF_careservice.`Volunteer` SET Skill_othertxt=Skill_other;
UPDATE FCF_careservice.`Volunteer` SET Skill_othertxt=TRIM(CONCAT(Skill_othertxt, ' ', Skill_markettxt, ' ', Skill_arttxt));
UPDATE FCF_careservice.`Volunteer` SET Skill_other=LENGTH(Skill_other)>0;
ALTER TABLE FCF_careservice.`Volunteer`
    MODIFY COLUMN `Skill_other` BOOLEAN DEFAULT 0 COMMENT '其他專長' AFTER `art_maker`,
    DROP COLUMN Skill_markettxt, DROP COLUMN Skill_arttxt;

-- 更新志工資訊
UPDATE FCF_careservice.Volunteer v
INNER JOIN FCF_careservice.Memberdata m ON v.MemberID=m.IDno
SET v.V_name=m.C_name, v.V_sex=m.C_sex, v.V_idno=m.C_idno, v.V_status=1;

-- 在志工裡面有資料，更新個案資訊
UPDATE FCF_careservice.Memberdata m SET m.FCFvolunte=1
WHERE IDno IN (SELECT MemberID FROM FCF_careservice.Volunteer v WHERE MemberID IS NOT NULL);
-- 個案資料若為一般民眾，更新為志工基本資料
UPDATE FCF_careservice.Memberdata m SET m.FCFvolunte=1, m.C_member=3
WHERE m.IDno IN (SELECT MemberID FROM FCF_careservice.Volunteer v WHERE MemberID IS NOT NULL) AND m.C_member=0;

-- 只有志工資料，無法連結基本資料
DELETE FROM FCF_careservice.Volunteer
WHERE IDno IN (
    SELECT v.IDno FROM FCF_careservice.Volunteer v
    LEFT JOIN FCF_careservice.Memberdata m ON v.MemberID=m.IDno
    WHERE m.IDno IS NULL
);

-- 志工課程紀錄 (直接去關聯志工 Volunteer)
DROP TABLE IF EXISTS FCF_careservice.`Rec_volunteerTrain`;
CREATE TABLE FCF_careservice.`Rec_volunteerTrain` (
    `IDno` INT NOT NULL AUTO_INCREMENT,
    `Keydate` DATETIME NULL DEFAULT NULL,
    `VolunteerID` INT NULL DEFAULT NULL,
    `AdminID` INT NULL DEFAULT NULL,
    `AdName` VARCHAR(50) NULL DEFAULT NULL,
    `ClassID` INT NOT NULL COMMENT '課程ID',
    PRIMARY KEY (`IDno`) USING BTREE,
    INDEX `volIndex` (`VolunteerID`) USING BTREE
)
COMMENT='志工訓練課程紀錄';

-- 志工訓練課程
INSERT INTO FCF_careservice.`Rec_volunteerTrain`
SELECT NULL, NOW(), v.IDno, m.AdminID, m.AdName, e.ClassID
FROM FCF_careservice.Eventpeop e
INNER JOIN FCF_careservice.Memberdata m ON e.MemberID=m.IDno
INNER JOIN FCF_careservice.Volunteer v ON e.MemberID=v.MemberID;

-- 刪除參加志工課程的資料
DELETE FROM FCF_careservice.Eventpeop
WHERE MemberID IN (
    SELECT DISTINCT v.MemberID FROM FCF_careservice.Rec_volunteerTrain vt
    INNER JOIN FCF_careservice.Volunteer v ON v.IDno=vt.VolunteerID
);

-- 刪除資料表(資料已轉換完成)
DROP TABLE FCF_careservice.Eventpeop;

-- 出席紀錄更名
DROP TABLE IF EXISTS FCF_careservice.`EventAttend`;
RENAME TABLE FCF_careservice.Eventtimes TO FCF_careservice.EventAttend;
ALTER TABLE FCF_careservice.EventAttend CHANGE COLUMN `IDno` `IDno` INT NOT NULL AUTO_INCREMENT FIRST;
ALTER TABLE FCF_careservice.`EventAttend` CHANGE COLUMN `Eventime` `Eventime` DATE NULL DEFAULT NULL AFTER `ClassID`;

-- 更新志工參與人員為志工ID (部分個案沒有建立志工的，將會被排除)
UPDATE FCF_careservice.EventAttend a
INNER JOIN (
    SELECT a.IDno, a.PeopID, GROUP_CONCAT(v.IDno
    ORDER BY v.IDno SEPARATOR ', ') AS newPeopID
    FROM FCF_careservice.EventAttend a
    INNER JOIN FCF_careservice.Volunteer v ON FIND_IN_SET(v.MemberID,REPLACE(REPLACE(a.PeopID, ', ', ','), ' ,', ','))
    INNER JOIN FCF_careservice.VolunteerTrain t ON a.ClassID=t.IDno AND t.C_kind='train'
    GROUP BY a.IDno) subtable ON a.IDno=subtable.IDno
SET a.PeopID=subtable.newPeopID;

-- 出席紀錄明細檔，看不出來有什麼用
DROP TABLE FCF_careservice.Eventcheckin;
-- 合作機構(舊的，直接刪掉)
DROP TABLE IF EXISTS FCF_careservice.PartnerCata;
DROP TABLE IF EXISTS FCF_careservice.Partnerdata;
DROP TABLE IF EXISTS FCF_careservice.PartnerHistory;

-- 課程照片檔案
ALTER TABLE FCF_careservice.`EventArchives` CHANGE COLUMN `IDno` `IDno` INT NOT NULL AUTO_INCREMENT FIRST;
ALTER TABLE FCF_careservice.`EventArchives` CHANGE COLUMN `keyid` `ClassID` INT(11) NULL DEFAULT NULL COMMENT '對應課程 ID' AFTER `Ftype`;

-- 志工課程紀錄 (直接去關聯志工 Volunteer)
DROP TABLE IF EXISTS FCF_careservice.Rec_referrals;
CREATE TABLE FCF_careservice.Rec_referrals (
    `IDno` INT NOT NULL AUTO_INCREMENT,
    `Keydate` DATETIME NULL DEFAULT NULL COMMENT '輸入資料時間',
    `MemberID` INT NULL DEFAULT NULL COMMENT '個案ID',
    `AdminID` INT NULL DEFAULT NULL COMMENT '轉介人ID',
    `AdName` VARCHAR(50) NULL DEFAULT NULL,
    `R_date` DATE NULL DEFAULT NULL COMMENT '轉介日期',
    `refComment` VARCHAR(500) DEFAULT '' COMMENT '轉介說明',
    `refID` INT NOT NULL COMMENT '被轉介人ID',
    `refReply` VARCHAR(500) DEFAULT '' COMMENT '被轉介回覆',
    PRIMARY KEY (`IDno`) USING BTREE,
    INDEX `memberIndex` (`MemberID`) USING BTREE
)
COMMENT='轉介單';

-- 建立索引
CREATE INDEX memberIndex ON FCF_careservice.Rec_medical(MemberID);
CREATE INDEX memberIndex ON FCF_careservice.Rec_nutrition(MemberID);
CREATE INDEX memberIndex ON FCF_careservice.Rec_social(MemberID);
CREATE INDEX memberIndex ON FCF_careservice.Rec_care(MemberID);

DROP TABLE IF EXISTS FCF_careservice.Selectmenu;

-- 資源清單
DROP TABLE IF EXISTS FCF_careservice.Resource;
CREATE TABLE FCF_careservice.Resource (
    `IDno` INT NOT NULL AUTO_INCREMENT,
    `Inputdate` DATETIME NULL DEFAULT NULL COMMENT '輸入資料時間',
    `AdminID` INT NULL DEFAULT NULL COMMENT '資料輸入者',
    `dataType` INT NULL DEFAULT 0 COMMENT '資料分類 0:無 1:癌症資源中心, 2:專科個管師, 3:醫院社工室, 4:醫院窗口, 5:北區醫院, 6:政府單位, 7:社福團體, 8:病友團體, 9:合作單位',
    `region` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '區域',
    `city` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '縣市',
    `hospital` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '醫院',
    `hospitalLevel` SMALLINT NOT NULL DEFAULT 0 COMMENT '醫院等級: 0:不適用 1:地區醫院 2:區域醫院 3:醫學中心',
    `category` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '資源類別',
    `unit` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '單位',
    `contact` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '姓名/窗口聯絡人',
    `doctor` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '主治醫師',
    `jobTitle` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '職稱',
    `tel` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '電話',
    `mobile` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '手機',
    `fax` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '傳真',
    `email` VARCHAR(100) NOT NULL DEFAULT '' COMMENT 'email',
    `address` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '地址',
    `patientGroup` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '病友團體',
    `resource` VARCHAR(500) NOT NULL DEFAULT '' COMMENT '資源',
    `memo` VARCHAR(600) NOT NULL DEFAULT '' COMMENT '備註',
    PRIMARY KEY (`IDno`) USING BTREE
)
COMMENT='資源清單';

-- 專家名單
DROP TABLE IF EXISTS FCF_careservice.Specialist;
CREATE TABLE FCF_careservice.Specialist (
    `IDno` INT NOT NULL AUTO_INCREMENT,
    `Inputdate` DATETIME NULL DEFAULT NULL COMMENT '輸入資料時間',
    `AdminID` INT NULL DEFAULT NULL COMMENT '資料輸入者',
    `dataType` INT NULL DEFAULT 0 COMMENT '資料分類 0:無, 1:醫療專家, 2:各領域專家',
    `region` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '區域',
    `category` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '資源類別',
    `unit` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '單位',
    `expertName` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '專家姓名',
    `jobTitle` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '職稱',
    `contact` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '聯絡人',
    `tel` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '電話',
    `mobile` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '手機',
    `fax` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '傳真',
    `email` VARCHAR(100) NOT NULL DEFAULT '' COMMENT 'email',
    `address` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '地址',
    `experience` VARCHAR(1000) NOT NULL DEFAULT '' COMMENT '經歷',
    `memo` VARCHAR(600) NOT NULL DEFAULT '' COMMENT '備註',
    PRIMARY KEY (`IDno`) USING BTREE
)
COMMENT='專家名單';

-- 匿名諮詢
DROP TABLE IF EXISTS FCF_careservice.Anonymous;
CREATE TABLE FCF_careservice.`Anonymous` (
    `IDno` INT(11) NOT NULL AUTO_INCREMENT,
    `Keydate` DATETIME NULL DEFAULT NULL COMMENT '紀錄時間',
    `AdminID` INT(11) NULL DEFAULT NULL,
    `AdName` VARCHAR(50) NULL DEFAULT NULL,
    `del` BOOLEAN NULL DEFAULT 0 COMMENT '0:有效資料 1:已刪除',
    `C_from` TINYINT NULL DEFAULT 1 COMMENT '諮詢方式 1:電話 2:email 3:APP',
    `C_name` VARCHAR(50) NULL DEFAULT NULL COMMENT '姓名/暱稱',
    `C_sex` VARCHAR(10) NULL DEFAULT NULL COMMENT '性別',
    `C_tele` VARCHAR(20) NULL DEFAULT NULL COMMENT '聯絡電話(不限手機或市話)',
    `C_mail` VARCHAR(80) NULL DEFAULT NULL COMMENT '電子信箱',
    `Last_update` DATETIME NULL DEFAULT NOW() ON UPDATE NOW() COMMENT '更新時間',
    PRIMARY KEY (`IDno`) USING BTREE
)
COMMENT='線上諮詢紀錄(匿名諮詢)';

-- 原始的第一筆資料
INSERT INTO FCF_careservice.Anonymous VALUES (NULL, NOW(), NULL, NULL, 0, 1, '舊資料匿名諮詢(電話)', NULL, NULL, NULL, NULL);
INSERT INTO FCF_careservice.Anonymous VALUES (NULL, NOW(), NULL, NULL, 0, 2, '舊資料匿名諮詢(網路/email)', NULL, NULL, NULL, NULL);
INSERT INTO FCF_careservice.Anonymous VALUES (NULL, NOW(), NULL, NULL, 0, 3, '舊資料匿名諮詢(APP)', NULL, NULL, NULL, NULL);

-- 需要匿名諮詢的全部多加一個是否匿名的欄位(預設非匿名)
ALTER TABLE FCF_careservice.Rec_medical ADD COLUMN `anonym` BOOLEAN DEFAULT 0 COMMENT '是否為匿名 0:個案諮詢 1匿名(線上)' AFTER `AdName`;
ALTER TABLE FCF_careservice.Rec_mental ADD COLUMN `anonym` BOOLEAN DEFAULT 0 COMMENT '是否為匿名 0:個案諮詢 1匿名(線上)' AFTER `AdName`;
ALTER TABLE FCF_careservice.Rec_social ADD COLUMN `anonym` BOOLEAN DEFAULT 0 COMMENT '是否為匿名 0:個案諮詢 1匿名(線上)' AFTER `AdName`;
ALTER TABLE FCF_careservice.Rec_nutrition ADD COLUMN `anonym` BOOLEAN DEFAULT 0 COMMENT '是否為匿名 0:個案諮詢 1匿名(線上)' AFTER `AdName`;
ALTER TABLE FCF_careservice.Rec_insurance ADD COLUMN `anonym` BOOLEAN DEFAULT 0 COMMENT '是否為匿名 0:個案諮詢 1匿名(線上)' AFTER `AdName`;
ALTER TABLE FCF_careservice.Rec_care ADD COLUMN `anonym` BOOLEAN DEFAULT 0 COMMENT '是否為匿名 0:個案諮詢 1匿名(線上)' AFTER `AdName`;
ALTER TABLE FCF_careservice.Rec_ask ADD COLUMN `anonym` BOOLEAN DEFAULT 0 COMMENT '是否為匿名 0:個案諮詢 1匿名(線上)' AFTER `AdName`;

-- 匿名資料新增狀態，未來 MemberID 對應到 Anonymous 的 IDno
UPDATE FCF_careservice.Rec_medical SET anonym=1 WHERE MemberID=1;
UPDATE FCF_careservice.Rec_mental SET anonym=1 WHERE MemberID=1;
UPDATE FCF_careservice.Rec_social SET anonym=1 WHERE MemberID=1;
UPDATE FCF_careservice.Rec_nutrition SET anonym=1 WHERE MemberID=1;
UPDATE FCF_careservice.Rec_insurance SET anonym=1 WHERE MemberID=1;
UPDATE FCF_careservice.Rec_care SET anonym=1 WHERE MemberID=1;
UPDATE FCF_careservice.Rec_ask SET anonym=1 WHERE MemberID=1;

-- 修改一般諮詢資料
ALTER TABLE FCF_careservice.Rec_ask MODIFY COLUMN `R_date` DATE DEFAULT NULL;
ALTER TABLE FCF_careservice.Rec_ask
	CHANGE COLUMN `IDno` `IDno` INT NOT NULL AUTO_INCREMENT FIRST,
	CHANGE COLUMN `Vege_4` `Vege_4` INT NULL DEFAULT 0 AFTER `Vege_3`,
	CHANGE COLUMN `Vege_5` `Vege_5` INT NULL DEFAULT 0 AFTER `Vege_4`,
	CHANGE COLUMN `Vege_6` `Vege_6` INT NULL DEFAULT 0 AFTER `Vege_5`,
	CHANGE COLUMN `Vege_other` `Vege_other` INT NULL DEFAULT 0 AFTER `Vege_6`,
	CHANGE COLUMN `Vege_othertxt` `Vege_othertxt` VARCHAR(30) NULL DEFAULT '' AFTER `Vege_other`,
	ADD COLUMN `hair` BOOLEAN DEFAULT 0 COMMENT '捐贈頭髮' AFTER `Oth_3`,
	CHANGE COLUMN `R_note` `R_note` LONGTEXT NULL DEFAULT NULL AFTER `hair`,
    ADD INDEX `MemberID` (`MemberID`);

-- 匿名資料刪除2019以前資料
DELETE FROM FCF_careservice.Rec_medical WHERE anonym=1 AND YEAR(Keydate)<=2019;
DELETE FROM FCF_careservice.Rec_mental WHERE anonym=1 AND YEAR(Keydate)<=2019;
DELETE FROM FCF_careservice.Rec_social WHERE anonym=1 AND YEAR(Keydate)<=2019;
DELETE FROM FCF_careservice.Rec_nutrition WHERE anonym=1 AND YEAR(Keydate)<=2019;
DELETE FROM FCF_careservice.Rec_insurance WHERE anonym=1 AND YEAR(Keydate)<=2019;
DELETE FROM FCF_careservice.Rec_care WHERE anonym=1 AND YEAR(Keydate)<=2019;
DELETE FROM FCF_careservice.Rec_ask WHERE anonym=1 AND YEAR(Keydate)<=2019;

-- 撈一般民眾，無疾病史，有諮詢紀錄，不含課程，且非志工 (有疾病史的已更新為個案)移到匿名諮詢
INSERT INTO FCF_careservice.Anonymous
(`IDno`, `Keydate`, `AdminID`, `AdName`, `del`, `C_from`, `C_name`, `C_sex`, `C_tele`, `C_mail`)
SELECT om.IDno, om.Keydate, om.AdminID, om.AdName, 0, 1, om.C_name, om.C_sex, om.C_tele, om.C_mail
FROM FCF_careservice.Memberdata om
LEFT JOIN FCF_careservice.Rec_cancer rc ON om.IDno=rc.MemberID
WHERE om.IDno NOT IN (
    SELECT m.IDno
    FROM FCF_careservice.Memberdata m
    LEFT JOIN FCF_careservice.Rec_ask ask ON m.IDno=ask.MemberID
    LEFT JOIN FCF_careservice.Rec_medical med ON m.IDno=med.MemberID
    LEFT JOIN FCF_careservice.Rec_nutrition nu ON m.IDno=nu.MemberID
    LEFT JOIN FCF_careservice.Rec_social so ON m.IDno=so.MemberID
    LEFT JOIN FCF_careservice.Rec_care cr ON m.IDno=cr.MemberID
    LEFT JOIN FCF_careservice.Rec_mental mt ON m.IDno=mt.MemberID
--     LEFT JOIN FCF_careservice.Rec_courses cs ON m.IDno=cs.MemberID
--     LEFT JOIN FCF_careservice.Rec_projects pj ON m.IDno=pj.MemberID
    WHERE m.C_member=0 AND m.C_status=1
    AND ask.IDno IS NULL AND med.IDno IS NULL AND nu.IDno IS NULL AND so.IDno IS NULL
    AND cr.IDno IS NULL AND mt.IDno IS NULL
--     AND cs.IDno IS NULL AND pj.IDno IS NULL
)
AND rc.IDno IS NULL AND om.C_member=0 AND om.C_status=1 AND om.FCFvolunte=0
AND om.IDno!=1;

-- 更新細項
UPDATE FCF_careservice.Rec_medical SET anonym=1 WHERE MemberID IN (
    SELECT om.IDno
    FROM FCF_careservice.Memberdata om
    LEFT JOIN FCF_careservice.Rec_cancer rc ON om.IDno=rc.MemberID
    WHERE om.IDno NOT IN (
        SELECT m.IDno
        FROM FCF_careservice.Memberdata m
        LEFT JOIN FCF_careservice.Rec_ask ask ON m.IDno=ask.MemberID
        LEFT JOIN FCF_careservice.Rec_medical med ON m.IDno=med.MemberID
        LEFT JOIN FCF_careservice.Rec_nutrition nu ON m.IDno=nu.MemberID
        LEFT JOIN FCF_careservice.Rec_social so ON m.IDno=so.MemberID
        LEFT JOIN FCF_careservice.Rec_care cr ON m.IDno=cr.MemberID
        LEFT JOIN FCF_careservice.Rec_mental mt ON m.IDno=mt.MemberID
    --     LEFT JOIN FCF_careservice.Rec_courses cs ON m.IDno=cs.MemberID
    --     LEFT JOIN FCF_careservice.Rec_projects pj ON m.IDno=pj.MemberID
        WHERE m.C_member=0 AND m.C_status=1
        AND ask.IDno IS NULL AND med.IDno IS NULL AND nu.IDno IS NULL AND so.IDno IS NULL
        AND cr.IDno IS NULL AND mt.IDno IS NULL
    --     AND cs.IDno IS NULL AND pj.IDno IS NULL
    )
    AND rc.IDno IS NULL AND om.C_member=0 AND om.C_status=1 AND om.FCFvolunte=0
    AND om.IDno!=1
);
UPDATE FCF_careservice.Rec_mental SET anonym=1 WHERE MemberID IN (
    SELECT om.IDno
    FROM FCF_careservice.Memberdata om
    LEFT JOIN FCF_careservice.Rec_cancer rc ON om.IDno=rc.MemberID
    WHERE om.IDno NOT IN (
        SELECT m.IDno
        FROM FCF_careservice.Memberdata m
        LEFT JOIN FCF_careservice.Rec_ask ask ON m.IDno=ask.MemberID
        LEFT JOIN FCF_careservice.Rec_medical med ON m.IDno=med.MemberID
        LEFT JOIN FCF_careservice.Rec_nutrition nu ON m.IDno=nu.MemberID
        LEFT JOIN FCF_careservice.Rec_social so ON m.IDno=so.MemberID
        LEFT JOIN FCF_careservice.Rec_care cr ON m.IDno=cr.MemberID
        LEFT JOIN FCF_careservice.Rec_mental mt ON m.IDno=mt.MemberID
    --     LEFT JOIN FCF_careservice.Rec_courses cs ON m.IDno=cs.MemberID
    --     LEFT JOIN FCF_careservice.Rec_projects pj ON m.IDno=pj.MemberID
        WHERE m.C_member=0 AND m.C_status=1
        AND ask.IDno IS NULL AND med.IDno IS NULL AND nu.IDno IS NULL AND so.IDno IS NULL
        AND cr.IDno IS NULL AND mt.IDno IS NULL
    --     AND cs.IDno IS NULL AND pj.IDno IS NULL
    )
    AND rc.IDno IS NULL AND om.C_member=0 AND om.C_status=1 AND om.FCFvolunte=0
    AND om.IDno!=1
);
UPDATE FCF_careservice.Rec_social SET anonym=1 WHERE MemberID IN (
    SELECT om.IDno
    FROM FCF_careservice.Memberdata om
    LEFT JOIN FCF_careservice.Rec_cancer rc ON om.IDno=rc.MemberID
    WHERE om.IDno NOT IN (
        SELECT m.IDno
        FROM FCF_careservice.Memberdata m
        LEFT JOIN FCF_careservice.Rec_ask ask ON m.IDno=ask.MemberID
        LEFT JOIN FCF_careservice.Rec_medical med ON m.IDno=med.MemberID
        LEFT JOIN FCF_careservice.Rec_nutrition nu ON m.IDno=nu.MemberID
        LEFT JOIN FCF_careservice.Rec_social so ON m.IDno=so.MemberID
        LEFT JOIN FCF_careservice.Rec_care cr ON m.IDno=cr.MemberID
        LEFT JOIN FCF_careservice.Rec_mental mt ON m.IDno=mt.MemberID
    --     LEFT JOIN FCF_careservice.Rec_courses cs ON m.IDno=cs.MemberID
    --     LEFT JOIN FCF_careservice.Rec_projects pj ON m.IDno=pj.MemberID
        WHERE m.C_member=0 AND m.C_status=1
        AND ask.IDno IS NULL AND med.IDno IS NULL AND nu.IDno IS NULL AND so.IDno IS NULL
        AND cr.IDno IS NULL AND mt.IDno IS NULL
    --     AND cs.IDno IS NULL AND pj.IDno IS NULL
    )
    AND rc.IDno IS NULL AND om.C_member=0 AND om.C_status=1 AND om.FCFvolunte=0
    AND om.IDno!=1
);
UPDATE FCF_careservice.Rec_nutrition SET anonym=1 WHERE MemberID IN (
    SELECT om.IDno
    FROM FCF_careservice.Memberdata om
    LEFT JOIN FCF_careservice.Rec_cancer rc ON om.IDno=rc.MemberID
    WHERE om.IDno NOT IN (
        SELECT m.IDno
        FROM FCF_careservice.Memberdata m
        LEFT JOIN FCF_careservice.Rec_ask ask ON m.IDno=ask.MemberID
        LEFT JOIN FCF_careservice.Rec_medical med ON m.IDno=med.MemberID
        LEFT JOIN FCF_careservice.Rec_nutrition nu ON m.IDno=nu.MemberID
        LEFT JOIN FCF_careservice.Rec_social so ON m.IDno=so.MemberID
        LEFT JOIN FCF_careservice.Rec_care cr ON m.IDno=cr.MemberID
        LEFT JOIN FCF_careservice.Rec_mental mt ON m.IDno=mt.MemberID
    --     LEFT JOIN FCF_careservice.Rec_courses cs ON m.IDno=cs.MemberID
    --     LEFT JOIN FCF_careservice.Rec_projects pj ON m.IDno=pj.MemberID
        WHERE m.C_member=0 AND m.C_status=1
        AND ask.IDno IS NULL AND med.IDno IS NULL AND nu.IDno IS NULL AND so.IDno IS NULL
        AND cr.IDno IS NULL AND mt.IDno IS NULL
    --     AND cs.IDno IS NULL AND pj.IDno IS NULL
    )
    AND rc.IDno IS NULL AND om.C_member=0 AND om.C_status=1 AND om.FCFvolunte=0
    AND om.IDno!=1
);
UPDATE FCF_careservice.Rec_insurance SET anonym=1 WHERE MemberID IN (
    SELECT om.IDno
    FROM FCF_careservice.Memberdata om
    LEFT JOIN FCF_careservice.Rec_cancer rc ON om.IDno=rc.MemberID
    WHERE om.IDno NOT IN (
        SELECT m.IDno
        FROM FCF_careservice.Memberdata m
        LEFT JOIN FCF_careservice.Rec_ask ask ON m.IDno=ask.MemberID
        LEFT JOIN FCF_careservice.Rec_medical med ON m.IDno=med.MemberID
        LEFT JOIN FCF_careservice.Rec_nutrition nu ON m.IDno=nu.MemberID
        LEFT JOIN FCF_careservice.Rec_social so ON m.IDno=so.MemberID
        LEFT JOIN FCF_careservice.Rec_care cr ON m.IDno=cr.MemberID
        LEFT JOIN FCF_careservice.Rec_mental mt ON m.IDno=mt.MemberID
    --     LEFT JOIN FCF_careservice.Rec_courses cs ON m.IDno=cs.MemberID
    --     LEFT JOIN FCF_careservice.Rec_projects pj ON m.IDno=pj.MemberID
        WHERE m.C_member=0 AND m.C_status=1
        AND ask.IDno IS NULL AND med.IDno IS NULL AND nu.IDno IS NULL AND so.IDno IS NULL
        AND cr.IDno IS NULL AND mt.IDno IS NULL
    --     AND cs.IDno IS NULL AND pj.IDno IS NULL
    )
    AND rc.IDno IS NULL AND om.C_member=0 AND om.C_status=1 AND om.FCFvolunte=0
    AND om.IDno!=1
);
UPDATE FCF_careservice.Rec_care SET anonym=1 WHERE MemberID IN (
    SELECT om.IDno
    FROM FCF_careservice.Memberdata om
    LEFT JOIN FCF_careservice.Rec_cancer rc ON om.IDno=rc.MemberID
    WHERE om.IDno NOT IN (
        SELECT m.IDno
        FROM FCF_careservice.Memberdata m
        LEFT JOIN FCF_careservice.Rec_ask ask ON m.IDno=ask.MemberID
        LEFT JOIN FCF_careservice.Rec_medical med ON m.IDno=med.MemberID
        LEFT JOIN FCF_careservice.Rec_nutrition nu ON m.IDno=nu.MemberID
        LEFT JOIN FCF_careservice.Rec_social so ON m.IDno=so.MemberID
        LEFT JOIN FCF_careservice.Rec_care cr ON m.IDno=cr.MemberID
        LEFT JOIN FCF_careservice.Rec_mental mt ON m.IDno=mt.MemberID
    --     LEFT JOIN FCF_careservice.Rec_courses cs ON m.IDno=cs.MemberID
    --     LEFT JOIN FCF_careservice.Rec_projects pj ON m.IDno=pj.MemberID
        WHERE m.C_member=0 AND m.C_status=1
        AND ask.IDno IS NULL AND med.IDno IS NULL AND nu.IDno IS NULL AND so.IDno IS NULL
        AND cr.IDno IS NULL AND mt.IDno IS NULL
    --     AND cs.IDno IS NULL AND pj.IDno IS NULL
    )
    AND rc.IDno IS NULL AND om.C_member=0 AND om.C_status=1 AND om.FCFvolunte=0
    AND om.IDno!=1
);
    UPDATE FCF_careservice.Rec_ask SET anonym=1 WHERE MemberID IN (
    SELECT om.IDno
    FROM FCF_careservice.Memberdata om
    LEFT JOIN FCF_careservice.Rec_cancer rc ON om.IDno=rc.MemberID
    WHERE om.IDno NOT IN (
        SELECT m.IDno
        FROM FCF_careservice.Memberdata m
        LEFT JOIN FCF_careservice.Rec_ask ask ON m.IDno=ask.MemberID
        LEFT JOIN FCF_careservice.Rec_medical med ON m.IDno=med.MemberID
        LEFT JOIN FCF_careservice.Rec_nutrition nu ON m.IDno=nu.MemberID
        LEFT JOIN FCF_careservice.Rec_social so ON m.IDno=so.MemberID
        LEFT JOIN FCF_careservice.Rec_care cr ON m.IDno=cr.MemberID
        LEFT JOIN FCF_careservice.Rec_mental mt ON m.IDno=mt.MemberID
    --     LEFT JOIN FCF_careservice.Rec_courses cs ON m.IDno=cs.MemberID
    --     LEFT JOIN FCF_careservice.Rec_projects pj ON m.IDno=pj.MemberID
        WHERE m.C_member=0 AND m.C_status=1
        AND ask.IDno IS NULL AND med.IDno IS NULL AND nu.IDno IS NULL AND so.IDno IS NULL
        AND cr.IDno IS NULL AND mt.IDno IS NULL
    --     AND cs.IDno IS NULL AND pj.IDno IS NULL
    )
    AND rc.IDno IS NULL AND om.C_member=0 AND om.C_status=1 AND om.FCFvolunte=0
    AND om.IDno!=1
);

-- 刪除一般民眾資料
DELETE FROM FCF_careservice.Memberdata WHERE IDno IN
(SELECT om.IDno
FROM FCF_careservice.Memberdata om
LEFT JOIN FCF_careservice.Rec_cancer rc ON om.IDno=rc.MemberID
WHERE om.IDno NOT IN (
    SELECT m.IDno
    FROM FCF_careservice.Memberdata m
    LEFT JOIN FCF_careservice.Rec_ask ask ON m.IDno=ask.MemberID
    LEFT JOIN FCF_careservice.Rec_medical med ON m.IDno=med.MemberID
    LEFT JOIN FCF_careservice.Rec_nutrition nu ON m.IDno=nu.MemberID
    LEFT JOIN FCF_careservice.Rec_social so ON m.IDno=so.MemberID
    LEFT JOIN FCF_careservice.Rec_care cr ON m.IDno=cr.MemberID
    LEFT JOIN FCF_careservice.Rec_mental mt ON m.IDno=mt.MemberID
--     LEFT JOIN FCF_careservice.Rec_courses cs ON m.IDno=cs.MemberID
--     LEFT JOIN FCF_careservice.Rec_projects pj ON m.IDno=pj.MemberID
    WHERE m.C_member=0 AND m.C_status=1
    AND ask.IDno IS NULL AND med.IDno IS NULL AND nu.IDno IS NULL AND so.IDno IS NULL
    AND cr.IDno IS NULL AND mt.IDno IS NULL
--     AND cs.IDno IS NULL AND pj.IDno IS NULL
)
AND rc.IDno IS NULL AND om.C_member=0 AND om.C_status=1 AND om.FCFvolunte=0
AND om.IDno!=1);

-- 護理評估量表
DROP TABLE IF EXISTS FCF_careservice.assess_nursing;
CREATE TABLE FCF_careservice.assess_nursing (
	`IDno` INT NOT NULL AUTO_INCREMENT,
	`Keydate` DATETIME NULL DEFAULT NULL,
	`MemberID` INT NULL DEFAULT NULL COMMENT '個案ID',
	`AdminID` INT NULL DEFAULT NULL COMMENT '填寫者帳號ID',
	`AdName` VARCHAR(50) NULL DEFAULT NULL COMMENT '填寫者名稱',
	`physical` SMALLINT NULL DEFAULT '0' COMMENT '個案目前體能狀態評估(ECOG)',
	`mucosal` BOOLEAN NULL DEFAULT '0' COMMENT '是否因治療造成腸胃道黏膜受損情形',
	`mucosal_area` VARCHAR(10) NULL DEFAULT '' COMMENT '1:腹瀉 2:口腔',
	`mucosal_grade` SMALLINT NULL DEFAULT '0' COMMENT '咽喉黏膜潰瘍等級 第1級 紅、腫、痛、無潰瘍 第2級 紅、腫、潰瘍 (範圍＜25%) 可吃固體食物 第3級 紅、腫、潰瘍 (範圍25~50%) 只能吃流質食物 第4級 出血性潰瘍範圍＞50% 無法由口腔進食',
	`pain` BOOLEAN NULL DEFAULT 0 COMMENT '是否有疼痛問題 1:有 0:無',
	`pain_area` VARCHAR(20) NULL DEFAULT '' COMMENT '疼痛部位',
	`pain_analgesics` VARCHAR(20) NULL DEFAULT '' COMMENT '止痛用藥',
	`pain_level` SMALLINT NULL DEFAULT 1 COMMENT '疼痛指數 1~10',
	`exhausted` BOOLEAN NULL DEFAULT 0 COMMENT '是否有癌因性疲憊',
	`exhaust_level` SMALLINT NULL DEFAULT 1 COMMENT '疲憊指數 1~10',
	`nutrition_tube` BOOLEAN NULL DEFAULT 0 COMMENT '供給營養補充之管路',
	`nutrition_tube_type` VARCHAR(10) NULL DEFAULT '' COMMENT '供給營養補充之管路 1:鼻胃管 2:胃管 3空腸',
	`special_tube` BOOLEAN NULL DEFAULT 0 COMMENT '特殊管路',
	`special_tube_type` VARCHAR(10) NULL DEFAULT '' COMMENT '特殊管路 1:氣切 2:尿管 3:人工肛門 4:洗腎用動靜脈廔管',
	`caregiver` BOOLEAN NULL DEFAULT 0 COMMENT '照顧者協助',
	`score` INT NULL DEFAULT 0 COMMENT '得到總分',
    `R_note` LONGTEXT NULL DEFAULT '' COMMENT '備註' COLLATE 'utf8mb4_general_ci',
	`AdmincheckID` INT NULL DEFAULT '0',
	`Admincheck` INT NULL DEFAULT '0',
	`checktime` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`IDno`) USING BTREE,
	INDEX `memberIndex` (`MemberID`) USING BTREE
)
COMMENT='護理評估量表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;

-- 營養評估量表
DROP TABLE IF EXISTS FCF_careservice.assess_nutrition;
CREATE TABLE FCF_careservice.assess_nutrition (
	`IDno` INT NOT NULL AUTO_INCREMENT,
	`Keydate` DATETIME NULL DEFAULT NULL,
	`MemberID` INT NULL DEFAULT NULL COMMENT '個案ID',
	`AdminID` INT NULL DEFAULT NULL COMMENT '填寫者帳號ID',
	`AdName` VARCHAR(50) NULL DEFAULT NULL COMMENT '填寫者名稱',
	`height` SMALLINT NULL DEFAULT '0' COMMENT '身高',
	`weight` SMALLINT NULL DEFAULT '0' COMMENT '體重',
	`weight_year` SMALLINT NULL DEFAULT '0' COMMENT '半年前體重',
	`weight_month` SMALLINT NULL DEFAULT '0' COMMENT '一個月前體重',
	`weight_change` SMALLINT NULL DEFAULT '0' COMMENT '體重變化 0:沒有改變 1:減少 2:增加',
	`eat_change` SMALLINT NULL DEFAULT '0' COMMENT '吃食物的量與以往相比 0:沒有改變 1:減少 2:增加',
	`eat_only` SMALLINT NULL DEFAULT '0' COMMENT '0: 比較少的正常食物 1:固體食物 2:液體食物 3:營養補充品 4:非常少的食物 5:靜脈注射',
	`other_nutrition` BOOLEAN NULL DEFAULT '0' COMMENT '補充其他營養品',
	`use_nutrition` VARCHAR(50) NULL DEFAULT '' COMMENT '敘述營養補充品項及使用頻率',
	`symptom` VARCHAR(20) NULL DEFAULT '' COMMENT '0:沒有問題 1:沒有食慾 2:噁心 3:嘔吐 4:便秘 5:腹瀉 6:口痛 7:口乾 8:疼痛 9:吃起來沒有味道 10:怪味 11:其他',
	`symptom_pain` VARCHAR(20) NULL DEFAULT '' COMMENT '症狀疼痛部位',
	`symptom_other` VARCHAR(20) NULL DEFAULT '' COMMENT '其他症狀',
	`body_status` SMALLINT NULL DEFAULT 0 COMMENT '身體狀況 0:無限制 1:有點不同，但可自理 2:不舒服，床上半天 3:少數活動，大部分床上 4:絕大多數時間在床上',
	`diagnosis` VARCHAR(20) NULL DEFAULT 0 COMMENT '疾病診斷',
	`disease_level` SMALLINT NULL DEFAULT 0 COMMENT '1 2 3 4期 5:其他',
	`age` SMALLINT NULL DEFAULT 0 COMMENT '年齡',
	`score` INT NULL DEFAULT 0 COMMENT '得到總分',
    `R_note` LONGTEXT NULL DEFAULT '' COMMENT '備註' COLLATE 'utf8mb4_general_ci',
	`AdmincheckID` INT NULL DEFAULT '0',
	`Admincheck` INT NULL DEFAULT '0',
	`checktime` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`IDno`) USING BTREE,
	INDEX `memberIndex` (`MemberID`) USING BTREE
)
COMMENT='營養評估量表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;

-- 社工評估量表
DROP TABLE IF EXISTS FCF_careservice.assess_social;
CREATE TABLE FCF_careservice.assess_social (
	`IDno` INT NOT NULL AUTO_INCREMENT,
	`Keydate` DATETIME NULL DEFAULT NULL,
	`MemberID` INT NULL DEFAULT NULL COMMENT '個案ID',
	`AdminID` INT NULL DEFAULT NULL COMMENT '填寫者帳號ID',
	`AdName` VARCHAR(50) NULL DEFAULT NULL COMMENT '填寫者名稱',
	`disability_book` BOOLEAN NULL DEFAULT '0' COMMENT '身心障礙手冊 0:無 1:有 2:其他',
	`other_source` BOOLEAN NULL DEFAULT '0' COMMENT '其他資源輔助 0:無 1:有 2:其他',
	`disability_other` VARCHAR(20) NULL DEFAULT '' COMMENT '身心障礙手冊其他文字',
	`source_other` VARCHAR(20) NULL DEFAULT '' COMMENT '其他資源輔助文字',
	`work_status` BOOLEAN NULL DEFAULT '0' COMMENT '目前工作狀態 1:有工作 0:無工作',
	`life_before` SMALLINT NULL DEFAULT '0' COMMENT '生活適應(病前) 1~5 不佳~良好',
	`life_after` SMALLINT NULL DEFAULT '0' COMMENT '生活適應(病後) 1~5 不佳~良好',
	`relation_before` SMALLINT NULL DEFAULT '0' COMMENT '人際關係(病前) 1~5 不佳~良好',
	`relation_after` SMALLINT NULL DEFAULT '0' COMMENT '人際關係(病後) 1~5 不佳~良好',
	`social_before` SMALLINT NULL DEFAULT '0' COMMENT '社交關係(病前) 1~5 不佳~良好',
	`social_after` SMALLINT NULL DEFAULT '0' COMMENT '社交關係(病後) 1~5 不佳~良好',
	`tele_talk` SMALLINT NULL DEFAULT '0' COMMENT '一周內與他人電話交談 0:從未有過 1:一次 2:兩次 3:三次 4:每天一次或更多',
	`get_together` SMALLINT NULL DEFAULT '0' COMMENT '與你同住的人相聚 0:從未有過 1:一次 2:兩次 3:三次 4:每天一次或更多',
	`trust_people` BOOLEAN NULL DEFAULT '0' COMMENT '0:否 1:是',
	`give_help` BOOLEAN NULL DEFAULT '0' COMMENT '是否有人能在你生病給你幫助 0:否 1:是',
	`give_help_peop` VARCHAR(20) NULL DEFAULT '' COMMENT '提供幫助的人',
	`give_help_type` VARCHAR(10) NULL DEFAULT '' COMMENT '1:經濟 2:醫療 3:照顧 4:情感',
	`take_care` SMALLINT NULL DEFAULT '0' COMMENT '1:無 2:幾乎沒有 3:偶爾 4:短期照顧 5:無限期協助',
	`score` INT NULL DEFAULT 0 COMMENT '得到總分',
    `R_note` LONGTEXT NULL DEFAULT '' COMMENT '備註' COLLATE 'utf8mb4_general_ci',
	`AdmincheckID` INT NULL DEFAULT '0',
	`Admincheck` INT NULL DEFAULT '0',
	`checktime` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`IDno`) USING BTREE,
	INDEX `memberIndex` (`MemberID`) USING BTREE
)
COMMENT='社工評估量表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;

-- 心理評估量表
DROP TABLE IF EXISTS FCF_careservice.assess_mental;
CREATE TABLE FCF_careservice.assess_mental (
	`IDno` INT NOT NULL AUTO_INCREMENT,
	`Keydate` DATETIME NULL DEFAULT NULL,
	`MemberID` INT NULL DEFAULT NULL COMMENT '個案ID',
	`AdminID` INT NULL DEFAULT NULL COMMENT '填寫者帳號ID',
	`AdName` VARCHAR(50) NULL DEFAULT NULL COMMENT '填寫者名稱',
    `mind_trouble` SMALLINT NULL DEFAULT '0' COMMENT '心理困擾程度有多大，請以數字表示。(0為無困擾，10為極度困擾)',
    `life_child` BOOLEAN NULL DEFAULT 0 COMMENT '照顧小孩問題',
    `life_live` BOOLEAN NULL DEFAULT 0 COMMENT '居住問題',
    `life_eco` BOOLEAN NULL DEFAULT 0 COMMENT '保險經濟問題',
    `life_traffic` BOOLEAN NULL DEFAULT 0 COMMENT '交通問題',
    `life_work` BOOLEAN NULL DEFAULT 0 COMMENT '工作或就學問題',
    `life_treatment` BOOLEAN NULL DEFAULT 0 COMMENT '治療方式問題',
    `mood_melancholy` BOOLEAN NULL DEFAULT 0 COMMENT '憂鬱',
    `mood_afraid` BOOLEAN NULL DEFAULT 0 COMMENT '害怕',
    `mood_tension` BOOLEAN NULL DEFAULT 0 COMMENT '緊張',
    `mood_sad` BOOLEAN NULL DEFAULT 0 COMMENT '難過、傷心',
    `mood_worry` BOOLEAN NULL DEFAULT 0 COMMENT '擔心',
    `mood_lose_interest` BOOLEAN NULL DEFAULT 0 COMMENT '對日常生活失去興趣',
    `family_kid` BOOLEAN NULL DEFAULT 0 COMMENT '和小孩相處',
    `family_mate` BOOLEAN NULL DEFAULT 0 COMMENT '和伴侶相處',
    `family_infertility` BOOLEAN NULL DEFAULT 0 COMMENT '擔心不孕',
    `family_health` BOOLEAN NULL DEFAULT 0 COMMENT '家人健康議題',
    `religious` BOOLEAN NULL DEFAULT 0 COMMENT '靈性或宗教問題',
    `body_exterior` BOOLEAN NULL DEFAULT 0 COMMENT '身體外觀方面',
    `body_bath_dress` BOOLEAN NULL DEFAULT 0 COMMENT '身體穿衣沐浴方面',
    `body_breathe` BOOLEAN NULL DEFAULT 0 COMMENT '呼吸方面',
    `body_pee` BOOLEAN NULL DEFAULT 0 COMMENT '小便方面',
    `body_constipation` BOOLEAN NULL DEFAULT 0 COMMENT '便祕',
    `body_diarrhea` BOOLEAN NULL DEFAULT 0 COMMENT '腹瀉',
    `body_eat` BOOLEAN NULL DEFAULT 0 COMMENT '進食方面',
    `body_tired` BOOLEAN NULL DEFAULT 0 COMMENT '疲倦',
    `body_swelling` BOOLEAN NULL DEFAULT 0 COMMENT '腫脹感',
    `body_fever` BOOLEAN NULL DEFAULT 0 COMMENT '發燒',
    `body_action` BOOLEAN NULL DEFAULT 0 COMMENT '行動方面',
    `body_indigestion` BOOLEAN NULL DEFAULT 0 COMMENT '消化不良',
    `body_memory` BOOLEAN NULL DEFAULT 0 COMMENT '記憶力或專注力方面',
    `body_mouth` BOOLEAN NULL DEFAULT 0 COMMENT '嘴巴破皮',
    `body_nausea` BOOLEAN NULL DEFAULT 0 COMMENT '噁心',
    `body_nose` BOOLEAN NULL DEFAULT 0 COMMENT '鼻子乾或鼻塞',
    `body_pain` BOOLEAN NULL DEFAULT 0 COMMENT '疼痛',
    `body_sexual` BOOLEAN NULL DEFAULT 0 COMMENT '性生活方面',
    `body_skin` BOOLEAN NULL DEFAULT 0 COMMENT '皮膚乾或癢',
    `body_sleep` BOOLEAN NULL DEFAULT 0 COMMENT '睡眠方面',
    `body_abuse` BOOLEAN NULL DEFAULT 0 COMMENT '物質濫用',
    `body_tingling` BOOLEAN NULL DEFAULT 0 COMMENT '手或腳有刺痛感',
	`score` INT NULL DEFAULT 0 COMMENT '得到總分',
    `R_note` LONGTEXT NULL DEFAULT '' COMMENT '備註' COLLATE 'utf8mb4_general_ci',
	`AdmincheckID` INT NULL DEFAULT '0',
	`Admincheck` INT NULL DEFAULT '0',
	`checktime` DATETIME NULL DEFAULT NULL,
	PRIMARY KEY (`IDno`) USING BTREE,
	INDEX `memberIndex` (`MemberID`) USING BTREE
)
COMMENT='心理評估量表'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;

-- 追蹤清單
ALTER TABLE FCF_careservice.Tracklist
    ADD INDEX `MemberID` (`MemberID`),
    CHANGE COLUMN `IDno` `IDno` INT NOT NULL AUTO_INCREMENT FIRST,
    ADD COLUMN `assess_type` SMALLINT DEFAULT 0 COMMENT '追蹤類型 0:舊資料 1:護理 2:營養 3:社工 4:心理' AFTER `MemberID`,
    CHANGE COLUMN `Opendate` `Opendate` DATETIME NULL DEFAULT NULL COMMENT '建立追蹤日期' AFTER `assess_type`,
    CHANGE COLUMN `Closedate` `Closedate` DATETIME NULL DEFAULT NULL COMMENT '結束此次追蹤日期' AFTER `Opendate`,
    ADD COLUMN `judgeID` INT NULL DEFAULT '0' COMMENT '窗口評估帳號' AFTER `Closedate`,
    ADD COLUMN `judgedate` DATETIME NULL DEFAULT NULL COMMENT '窗口處理日期' AFTER `judgeID`,
	ADD COLUMN `judgenote` TEXT NULL DEFAULT '' COMMENT '窗口指派說明' AFTER `judgedate`,
	CHANGE COLUMN `AdminID` `AdminID` INT NULL DEFAULT '0' COMMENT '追蹤專人帳號' AFTER `judgenote`,
	DROP COLUMN `ADname`,
    CHANGE COLUMN `T_date` `T_date` DATE NULL DEFAULT NULL COMMENT '預計追蹤日期' AFTER `AdminID`,
    CHANGE COLUMN `T_name` `T_name` VARCHAR(50) NULL DEFAULT '' COMMENT '追蹤個案姓名' AFTER `T_date`,
    CHANGE COLUMN `T_status` `T_status` BOOLEAN NULL DEFAULT '0' COMMENT '追蹤狀態 1:已追蹤完成 0:待追蹤' AFTER `T_name`,
    CHANGE COLUMN `T_phone` `T_phone` VARCHAR(50) NULL DEFAULT '' COMMENT '追蹤電話' AFTER `T_status`,
    CHANGE COLUMN `T_note` `T_note` TEXT NULL DEFAULT '' COMMENT '追蹤註記' AFTER `T_phone`;

-- 通知
DROP TABLE IF EXISTS FCF_careservice.notification;
CREATE TABLE FCF_careservice.notification (
    `IDno` INT NOT NULL AUTO_INCREMENT,
    `noticeTime` DATETIME NULL DEFAULT NULL,
    `noticeAcc` INT NULL DEFAULT NULL COMMENT '通知帳號ID',
    `noticeType` INT DEFAULT 0 COMMENT '通知類型 0:一般通知, 1:新增資料通知 2:資料修改通知 3:資料結案通知 4:資料刪除通知',
    `noticeLink` VARCHAR(100) DEFAULT '' COMMENT '通知連結',
    `noticeMsg` MEDIUMTEXT DEFAULT '' COMMENT '通知訊息',
    `checktime` DATETIME NULL DEFAULT NULL COMMENT '確認時間',
    PRIMARY KEY (`IDno`) USING BTREE,
    INDEX `accIndex` (`NoticeAcc`) USING BTREE
)
COMMENT='系統通知'
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB;

